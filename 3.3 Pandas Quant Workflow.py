# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     comment_magics: true
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %% [markdown]
# ![Python4Proteomics](images/logo_p4p.png) **(Python For Poteomics)**
#
# # Ejemplo de flujo de trabajo ***cuantitativo*** en Pandas: PME12
#
# Ya hemos visto un ejemplo de flujo de trabajo (*workflow*) de proteómica <u>cualitativa</u> con **Python**. En este segundo ejemplo trataremos un caso de proteómica <u>cuantitativa</u> aprovechando los datos del *ProteoRed Multicentric Estudy \#12* (PME12). El PME12 consta de dos muestras (A y B), cada una formada por un cóctel de proteomas de tres organismos diferentes: *Escherichia coli*, *Saccharomyces cerevisiae* y *Homo sapiens*. Las proporciones en las que se han mezclado los tres proteomas son distintas en cada muestra:
#
# | Especie                    | Muestra A | Muestra B | *Fold Change*     | log<sub>2</sub> *Fold Change* |
# |:--------------------------:|:---------:|:---------:|:-----------------:|:-----------------------------:|
# | *Homo sapiens*             | 65%       | 65%       | 0.65/0.65 = 1.00  |  0                            |
# | *Escherichia coli*         | 20%       |  5%       | 0.05/0.20  = 0.25 | -2                            |
# | *Saccharomyces cerevisiae* | 15%       | 30%       | 0.30/0.15 = 2.00  |  1                            |
#
# La elección de estas proporciones no es arbitraria, pretende simular un análisis de expresión diferencial. En este tipo de estudios tenemos una parte del proteoma que experimentará cambios entre las condiciones A y B (las proteínas incrementarán o disminuirán su concentración), mientras que la otra parte se mantendrá sin cambios.
#
# En un análisis de expresión diferencial la parte del proteoma que varía entre las dos condiciones es minoritaria comparada con la que se mantien constante. En nuestro caso, el 65% correspondiente a *Homo sapiens* simularía esta parte del proteoma <u>no diferencial</u>. El 35% restante simularía la parte <u>diferencial</u>, representando *Escherichia coli* las proteínas subexpresadas (pasamos del 20% al 5%) y *Saccharomyces cerevisiae* las proteínas sobreexpresadas (pasamos del 15% al 30%).
#
# Tenemos pues un experimento cuya búsqueda de proteínas se ha realizado con el *software* MaxQuant (MQ). En la carpeta `data/quatitative/MQ/` del repositorio **p4p** se encuentra un archivo de salida de *Protein Groups* de MaxQuant llamado `3319_FE_MBR_U_02_proteinGroups.txt` (clica [aquí](data/quantitative/MQ/3319_FE_MBR_U_02_proteinGroups.txt "data/quantitative/MQ/3319_FE_MBR_U_02_proteinGroups.txt") para abrir y poder visualizar el fichero).
#
# Este archivo contiene datos de tres variables cuantitativas distintas: La **Intensidad** ("*Intensity*"), la **Cuantificación absoluta basada en intensidad** ("*iBAQ*"), y la **Cuantificación sin etiqueta** ("*LFQ intensity*"). En el presente *workflow* analizaremos los datos de estas tres variables cuantitativas <u>en paralelo</u>. Seguiremos practicando los conceptos básicos de **Pandas** que hemos visto en capítulos anteriores e introduciremos herramientas y técnicas un poco más sofisticadas.
#
# En la primera parte del presente *workflow* <u>importaremos</u> y <u>filtraremos</u> los datos. A continuación a <u>anotaremos la taxonomía</u> asociada a cada uno de los grupos de proteínas identificados. Veremos como lo que en apariencia es una tarea trivial, puede albergar cierta complejidad técnica si uno se propone hacerlo prestando atención a los detalles. En el siguiente paso <u>ordenaremos</u> los datos. En esta etapa destacaremos la importancia de trabajar con *tidy-DataFrames* y pondremos el foco en el análisis exploratorio de datos (*exploratory data analysis* o **EDA**). A partir de este punto comenzaremos el análisis cuantitativo propiapemente dicho. <u>Imputaremos los valores nulos</u>, <u>calcularemos el *Fold Change*</u> y luego <u>realizaremos un *t-test*</u>. Acabaremos generando y exportando un *volcano plot* y la lista de proteínas diferenciales.

# %% [markdown]
# **Índice:**
#
# * [I.- Importando datos cuantitativos de grupos de proteínas de MaxQuant](#I.--Importando-datos-cuantitativos-de-grupos-de-proteínas-de-MaxQuant)
#    * [Seleccionando las columnas de interés](#Seleccionando-las-columnas-de-interés)
#    * [Simplificando y ordenando nombres de columnas](#Simplificando-y-ordenando-nombres-de-columnas)
# * [II.- Filtrando](#II.--Filtrando)
#    * [Filtrando a nivel de grupo de proteínas](#Filtrando-a-nivel-de-grupo-de-proteínas)
#    * [Filtrando a nivel de proteína](#Filtrando-a-nivel-de-proteína)
# * [III.- Anotando la taxonomía de los grupos de proteínas](#III.--Anotando-la-taxonomía-de-los-grupos-de-proteínas)
#     * [Obteniendo el listado de proteínas individuales](#Obteniendo-el-listado-de-proteínas-individuales)
#     * [Buscando taxonomías en UniProt](#Buscando-taxonomías-en-UniProt)
#     * [Creando el diccionario taxonómico](#Creando-el-diccionario-taxonómico)
#     * [Mapeando la taxonomía](#Mapeando-la-taxonomía)
#     * [Filtrando y simplificando taxonomías redundantes](#Filtrando-y-simplificando-taxonomías-redundantes)
# * [IV.- Construyendo un *Tidy-DataFrame*](#IV.--Construyendo-un-Tidy-DataFrame)
# * [V.- Trabajando con *Tidy-DataFrames* I](#V.--Trabajando-con-Tidy-DataFrames-I)
#     * [Estimando los valores nulos](#Estimando-los-valores-nulos)
#     * [Transformando a logaritmo](#Transformando-a-logaritmo)
#     * [Imputando valores nulos](#Imputando-valores-nulos)
# * [VI.- Trabajando con *Tidy-DataFrames* II](#VI.--Trabajando-con-Tidy-DataFrames-II)
#     * [Calculando el Fold Change](#Calculando-el-Fold-Change)
#     * [Estimando de nuevo valores nulos](#Estimando-de-nuevo-valores-nulos)
#     * [Aplicando un t-test](#Aplicando-un-t-test)
# * [VII.- Representando el _Volcano Plot_](#VII.--Representando-el-Volcano-Plot)
# * [VIII.- Conclusiones finales y *take home message*](#VIII.--Conclusiones-finales-y-take-home-message) 
#  
# ---

# %% [markdown] toc-hr-collapsed=true
# # I.- Importando datos cuantitativos de grupos de proteínas de MaxQuant
#
# Empezaremos importando las librerías **Pandas** y **Numpy** con los alias `pd` y `np`, respectivamente. También importaremos las librerías de visualización de datos **Matplotlib.Pyplot** y **Seaborn** con los alias `plt` y `sns`, respectivamente.

# %%
# Importing the Pandas/Numpy/Seaborn package and assign the 'pd'/'np'/'sns' alias to it
import pandas as pd
import numpy as np
import seaborn as sns

# Importing the Pyplot module from the Matplotlib package and assign the 'plt' alias to it
from matplotlib import pyplot as plt

# %% [markdown]
# Sabiendo dónde está y cómo se llama nuestro archivo, es sencillo importarlo en forma de *DataFrame*. Sólo debemos tener claro el formato de entrada de los datos (en nuestro caso `.txt`) y otros detalles como el delimitador de sus columnas (en nuestro caso tabulador `\t`).

# %%
# Reading our Protein Groups data file from the corresponding folder
proteinGroups = pd.read_csv('data/quantitative/MQ/3319_FE_MBR_U_02_proteinGroups.txt', sep='\t')

# %% [markdown]
# Exploraremos un poco el *DataFrame* antes de empezar...

# %%
# DataFrame shape?
proteinGroups.shape

# %%
# DataFrame general info?
proteinGroups.info()

# %%
# DataFrame general info? (Showing all columns and the non-null count)
proteinGroups.info(verbose=True, null_counts=True)

# %% [markdown]
# El *DataFrame* es bastante extenso (5.3 MB), especialmente por su elevado número de columnas (107). Estaría bien aligerarlo un poco seleccionando solo las columnas necesarias. Para ello el primer paso será construir una lista con los nombres de las columnas de interés.

# %% [markdown]
# ## Seleccionando las columnas de interés
#
# Como ya hemos comentado, MQ proporciona tres magnitudes cuantitativas: __I__ (`'Intensity'`), **iBAQ** (`'iBAQ'`), y **LFQ** (`'LFQ intensity'`). Seleccionaremos todas estas columnas y las guardaremos en una lista llamada `selected_cols`:

# %%
# Initiating empty list to append quantitative columns (Intensity, iBAQ and LFQ)
selected_cols = []

# For each column name in proteinGroups columns...
for col in proteinGroups.columns:
    # ... if 'Intensity ' or 'iBAQ ' or 'LFQ intensity ' are in the running column name,
    if 'Intensity ' in col or 'iBAQ ' in col or 'LFQ intensity ' in col:
        # append such column to the selected_cols list
        selected_cols.append(col)

# Printing selected columns
print(selected_cols)

# %% [markdown]
# Ahora necesitamos añadir a `selected_cols` aquellas columnas que utilizaremos para filtrar los grupos de proteínas descartables, las cuales son: `'Potential contaminant'` (proteínas contaminantes), `'Reverse'` (proteínas de secuencia reversa) y `'Only identified by site'` (proteínas identificadas únicamente con péptidos modificados).

# %%
# Adding filtering columns to our selected columns list (by hand)
selected_cols = selected_cols + ['Potential contaminant', 'Reverse', 'Only identified by site']

# Printing selected columns list
print(selected_cols)

# %% [markdown]
# Finalmente, añadiremos la columna `'Protein IDs'` con los identificadores de los grupos de proteínas.

# %%
# Adding IDs column to our selected columns list (by hand)
selected_cols = ['Protein IDs'] + selected_cols

# Printing selected columns list
print(selected_cols)

# %% [markdown]
# Ya tenemos almacenados en `selected_cols` los nombres de las columnas de interés. Ahora ya podemos seleccionar una parte más manejable de `proteinGroups`.

# %%
# Slicing selected columns from proteinGroups DataFrame and copying in a new df DataFrame
df = proteinGroups[selected_cols].copy()

# %% [markdown]
# Exploremos el *DataFrame* `df` resultante...

# %%
# DataFrame shape?
df.shape

# %%
# DataFrame general info?
df.info()

# %% [markdown]
# Ahora tenemos un *DataFrame* una poco más ligero. Hemos pasado de 5.3 a 1.4 MB, y de 107 a 28 columnas.

# %% [markdown]
# ## Simplificando y ordenando nombres de columnas
#
# A menudo los nombres de las columnas de los _DataFrames_ son largos. Esto hace que nuestras líneas de código sean excesivamente largas y nuestros _scripts_ se vuelvan menos leibles y más difíciles de seguir. Algunas de las columnas del _DataFrame_ que acabamos de construir tienen nombres muy extensos. Deberíamos simplificarlos antes de continuar. Podríamos renombrar los nombres "*Intensity*" y "*LFQ intensity*" utilizando el atributo `.columns` junto con los métodos `.str` y `.replace()`.

# %%
# Replacing substrings in df columns to shorten column names
df.columns = df.columns.str.replace('Intensity ', 'I ')
df.columns = df.columns.str.replace('LFQ intensity ', 'LFQ ')
df.columns = df.columns.str.replace('Protein IDs', 'Proteins')

# Printing resulting column names
print(df.columns)

# %% [markdown]
# Por otro lado, vemos que a la hora de nombrar las muestras no se ha seguido un orden secuencial. En general, si las réplicas no están emparejadas, és buena práctica no repetir sus etiquetas y utilizar una numeración secuencial.
#
# | Muestra | Réplica | Réplica (secuencial) |
# |:-------:|:-------:|:--------------------:|
# | A       | 01      | 01                   |
# | A       | 02      | 02                   |
# | A       | 03      | 03                   |
# | A       | 04      | 04                   |
# | B       | 01      | 05                   |
# | B       | 02      | 06                   |
# | B       | 03      | 07                   |
# | B       | 04      | 08                   |
#
# Podemos arreglar este inconveniente de la misma manera que hemos hecho justo antes.

# %%
# Replacing substrings in df columns to get sequential order in the replicate tags
df.columns = df.columns.str.replace('B_01', 'B_05')
df.columns = df.columns.str.replace('B_02', 'B_06')
df.columns = df.columns.str.replace('B_03', 'B_07')
df.columns = df.columns.str.replace('B_04', 'B_08')

# Printing resulting column names
print(df.columns)

# %% [markdown]
# Exploremos el *DataFrame* `df` resultante...

# %%
# DataFrame general info?
df.info()

# %% [markdown]
# Ahora los nombres de las columnas del *DataFrame* `df` son un poco más manejables y las réplicas siguen una numeración secuencial.
#
# > 🧠 **Reflexión:**
# >
# > Mira la información general del DataFrame `df` obtenida con el método `.info()`. ¿Qué significa que las columnas `'Potential contaminant'`, `'Reverse'` y `'Only identified by site'` tengan tan pocos valores no nulos?

# %% [markdown] toc-hr-collapsed=true
# # II.- Filtrando
#
# En el *workflow* de proteómica cualitativa ya vimos la importancia de crear pequeñas funciones para agilizar aquellas manipulaciones recurrentes. El filtrado es una de esas tareas que merece la pena ser implementada en forma de función de **Python**. En este *workflow* iremos un poco más allá. Veremos cómo guardar todas nuestras funciones en un único archivo `.py` (o módulo) y cómo importar y utilizar dichas funciones a nuestro antojo.

# %% [markdown]
# ## Filtrando a nivel de grupo de proteínas
#
# > 🧠 **Reflexión:**
# >
# > En la carpeta raíz del repositorio **p4p** hay un archivo llamado [`p4p.py`](p4p.py). Ábrelo, busca la función `MQ_proteinGroups_filter()`, inspecciónala e intenta entender su funcionamiento.
#
# Aprovecharemos esta función `MQ_proteinGroups_filter()` para filtrar nuestros grupos de proteínas. Para ello, primero debemos importarla. De paso consultaremos su descripción, o *docstring*, utilizando el comando `?`

# %%
# Importing MaxQuant Protein Groups Filter user-defined function from the p4p module
from p4p import MQ_proteinGroups_filter

# Asking for help with MQ_proteinGroups_filter user-defined function
# MQ_proteinGroups_filter?

# %% [markdown]
# La función `MQ_proteinGroups_filter()` nos irá genial para filtrar y descartar los grupos de proteínas que no nos interesen.

# %%
# Filtering-out 'Potential contaminant', 'Reverse' and 'Only identified by site' Protein Groups
df = MQ_proteinGroups_filter(df)

# DataFrame general info?
df.info()

# %% [markdown]
# > 🧠 **Reflexión:**
# >
# > Mira la información general del *DataFrame* `df` obtenida con el método `.info()`. ¿Qué significa que las columnas `'Potential contaminant'`, `'Reverse'` y `'Only identified by site'` tengan cero valores no nulos? ¿Ha funcionado el filtrado que acabamos de realizar?
#
# Una vez aplicados los filtros, las columnas `'Potential contaminant'`, `'Reverse'` y `'Only identified by site'` ya no nos sirven (sólo contienen elementos nulos). Las podemos descartar con el método de **Pandas** `.dropna()`.

# %%
# Dropping futile columns
df = df.dropna(axis=1, how='all')
# axis=1 and how='all' mean that we want to drop those columns (1) that are completely full ('all') of nans

# DataFrame general info?
df.info()

# %% [markdown]
# ##  Filtrando a nivel de proteína
#
# Ya hemos filtrado aquellos grupos de proteínas marcados como `'Potential contaminant'`, `'Reverse'` y `'Only identified by site'`. Desafortunadamente, en algunos de los grupos restantes se siguen colando proteínas contaminantes y reversas:

# %%
# Masking contaminant and reverse ACs
cont_mask = df['Proteins'].str.contains('CON__')
rev_mask = df['Proteins'].str.contains('REV__')

# Showing Protein Groups containing contaminant or reverse ACs
df[cont_mask | rev_mask]

# %% [markdown]
# Fíjate que los *Accession Numbers* (ACs) de las proteínas contaminantes y reversas están precedidos por los *substrings* `'CON__'` y `'REV__'`, respectivamente.
#
# > 🧠 **Reflexión:**
# >
# > Vuelve a consultar el módulo [`p4p.py`](p4p.py), busca la función `list_cleaner()` e intenta entender su funcionamiento.
#
# Aprovecharemos esta función para acabar de limpiar los grupos de proteínas contaminantes y reversas (`REV__` y `CON__`). Para ello, primero debemos importarla y consultar su _docstring_.

# %%
# Importing list_cleaner user-defined function from the p4p module
from p4p import list_cleaner

# Asking for help with list_cleaner user-defined function
# list_cleaner?

# %% [markdown]
# Esta función nos ayudará a limpiar aquellos ACs correspondientes a proteínas contaminantes y reversas de la columna `df['Proteins']`:

# %%
# Dropping 'REV__' and 'CON__' ACs
df['Proteins'] = df['Proteins'].apply(list_cleaner, args=('REV__', ';'))
df['Proteins'] = df['Proteins'].apply(list_cleaner, args=('CON__', ';'))

# %% [markdown]
# > 💡 **Más información:**
# >
# > La función `list_cleaner()` requiere explícitamente los argumentos `pattern=` y `delimiter=`. Nótese que al usar `.apply()` con una función así, hemos de utilizar (a su vez) el argumento `args=`. En `args=` especificaremos los argumentos que `.apply()` le pasará a `list_cleaner()`.
#
# Comprobemos el resultado del limpiado:

# %%
# Checking if the cleaning process succeeded
df[cont_mask | rev_mask]

# %% [markdown] toc-hr-collapsed=true
# # III.- Anotando la taxonomía de los grupos de proteínas
#
# Como ya hemos comentado, las muestras del PME12 contienen un "cóctel" de proteomas de tres especies distintas:
# * *Escherichia coli*
# * *Saccharomyces cerevisiae*
# * *Homo sapiens*
#
# Debemos etiquetar la taxonomía correspondiente a cada uno de los grupos de proteínas presentes en la columna `df['Proteins']`. Pero, ¿todas las proteínas de un grupo comparten la misma taxonomía? *A priori* debería ser así, sin embargo veremos que hay algunos casos "especiales".
#
# El método para realizar la anotación taxonómica que hemos elegido se basa en la utilización de la [**Interfaz de Programación de Aplicaciones** de **UniProt**](https://www.uniprot.org/help/api_idmapping) (en inglés *Application Programming Interface* o simplmente **API**). Esta herramienta tiene cierta complejidad, pero su potencia y sobretodo su gran utilidad hacen que merezca la pena presentarla. Con la intención de simplificar un poco su utilización hemos desarrollado y añadido al módulo **p4p** la función `get_uniprot()`.
#
# Estos son los pasos que seguimeros a continuación:
#
#  1. Obtención de la lista con los ACs de todas las proteínas aparecidas en la columna `df['Proteins']`.
#  2. Anotación taxonómica de cada uno de dichos AC.
#  3. Contrucción del diccionario taxonómico `AC: taxonomía` como parejas `llave: valor`.
#  4. Mapeo de los ACs de nuestro *DataFrame* mediante el diccionario taxonómico.
#  5. Reducción a taxonomías únicas por cada grupo de proteínas.

# %% [markdown]
# ## Obteniendo el listado de proteínas individuales
#
# Copiaremos la columna `df['Proteins']` en un *DataFrame* aparte para avanzar en esta primera etapa.

# %%
# Selecting the 'Proteins' column
proteins = df[['Proteins']].copy()

# DataFrame head?
proteins.head()

# %% [markdown]
# Vemos que en cada grupo de proteínas tenemos varios ACs separados por punto y coma. Primero cortaremos cada grupo por `;` utilizando los métodos `.str` y `.split()`, y luego guardaremos las listas resultantes en una nueva columna llamada `proteins['Proteins lists']`.

# %%
# Splitting to get lists of proteins ACs:
proteins['Proteins lists'] = proteins['Proteins'].str.split(';')

# DataFrame head?
proteins.head()

# %% [markdown]
# A continuación, iteraremos a lo largo de las celdas de la columna `proteins['Proteins lists']` para poder juntar todos los ACs en una única <u>lista plana</u> llamada `protein_acs`:

# %%
# Initiating empty list to extend it in incomming for-loop
protein_acs = list()

# For each protein list in the column 'Proteins lists'...
for protein_list in proteins['Proteins lists']:
    # ... extend the protein_acs list with the running protein_list
    protein_acs.extend(protein_list)

# %% [markdown]
# > 💡 **Más información:**
# >
# > El método de lista `.extend()` simplemente añade la lista `protein_list` al final de la lista `protein_acs`. De esta manera iremos concatenando las listas de la columna `proteins['Proteins lists']` sucesivamente.
#
# Inspeccionemos la lista `protein_acs` que acabamos de obtener:

# %%
# Checking the list length
print(len(protein_acs))

# Checking the first 9 elements of the list
protein_acs[:9]

# %% [markdown] tags=["ejercicio"]
# Hemos pasado de 6321 grupos de proteínas a un total de 11772 proteínas individuales.
#
# > ✏️ **Práctica:**
# >
# > ¿Hay algún AC repetido en la lista `protein_acs`?

# %% tags=["ejercicio", "error"]
# Asking if the number of entries of `protein_acs` is equal to its number of unique entries
#len(___) == len(___)

# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# SOLUTION
# Asking if the number of entries of `protein_acs` is equal to its number of unique entries
len(protein_acs) == len(set(protein_acs))

# %% [markdown]
# ## Buscando taxonomías en UniProt
#
# Ya tenemos la lista con los 11772 ACs que aparecen en nuestros 6321 grupos de proteínas. Ahora podríamos utilizar el servicio [_database identifier mapping (Retrieve/ID mapping)_](https://www.uniprot.org/uploadlists/) de **UniProt** para obtener la anotación taxonómica. Simplemente deberíamos cargar <u>manualmente</u> la lista de ACs que acabamos de obtener a la interfaz de la web.
#
# Nosotros iremos un poco más allá y haremos exactamente los mismo pero <u>programáticamente</u>. Para ello recurriremos a la **API** de **UniProt**. Con la idea de facilitar esta fase del *workflow* hemos preparado una función llamada `get_uniprot()` en el módulo **p4p**. Antes de continuar debemos importarla y consultar su *docstring*.

# %%
# Importing `get_uniprot( )` user-defined function from the p4p module:
from p4p import get_uniprot

# Asking for help with `get_uniprot( )`:
# get_uniprot?

# %% [markdown] tags=["ejercicio"]
# > 🧠 **Reflexión:**
# >
# > Lee el *docstring* de la función `get_uniprot( )` y familiarízate con su sintaxis (qué *inputs* pide, y qué *outputs* da):

# %% [markdown] tags=["ejercicio", "solucion"]
# El *input* más importante de `get_uniprot( )` es la lista con los ACs de los que queremos obtener información anotada en **UniProt** (en nuestro caso, `protein_acs`). El *output* será un diccionario de diccionarios en el que cada AC (llave) estará asociado a un diccionario con la correspondiente información anotada por **UniProt** (valor):
#
# <br/>
#
# <center><em>AC ➞ Diccionario con la información anotada por <b>UniProt</b></em></center>

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > Usa la función `get_uniprot()` para obtener información de la proteína que quieras.

# %% tags=["ejercicio", "error"]
# Querying UniProt information of ___ protein
#get_uniprot([___])

# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# SOLUTION
# Querying UniProt information of human hemoglobin subunit beta protein
get_uniprot(['P68871'])

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > Usa de nuevo la función `get_uniprot()` sobre la misma proteína del ejercicio anterior, pero está vez utiliza el parámetro `extra_columns=` para solicitar también su nombre (`'protein names'`), genes asociados (`'genes'`) y ontología génica (`'go'`).

# %% tags=["ejercicio", "error"]
# Querying UniProt information of ___ protein (request species and genus)
#get_uniprot([___], extra_columns=[___])

# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# SOLUTION
# Querying UniProt information of human hemoglobin subunit beta protein (request species, genus and GO)
get_uniprot(['P68871'], extra_columns=['protein names', 'genes', 'go'])

# %% [markdown]
# > 💡 **Más información:**
# >
# > [Aquí](https://www.uniprot.org/help/uniprotkb_column_names "UniProtKB column names for programmatic access") puedes consultar todas las opciones disponible para añadir al parámetro `extra_columns=` de la función `get_uniprot()`.
#
# Ahora que nos hemos familiarizado con la función `get_uniprot()`, ya podemos continuar con el *workflow*. Teníamos el listado de proteínas individuales en `protein_acs`. Usando el parámetro `extra_columns=['lineage(GENUS)', 'organism']`, podemos obtener la información taxonómica de cada proteína.

# %%
# Querying UniProt information for a list of proteins (requesting species and genus)
uniprot_data = get_uniprot(protein_acs, extra_columns=['lineage(GENUS)', 'organism'])

# %% [markdown]
# Inspeccionemos un poco el diccionario de diccionarios `uniprot_data`:

# %%
# Variable type?
type(uniprot_data)

# %%
# Dictionary length
len(uniprot_data)

# %%
# First dictionary entry?
uniprot_data['A0A024RBG1']

# %%
# 'Organism' entry of the last dictionary entry?
uniprot_data['Q9ZZX0']['Organism']

# %% [markdown]
# Para poder trabajar más cómodamente con los datos taxonómicos obtenidos, vamos a convertir el diccionario de diccionarios `uniprot_data`, en un *DataFrame* de **Pandas** que llamaremos `uniprot_df`:

# %%
# Get a DataFrame from a list of dictionaries sharing the same keys:
uniprot_df = pd.DataFrame(list(uniprot_data.values()))

# DataFrame head?
uniprot_df.head()

# %% [markdown]
# > 💡 **Más información:**
# >
# > Fíjate que hemos utilizado la función de **Pandas** `pd.DataFrame()` para construir un *DataFrame* a partir de una lista de diccionarios. El método de diccionario `.values()` aplicado al diccionario de diccionarios `uniprot_data` selecciona sus valores (que son diccionarios).
#
# Renombremos algunas columnas:

# %%
# Renaming some columns inplace:
uniprot_df.rename(columns={'Organism': 'Species', 
                           'Taxonomic lineage (GENUS)': 'Genus'}, 
                  inplace=True)

# DataFrame tail?
uniprot_df.tail()

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > ¿Cuántas especies distintas hay en las columnas `uniprot_df['Species']`?

# %% tags=["ejercicio", "error"]
# Number of different species in uniprot_df['Species']?
#print(len(___))

# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# SOLUTION
# Number of different species in uniprot_df['Species']?
print(len(uniprot_df['Species'].unique()))

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > ¿Qué especies distintas hay en las columnas `uniprot_df['Species']`?

# %% tags=["ejercicio", "error"]
# Which species are in uniprot_df['Species']?
#print(___)

# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# SOLUTION
# Which species are in uniprot_df['Species']?
print(uniprot_df['Species'].unique())

# %% [markdown]
# ## Creando el diccionario taxonómico
#
# Las 11772 proteínas de nuestros 6321 grupos de proteínas abarcan a 41 especies/cepas diferentes. En vista de la gran variedad de especies presentes, puede ser buena idea restringirnos al **género** como "etiqueta" taxonómica. Veamos cuántos géneros distintos tenemos:

# %%
# Asking the number of unique genera in 'Genus' column
len(uniprot_df['Genus'].unique())

# %% [markdown]
# *A priori* en nuestra muestra solo debería haber 3 géneros, pero tenemos 4... Comprobemos de qué géneros se trata:

# %%
# Asking the names of unique genera in 'Genus' column
uniprot_df['Genus'].unique()

# %% [markdown]
# Además de los tres géneros que esperábamos de antemano (*Homo*, *Escherichia* y *Saccharomyces*), parece ser que se ha colado un "polizón" en nuestra muestra, un virus del género [Totivirus](https://viralzone.expasy.org/646?outline=all_by_species). Contemos cómo se reparten nuestras 11772 proteínas entre los 4 géneros:

# %%
# Counting the occurrences of each genus
uniprot_df['Genus'].value_counts().to_frame()

# %% [markdown]
# Vemos que los ACs identificados pertenecen básicamente a los géneros *Homo*, *Escherichia* y *Saccharomyces*. Solo 4 proteínas se corresponden con el género *Totivirus*. Veámoslo con un poco más de detalle:

# %%
# Masking Totivirus
totivirus_mask = uniprot_df['Genus'] == 'Totivirus'

# Filtering-in Totivirus
uniprot_df[totivirus_mask]

# %% [markdown]
# Una vez esclarecido este misterio, ya podemos crear el diccionario taxonómico `taxonomy_dict`. En este diccionario las llaves serán los ACs y los valores serán los correspondientes géneros.

# %%
# Storing the ACs and their corresponding genera in separated Series 
acs = uniprot_df['AC']
genera = uniprot_df['Genus']

# Zipping ACs and genera Series and getting the taxonomy dictionary
taxonomy_dict = dict(zip(acs, genera))

# %% [markdown]
# > 💡 **Más información:**
# >
# > La utilización anidada de las funciones `dict()` y `zip()` resulta muy útil para construir diccionarios a partir de dos listas o dos *Series* de la misma longitud.
#
# Con el diccionario `taxonomy_dict`, podremos mapear cada AC con su correspondiente género de manera unívoca:

# %%
# Which is the taxonomy of this protein?
taxonomy_dict['Q87026']

# %% [markdown]
# ## Mapeando la taxonomía
#
# Finalmente estamos en disposición de mapear los ACs de la columna `df['Proteins']` com su respectiva taxonomía. Primero crearemos una nueva columna `df['Taxonomy']` como una copia exacta de la columna `df['Proteins']`:

# %%
# Inserting a new 'Taxonomy' column in position 1 as a exact copy of 'Proteins'
df.insert(1, 'Taxonomy', df['Proteins'])

# DataFrame head?
df.head()

# %% [markdown]
# Aplicaremos un renombrado a la columna `df['Taxonomy']` utilizando nuestro diccionario `taxonomy_dict` con sus duplas `AC: taxonomía` como parejas `llave: valor`.

# %%
# Very slow renaming (Less than 1 minut in Workstation, but for sure a little bit more in Laptop)
print('Please wait...')
df['Taxonomy'] = df['Taxonomy'].replace(taxonomy_dict, regex=True)
print('... Done!')

# DataFrame head?
df.head()

# %% [markdown]
# > 💡 **Más información:**
# >
# > En el *workflow* de cualitativa hicimos manipulaciones parecidas a esta utilizando el método `.map()` de las *Series* de **Pandas** (rápido). Aquí nos hemos visto obligados a usar el método `.replace()` con el parámetro `regex=True` (lento). Esto es debido a que aquí no estamos reemplazado *strings* completos, sino partes de *strings* o *substrings*.

# %% [markdown]
# ## Filtrando y simplificando taxonomías redundantes
#
# Ya hemos sustituido cada AC de la columna `df['Taxonomy']` por su correspondiente etiqueta taxonómica, pero queremos una única etiqueta por cada grupo de proteínas. Por ejemplo, en vez de `'Homo;Homo;Homo'` querríamos simplemente `'Homo'`. Arreglaremos esto aprovechando la clase de **Python** `frozenset`.
#
# Primero pasaremos los *strings* de la columna `df['Taxonomy']` a listas:

# %%
# Splitting to obtain lists of taxonomies
df['Taxonomy'] = df['Taxonomy'].str.split(';')

# DataFrame head?
df.head()

# %% [markdown]
# Ahora podemos convertir las listas de `df['Taxonomy']` en *frozensets*. De esta manera descartaremos automáticamente aquellas etiquetas taxonómicas redundantes:

# %%
# Converting lists into frozensets to drop duplicated taxonomy tags
df['Taxonomy'] = df['Taxonomy'].apply(frozenset)

# DataFrame head?
df.head()

# %% [markdown]
# > 💡 **Más información:**
# >
# > Los objetos *frozenset*, tal como su nombre indica, son <u>inmutables</u> (un *frozenset* no se puede modificar una vez creado), y se puede calcular su unicidad (son [_"hashables"_](https://docs.python.org/3.3/glossary.html#term-hashable)), con lo que métodos como `Series.unique()` o `Dataframe.groupby()` pueden funcionar con ellos (mientras que si *Series* o *DataFrames* contienen objetos *no "hashables"*, como los *sets*, estos y otros métodos provocan *Excepciones*).
#
# Comprobemos si hemos logrado una única taxonomía por grupo de proteínas:

# %%
# Which taxonomies are there in df['Taxonomy']?
df['Taxonomy'].unique()

# %% [markdown]
# Al parecer tenemos grupos mixtos (multitaxonómicos). Esto puede representar un problema dependiendo de la medida en la que haya ocurrido. ¿Cuántos grupos de proteínas hay por cada combinación de taxonómica?

# %%
# Counting the occurrences of each taxonomic group
df['Taxonomy'].value_counts().to_frame()

# %% [markdown]
# Por suerte, son sólo 30 + 3 + 2 + 1  = 36 casos, relativamente pocos. Podemos descartar estos 36 grupos multitaxonómicos junto con los 2 grupos de *Totivirus*, un total de 38 casos.

# %%
# Masking multitaxonomical protein groups
multitaxo = (df['Taxonomy'].apply(len) > 1)

# Masking Totivirus protein groups
totivirus = (df['Taxonomy'] == frozenset(['Totivirus']))

# Filtering-out multitaxonomical and Totivirus protein groups
df = df[~multitaxo & ~totivirus].copy()

# Resetting index and dropping it inplace
df.reset_index(inplace=True, drop=True)

# Which taxonomies there are in df['Taxonomy'] NOW?
df['Taxonomy'].unique()

# %%
# Counting the occurrences of each taxonomic group after dropping multitaxonomical and Totivirus
df['Taxonomy'].value_counts().to_frame()

# %% [markdown]
# Ya tenemos una única etiqueta taxonómica por cada grupo de proteínas. Finalmente extraeremos los nombres de las etiquetas del interior de los *frozensets* para limpiar un poco la apariencia de columna `df['Taxonomy']`:

# %%
# Extract taxonomy strings from inside frozensets
df['Taxonomy'] = df['Taxonomy'].apply(list).str[0]

# DataFrame head?
df.head()

# %% [markdown]
# Exportemos este *DataFrame*:

# %%
# Exporting the DataFrame
df.to_excel('data/quantitative/3319_FE_MBR_U_02_df.xlsx')

# %% [markdown]
# # IV.- Construyendo un *Tidy-DataFrame*
#
# De momento hemos conseguido un *DataFrame* filtrado y con la taxonomía asociada a cada grupo de proteínas bien anotada. ¿Podríamos obtener un *DataFrame* aún más <u>ordenado</u>?

# %%
# DataFrame head?
df.head()

# %% [markdown]
# El *DataFrame* `df` tiene 2 x 4 x 3 = 24 columnas que podríamos reorganizar (2 muestras, 4 réplicas por muestra, 3 variables cuantitativas por réplica). ¿Y si nuestro *DataFrame* tubiera una columna especificando la muestra (<b>A</b> y <b>B</b>), otra especificando la réplica (**01**, **02**, **03**, **04**, **05**, **06**, **07** y **08**), otra especificando la variable cuantitativa (__I__, **iBAQ** y **LFQ**), y justo al lado una única columna con el **valor númerico** de la correspondiente variable cuantitativa?
#
# Este tipo de *DataFrames* que colapsan muchas columnas en unas pocas se conocen como *Tidy-DataFrames*. A lo largo de las siguientes secciones veremos que trabajar de manera *Tidy* facilita muchísimo el análisis exploratorio de datos (*exploratory data analysis* o **EDA**). En esta sección transformaremos `df` en un *Tidy-DataFrame* e intentaremos habituarnos a manipular este tipo de *DataFrames*. 
#
# La función [`melt()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.melt.html) de **Pandas** permite convertir un *DataFrame* típico en un *Tidy-DataFrame* de manera muy fácil. Empecemos creando una lista con nuestras 24 columnas de valores cuantitativos:

# %%
# Initiating empty list to append quantitative columns (I, iBAQ and LFQ)
quant_cols = []

# For each column in df columns...
for col in df.columns:
    # ... if 'I ' or 'iBAQ ' or 'LFQ ' are in the running column name,
    if 'I ' in col or 'iBAQ ' in col or 'LFQ ' in col:
        # append such column to the quant_cols list
        quant_cols.append(col)

# Print quantitative columns
print(quant_cols)

# %% [markdown]
# Ahora fundiremos `df` con la función `melt()` y obtendremos el *Tidy-DataFrame* `tidy_df`:

# %%
# Melting our DataFrame
tidy_df = pd.melt(frame=df,
                  id_vars=['Proteins', 'Taxonomy'],
                  value_vars=quant_cols,
                  var_name='Variable',
                  value_name='Value')

# DataFrame head
tidy_df.head()

# %% [markdown]
# Ahora utilizaremos los métodos `.str` y `.split()` para conseguir una columna con la variable cuantitativa (`'Quant'`) a la que hace referencia cada una de las filas (grupos de proteínas) de `tidy_df`. Procederemos análogamente para obtener una columna con la pareja Muestra-Réplica (`'Sample_Replicate'`):

# %%
# Creating a column with the corresponding quantitative variables
tidy_df['Quant'] = tidy_df['Variable'].str.split(' ').str[0]

# Creating a column with the corresponding quantitative variables
tidy_df['Sample_Replicate'] = tidy_df['Variable'].str.split(' ').str[1]

# DataFrame head
tidy_df.head()

# %% [markdown]
# Para separar las etiquetas de muestras (`'Sample'`) y de réplicas (`'Replicate'`) en columnas independientes haremos los mismo:

# %%
# Creating columns with replicates and samples
tidy_df['Sample'] = tidy_df['Sample_Replicate'].str.split('_').str[0]
tidy_df['Replicate'] = tidy_df['Sample_Replicate'].str.split('_').str[1]

# DataFrame head
tidy_df.head()

# %% [markdown]
# Las columnas `'Variable'` y `'Sample_Replicate'` son redundates. Las podemos descartar con el método de **Pandas** `.drop()`:

# %%
# Dropping redundant columns inplace
tidy_df.drop(columns=['Variable', 'Sample_Replicate'], inplace=True)

# DataFrame head
tidy_df.head()

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > ¿Qué valores distintos hay en las columnas `tidy_df['Quant']`, `tidy_df['Sample']` y `tidy_df['Replicate']`?

# %% tags=["ejercicio", "error"]
# Initiating column list
#cols = ['___', '___', '___']

# For each column in the column list...
#for ___ in ___:
    # ... which unique values there are in tidy_df[___]?
#    print(___)

# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# SOLUTION
# Initiating column list
cols = ['Quant', 'Sample', 'Replicate']

# For each column in the column list...
for col in cols:
    # ... which unique values there are in tidy_df[___]?
    print(tidy_df[col].unique())

# %% [markdown]
# Exploremos un poco el *DataFrame*...

# %%
# DataFrame head?
tidy_df.info()

# %% [markdown]
# Vemos que `tidy_df` tiene 6 columnas y 150792 filas (`df` tenía  tenía 26 columnas y 6283 filas). La explosión en el número de filas es intrínseca a la hora de trabajar con un *Tidy-DataFrame*, ya que el número original de filas de `df` se multiplica por el número de muestras (2), el número de réplicas dentro de cada muestra (4), y el número de variables cuantitativas (3) dentro de cada réplica:

# %%
# 2 samples, 4 replicates, 3 quantitative variables
150792 == 6283 * 2 * 4 * 3

# %% [markdown]
# Trabajar con un *Tidy-DataFrame* tiene claras ventajas desde el punto de vista computacional, pero puede resultar bastante antiintuitivo. Haremos un ordenamiento de las filas de `tidy_df` (usando varias columnas sucesivamente) con tal de recuperar un poco la perspectiva:

# %%
# Sorting by 'Proteins', 'Quant', 'Sample' and 'Replicate' columns inplace
tidy_df.sort_values(by=['Proteins', 'Quant', 'Sample', 'Replicate'], inplace=True)

# DataFrame head?
tidy_df.head(24)

# %% [markdown]
# Comparemos la información del grupo de proteínas `A0A024RBG1;Q9NZJ9` contenida en `tidy_df`, con su contrapartida contenida en `df`:

# %%
# DataFrame head?
df.head(1)

# %% [markdown] toc-hr-collapsed=true
# # V.- Trabajando con *Tidy-DataFrames* I
#
# Ya tenemos nuestro *Tidy-DataFrame* preparado. Ahora empezaremos a trabajar con las tres variables cuantitativas __I__, **iBAQ** y **LFQ**. Y para ello recurriremos principalmente a los métodos `.groupby()` y `.transform()` de los *DataFrame*, entre otros.

# %% [markdown]
# ## Estimando los valores nulos
#
# Al explorar `tidy_df`, no parece que haya valores nulos pero si nos fijamos veremos que hay muchos ceros. En la práctica estos ceros deben ser tratados como valores nulos o perdidos. Reemplacemos `0` por `np.nan`:

# %%
# Replacing zeros with nans to be able to compute the log2 without problems
tidy_df = tidy_df.replace(to_replace=0, value=np.nan)

# DataFrame general info
tidy_df.info()

# %% [markdown]
# Al realizar un estudio cuantitativo, debemos conocer muy bien qué impacto tienen los valores nulos en nuestros datos. ¿Cuántos valores nulos tenemos por muestra, réplica y variable cuantitativa? El uso conjunto de los métodos de **Pandas** `.groupby()` y `.agg()` nos permiten responder estas preguntas prácticamente en una línea de código. Definiremos una máscara y luego haremos un agrupar-y-agregar para responderla:

# %%
# Masking nan values
nan_mask = tidy_df['Value'].isna()

# Filtering-in missings, grouping by 'Sample', 'Replicate', 'Quant' and aggregating 'Proteins' with len
tidy_df[nan_mask].groupby(['Sample', 'Replicate', 'Quant'])['Proteins'].agg(len).to_frame()

# %% [markdown]
# > 💡 **Más información:**
# >
# > Fíjate en la gran analogía entre nuestra pregunta y la sintaxis de **Pandas**: ¿Cuántos (`.agg(len)`) valores nulos (`tidy_df[nan_mask]`) tenemos por muestra, por réplica y por variable cuantitativa (`.groupby(['Sample', 'Replicate', 'Quant'])`)?
#
# > 🧠 **Reflexión:**
# >
# > ¿Qué hubiera pasado al hacer el agrupar-y-agregar si las réplicas no tubieran una numeración secuencial?
#
# De momento vemos que la cantidad de valores nulos en __I__ e <b>iBAQ</b> es idéntica para todas las muestras y réplicas. A su vez, observamos que, en general, la cantidad de valores nulos en <b>LFQ</b> es mucho mayor que en <b>I</b> e <b>iBAQ</b>.

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > ¿Dado un grupo de proteínas, dada una variable cuantitativa, y dada una muestra, cuántas réplicas con valores nulos hay?

# %% tags=["ejercicio", "error"]
# Filtering-in missings, grouping by 'Proteins', 'Sample', 'Quant'; and aggregating 'Replicate' with len function
#tidy_df[___].groupby(['___', '___', '___'])['___'].agg(___).to_frame().head(9)

# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# SOLUTION
# Filtering-in missings, grouping by 'Proteins', 'Sample', 'Quant'; and aggregating 'Replicate' with len function
tidy_df[nan_mask].groupby(['Proteins', 'Quant', 'Sample'])['Replicate'].agg(len).to_frame().head(9)

# %% [markdown]
# Incorporaremos esta información a nuestro *Tidy-DataFrame* nos iría muy bien. Para ello resulta especialmente útil el uso en tándem de los métodos de **Pandas** `.groupby()` y `.transform()` (en vez de `.groupby()` y `.agg()`).

# %%
# Filtering-in missings, grouping by 'Proteins', 'Quant', 'Sample'; and transforming 'Replicate' with len function
tidy_df['Missing'] = tidy_df[nan_mask].groupby(['Proteins', 'Quant', 'Sample'])['Replicate'].transform(len)

# DataFrame head?
tidy_df.head(24)

# %% [markdown]
# Tomaremos el número máximo de valores nulos por grupo de proteínas, variable cuantitativa y muestra:

# %%
# Grouping by 'Proteins', 'Quant', 'Sample'; and transforming 'Missing' with max function
tidy_df['Max missing'] = tidy_df.groupby(['Proteins', 'Quant', 'Sample'])['Missing'].transform(max)

# DataFrame head?
tidy_df.head(24)

# %% [markdown]
# La columna `tidy_df['Missing']` no nos sirve. Las podemos descartar con el método de **Pandas** `.drop()`. Además, sabemos que los valores `NaN` de la columna `tidy_df['Max missing']` que acabamos de crear deben ser ceros, y que su contenido son número enteros:

# %%
# Dropping redundant columns inplace
tidy_df.drop(columns=['Missing'], inplace=True)

# Replacing NaNs with zeros in column 'Max missing' inplace
tidy_df['Max missing'].fillna(value=0, inplace=True)

# Defining integer data type in column 'Max missing'
tidy_df['Max missing'] = tidy_df['Max missing'].astype('int')

# DataFrame head
tidy_df.head(24)

# %% [markdown]
# ## Transformando a logaritmo

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > Comprueba mediante EDA la distribución de valores cuantitativos usando la función de **Seaborn** [`catplot()`](https://seaborn.pydata.org/generated/seaborn.catplot.html). Para ello, utiliza el *snippet* de código proporcionado y sigue las instrucciones. Antes de empezar, ejecútalo para tener en mente el punto de partida.
# >
# > **a)** Especifica el parámetro `col='Quant'` y ejecuta el *snippet*.\
# > **b)** Especifica el parámetro `row='Taxonomy'` y ejecuta el *snippet*.\
# > **c)** Especifica el parámetro `hue='Sample'` y ejecuta el *snippet*.\
# > **d)** Especifica el parámetro `sharey=False` y ejecuta el *snippet*.\
# > **e)** Especifica el parámetro `kind='box'` y ejecuta el *snippet*. ¿Ha habido algún problema? Trata de arreglarlo.\
# > **f)** Especifica el parámetro `kind='violin'` y ejecuta el *snippet*.
# >
# > ¿Siguen una distribución normal los valores cuantitativos?

# %% tags=["ejercicio"]
# Plotting catplot
#sns.catplot(data=tidy_df, x='Replicate', y='Value', kind='strip',
#            hue=None, row=None, col=None,
#            sharex=True, sharey=True, alpha=0.05)

# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# SOLUTION
# Plotting catplot
sns.catplot(data=tidy_df, x='Replicate', y='Value', kind='violin',
            hue='Sample', row='Taxonomy', col='Quant',
            sharex=True, sharey=False)

# %% [markdown]
# Los valores cuantitativos __I__, **iBAQ** y **LFQ** no se distribuyen de manera normal. Para poder realizar un test estadístico más adelante, debemos normalizarlos de alguna manera. Conseguiremos valores con distribución normal simplemente pasando a logaritmo en base 2.

# %%
# Inserting a new 'log2 Value' column in position 3 as the log2 of 'Value'
tidy_df.insert(3, 'log2 Value', tidy_df['Value'].apply(np.log2))

# DataFrame head?
tidy_df.head(24)

# %% [markdown]
# Comprobémoslo aprovechando el *snippet* del ejercicio anterior:

# %%
# Plotting catplot
sns.catplot(data=tidy_df, x='Replicate', y='log2 Value', kind='violin',
            hue='Sample', row='Taxonomy', col='Quant',
            sharex=True, sharey=True)

# %% [markdown]
# ## Imputando valores nulos
#
# Para poder realizar un test estadístico más adelante, debemos partir de un *DataSet* <u>completo</u>, es decir, <u>sin valores nulos</u>. Podemos obtener un *DataSet* completo de dos maneras:
# + Descartando las observaciones (en nuestro caso grupos de proteínas) que contengan almenos un valor nulo. Para ello podríamos usar el método de **Pandas** [`.dropna()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.dropna.html).
# + Imputando los valores nulos. Para ello podríamos usar el método de **Pandas** [`.fillna()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.fillna.html).
#
# Nosotros nos decantaremos por imputar para no perder ningún grupo de proteínas. El método de imputación que utilizaremos se basa en generar números aleatorios con una distribución normal de media $\mu_i = \mu_e - 2\sigma_e$ y desviación estándar $\sigma_i = \frac{\sigma_e}{3}$, dónde $\mu_e$ y $\sigma_e$ son respectivamente la media y la desviación estándar de los valores experimentales (dada una muestra y dada una variable cuantitativa). Calcularemos la media y la desviación estándar de `'log2 Value'` por muestra y por variable cuantitativa:

# %%
# Grouping by 'Quant', 'Sample' and aggregating 'log2 value' with mean
tidy_df.groupby(['Quant', 'Sample'])['log2 Value'].agg('mean').to_frame()

# %%
# Grouping by 'Quant', 'Sample' and aggregating 'log2 value' with std
tidy_df.groupby(['Quant', 'Sample'])['log2 Value'].agg('std').to_frame()

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > Incorpora la media y la desviación estándar (por variable cuantitativa y por muestra) de `'Value log2'`a `tidy_df`. Utiliza los métodos de **Pandas** `.groupby()` y `.transform()`.

# %% tags=["ejercicio"]
# Incorporating the mean of 'log2 Value' (by 'Quant', 'Sample') into tidy_df
#tidy_df['mean log2 Value'] = ___

# Incorporating the std of 'log2 Value' (by 'Quant', 'Sample') into tidy_df
#tidy_df['std log2 Value'] = ___

# DataFrame head
#tidy_df.head(24)

# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# SOLUTION
# Incorporating the mean of 'log2 Value' (by 'Quant', 'Sample') into tidy_df
tidy_df['mean log2 Value'] = tidy_df.groupby(['Quant', 'Sample'])['log2 Value'].transform('mean')

# Incorporating the std of 'log2 Value' (by 'Quant', 'Sample') into tidy_df
tidy_df['std log2 Value'] = tidy_df.groupby(['Quant', 'Sample'])['log2 Value'].transform('std')

# DataFrame head
tidy_df.head(24)

# %% [markdown]
# Con las dos columnas que acabamos de construir, podemos generar una nueva columna con valores aleatorios gracias a la función [`normal()`](https://docs.scipy.org/doc/numpy-1.15.0/reference/generated/numpy.random.normal.html) del módulo `random` del paquete **Numpy**:

# %%
# Initiating random number seed
np.random.seed(seed=0)

# Creating column with random numbers normally distributed (downshift = 2*sigma) (spread = sigma/3)
tidy_df['Random log2 Value'] = np.random.normal(loc=tidy_df['mean log2 Value']-(2 * tidy_df['std log2 Value']),
                                                scale=tidy_df['std log2 Value']/3)

# DataFrame head
tidy_df.head(24)

# %% [markdown]
# Ahora imputaremos los valores nulos de la columna `df['log2 Value']` con los valores de la columna `df['Random log2 Value']`. Los valores resultantes los colocaremos en una nueva columna `df['Imputed log2 Value']`:

# %%
# Inserting a new 'Imputed log2 Value' column in position 4 as copy of 'log2 Value' with imputed values from 'Random log2 Value'
tidy_df.insert(4, 'Imputed log2 Value', tidy_df['log2 Value'].fillna(tidy_df['Random log2 Value']))

# DataFrame head
tidy_df.head(24)

# %% [markdown]
# Una vez realizada la imputación, las columnas `'mean log2 Value'`, `'std log2 Value'` y `'Random log2 Value'` no nos sirven. Las podemos descartar con el método de **Pandas** `.drop()`.

# %%
# Dropping futile columns
tidy_df.drop(columns=['mean log2 Value', 'std log2 Value', 'Random log2 Value'], inplace=True)

# DataFrame head
tidy_df.head(24)

# %% [markdown]
# Exportemos este *DataFrame*:

# %%
# Exporting the DataFrame
tidy_df.to_excel('data/quantitative/3319_FE_MBR_U_02_tidy_df.xlsx')

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > Comprueba mediante EDA la distribución de valores cuantitativos originales e imputados usando [`catplot()`](https://seaborn.pydata.org/generated/seaborn.catplot.html). Aprovecha el *snippet* de código proporcionado y sigue las instrucciones. Antes de empezar, ejecútalo para tener en mente el punto de partida.
# >
# > **a)** Especifica el parámetro `kind='violin'` y ejecuta el *snippet*.\
# > **b)** Especifica el parámetro `col='Quant'` y ejecuta el *snippet*.\
# > **c)** Especifica el parámetro `kind='box'` y ejecuta el *snippet*.\
# > **d)** Especifica el parámetro `kind='violin'` y ejecuta el *snippet*.\
# > **e)** Especifica el parámetro `hue='Taxonomy'` y ejecuta el *snippet*.
# >
# > ¿Para qué tríada `'Sample'` - `'Quant'` - `'Taxonomy'` tienen más peso los valores imputados?

# %% tags=["ejercicio"]
# Plotting catplot
#sns.catplot(data=tidy_df, x='Sample', y='log2 Value', kind='box',
#            hue=None, row=None, col=None)

# Plotting catplot for IMPUTED
#sns.catplot(data=tidy_df, x='Sample', y='Imputed log2 Value', kind='box',
#            hue=None, row=None, col=None)

# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# SOLUTION
# Plotting catplot
sns.catplot(data=tidy_df, x='Sample', y='log2 Value', kind='violin',
            hue='Taxonomy', row=None, col='Quant')

# Plotting catplot for IMPUTED
sns.catplot(data=tidy_df, x='Sample', y='Imputed log2 Value', kind='violin',
            hue='Taxonomy', row=None, col='Quant')

# %% [markdown] toc-hr-collapsed=false
# # VI.- Trabajando con *Tidy-DataFrames* II
#
# Hemos conseguido que los valores de nuestras tres variables cuantitativas __I__, **iBAQ** y **LFQ** presenten una distribución normal (transformando a logaritmo en base 2) y también hemos logrado tener un *Dataset* completo (imputando los valores nulos con números aleatorios). Adicionalmente, también hemos dedicado cierto esfuerzo a estimar el número de réplicas con valores nulos dado un grupo de proteínas, dada una variable cuantitativa, y dada una muestra. Vamos a seguir avanzando paso a paso para conseguir nuestro objetivo de representar el *volcano plot*.

# %% [markdown]
# ## Calculando el *Fold Change*
#
# El *Fold Change* no es más que una medida de en qué proporción cambia una magnitud entre dos estados. El *Fold Change* de la magnitud $q$ entre los estados A y B (A inicial $\rightarrow$ B final) viene dado por:
#
# \begin{equation} 
#  FC_q = \frac{q_B}{q_A} ,\\
# \end{equation}
#
# donde $q_A$ y $q_B$ son la magnitud $q$ en los estados $A$ y $B$, respectivamente. Nosotros queremos calcular el <u>*Fold Change* de la media de las 4 réplicas</u> entre las *2 muestras*, y esto para cada una de las 3 magnitudes cuantitativas:
#
# \begin{equation} 
#  FC_{I} = \frac{\overline{I}_B}{\overline{I}_A} \qquad FC_{LFQ} = \frac{\overline{LFQ}_B}{\overline{LFQ}_A} \qquad FC_{iBAQ} = \frac{\overline{iBAQ}_B}{\overline{iBAQ}_A} \
# \end{equation}
#
# Debemos pues promediar las 4 réplicas. Esto significa que una vez agregadas, perderemos de vista los datos originales de los que surgen los correspondientes valores $\overline{I}$, $\overline{LFQ}$ e $\overline{iBAQ}$.

# %%
# DataFrame head
tidy_df.head(8)

# %% [markdown]
# En esta situación el método de **Pandas** [`.pivot_table()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.pivot_table.html) exhibe todo su potencial. Pivotaremos `tidy_df` utilizando como índices (`index=`) `'Proteins'`, `'Taxonomy'` y `'Quant'`; y como columna  (`columns=`) `'Sample'`. Los valores (`values=`) que queremos agregar serán los de `'Imputed log2 Value'` mediante la función de agregación (`aggfunc=`) promedio (`'mean'`). Fíjate que para agregar los valored de `'Imputed log2 Value'` de las cuatro réplicas presentes en cada muestra, hemos tenido que excluir la columna `'Replicate'` del `.pivot_table()`.

# %%
# Pivoting tidy_df and aggregating with the mean
pivot_df = tidy_df.pivot_table(index=['Proteins', 'Taxonomy', 'Quant'],
                               columns=['Sample'],
                               values=['Imputed log2 Value'],
                               aggfunc='mean')

# DataFrame head?
pivot_df.head(9)

# %% [markdown]
# Con las columnas `pivot_df['Imputed log2 Value']['A']` y `pivot_df['Imputed log2 Value']['B']` que acabamos de obtener podemos calcular el logaritmo en base 2 del *Fold Change* mediante la siguiente expresión:
#
# \begin{equation} 
#  \log_2 FC_q = \log_2 \left( \frac{q_B}{q_A} \right) \\
#  \log_2 FC_q = \log_2q_B - \log_2q_A \\
# \end{equation}
#
#  
# > 💡 **Más información:**
# >
# > Fíjate qué sencillo es acceder a una columna multinivel de una tabla pivotada!!!

# %%
# Computing the Fold Change (notice how to acces the multicolumn levels)
pivot_df['log2 FC'] = pivot_df['Imputed log2 Value']['B'] - pivot_df['Imputed log2 Value']['A']

# DataFrame head
pivot_df.head(9)

# %% [markdown]
# Recuerda que estamos trabajando con el logaritmo en base 2 de __I__, **LFQ** e **iBAQ** por eso las diferencias B menos A para cada una de las tres variables cuantitativas nos dan el $\log_2 FC_q$. Si quisiéramos el *Fold Change* sin logaritmo:

# %% [markdown]
# \begin{equation} 
#  2^{\left( \log_2 FC_q \right)} = 2^{\left(\log_2q_B - \log_2q_A\right)} \\
#  FC_q = 2^{\left(\log_2q_B - \log_2q_A\right)}
# \end{equation}
#

# %%
# Computing the FC of the Fold Change (notice how to acces the multicolumn levels)
pivot_df.insert(2, 'FC', 2**(pivot_df['Imputed log2 Value']['B'] - pivot_df['Imputed log2 Value']['A']))

# DataFrame head
pivot_df.head(9)

# %% [markdown]
# ## Estimando de nuevo valores nulos
#
# Ya habíamos determinado el número de réplicas con valores nulos (dado un grupo de proteínas, dada una variable cuantitativa y dada una muestra). Al calcular el *Fold Change*, estamos comparando 4 réplicas de la muestra A con 4 réplicas de la muestra B. ¿Cual sería el número de réplicas con valores nulos de dicha comparación?

# %%
# DataFrame head
tidy_df.head(16)

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > Utiliza el método de **Pandas** `.pivot_table()` para obtener el máximo número de valores nulos por muestra (de entre las cuatro réplicas), dado un grupo de proteínas, una taxonomía, y una variable cuantitativa.

# %% tags=["ejercicio"]
# Pivoting tidy_df and aggregating with the max
#pivot2_df = tidy_df.pivot_table(index=['___', 'Taxonomy', '___'],
#                                columns=['Sample'],
#                                values=['___'],
#                                aggfunc='___')

# DataFrame head?
#pivot2_df.head(9)

# %% jupyter={"source_hidden": true} tags=["solucion", "ejercicio"]
# SOLUTION
# Pivoting tidy_df and aggregating with the max
pivot2_df = tidy_df.pivot_table(index=['Proteins', 'Taxonomy', 'Quant'],
                                columns=['Sample'],
                                values=['Max missing'],
                                aggfunc='max')

# DataFrame head?
pivot2_df.head(9)

# %% [markdown]
# Redefiniremos el número máximo de valores nulos para un grupo de proteínas y una variable cuantitativa, como el valor máximo de valores nulos de entre las muestras A y B:

# %%
# Pivoting tidy_df and aggregating with the max
pivot2_df = tidy_df.pivot_table(index=['Proteins', 'Taxonomy', 'Quant'],
                                columns=None,
                                values=['Max missing'],
                                aggfunc='max')

# DataFrame head?
pivot2_df.head(9)

# %% [markdown]
# Insertemos esta información a la tabla pivotada `pivot_df` dónde tenemos los *Fols Change*:

# %%
# Inserting a new 'Max missing' column in position 0 as copy of pivot2_df['Max missing']
pivot_df.insert(0, 'Max missing', pivot2_df['Max missing'])

# DataFrame head?
pivot_df.head(9)

# %% [markdown]
# Ahora que ya tenemos los *Fold change* calculados, podemos empezar a construir algunas visualizaciones más interesantes. Como paso previo a la siguiente práctica resetearemos el índice de `pivot_df` y prescindiremos de los multiniveles de sus columnas:

# %%
# Resetting index in pivot_df and redefining as plot_df
plot_df = pivot_df.reset_index()

# Flattening column levels in plot_df and renaming (by hand)
plot_df.columns = ['Proteins', 'Taxonomy', 'Quant', 'max Max missing', 'mean Imputed log2 Value A', 'max Imputed log2 Value B', 'FC', 'log2 FC']

# Shortening column names (by hand)
plot_df.columns = ['Proteins', 'Taxonomy', 'Quant', 'Miss', 'Value A', 'Value B', 'FC', 'log2 FC']

# DataFrame head?
plot_df.head()

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > Comprueba mediante EDA la relación entre de valores cuantitativos de las muestras A y B usando [`lmplot()`](https://seaborn.pydata.org/generated/seaborn.lmplot.html). Aprovecha el *snippet* de código proporcionado y sigue las instrucciones. Antes de empezar, ejecútalo para tener en mente el punto de partida.
# >
# > **a)** Especifica los parámetros `col='Quant'` y `col_order=['I', 'iBAQ', 'LFQ']`, y ejecuta el *snippet*.\
# > **b)** Especifica el parámetro `hue='Taxonomy'` y ejecuta el *snippet*.\
# > **c)** Reduce gradualmente la variable `miss_threshold` y ves ejecutando el *snippet* por cada valor (4, 3, 2, 1, 0).
# > 
# > Compara el peso representado por los valores imputados según la variable quantitativa.

# %% tags=["ejercicio"]
# Defining a maximum missing value threshold for incomming masking
#miss_threshold = 4

# Masking values with less than a certain number of missing values in both 'Samples'
#miss_mask = plot_df['Miss'] <= miss_threshold

# Plotting lmplot
#sns.lmplot(data=plot_df[miss_mask], x='Value A', y='Value B', 
#           hue=None, row=None, col=None, col_order=None,
#           sharex=True, sharey=True,
#           scatter=True, fit_reg=True, truncate=True,
#           scatter_kws={'alpha': 0.05},
#           line_kws={'alpha': 0.5})

# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
#SOLUTION
# Defining a maximum missing value threshold for incomming masking
miss_threshold = 0

# Masking values with less than a certain number of missing values in both 'Samples'
miss_mask = plot_df['Miss'] <= miss_threshold

# Plotting lmplot
sns.lmplot(data=plot_df[miss_mask], x='Value A', y='Value B', 
           hue='Taxonomy', row=None, col='Quant', col_order=['I', 'iBAQ', 'LFQ'],
           sharex=True, sharey=True,
           scatter=True, fit_reg=True, truncate=True,
           scatter_kws={'alpha': 0.05},
           line_kws={'alpha': 0.5})

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > Comprueba mediante EDA la distribución de FCs obtenidos usando [`catplot()`](https://seaborn.pydata.org/generated/seaborn.lmplot.html). Aprovecha el *snippet* de código proporcionado y sigue las instrucciones. Antes de empezar, ejecútalo para tener en mente el punto de partida.
# >
# > **a)** Especifica los parámetros `col='Quant'` y `col_order=['I', 'iBAQ', 'LFQ']`, y ejecuta el *snippet*.
# > 
# > ¿Qué variables cuantitativas funionan mejor cuando se tienen pocos valores nulos? ¿Y cuándo se tienen muchos?

# %% tags=["ejercicio"]
# Defining a maximum missing value threshold for incomming masking
#miss_threshold = 0

# Masking values with less than a certain number of missing values in both 'Samples'
#miss_mask = plot_df['Miss'] <= miss_threshold

# Plotting catplot
#g = sns.catplot(data=plot_df[miss_mask], x='Taxonomy', y='log2 FC', kind='box',
#                hue=None, row=None, col=None, col_order=None)

# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# SOLUTION
# Defining a maximum missing value threshold for incomming masking
miss_threshold = 0

# Masking values with less than a certain number of missing values in both 'Samples'
miss_mask = plot_df['Miss'] <= miss_threshold

# Plotting catplot
g = sns.catplot(data=plot_df[miss_mask], x='Taxonomy', y='log2 FC', kind='box',
                hue=None, row=None, col='Quant', col_order=['I', 'iBAQ', 'LFQ'])

# %% [markdown]
# Partiendo del gráfico que acabas de generar, podemos seguir trabajando hasta obtener gráficos con un acabado bastante aceptable:

# %%
# Defining a maximum missing value threshold for incomming masking
miss_threshold = 0

# Masking values with less than a certain number of missing values in both 'Samples'
miss_mask = plot_df['Miss'] <= miss_threshold

# Plotting catplot
g = sns.catplot(data=plot_df[miss_mask], x='Taxonomy', y='log2 FC', kind='box',
                hue='Taxonomy', row=None, col='Quant', col_order=['I', 'iBAQ', 'LFQ'],
                fliersize=0, boxprops=dict(alpha=0.25), dodge=False)

# Overlying a stripplot ontop the catplot
g.map(sns.stripplot, "Taxonomy", "log2 FC",
      alpha=0.1, jitter=1/3, palette=sns.color_palette(), order=None)

# Adding horizontal lines with theoretical FC values
for ax_row in range(0, g.axes.shape[0]):
    for ax_col in range(0, g.axes.shape[1]):
        g.axes[ax_row, ax_col].hlines(0, xmin=-0.5, xmax=2.5, linestyle=':', color=sns.color_palette()[0])  # '#1f77b4'
        g.axes[ax_row, ax_col].hlines(-2, xmin=-0.5, xmax=2.5, linestyle=':', color=sns.color_palette()[1]) # '#ff7f0e'
        g.axes[ax_row, ax_col].hlines(1, xmin=-0.5, xmax=2.5, linestyle=':', color=sns.color_palette()[2])  # '#2ca02c'

# %% [markdown]
# ## Aplicando un t-test
#
# <img src="images/scipy/scipy_logo.png" width="150" style="float: left; margin-right: 10px;" />
#
# La librería [**Scipy**](https://scipy.org/scipylib/ "Web de SciPy") proporciona multitud de rutinas numéricas fáciles de usar y eficientes, así como herramientas para integración numérica, interpolación, optimización, álgebra lineal y estadística. Nosotros utilizaremos la función [`ttest_ind`]( https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.ttest_ind.html) del sub-paquete `stats` del paquete **Scipy**. Importémosla y leamos su *docstring*:

# %%
# Importing the ttest_ind function from the stats sub-package in the scipy package
from scipy.stats import ttest_ind

# Asking for help with `ttest_ind( )`:
# ttest_ind?

# %% [markdown]
# Veamos cómo se utiliza con un par de ejemplos sencillos:

# %%
# Creating arbitrary lists of numbers to the the t-test function
a_reps = [1.0, 1.1, 1.2, 1.3, 1.4]
b_reps = [1.5, 1.6, 1.7, 1.8, 1.9]

# Performing t-test
ttest_ind(a=a_reps, b=b_reps)

# %%
# Creating arbitrary lists of numbers to the the t-test function
a_reps = [1.1, 1.2, 1.3, 1.4, 1.5]
b_reps = [1.4, 1.5, 1.6, 1.7, 1.8]

# Performing t-test
ttest_ind(a=a_reps, b=b_reps)

# %% [markdown]
# Podemos guardar el resultado del test en una variable...

# %%
# Creating arbitrary lists of numbers to the the t-test function
a_reps = [1.2, 1.3, 1.4, 1.5, 1.6]
b_reps = [1.3, 1.4, 1.5, 1.6, 1.7]

# Performing t-test
test_result = ttest_ind(a=a_reps, b=b_reps)

# Variable type?
type(test_result)

# %% [markdown]
# ... y podemos acceder a $p$ valor y al estadístico $t$ fácilmente con los atributos `.pvalue` y `.statistic` (o con los índices `[1]` y `[0]`, respectivamente):

# %%
# Getting the p-value from the test result
print(test_result.pvalue)
print(test_result[1])

# Getting the t-statistic from the test result
print(test_result.statistic)
print(test_result[0])

# %% [markdown]
# Para utilizar la función `ttest_ind` con nuestros datos más fácilmente, antes debemos pivotar *Tidy-DataFrame* `tidy_df`:

# %%
# Pivoting tidy_df without aggregating
pivot3_df = tidy_df.pivot_table(index=['Proteins', 'Taxonomy', 'Quant'],
                                columns='Replicate',
                                values=['Imputed log2 Value'])
                                #aggfunc=)

# DataFrame head?
pivot3_df.head(6)

# %% [markdown]
# A modo de ejemplo, aplicaremos el test estadístico con el grupo de proteínas `'A0A024RBG1;Q9NZJ9'`, para la variable cuantitativa `'I'`.
#
# > 💡 **Más información:**
# >
# > Fíjate qué sencillo es acceder a una celda de una tabla pivotada con índices y columnas multinivel!!!

# %%
# Slicing some data from a pivot table
A = pivot3_df.loc[('A0A024RBG1;Q9NZJ9', 'Homo', 'I'), ('Imputed log2 Value', ['01', '02', '03', '04'])]
B = pivot3_df.loc[('A0A024RBG1;Q9NZJ9', 'Homo', 'I'), ('Imputed log2 Value', ['05', '06', '07', '08'])]

# Printing the data we just sliced
print(A, '\n\n', B, '\n')

# Performing t-test
ttest_ind(a=A, b=B)

# %% [markdown]
# Ahora que ya nos hemos familiarizado con el funcionamiento de la función `ttest_ind`, ya podemos aplicarla sobre el *DataFrame* `pivot3_df` que habíamos obtenido más arriba:

# %%
# DataFrame head?
pivot3_df.head(6)


# %% [markdown]
# Para facilitar este paso, nos definiremos una pequeña función `t_test` que (a su vez) nos ayude a aplicar la función `ttest_ind` a nuestro *DataFrame* `pivot3_df`:

# %%
# Defining a function that will be applied to a DataFrame in order to perform a t-test
def t_test(df, A_list, B_list):
    '''Performs a t-test given an input DataFrame (df), and given input lists
    with the column names of the two groups to be compared (A_list and B_list)'''
    # Performing t-test with input data
    test_result = ttest_ind(df[A_list], df[B_list])
    return test_result


# %% [markdown]
# Ahora simplemente utilizaremos el método `.apply()` para aplicar nuestra función `t_test` sobre `pivot3_df`:

# %%
# Initiating lists with the column names of the two groups to be compared in the t-test
A_list = ['01', '02', '03', '04']
B_list = ['05', '06', '07', '08']

# Performing a t-test
pivot3_df['t-test'] = pivot3_df['Imputed log2 Value'].apply(t_test, axis=1, args=[A_list, B_list])

# DataFrame Head?
pivot3_df.head(6)

# %% [markdown]
# En la columna en la que acabamos de guardar los resultados del *t-test* tenemos disponibles los valores del estadístico $t$ y el correspondiente $p$ valor. Nosotros estamos interesados en el $p$ valor y, de paso, también calcularemos $-\log_{10}p$.

# %%
# Getting the p-value from the t-test result
pivot3_df['p'] = pivot3_df['t-test'].str[1]

# Computing -log10(p)
pivot3_df['-log10 p'] = -np.log10(pivot3_df['p'])

# DataFrame Head?
pivot3_df.head(6)

# %% [markdown]
# Finalmente, podemos incorporar el $p$ valor a la tabla pivotada `pivot_df` dónde teníamos los $FC$ calculados:

# %%
# Adding 'p' and '-log10 p' column to pivot_df
pivot_df['p'] = pivot3_df['p']
pivot_df['-log10 p'] = pivot3_df['-log10 p']

# DataFrame head?
pivot_df.head(6)

# %% [markdown]
# Exportemos esta tabla pivotada `pivot_df` como un archivo **Excel**:

# %%
# Exporting the DataFrame
pivot_df.to_excel('data/quantitative/3319_FE_MBR_U_02_pivot_df.xlsx')

# %% [markdown]
# # VII.- Representando el *Volcano Plot*
#
# Finalmente tenemos todos los ingredientes para representar el *Volcano Plot*. En estos gráficos primero se representa $-\log_{10}p$ en función de $\log_{2}FC$ para cada proteína, y luego se establecen unas fronteras arbitrarias para distinguir entre proteínas diferenciales y no diferenciales. Normalmente, los límites de bondad para el $p$ valor y el $FC$ son:
#
# \begin{equation} 
# \left| FC \right| > FC_{lim} \equiv 1.5 \qquad p < p_{lim} \equiv 0.05 \\
# \end{equation}
#
# Que una vez convertidos a $-\log_{10}p$ y $\log_{2}FC$ quedan:
#
# \begin{equation} 
# \left| \log_{2} FC \right| \gtrsim 0.5850 \qquad -\log_{10} p \gtrsim 1.3010\\
# \end{equation}
#
# Una vez definidos los criterios de bondad, podemos incorporarlos como variables en nuestros *workflow*:
#

# %%
# Defining the goodness criteria limits
FC_lim, p_lim = 1.5, 0.05

# Computing the x and y limits in the Volcan Plot
x_lim, y_lim = np.log2(FC_lim), -np.log10(p_lim)

#Printing the x and y limits we just computed
print(x_lim, y_lim)

# %% [markdown]
# Ya podemos empezar a construir el *Volcano Plot*. Como paso previo a la siguiente práctica resetearemos el índice de `pivot_df` y prescindiremos de los multiniveles de sus columnas:

# %%
# Resetting index in pivot_df and redefining as plot_df
plot_df = pivot_df.reset_index()

# Flattening column levels in plot_df and renaming (by hand)
plot_df.columns = ['Proteins', 'Taxonomy', 'Quant', 'max Max missing', 'mean Imputed log2 Value A', 'max Imputed log2 Value B', 'FC', 'log2 FC', 'p', '-log10 p']

# Shortening column names (by hand)
plot_df.columns = ['Proteins', 'Taxonomy', 'Quant', 'Miss', 'Value A', 'Value B', 'FC', 'log2 FC', 'p', '-log10 p']

# DataFrame head?
plot_df.head(6)

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > Crea una máscara que indique qué grupos de proteínas de `plot_df` son diferenciales (`True`) y no diferenciales (`False`) y añádela al *DataFrame* `plot_df` como una columna llamada `'Differential'`.

# %% tags=["ejercicio"]
# Masking values satisfying the goodness criteria and adding such mask into the DataFrame
#plot_df['Differential'] = (plot_df['___'].abs() > ___) & (plot_df['___'] > ___)

# DataFrame head?
#plot_df.head(6)

# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
#SOLUTION
# Masking values satisfying the goodness criteria and adding such mask into the DataFrame
plot_df['Differential'] = (plot_df['log2 FC'].abs() > x_lim) & (plot_df['-log10 p'] > y_lim)

# DataFrame head?
plot_df.head(9)

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > Comprueba mediante EDA la relación entre de valores cuantitativos de las muestras A y B usando [`lmplot()`](https://seaborn.pydata.org/generated/seaborn.lmplot.html). Aprovecha el *snippet* de código proporcionado y sigue las instrucciones. Antes de empezar, ejecútalo para tener en mente el punto de partida.  
# > **a)** Especifica los parámetros `col='Quant'` y `col_order=['I', 'iBAQ', 'LFQ']`, y ejecuta el bloque código.  
# > **b)** Especifica el parámetro `hue='Taxonomy'` y `hue_order=['Homo', 'Escherichia', 'Saccharomyces']` y ejecuta el bloque código.  
# > **c)** Especifica el parámetro `row='Differential'` y ejecuta el bloque código.  
# > **d)** Reduce gradualmente la variable `miss_threshold` y ves ejecutando el bloque código por cada valor (4, 3, 2, 1, 0).  
# > 
# > Finalmente, compara el peso representado por los valores imputados según la variable quantitativa.

# %%
# Defining a maximum missing value threshold for incomming masking
miss_threshold = 4

# Masking values with less than a certain number of missing values in both 'Samples'
miss_mask = plot_df['Miss'] <= miss_threshold

# Plotting lmplot
g = sns.lmplot(data=plot_df[miss_mask], x='log2 FC', y='-log10 p', 
               hue=None, hue_order=None,
               row=None, col=None, col_order=None,
               sharex=True, sharey=True,
               scatter=True, fit_reg=False, truncate=False,
               scatter_kws={'alpha': 0.10})

# Setting custom x/y-limits matching the uppermost values
y_max = plot_df.loc[miss_mask, '-log10 p'].max()
x_max = plot_df.loc[miss_mask, 'log2 FC'].max()

# Adding horizontal and vertical lines delimiting the boundaries of our goodness criteria
for ax_row in range(0, g.axes.shape[0]):
    for ax_col in range(0, g.axes.shape[1]):
        g.axes[ax_row, ax_col].vlines(x_lim, ymin=y_lim, ymax=y_max, linestyle=':', color='grey')
        g.axes[ax_row, ax_col].vlines(-x_lim, ymin=y_lim, ymax=y_max, linestyle=':', color='grey')
        g.axes[ax_row, ax_col].hlines(y_lim, xmin=-x_max, xmax=-x_lim, linestyle=':', color='grey')
        g.axes[ax_row, ax_col].hlines(y_lim, xmin=x_lim, xmax=x_max, linestyle=':', color='grey')

# Saving this nice plot
g.savefig('data/quantitative/Volcano.pdf')

# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# SOLUTION
# Defining a maximum missing value threshold for incomming masking
miss_threshold = 0

# Masking values with less than a certain number of missing values in both 'Samples'
miss_mask = plot_df['Miss'] <= miss_threshold

# Plotting lmplot
g = sns.lmplot(data=plot_df[miss_mask], x='log2 FC', y='-log10 p', 
           hue='Taxonomy', hue_order=['Homo', 'Escherichia', 'Saccharomyces'],
           row='Differential', col='Quant', col_order=['I', 'iBAQ', 'LFQ'],
           sharex=True, sharey=True,
           scatter=True, fit_reg=False, truncate=False,
           scatter_kws={'alpha': 0.10})

# Setting custom x/y-limits matching the uppermost values
y_max = plot_df.loc[miss_mask, '-log10 p'].max()
x_max = plot_df.loc[miss_mask, 'log2 FC'].max()

# Adding horizontal and vertical lines delimiting the boundaries of our goodness criteria
for ax_row in range(0, g.axes.shape[0]):
    for ax_col in range(0, g.axes.shape[1]):
        g.axes[ax_row, ax_col].vlines(x_lim, ymin=y_lim, ymax=y_max, linestyle=':', color='grey')
        g.axes[ax_row, ax_col].vlines(-x_lim, ymin=y_lim, ymax=y_max, linestyle=':', color='grey')
        g.axes[ax_row, ax_col].hlines(y_lim, xmin=-x_max, xmax=-x_lim, linestyle=':', color='grey')
        g.axes[ax_row, ax_col].hlines(y_lim, xmin=x_lim, xmax=x_max, linestyle=':', color='grey')

# Saving this nice plot
g.savefig('data/quantitative/Volcano.pdf')

# %% [markdown]
# # VIII.- Conclusiones finales y *take home message*
#
# Hemos intentado que este segundo *workflow* se acercara más a un caso real de análisis cuantitativo de proteómica, lo que ha supuesto un incremento sustancial en la complejidad de las herramientas y las técnicas de **Python** que hemos tenido que desplegar.
#
# Partiendo de un único archivo de Grupos de Proteínas de MQ, hemos analizado tres variables cuantitativas distintas (__I__ , **iBAQ** , y **LFQ**). Además, hemos anotado el número de valores perdidos (**0**, **1**, **2**, **3**, **4**) para cada uno de los grupos de proteínas presentes en cada una de las tres variables cuantitativas. Al final del día, en un único *workflow*, hemos analizado paralelamente $3 \times 5 = 15$ casos diferentes.
#
# Este enfoque múltiple flexibiliza mucho la toma de decisiones, esto nos permite elegir la mejor estrategia dependiendo de nuestro caso particular.
