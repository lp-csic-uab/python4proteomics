---

**WARNING!**: This is the *Old* source-code repository for Python4Proteomics Course from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/p4p/) located at https://sourceforge.net/p/lp-csic-uab/p4p/**  

---  
  
  
![logo](images/logo_p4p.png)

---

  
**WARNING!**: This is the *Old* source-code repository for Python4Proteomics Course from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/p4p/) located at https://sourceforge.net/p/lp-csic-uab/p4p/**  

---  
  
  
Python4Proteomics (P4P)
========================

**Introducción al manejo de datos proteómicos mediante Python.**

Python es un lenguaje de programación muy cercano al lenguaje natural, muy sencillo de aprender y con un gran número de usuarios en diversos campos de aplicación, especialmente en el ámbito científico.

Python nos permite el tratamiento de datos proteómicos de manera reproducible y trazable. Su uso permite automatizar procedimientos rutinarios mediante _scripts_ reutilizables y flexibles, evitando errores y otros problemas inherentes al tratamiento manual de datos con hojas de cálculo. Python dispone además de potentes herramientas para el manejo, transformación y visualización de datos.

El gran número de usuarios de este lenguaje en diversos campos de aplicación y especialmente en el ámbito científico, facilita la resolución de los problemas que nos podamos encontrar en nuestro proceso de datos.

### Contenido:

En este curso aprenderemos a manejar de forma sencilla y automatizada datos proteómicos en formato excel, csv o similares procedentes de motores de búsqueda como Proteome Discoverer y MaxQuant.
Aprenderemos a leer y operar archivos fasta y mgf y a completar nuestros datos con información extraída de UniProt u otros servicios _on-line_ como BLAST.
El contenido se organiza con criterios eminentemente prácticos, centrándose en problemas reales del trabajo diario y dando una especial importancia a los métodos de visualización de datos a través de librerías como `matplotlib`.

### Organización y docencia:

Joaquín Abián [^1], Gianluca Arauz [^2] y Óscar Gallardo [^1]. 
[^1]: [IIBB-CSIC](https://www.iibb.csic.es/), Barcelona.
[^2]: [IRB](https://www.irbbarcelona.org), Barcelona.

### A quién va dirigido:

Se trata de un curso introductorio y eminentemente práctico, dirigido a cualquier personal implicado en el manejo de datos proteómicos.
No es necesaria experiencia previa en programación.

### Requisitos técnicos:

Todos los alumnos deberán contar con su propio ordenador portátil. Las instrucciones para la instalación del software necesario se harán llegar a los asistentes unas semanas antes del inicio del curso.

### Inscripción:

Interesados enviar correo electrónico a [Joaquim.abian.csic@uab.cat](mailto:Joaquim.abian.csic@uab.cat).  
  
  
---

**WARNING!**: This is the *Old* source-code repository for Python4Proteomics Course from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/p4p/) located at https://sourceforge.net/p/lp-csic-uab/p4p/**  

---  
  
  

