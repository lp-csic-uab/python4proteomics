# ---
# jupyter:
#   jupytext:
#     comment_magics: true
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.2'
#       jupytext_version: 0.8.6rc1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Intoduction to Python
#
# ---

# %% [markdown]
# ## I.- Variables

# %% [markdown]
# As we have seen, one easy thing to do in Python are mathematical operations, as in a basic calculator:

# %%
5 * 2 + 3

# %% [markdown]
# We see the result of the operation, so, as in a basic calculator without memory, if we want to use this result in another operation we have to type it again:

# %%
13 + 2

# %% [markdown]
# Those numbers (13, 15) are easy to remember and write down again.
#
# But, what hapens if the result is different?:

# %%
15 / 7

# %% [markdown]
# It's not so easy to remember and write it down again every time we need it.
#
# So we use what is called a **variable**, this is, a *"name"* to *reference* the value we want to recall later.
#
# ![variable_value](images/python_intro/variables_1.svg)
#
# To assign a variable to a value, we use the equal sing ( **=** ):
# ```python
#     variable_name = value
# ```

# %%
x = 15 / 7

# %% [markdown]
# Note that now we don't see any output, because after the operation (13 / 7) its result as been assigned/linked to the **x** variable.
#
# To see the **value** a variable _points to_, we only have to write it as the last stament of a notebook cell and execute the cell; or we can use the `print` function at any point of the notebookk cell:

# %%
x

# %%
print(x)

# %% [markdown]
# And, of course, variables are used as _placeholders_ of the real values to do more operations:

# %%
y = 2 * x + 2
z = y + x - 10

# %% [markdown]
# > **Exercise:**  
# > Now, try by yourself to see the _values_ referenced by variables *y and z*, but using **only a notebook cell**:

# %%



# %% [markdown]
# Different variables can reference the same value:
# ```python
#     first_variable = second_variable
# ```
# With this syntax *first_variable* will also references the same value referenced by *second_variable*:

# %%
x = 42 # x points to the value 42
y = x # y points to the value pointed by x, this is, also 42

print(x)
print(y)

# %% [markdown]
# The situation with *x and y* is now as follows:
#
# ![variables_same_value](images/python_intro/variables_2.svg)
#
# So, if we assing *x* to another value, we can still access the first value ussing _y_:

# %%
x = -1 # Now x points to -1

print(x)
print(y)

# %% [markdown]
# ![variables_same_value](images/python_intro/variables_3.svg)

# %% [markdown]
# ### Variable names
#  - Can be any length.
#  - Can have uppercase and lowercase letters (**A-Z**, **a-z**), digits (**0-9**), and the underscore character ( **_** ).
#  - But, the _first_ character of a variable name can not be a digit (so *1st* or *4teen* are not valid variable names).
#  - Python is case sensitive, so *x* and _X_ are completly different variable names.
#  - Can not math any of Python's _reserved keywords_.

# %%
help('keywords')

# %%
# Allowed variable names:

X = 0 # Uppercase `X` is a different name from previous lowercase `x`
pi = 3.141592653589793
pI = 7 # Mixedcase `pI` is a different name from `pi` (lowercase)
text_1 = "Hello" # Of course, variables can also reference text
# By convention, variable names with all uppercase letters are considered
# constants, an it's referenced value should not be changed:
CONSTANT = "I'm a CONSTANT: don't change me!"
# Also by convention, variable names starting with a underscore are considered
# private and only should be used by the developer who named them:
_private = "I'm private: avoid ussing me"

print(x)
print(X)
print(pI)
print(pi)
print(text_1)
print(CONSTANT)
print(_private)

# %%
# Not allowed variable names:

2digits = 12 # Varible names cannot start with a digit

# %%
text$ = "Hi!" # Varible names can only contain letters, digits and the underscore ( _ )

# %%
another-text = "Meeeec!" # Varible names can only contain letters, digits and the underscore ( _ )

# %%
and = -1 # Varible names cannot match reserved Python keywords

# %% [markdown]
# ### Inspecting and Deleting variable names

# %%
# %whos

# %%
del x
del CONSTANT

# %%
# %whos

# %% [markdown]
# ## II.- Types/Classes of Objects
# _"In Python everything is an **object**()"_

# %% [markdown]
# ### Booleans ( bool )
#
#  * ***True***
#  * ***False***

# %%
type(True)

# %%
False == False

# %%
True > False

# %%
True and False

# %%
True and True

# %%
False and False

# %%
True == 1

# %% [markdown]
# ### Numbers
#
#  * **Integer ( int )** -> 1, -2, 0
#  * **Float ( float )** -> 0.1, -10.3, 9.67E6, 0.0
#  * **Complex ( complex )** -> 2+3j

# %%
a = -2
b = 3
c = 1.556
d = 1.420E7
e = 1-1j

print( type(a) )
print(a)
print( type(d) )
print(d)
print( type(e) )
print(e)

# %% [markdown]
# #### Operations with numbers

# %%
print('add:      ',  a + b )
print('multiply: ',  a * b )
print('division: ',  a / b )
print('exponent: ',  a ** b)
print('module:   ',  a % b )
print('absolute: ',  abs(a))

# %%
print('conversion: ', float(a))
print('conversion: ', int(c))
print('round:      ', round(c, 2))

# %% [markdown]
# ### Sequences
#
#  * **Strings of characters ( str )**
#  * **Ranges of numbers ( range )**
#  * **Lists of objects ( list )**
#  * **Tuples of objects ( tuple )**

# %% [markdown]
# #### Strings ( str, '', "" )

# %%
# string
a = 'This is a text string'
b = 'Another string, with 1 number'
print(a)

# %%
print('capitalize', a.capitalize())
print('up case   ', a.upper())

# %%
# can be concatenated
a + ". " + b

# %%
# cannot be substracted
a - b

# %% [markdown]
# many other properties

# %% [markdown]
# #### Ranges ( range )

# %%
# Ranges
a = range(10)

print( type(a) )
print( a )
print( list(a) )

# %% [markdown]
# #### Tuples ( tuple, ( ,) )

# %%
a = (1, 3, 5 ,7)
b = ('casa', 'home')

# %% [markdown]
# concatenate, indexing, ... like Lists
#
# * tuples are inmutable
# * No append
# * No sort

# %% [markdown]
# #### Lists ( list, [ ] )

# %%
# Lists
a = [1, 3, 'casa', 7.0, 9]
b = ['single']
c = [1, 3, 7, 2, 5, 4, 6]

# %%
print( 'index      ', a[2] )
print( 'slice      ', a[1:3] )
print( 'concatenate', a + b )
print( 'sort       ', sorted(c))

# %%
3 in a

# %%
a.index('casa')

# %% [markdown]
# ### Sets ( set, { } )

# %%
a = {1, 2, 4, 6, 4, 3, 3, 3}

# %%
# no duplicate objects, do no keep order
a

# %%
# Useful to remove duplicates
alist = [1, 2, 4, 6, 4, 3, 3, 3]
aset = set(alist)

print( alist )
print( aset )

# %% [markdown]
# ### Dictionaries ( dict, {key: value} )

# %%
fruits = {'apples': 2, 'oranges': 23}
fruits_1 = dict( apples=2, oranges=23 )
fruits_2 = dict( [ ('apples', 2), ('oranges', 23) ] )

# %%
fruits == fruits_1, fruits == fruits_2

# %%
fruits['apples']

# %%



# %% [markdown]
# ## III.- Flow control

# %% [markdown]
# ### Conditionals ( if ... elif ... else ... )
#
# ```python
#     if first_condition:
#         do_first_action
#     elif second_condition:
#         do_second_action
#     else:
#         do_action_when_no_condition_is_satisfied
# ```

# %% [markdown]
# ### Loops

# %% [markdown]
# #### For loop ( for ... )
#
# ```python
#     for item in iterable:
#         do_something
# ```

# %%
bucket_of_fruits = ['orange', 'apple', 'pear']

for fruit in bucket_of_fruits:
    print(fruit)

# %% [markdown]
# #### While loop ( while ... )
#
# ```python
#     while condition:
#         do_something
# ```

# %%
countdown = 3

while countdown > 0:
    print(countdown)
    countdown = countdown - 1

print('Zero!')

# %%
countup = 0

while True: # This is an infinite loop. To interrupt it press two time the *i* letter of your keyboard...
    countup = countup + 1

# %%
countup

# %% [markdown]
# ### Exceptions ( try ... except ... else ... )
#
# ```python
#     try:
#         code_that_can_fail
#     except Exception as variable:
#         do_only_if_error
#     else:
#         do_only_if_no_errors
# ```

# %%
try:
    x = 1 / 0
except Exception as error:
    print(error)
else:
    print(x)

# %% [markdown]
# ## IV.- Functions

# %% [markdown]
# ```python
#     def function_name(argument1, argument2, ...):
#         do_something
#         return some_value
# ```

# %%
def add(x, y):
    z = x + y
    return z

# %%
add(3, 2)

# %%
uniprot_header = '>sp|P00761|TRYP_PIG Trypsin OS=Sus scrofa PE=1 SV=1'

# %%
def get_accession(header):
    atoms = header.split('|')
    return atoms[1]

# %%
get_accession(uniprot_header)

# %% [markdown]
# ### Use variable number of arguments within a user defined function

# %% [markdown]
# Now; going back to our `add` function; if we want to sum 3 numbers:

# %%
add(3, 2, 5)

# %%
def add_multiple(*values):
    total = 0
    for value in values:
        total = total + value
    return total    

# %%
add_multiple(2, 3, 4, 5)

# %%



