Python4Proteomics. Course Contents
======================================

Version 0.3

## Course Introduction

- Python interpreter and versions
- Installation of the development environment

## Basic Python

- REPL/shell, scripts, and *notebooks*
- Variables and basic types/classes (*built-in*)
- Program flow control, iteration and exceptions (*if*...*elif*...*else*..., *while*..., *for*..., *try*...*except*...*else*, *break*, *continue*)
- Basic Python functions (*built-in*) and user defined functions (*def*)
- Modules and packages (*import*)
- Classes and objects

## BioPython

- FASTA files treatment
- Blastp searches
- UniProt protein information retrieval

## Pandas and numpy

- Numeric arrays (*ndarray*) and vector calculus
- *Series* and *DataFrame*
- Load/import and save/export data-sets (CSV, Excel)
- Basic operations with *dataframes*: *slicing*, boolean filters, add and delete rows and columns, *concat*, *merge*, iteration, *map*, *apply*, *groupby*

## Charts (matplotlib, plotnine)

- Basic plotting
- Plotting spectra
- Interactive plots

## Proteomics with pyteomics

- MGF spectra files manipulation
- mzML spectra files manipulation

## Statistics (scipy, scikit-learn)