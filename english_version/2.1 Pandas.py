# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     comment_magics: true
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.2'
#       jupytext_version: 0.8.6rc1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Introduction to Pandas
#
# ---

# %% [markdown]
# TODO XXX: QUEDA PENDIENTE DAR FORMATO DE HYPERLINKS AL ÍNDICE... LO HAREMOS UNA VEZ ACABADA LA SECCIÓN.
#
# Outline:
#
# I. Importing our data as DataFrame
#
# II.- Basic DataFrame exploration
#
# III.- DataFrame acces
#
# III.- DataFrame acces (optional)
#
# IV.- EXTRA (Dealing with nan values)
#
# V.- Transforming DataFrames
#
#
#
# Creating DF
# Basic DF exploration
# DF access
# Filtering
# Multi-indexing
# Pivoting, Melting, Stacking, Unstacking
# Grouping-by-and-Aggregating
#
# Pandas 2
#
# Importing data
# Concatenating, Appending, Joining, Merging

# %%
# Importing the Pandas package and assign the 'pd' alias to it
import pandas as pd

# %% [markdown]
# ## I.- Importing our data as DataFrame

# %% [markdown]
# Here we will introduce the basic fuctions to import data in order to get our DataFrames. We should use different functions depending on the input data format:
#
# We use `pd.read_excel()` to import \*.xlsx and \*.lsx.<br>
# We use `pd.to_csv()` to import \*.csv.<br>
# We use `pd.read_table()` to import \*.txt.

# %%
# Reading an Excel SpreadSheet (*.xlsx or *.lsx) and storing it in as a DataFrame: df
df = pd.read_excel('data/Excel.xlsx')
#df = pd.to_csv('data\CommaSeparatedValues.csv')
#df = pd.read_table('data\TextFile.txt')

# %%
# DataFrame data type
type(df)

# %% [markdown]
# ## II.- Basic DataFrame exploration

# %% [markdown]
# Here we will introduce some methods to get used to this new powerfull datatype called DataFrame: `.shape`, `.columns` and `.index`.

# %%
# DataFrame shape. Remember: (Rows, Columns)
df.shape

# %%
# DataFrame columns
df.columns

# %%
# DataFrame rows
df.index

# %% [markdown]
# Here we will introduce some useful DataFrame methods: `.head()`, `.tail()`, `.info()` and `.describe()`.

# %%
# DataFrame head?
df.head(4)

# %%
# DataFrame tail?
df.tail(2)

# %%
# DataFrame general information
df.info()

# %%
# DataFrame (basic) statistical description
df.describe()

# %% [markdown]
# ## III.- DataFrame acces

# %% [markdown]
# ### Accessing columns

# %% [markdown]
# We access DataFrame columns using brackets `df[]`. We must pass a list containing the columns we want `['Raw', 'Intensity']` inside the outermost brackets, like this `df[['Raw', 'Intensity']]`. Notice that there are two pairs of brackets, those comming from our column name list (inner), and those comming from DataFrame column access (outter).

# %%
# Accessing DataFrame columns
df[['Raw', 'Intensity']]

# %% [markdown]
# ### Accessing rows

# %% [markdown]
# We access DataFrame rows using the `.loc` DataFrame method followed by brackets `df.loc[]`. We must pass a list containing the row indexes we want `[4, 1]` inside the outermost brackets, like this `df.loc[[4, 1]]`. Again, there are two pairs of brackets, those comming from our row indexes list (inner), and those comming from DataFrame row access (outter).

# %%
# Accessing DataFrame rows
df.loc[[4, 1]]

# %% [markdown]
# ### Accessing columns & rows simultaneously

# %% [markdown]
# To access multiple DataFrame columns and rows simultaneously, we use the `.loc` DataFrame method followed by a  bracket containing a comma `,` `df.loc[, ]`. This time, we must pass two lists inside the outermost brackets: one for the rows (left side of the comma), and one for the columns (rigth side of the comma), like this `df.loc[[4, 1], ['Raw', 'Intensity']]`.

# %%
# Accessing multiple DataFrame rows and columns simultaneously
df.loc[[4, 1],  ['Raw', 'Intensity']]

# %%
# Accessing a single DataFrame row and column simultaneously
df.loc[[4],  ['Intensity']]

# %% [markdown]
# ### Slicing

# %% [markdown]
# TODO XXX: HASTA QUÉ PUNTO SE HA EXPLICADO YA ELS SLICING?
#
# We also can use slicing notation `:` to acces DataFrame columns and/or rows. 

# %%
# Accessing SOME DataFrame rows and ALL columns
df.loc[[2, 3, 4, 5], ['Raw', 'Software', 'Node', 'Sequence', 'Intensity', 'Amplitude']]

# %%
# Accessing SOME DataFrame rows and ALL columns (using slicing)
df.loc[2:5, :]

# %%
# Accessing ALL DataFrame rows and SOME columns
df.loc[[0, 1, 2, 3, 4, 5, 6 ,7, 8, 9, 10, 11], ['Node', 'Sequence', 'Intensity']]

# %%
# Accessing ALL DataFrame rows and SOME columns (using slicing)
df.loc[:, 'Node': 'Intensity']

# %%
# Accessing ALL DataFrame rows and ALL columns (using slicing)
df.loc[:, :]

# %% [markdown]
# ## III.- DataFrame acces (Optional)

# %% [markdown]
# PENDING

# %% [markdown]
# ## IV.- Boolean Indexing and Filtering

# %% [markdown]
# By using comparison operators we can *make questions* to our DataFrame. Some common comparison operators are:
#  
# - Is `A` **equal** than `B`? $\rightarrow$ `A == B` 
# - Is `A` **different** than `B`? $\rightarrow$ `A != B`
# - Is `A` **smaller** than `B`? $\rightarrow$ `A < B`
# - Is `A` **greater** than `B`? $\rightarrow$ `A > B`
# - Is `A` **smaller or equal** than `B`? $\rightarrow$ `A <= B`
# - Is `A` **greater or equal** than `B`? $\rightarrow$ `A >= B`

# %%
# Is the current index 'Intensity' greater than 100?
df['Intensity'] > 100

# %% [markdown]
# We can also ask multiple questions simultaneously by using boolean operators:
#
# - And $\rightarrow$ `&`
# - Or $\rightarrow$ `|`
#
# To do this you must enclose each question between parenthesis!!!

# %%
# Is the current index 'Intensity' greater than 100 AND 'Amplitude' smaller than 1.6?
(df['Intensity'] > 100) & (df['Amplitude'] < 1.6)

# %%
# Is the current index 'Software' not equal to 'PD' OR 'Node' equal to 'Amanda'?
(df['Software'] != 'PD') | (df['Node'] == 'Amanda')

# %% [markdown]
# The output data type for these questions is a **pandas series**. In particular, since such Series only contain boolean values (`True`, `False`), we call them **boolean series**.

# %%
# Create filter to get Proteome Discoverer software AND no to get Amanda search node.
my_1st_filter = (df['Software'] == 'PD') & (df['Node'] != 'Amanda')
type(my_1st_filter)

# %% [markdown]
# We can use such boolean series to perform boolean indexing of DataFrames.

# %%
# Applying my (first) filter to my DataFrame
df[my_1st_filter]

# %%
# Retrieving the 75% quantile of the 'Intensity': I_quantile
I_quantile = df['Intensity'].quantile(0.60)

# Retrieving the 25% quantile of the 'Amplitude': A_quantile
A_quantile = df['Amplitude'].quantile(0.40)

# Create filter to get high peak intensity (first quartile) OR low peak amplitude (last quartile)
my_2nd_filter = (df['Intensity'] > I_quantile) | (df['Amplitude'] < A_quantile)

# Create filter to get high peak intensity (first quartile) AND low peak amplitude (last quartile)
#my_2nd_filter = (df['Intensity'] > I_quantile) & (df['Amplitude'] < A_quantile)

# Applying my (second) filter to my DataFrame
df[my_2nd_filter]


# %%



# %% [markdown]
# ## IV.- EXTRA (Dealing with nan values)

# %% [markdown]
# `all()` select columns with all non-zero values
#
# `any()` select columns with any non-zero values
#
# `isnull()` select columns with nan values
#
# `notnull()` select columns with nan values

# %%
df.isnull().any()

# %% [markdown]
# ## V.- Transforming DataFrames

# %% [markdown]
# Usually, we will need to process the data stored in a DataFrame. For example we may need to create a new column from another column, or to edit a group of columns at the same time, or anything else you can imagine...
#
#
#
# `apply()` `map()`
#
# Apply only works with function with only one possible input...

# %% [markdown]
# Let us try to compute the $Z$-score of the Intensity column of our DataFrame. Remember that the $i$<sup>th</sup> observation of an $x$ magnitude, $x_i$, has a Z-score, $Z_i$, given by the following equation:
#
# \begin{equation}
# Z_i = \frac{x_i - \mu(x)}{\sigma(x)}
# \end{equation}
#
#  Here, $\mu(x)$ and $\sigma(x)$ denote the mean and the standard deviation of $x$, respectively.

# %%
# Computing the Z-score of the 'Intensity' column and storing it in a new 'Z-Intensity' column
df['Z-Intensity'] = (df['Intensity'] - df['Intensity'].mean()) / df['Intensity'].std()
df.head(2)

# %% [markdown]
# PRACTICE
#
# Compute the 0-to-1 normalization of the Amplitude column of our dataframe. Remember that the $i$<sup>th</sup> observation of an $x$ magnitude, $x_i$, has a 0-to-1 normalization value, $N_i$, given by the following equation:
#
# \begin{equation}
# N_i = \frac{x_i - m(x)}{M(x) - m(x)}
# \end{equation}
#
#  Here, $m(x)$ and $M(x)$ just denote the minimum and the maximum values of $x$, respectively.
#  
#  Hint: Pandas DataFrames in Python have very useful `.min()` and `.max()` methods.

# %%
# Computing the N-normalization of the 'Amplitude' column and storing it in a new 'N-Amplitude' column
df['N-Amplitude'] = (df['Amplitude'] - df['Amplitude'].min()) / (df['Amplitude'].max() - df['Amplitude'].min())
#df['N-Amplitude'] = (INSERT CODE HERE) / (INSERT CODE HERE)
df.head(2)

# %% [markdown]
# We can also perform very versatile string operations with Pandas DataFrames in python (this is particularly useful when dealing with protein sequences). For example, if we look at the 'Raw' column of our DataFrame we notice that several substrings comprise the raw file name such dates, identifiers, names, experimental conditions (test / control), replicates... Sometimes it is very useful to incorporate this information into our DataFrame.

# %%
# Splitting the 'Raw' column by the underscore '_'
df['Splitted Raw'] = df['Raw'].str.split('_')
df.head(2)

# %%
# Taking the 4th element of the list in 'Splitted Raw' as 'Condition'
df['Condition'] = df['Splitted Raw'].str[3]
df.head(2)

# %% [markdown]
# PRACTICE

# %% [markdown]
# Create a new column called `Replicate` in our DataFrame `df`, with the replicate `R1`, `R2`, `R3` or `R4`  as it appears in the raw file name from `df['Raw']`. 

# %%
# Taking the 5th element of the list in 'Splitted Raw' as 'Replicate'
df['Replicate'] = df['Splitted Raw'].str[4]
#df['Replicate'] = INSERT CODE HERE
df.head(2)

# %%
df.drop(['Splitted Raw'], axis=1, inplace=True)
df.head()

# %% [markdown]
# At this point, we processed the 'Intensity' and 'Amplitude' data columns and we also parsed the column 'Raw' of our DataFrame. We can finally drop this redundant columns whit the `.drop()` method
#

# %%
df.drop(index=None, columns=['Raw', 'Intensity', 'Amplitude'], inplace=True)
df.head(2)

# %% [markdown]
# Notice that we passed a list with the columns we want to drop to the `columns=` parameter of the `.drop()` method. Since we don't want to drop any rows, we also passed `None` to its `index=` parameter. The `inplace=` parameter set `True` just allow us to drop the columns "on the fligth", with no need to reassign the resulting DataFrame to a new DataFrame.
#

# %% [markdown]
# ## VI.- Reshaping DataFrames

# %% [markdown]
# In this section we will learn how to pivot, stack, melt and group-by our DataFrames.

# %% [markdown]
# <img src="images/pandas/reshaping_pivot.png" width="500">

# %%
df

# %%
filter = (df['Replicate'] == 'R1') | (df['Replicate'] == 'R3')
dff = df[filter]
dff.pivot(index='Node', columns='Condition')

# %%
df.pivot_table(index='Node', columns='Condition')

# %%



