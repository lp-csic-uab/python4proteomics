Python for Proteomics. Course Syllabus
======================================

Version 0.1

## Course Introduction

- Objectives
- Python interpreter and versions
- Installation of the development environment

## Introduction to Python

- REPL/shell, scripts, and *notebooks*
- Variables and basic types/classes (*built-in*)
- Program flow control, iteration and exceptions (*if*...*elif*...*else*..., *while*..., *for*..., *try*...*except*...*else*, *break*, *continue*)
- Basic Python functions (*built-in*) and user defined functions (*def*)
- Modules and packages (*import*)
- Classes and objects
