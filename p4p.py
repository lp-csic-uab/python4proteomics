# -*- coding: utf-8 -*-

"""
Información sobre el módulo:

:synopsis: Módulo para el curso Python4Proteomics. Este módulo está pensado para ser importado.

:created:    2019/01/25

:authors:    Òscar Gallardo Román (ogallardo@protonmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
             Gianluca Arauz-Garofalo (gianluca.arauz@irbbarcelona.org ) at MSPCF/IRBBarcelona (https://www.irbbarcelona.org/es/research/mass-spectrometry-proteomics)
:copyright:  2019 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '0.12'
__UPDATED__ = '2019-11-26'

#===============================================================================
# Imports comunes
#===============================================================================
import time # Module time.
from urllib import request, parse # Modules `request` (to make web requests) and `parse` (to url-encode data to send in web requests).
from urllib.error import HTTPError, URLError # Exceptions for tracking connection errors.


#===============================================================================
# Variables del módulo
#===============================================================================
# aa2mass = {'Amino Acid Letter: (Average mass, Monoisotopic mass), ...}
aa2mass = {'G': (57.0513, 57.021464),  'A': (71.0779, 71.037114),
           'S': (87.0773, 87.032029),  'P': (97.1152, 97.052764),
           'V': (99.1311, 99.068414),  'T': (101.1039, 101.04768),
           'C': (103.1429, 103.00919), 'J': (113.1576, 113.08406),
           'L': (113.1576, 113.08406), 'I': (113.1576, 113.08406),
           'N': (114.1026, 114.04293), 'W': (186.2099, 186.07931),
           'D': (115.0874, 115.02694), 'Q': (128.1292, 128.05858),
           'K': (128.1723, 128.09496), 'E': (129.1140, 129.04259),
           'M': (131.1961, 131.04048), 'H': (137.1393, 137.05891),
           'F': (147.1739, 147.06841), 'Y': (163.1733, 163.06333),
           'R': (156.1857, 156.10111),
           }


#===============================================================================
# Definiciones de Clases
#===============================================================================
class UniprotProtein(object):
    """Clase simple para representar proteínas de UniProt"""
    
    ## Atributos :
    ac = '' # UniProt ACcession number.
    sequence = '' # Secuencia aminoacídica (código de AAs de 1 letra).
    length = 0 # Longitud de la secuencia proteica.
    description = '' # Descripción de la proteína.
    
    ## Métodos :
    def __init__(self, ac, sequence, description=''):
        """Método para la inicialización de los atributos del nuevo objeto
        UniprotProtein creado cuando se llama a la clase (el primer argumento `self` 
        referencía éste nuevo objeto UniprotProtein recién creado)"""
        self.ac = ac
        self.sequence = sequence.strip().upper()
        self.length = len(self.sequence)
        self.description = description


#===============================================================================
# Definiciones de Funciones
#===============================================================================
def get_mass4mz_charge(mz, charge):
    """Calcula la masa a partir de la masa/carga (`mz`) y la carga (`charge`), utilizando una fórmula
    que tiene en cuenta la masa del protón y del electrón"""
    return (mz * charge) - ( charge * (1.00782503224 - 0.00054857990907) )


def get_mz(mass, charge):
    """Calcula la masa/carga (m/z) a partir de la masa (`mass`) y la carga (`charge`), utilizando una fórmula
    que tiene en cuenta la masa del protón y del electrón"""
    return ( mass + charge * (1.00782503224 - 0.00054857990907) ) / charge


def protein_coverage(protein_seq, peptides):
    """
    Calcula el 'coverage' (%) de una secuencia proteica por una serie de péptidos.
    
    :param str protein_seq: secuencia de aminoácidos de una proteína.
    :param iterable peptides: una colección de secuencias peptídicas.
    
    :return float : el 'coverage' de la proteína, como porcentaje (%).
    """
    covered_aa_pos = set() # Posiciones aminoacídicas de la proteína cubiertas por los péptidos.
    for peptide in peptides: # Iteración entre los péptidos de la colección:
        pep_len = len(peptide) # Longitud del péptido actual.
        pos_pepinprot = protein_seq.find(peptide) + 1 # Buscar la 1era coincidencia del péptido en la proteína.
        while pos_pepinprot != 0: # Mientras se encuentren coincidencias:
            covered_aa_pos.update( range(pos_pepinprot, 
                                         pos_pepinprot + pep_len) )
            pos_pepinprot = protein_seq.find(peptide, pos_pepinprot) + 1 # Buscar la siguiente coincidencia del péptido
    # Cálculo del 'coverage' (%):
    return len(covered_aa_pos) / len(protein_seq) * 100


def MQ_proteinGroups_filter(proteinGroups, cont=True, rev=True, only=True):
    """
    Filters-out those Protein Groups tagged as "Potential contaminant" (CON),
    "Reverse" (REV) or "Only identified by site" (ONL) by MaxQuant.

    INPUTS: proteinGroups --> Protein Groups-like DataFrame. It is mandatory
    that proteinGroups contains the following columns 'Potential contaminant',
    'Reverse' and 'Only identified by site'.

    OUTPUTS: out_df --> Filtered Protein Groups-like DataFrame.

    PARAMETERS:
        cont --> True/False (If False, don't filters-out CON)
        rev --> True/False (If False, don't filters-out REV)
        only --> True/False (If False, don't filters-out ONL)
    """
    # Copying input proteinGroups DataFrame to avoid warnings
    df = proteinGroups.copy()
    # Create mask full of trues with a little trick
    mask = proteinGroups.index == proteinGroups.index
    # Masking contaminant, reverse and only based on input parameters
    if cont:
        cont_mask = df['Potential contaminant'] == '+'
        mask = mask & ~cont_mask
    if rev:
        rev_mask = df['Reverse'] == '+'
        mask = mask & ~rev_mask
    if only:
        only_mask = df['Only identified by site'] == '+'
        mask = mask & ~only_mask
    # Filtering proteinGroups and returning such just-filtered proteinGroups
    df_out = df[mask].copy()
    return df_out


def list_cleaner(string, pattern='', delimiter=''):
    """
    In a list-like string, list_cleaner function drops all list-like elements
    containing a specified string pattern.
    
    EXAMPLES:
    list_cleaner('item01-item02-thing03-thing04', pattern='01', delimiter='-')
    'item02-thing03-thing04'
    list_cleaner('item01 item02 thing03 thing04', pattern='ite', delimiter=' ')
    'thing03 thing04'

    INPUTS: string --> List-like string with a well defined delimiter.

    OUTPUTS: out_string --> Cleaned list-like string. 

    PARAMETERS:
        pattern --> String pattern to search in each list-like element
        delimiter --> Delimiter of the list-like string
    """
    # Initiates an empty string list
    out_string_list = []
    # Splits the input list-like string by the delimiter provided
    string_list = string.split(delimiter)
    # For each string in string_list...
    for string in string_list:
        # ... if the pattern provided is not in such string...
        if pattern not in string:
            # ... append such string to out_string_list
            out_string_list.append(string)
    # Join the resulting out_string_list using the delimiter provided
    out_string = delimiter.join(out_string_list)
    return out_string


def _filecache_decorator(original_func):
    """
    File-caching logic for the function `get_uniprot( )`, as a decorator.
    Caution! This is not a generic file-caching decorator!
    """
    # Define the file-cache decorated function(`filecached_func( )`):
    def filecached_func(protein_acs, extra_columns=None, use_cache=True, compressed_cache=True):
        """ OPTIONAL FILE-CACHING PARAMETERS:
        use_cache --> True/False. If True (the default) the function will try to get the data first 
        from a cache file with data previously fetched from UniProt.
        compressed_cache --> True/False. If True (the default) the cache file should/will be 
        GZip-compressed. 
        """
        if use_cache:
            import gzip, hashlib, json # Needed for file-caching related functionality.
            
            # 1.- Make a unique file name for the supplied arguments:
            argss = "".join(sorted(protein_acs)) # Protein ACs as a long string (iterable of str ➞ single str).
            if extra_columns: # Add extra columns names:
                argss += "".join(sorted(extra_columns))
            argss_hash = hashlib.sha256(bytes(argss, encoding='utf8')).hexdigest() # Calculate a unique hash
            cache_filen = "ac2txndata-" + argss_hash + ".json" # Cached data should be in JSON format.
            if compressed_cache: # Cache file should be GZip-compressed?:
                open_func = gzip.open # Use `open( )` from `gzip` module
                cache_filen = cache_filen + ".gz" # Add '.gz' extension to file cache name
            else:
                open_func = open # Use built-in `open( )` for uncompressed cache file
            
            # 2.- Try to load previous cached data from file:
            try:
                with open_func(cache_filen, 'rt') as iofile:
                    print("Loading data from cache file:\n ", cache_filen, "...")
                    ac2txndata = json.load(iofile) # Load and decode JSON data into a dictionary
            except (FileNotFoundError, IOError, json.JSONDecodeError) as error:
                print("Cannot load data from cache file! Fallback to Internet connection.")
                # As data hasn't been loaded from cache file (not found, corrupt file, ...), 
                # function will continue to the next step: retrieve data on-line from UniProt.
            else:
                print('Done!')
                # Return cached dictionary of 'Accesssion number'➞Organism Data (dict) items:
                return ac2txndata
            
            # 3.- No cached data ➞ Retrieve it on-line from UniProt:
            ac2txndata = original_func(protein_acs, extra_columns)
            
            # 4.- Write on-line retrieved data (`ac2txndata`) to cache file:
            if ac2txndata:
                try:
                    with open_func(cache_filen, 'wt') as iofile:
                        json.dump( ac2txndata, iofile, separators=(',', ':') ) # Write/serialize dictionary data as JSON data
                except (IOError, TypeError) as error:
                    print("Error writing data to cache file", cache_filen, "!")
                else:
                    print("Also serialized data to cache file:\n ", cache_filen)

            # 5.- Return data as a dictionary of 'Accesssion number'➞Taxonomic Data (dict) items:
            return ac2txndata
        
        else:
            # Directly retrieve on-line data from UniProt:
            return original_func(protein_acs, extra_columns)
    
    # Modify file-cache decorated function(`filecached_func( )`) with information
    # from the original function (`original_func`):
    filecached_func.__doc__ = original_func.__doc__ + filecached_func.__doc__
    filecached_func.__name__ = original_func.__name__
    filecached_func.__qualname__ = original_func.__qualname__
    # Store the original function as the `__wrapped__` attribute, so its signature can 
    # be retrieved and shown (by IDEs, `help( )`, ...):
    filecached_func.__wrapped__ = original_func
    
    return filecached_func # Return the file-cache decorated function.

@_filecache_decorator
def get_uniprot(protein_acs, extra_columns=None):
    """
    Gets protein data from UniProt for the supplied ACs, and returns
    this data as a dictionary of 'Accession Number ➞ Protein Data (dict)' items.
    
    INPUTS: protein_acs --> An iterable object (list, set, ...) of protein Accession 
    Numbers (strings) to get UniProt data for.

    OUTPUTS: ac2txndata --> A dictionary of dictionaries, containing items (key ➞ value pairs)
    of the kind 'Accesssion number' ➞ Protein Data (dict). Each Protein Data dictionary 
    will have at least an 'AC' key if no extra_columns are provided.

    OPTIONAL PARAMETERS:
        extra_columns --> An iterable of strings representing the names of extra columns 
        of data to get from UniProt for each AC (see possible column names at: 
        https://www.uniprot.org/help/uniprotkb_column_names). If no `extra_columns` are supplied,
        only protein Accesssion Numbers will be fetched.

   """
    WSPCS_WO_TAB = ' \n\r\x0b\x0c' # All standard Python `whitespace` characters excluding tabs ('\t').
    # Wait a second between multiple requests to avoid overloading UniProt server (http error 503):
    time.sleep(1)
    
    # Process parameters:
    columns_to_get = '' # Default data columns to get from UniProt.
    if extra_columns:
        # Add extra columns to the default ones as comma-separated names:
        columns_to_get = columns_to_get + "," + ",".join(extra_columns)

    ac2txndata = dict() # { Accession Number: Organism Data (dict) }
    # Get data information for the proteins using UniProt 'database identifier mapping'
    # service (it allows getting information about multiple proteins simultaneously):
    # - Set URL and arguments to use the UniProt web API of 'database identifier mapping' service:
    #   (https://www.uniprot.org/help/api_idmapping ; also useful: https://www.uniprot.org/help/api_queries)
    url = 'https://www.uniprot.org/uploadlists/'
    args = {'from': 'ACC', # UniProtKB AC
            'to': 'ACC', # UniProtKB AC
            'format': 'tab', # Get data in a table format where columns are separated by tabs (TSV table)
            'query': " ".join(sorted(protein_acs)), # Protein ACs separated by spaces (iterable of str ➞ single str)
            'columns': columns_to_get, # Columns to get, with the desired information (https://www.uniprot.org/help/uniprotkb_column_names)
            }
    urlencoded_args = parse.urlencode(args) # Dictionary ➞ ASCII text (str) encoded for requests.
    post_args = bytes(urlencoded_args, encoding='ascii') # ASCII text (str) ➞ Binary data (bytes) needed for POST requests.
    # - Send Request to UniProt web server and wait for a Response:
    print("Contacting UniProt to retrieve data for", len(protein_acs), "proteins.")
    print('Please, wait ...')
    try:
        uprot_response = request.urlopen(url, post_args)
    except (ConnectionError, HTTPError, URLError) as error: # Some connection error:
        print("Internet connection Error", error, "!\nUnable to retrieve UniProt data.")
        return None # No data can be returned, so return None to mark it!
    # - Process UniProt Response (TSV table) as a `ac2txndata` dict:
    line_headers = uprot_response.readline() # Read first line, containing the table headers.
    line_headers = line_headers.decode('utf8') # Binary data (bytes) ➞ Unicode UTF-8 text (str).
    txndata_keys = line_headers.strip(WSPCS_WO_TAB).split('\t')[:-1] # Discard last header (ID. of the sent list of ACs).
    for line in uprot_response: # Read each data table line:
        line = line.decode('utf8')
        line_values = line.strip(WSPCS_WO_TAB).split('\t')
        txndata_values = line_values[:-1] # Discard ACs (last value) for only taxonomy common data
        txndata_dict = dict(zip(txndata_keys, txndata_values)) # Make a base Data dictionary
        # If more than one sent ACs refer to the same protein, they are returned as multiple 
        # comma-separated ACs, so the data must be duplicated for each of these ACs:
        for ac in line_values[-1].split(','):
            new_txndata_dict = txndata_dict.copy() # Make the current Data dict as a copy of the base one
            new_txndata_dict['AC'] = ac # Add current AC to the current Data dict
            ac2txndata[ac] = new_txndata_dict # Add the new 'Accesssion number' ➞ Data (dict) item
    # - Close the connection (the Response object):
    uprot_response.close()
    print('Done!')
    
    # Return data as a dictionary of 'Accesssion number' ➞ Data (dict) items:
    return ac2txndata


def normal_imputer(in_df, columns, downshift=1.8, spread=1/3, random_seed=0):
    """
    Imputes random numbers following a normal distribution downshifted and
    with a certain amount of spread.

    INPUTS: in_df --> Input DataFrame.
            columns --> List of columns were we want to impute missing values.

    OUTPUTS: out_df --> The original dataframe with the normal imputation performed.
    
    PARAMETERS:
            downshift --> How many sigmas (with respect mu) imputed values will be.
            spread --> How spread imputed values will be
            random_seed --> Random seed just to reproduce randomly imputed values.
    """
    # Importing packages
    import numpy as np
    # Initialize random seed
    np.random.seed(seed=random_seed)
    # Copying in_df: out_df
    out_df = in_df.copy()
    # For each column in the input column list...
    for col in columns:
        # ... mask the nan values
        mask = np.isnan(out_df[col])
        # ... compute the mean and the standard deviation
        mu, sigma = out_df[col].mean(), out_df[col].std()
        # ... creating imputations array of same size than True counts in mask
        imputations = np.random.normal(loc=mu-(downshift * sigma),
                                       scale=sigma * spread,
                                       size=mask.sum())
        # ... imputing the values
        out_df.loc[mask, col] = imputations
    
    return out_df


def get_psites4protrecord(prot_record):
    """Devuelve los sitios de fosforilación (p-sites) en la secuencia de la proteína, 
    a partir de la información contenida en algunos de los objetos SeqFeature del 
    objeto SeqRecord proporcionado (argumento `prot_record`)"""
    psites = list() # La lista de phospho-sites (p-sites).
    # Recorre los objetos `SeqFeature` de la lista asociada al atributo `SeqRecord.features` 
    # de `prot_record` para seleccionar los posibles AAs fosforilables (p-sites):
    for feature in prot_record.features:
        # Selecciona p-sites: `SeqFeature.type` == "modified residue" 
        #                     `SeqFeature.qualifiers`["description"] == "phosphoserine" o "phosphotyrosine" o "phosphothreonine"
        if ( feature.type.lower() == 'modified residue' and 
             'phospho' in feature.qualifiers['description'].lower() ):
            aa_pypos = int(feature.location.start) # Posición del AA fosforilable (Ej. 203). `SeqFeature.location.start`
            aa = prot_record.seq[aa_pypos] # AA fosforilable a partir de la secuencia (`SeqRecord.seq`) y la posición (Ej. 'Y')
            psites.append( aa + str(aa_pos + 1) ) # AA + Posición Biológica (Ej.: 'Y204')
    return psites

def get_uniprot_data4ac(ac):
    """Devuelve el nombre (`name`, str), las funciones (`functions`, list) y los `psites` (list) 
    de una proteína, a partir de su Accesssion Number (argumento `ac`)"""
    from Bio import SeqIO # Importamos el sub-paquete `SeqIO` de Biopython.
    
    # Esperar un segundo, para no saturar el servidor (http error 503) en caso de
    # múltiples llamadas consecutivas a esta función:
    time.sleep(1) # Función sleep(seconds) del módulo time.
    
    # Recuperar la información sobre la proteína:
    # - Construir la URL para usar la API Web de UniProt:
    prot_url = "https://www.uniprot.org/uniprot/" + ac + ".xml"
    # - Petición al servidor web de UniProt:
    try:
        uprot_response = request.urlopen(prot_url) # Petición web y obtención de la respuesta de UniProt.
    except (ConnectionError, HTTPError, URLError) as error: # En caso de errores de conexión:
        print("Error de conexión", error, ". No se pudo recuperar información para", ac, "\n")
        return None
    # - Conversión de datos XML en un objeto SeqRecord:
    prot_record = SeqIO.read(uprot_response, 'uniprot-xml') # Formato de la información: 'uniprot-xml'.
    # - Cerrar la conexión (el objeto Response):
    uprot_response.close()
    
    # Procesar la información obtenida:
    # - Obtener el nombre y la lista de funciones de la proteína:
    name = prot_record.description
    functions = prot_record.annotations.get('comment_function', list() )
    # - Obtener los sitios de fosforilación en la secuencia de la proteína:
    psites = get_psites4protrecord(prot_record)
            
    return name, functions, psites # Retorna los datos como una tupla de 3 elementos


def molecular_weight(seq, alphabet2mass=None):
    """
    Calcula y devuelve la masa molecular de una secuencia peptídica (`seq`), según
    las masas de los diferentes aminoácidos proporcionada por un diccionario (`alphabet2mass`), 
    que por defecto es igual al diccionario global `aa2mass` para masas monoisotópicas.
    """
    seq = seq.strip().upper() # Limpia la secuencia y la pasa a mayúsculas
    if not seq: # Evita procesar una secuencia vacía:
        return 0
    if alphabet2mass is None:
        # Por defecto construye un diccionario 'AA' ➞ monoisotopic mass a partir del diccionario 
        # global `aa2mass`:
        alphabet2mass = {aa: masses[1] for aa, masses in aa2mass}
    mass = 0 # Masa inicial
    # Añade la masa de cada aminoácido de la secuencia peptídica:
    for aa in seq:
        mass += alphabet2mass[aa] # Equivale a mass = mass + alphabet2mass[aa]
    # Añadir la masa de 1 molécula de H₂O por los aminoácidos de los extremos (+1H por el
    # N-term y +1OH por el C-term):
    mass += 18.010565 # Masa monoisotópica del H₂O
    #
    return round(mass, 6)


def add_multiple(*values):
    """Sum/concatenate all supplied values (all values should be of the same type)"""
    print("Passed values (" + str(len(values)) + "): ", values)
    # `result` comienza con el primer objeto de los argumentos contenidos en `values`:
    result = values[0]
    # Iteramos sobre `values`, a partir del segundo argumento que contiene, 
    # para así obtener y sumar/concatenar todos los argumentos pasados a la función:
    for value in values[1:]:
        result = result + value
    return result



#===============================================================================
# Ejecución Directa
#===============================================================================
if __name__ == '__main__':
    # Este bloque de código sólo se ejecutará si el módulo/fichero es ejecutado
    # directamente en lugar de ser importado desde otro módulo:
    
    # Aviso a navegantes:
    print('Warning!: This module should be used by importing it.\n')
    
    # Ejemplo de uso de la clase `UniprotProtein` y la función `protein_coverage`:
    protein = UniprotProtein(ac='O00631', sequence='MGINTRELFLNFTIVLITVILMWLLVRSYQY', 
                             description=('Sarcolipin. Reversibly inhibits the activity '
                                          'of ATP2A1 in sarcoplasmic reticulum'))
    peptides = {'MGINTR', 'VILMWLLVR', 'ELFLNF'}
    print("Protein", protein.ac, "length:", protein.length)
    print("Total length of the Peptides:", sum(map(len, peptides)))
    print("Protein", protein.ac, "'coverage':", 
          protein_coverage(protein.sequence, peptides) )
