# -*- coding: utf-8 -*-

"""
:synopsis: Ejemplo de módulo dentro de un sub-paquete. Este módulo está pensado para ser importado.

:created:    2019/01/25

:authors:    Òscar Gallardo Román (ogallardo@protonmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2019 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '0.1'
__UPDATED__ = '2019-11-26'


#===============================================================================
# Imports
#===============================================================================
from .. import module2 # Importa module2 desde el paquete principal example_package


#===============================================================================
# Variables de Módulo
#===============================================================================
a = frozenset([1,2,3])


#===============================================================================
# Definiciones de Funciones
#===============================================================================
def f(x):
    """Retorna `x` elevado a `x`"""
    return x**x

