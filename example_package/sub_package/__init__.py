# -*- coding: utf-8 -*-

"""
:synopsis: Ejemplo de un Sub-Paquete. Este es el módulo de inicialización del paquete (se ejecuta al importar el paquete).

:created:    2019/01/25

:authors:    Òscar Gallardo Román (ogallardo@protonmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2019 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '0.1'
__UPDATED__ = '2019-11-26'

#===============================================================================
# Package imports
#===============================================================================
from . import module1  # Importa el módulo module1 del sub-paquete, de manera que éste sea accesible incluso si sólo se importa el sub-paquete.
