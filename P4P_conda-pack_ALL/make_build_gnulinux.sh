#!/bin/bash

# CONFIGURATION GLOBAL VARIABLES:

# Current script working directory (based on https://stackoverflow.com/a/4774063):
cd $(dirname "$0")
CWD="$(pwd -P)"
if [ "$(echo "${CWD}" | tail -c 2 | head -c 1)" != '/' ]; then
    CWD="${CWD}/"
else
    CWD="${CWD}"
fi

# Source Data Folders:
BASE_SRC_FOLDER="${CWD}"
SRC_COURSE_TMPLT="${BASE_SRC_FOLDER}P4P_Course - Template from Repository/"

# Destination Builds Folder:
BASE_DST_FOLDER="${CWD}Builds/"
DST_FOLDER="${BASE_DST_FOLDER}ADD_TO_GNULINUX_VM_AND_BUILD/"

LOG_FOLDER="${BASE_DST_FOLDER}"



# FUNCTIONS:

now() {
    # Date and time in a file-friendly pseudo-ISO format:
    date +%Y%m%d_%H%M%S
}


move_old_builds() {
    # 
    if [ -e "${DST_FOLDER}" ]; then
        mv "${DST_FOLDER}" "${BASE_DST_FOLDER}OLD_ADD_TO_GNULINUX_VM_$(now)"
    fi
    mkdir "${DST_FOLDER}"
}


copy_course_materials() {
    # Copy referenced files (no symlinks) from source template folder containing symlinks (based on https://stackoverflow.com/a/10738708) to a new folder (based on and https://superuser.com/a/61619):
    cp -RL "${SRC_COURSE_TMPLT}" "${DST_FOLDER}P4P_Course"
}


copy_general_files() {
    cp -L "${BASE_SRC_FOLDER}conda_requirements.txt" "${DST_FOLDER}"
    copy_course_materials
}


copy_files4linux() {
#     files="p4p_easyconf
# conda_requirements.txt
# anaconda_prompt
# .anaconda_prompt_rc
# python
# jupyterlab
# ipython
# qtconsole"
#     for file in ${files}; do
#         cp -L "${BASE_SRC_FOLDERCWD}${file}" "${DST_FOLDER}"
#     done
    cp -R "${BASE_SRC_FOLDER}Only for GNULINUX and MACOSX/." "${DST_FOLDER}"
    cp -L "${BASE_SRC_FOLDER}Miniconda3/Miniconda3-latest-Linux-x86_64.sh" "${DST_FOLDER}"
}


main() {
    local last_error=0
    
    # Main Workflow:
    move_old_builds || last_error=$?
    copy_general_files || last_error=$?
    copy_files4linux || last_error=$?
    
    # End:
    if [ ${last_error} -eq 0 ]; then
        echo 
        echo "** Build Artifacts for VM succesfully copied **"
        echo 
    fi
    return ${last_error}
}



# MAIN:

main
exit $?
