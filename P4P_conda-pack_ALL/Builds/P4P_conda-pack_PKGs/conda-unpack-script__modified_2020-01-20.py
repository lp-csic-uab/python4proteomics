#!python.exe
# Much of this file borrowed from conda/core/portability.py:
#
# https://github.com/conda/conda/blob/master/conda/core/portability.py
#
# The license of which has been provided below:
#
# -----------------------------------------------------------------------------
#
# BSD 3-Clause License
#
# Copyright (c) 2012, Continuum Analytics, Inc.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the copyright holder nor the names of its
#       contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import re
import struct
import sys

on_win = sys.platform == 'win32'

# three capture groups: whole_shebang, executable, options
SHEBANG_REGEX = (
        # pretty much the whole match string
        br'^(#!'
        # allow spaces between #! and beginning of the executable path
        br'(?:[ ]*)'
        # the executable is the next text block without an escaped
        # space or non-space whitespace character
        br'(/(?:\\ |[^ \n\r\t])*)'
        # the rest of the line can contain option flags
        br'(.*)'
        # end whole_shebang group
        br')$')


def update_prefix(path, new_prefix, placeholder, mode='text'):
    if on_win and mode == 'text':
        # force all prefix replacements to forward slashes to simplify need to
        # escape backslashes replace with unix-style path separators
        new_prefix = new_prefix.replace('\\', '/')

    with open(path, 'rb+') as fh:
        original_data = fh.read()
        fh.seek(0)

        data = replace_prefix(original_data, mode, placeholder, new_prefix)

        # If the before and after content is the same, skip writing
        if data != original_data:
            fh.write(data)
            fh.truncate()


def replace_prefix(data, mode, placeholder, new_prefix):
    if mode == 'text':
        data2 = text_replace(data, placeholder, new_prefix)
    elif mode == 'binary':
        data2 = binary_replace(data,
                               placeholder.encode('utf-8'),
                               new_prefix.encode('utf-8'))
        if len(data2) != len(data):
            message = ("Found mismatched data length in binary file:\n"
                       "original data length: {len_orig!d})\n"
                       "new data length: {len_new!d}\n"
                       ).format(len_orig=len(data),
                                len_new=len(data2))
            raise ValueError(message)
    else:
        raise ValueError("Invalid mode: %r" % mode)
    return data2


def text_replace(data, placeholder, new_prefix):
    return data.replace(placeholder.encode('utf-8'), new_prefix.encode('utf-8'))


if on_win:
    def binary_replace(data, placeholder, new_prefix):
        if placeholder not in data:
            return data
        return replace_pyzzer_entry_point_shebang(data, placeholder, new_prefix)

else:
    def binary_replace(data, placeholder, new_prefix):
        """Perform a binary replacement of `data`, where ``placeholder`` is
        replaced with ``new_prefix`` and the remaining string is padded with null
        characters.  All input arguments are expected to be bytes objects."""

        def replace(match):
            occurances = match.group().count(placeholder)
            padding = (len(placeholder) - len(new_prefix)) * occurances
            if padding < 0:
                raise ValueError("negative padding")
            return match.group().replace(placeholder, new_prefix) + b'\0' * padding

        pat = re.compile(re.escape(placeholder) + b'([^\0]*?)\0')
        return pat.sub(replace, data)


def replace_pyzzer_entry_point_shebang(all_data, placeholder, new_prefix):
    """Code adapted from pyzzer. This is meant to deal with entry point exe's
    created by distlib, which consist of a launcher, then a shebang, then a zip
    archive of the entry point code to run.  We need to change the shebang.
    """
    # Copyright (c) 2013 Vinay Sajip.
    #
    # Permission is hereby granted, free of charge, to any person obtaining a copy
    # of this software and associated documentation files (the "Software"), to deal
    # in the Software without restriction, including without limitation the rights
    # to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    # copies of the Software, and to permit persons to whom the Software is
    # furnished to do so, subject to the following conditions:
    #
    # The above copyright notice and this permission notice shall be included in
    # all copies or substantial portions of the Software.
    #
    # THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    # IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    # FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    # AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    # LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    # OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    # THE SOFTWARE.
    launcher = shebang = None
    pos = all_data.rfind(b'PK\x05\x06')
    if pos >= 0:
        end_cdr = all_data[pos + 12:pos + 20]
        cdr_size, cdr_offset = struct.unpack('<LL', end_cdr)
        arc_pos = pos - cdr_size - cdr_offset
        data = all_data[arc_pos:]
        if arc_pos > 0:
            pos = all_data.rfind(b'#!', 0, arc_pos)
            if pos >= 0:
                shebang = all_data[pos:arc_pos]
                if pos > 0:
                    launcher = all_data[:pos]

        if data and shebang and launcher:
            if hasattr(placeholder, 'encode'):
                placeholder = placeholder.encode('utf-8')
            if hasattr(new_prefix, 'encode'):
                new_prefix = new_prefix.encode('utf-8')
            shebang = shebang.replace(placeholder, new_prefix)
            all_data = b"".join([launcher, shebang, data])
    return all_data


_prefix_records = [
('Scripts/binstar-script.py', 'C:\\ci\\anaconda-client_1535570530104\\_h_env', 'text'),
('Scripts/conda-server-script.py', 'C:\\ci\\anaconda-client_1535570530104\\_h_env', 'text'),
('Scripts/anaconda-navigator-script.pyw', 'C:\\ci\\anaconda-navigator_1553808134707\\_h_env', 'text'),
('Library/lib/pkgconfig/blosc.pc', 'C:/ci/blosc_1558554157273/_h_env', 'text'),
('Lib/site-packages/xontrib/conda.xsh', 'C:/ci/conda_1576798121089/_h_env', 'text'),
('Scripts/activate', 'C:\\ci\\conda_1576798121089\\_h_env', 'text'),
('Scripts/deactivate', 'C:\\ci\\conda_1576798121089\\_h_env', 'text'),
('etc/fish/conf.d/conda.fish', 'C:\\ci\\conda_1576798121089\\_h_env', 'text'),
('etc/profile.d/conda.csh', 'C:\\ci\\conda_1576798121089\\_h_env', 'text'),
('etc/profile.d/conda.sh', 'C:\\ci\\conda_1576798121089\\_h_env', 'text'),
('shell/condabin/conda-hook.ps1', 'C:\\ci\\conda_1576798121089\\_h_env', 'text'),
('Scripts/conda-pack-script.py', 'C:\\Users\\usuari\\p4p', 'text'),
('Scripts/conda-pack.exe', 'C:\\Users\\usuari\\p4p', 'binary'),
('Library/bin/gif2h5.exe', 'C:/ci/hdf5_1545244154871/_h_env', 'binary'),
('Library/bin/h52gif.exe', 'C:/ci/hdf5_1545244154871/_h_env', 'binary'),
('Library/bin/h5clear.exe', 'C:/ci/hdf5_1545244154871/_h_env', 'binary'),
('Library/bin/h5copy.exe', 'C:/ci/hdf5_1545244154871/_h_env', 'binary'),
('Library/bin/h5debug.exe', 'C:/ci/hdf5_1545244154871/_h_env', 'binary'),
('Library/bin/h5diff.exe', 'C:/ci/hdf5_1545244154871/_h_env', 'binary'),
('Library/bin/h5dump.exe', 'C:/ci/hdf5_1545244154871/_h_env', 'binary'),
('Library/bin/h5format_convert.exe', 'C:/ci/hdf5_1545244154871/_h_env', 'binary'),
('Library/bin/h5import.exe', 'C:/ci/hdf5_1545244154871/_h_env', 'binary'),
('Library/bin/h5jam.exe', 'C:/ci/hdf5_1545244154871/_h_env', 'binary'),
('Library/bin/h5ls.exe', 'C:/ci/hdf5_1545244154871/_h_env', 'binary'),
('Library/bin/h5mkgrp.exe', 'C:/ci/hdf5_1545244154871/_h_env', 'binary'),
('Library/bin/h5repack.exe', 'C:/ci/hdf5_1545244154871/_h_env', 'binary'),
('Library/bin/h5repart.exe', 'C:/ci/hdf5_1545244154871/_h_env', 'binary'),
('Library/bin/h5stat.exe', 'C:/ci/hdf5_1545244154871/_h_env', 'binary'),
('Library/bin/h5unjam.exe', 'C:/ci/hdf5_1545244154871/_h_env', 'binary'),
('Library/bin/h5watch.exe', 'C:/ci/hdf5_1545244154871/_h_env', 'binary'),
('Library/bin/hdf5.dll', 'C:/ci/hdf5_1545244154871/_h_env', 'binary'),
('Library/cmake/hdf5/hdf5-targets.cmake', 'C:/ci/hdf5_1545244154871/_h_env', 'text'),
('Library/lib/libhdf5.lib', 'C:/ci/hdf5_1545244154871/_h_env', 'binary'),
('Library/lib/libhdf5.settings', 'C:/ci/hdf5_1545244154871/_h_env', 'text'),
('Library/lib/pkgconfig/hdf5-1.10.4.pc', 'C:/ci/hdf5_1545244154871/_h_env', 'text'),
('Library/lib/pkgconfig/hdf5_cpp-1.10.4.pc', 'C:/ci/hdf5_1545244154871/_h_env', 'text'),
('Library/lib/pkgconfig/hdf5_hl-1.10.4.pc', 'C:/ci/hdf5_1545244154871/_h_env', 'text'),
('Library/lib/pkgconfig/hdf5_hl_cpp-1.10.4.pc', 'C:/ci/hdf5_1545244154871/_h_env', 'text'),
('share/jupyter/kernels/python3/kernel.json', 'C:\\\\ci\\\\ipykernel_1579117843818\\\\_h_env', 'text'),
('Scripts/pyjson5-script.py', 'C:\\Users\\usuari\\p4p', 'text'),
('Scripts/pyjson5.exe', 'C:\\Users\\usuari\\p4p', 'binary'),
('Scripts/jupyter-lab-script.py', 'C:\\Users\\usuari\\p4p', 'text'),
('Scripts/jupyter-labextension-script.py', 'C:\\Users\\usuari\\p4p', 'text'),
('Scripts/jupyter-labhub-script.py', 'C:\\Users\\usuari\\p4p', 'text'),
('Scripts/jlpm-script.py', 'C:\\Users\\usuari\\p4p', 'text'),
('Scripts/jupyter-lab.exe', 'C:\\Users\\usuari\\p4p', 'binary'),
('Scripts/jupyter-labextension.exe', 'C:\\Users\\usuari\\p4p', 'binary'),
('Scripts/jupyter-labhub.exe', 'C:\\Users\\usuari\\p4p', 'binary'),
('Scripts/jlpm.exe', 'C:\\Users\\usuari\\p4p', 'binary'),
('Library/lib/libpng/libpng16.cmake', 'C:\\\\ci\\\\libpng_1556041840193\\\\_h_env', 'text'),
('Library/lib/pkgconfig/libpng.pc', 'C:/ci/libpng_1556041840193/_h_env', 'text'),
('Library/lib/pkgconfig/libpng16.pc', 'C:/ci/libpng_1556041840193/_h_env', 'text'),
('Library/lib/pkgconfig/libtiff-4.pc', 'C:/ci/libtiff_1573232737676/_h_env', 'text'),
('Lib/site-packages/numexpr/__config__.py', 'C:/ci/numexpr_1565958325715/_h_env', 'text'),
('Lib/site-packages/numpy/__config__.py', 'C:/ci/numpy_base_and_dev_1574867355112/_h_env', 'text'),
('Lib/site-packages/numpy/distutils/__config__.py', 'C:/ci/numpy_base_and_dev_1574867355112/_h_env', 'text'),
('Lib/site-packages/numpy/distutils/site.cfg', 'C:/ci/numpy_base_and_dev_1574867355112/_h_env', 'text'),
('Scripts/f2py-script.py', 'C:\\ci\\numpy_base_and_dev_1574867355112\\_h_env', 'text'),
('Library/bin/c_rehash.pl', 'C:\\\\ci\\\\openssl_1571238319275\\\\_h_env', 'text'),
('Library/bin/libcrypto-1_1-x64.dll', 'C:\\ci\\openssl_1571238319275\\_h_env', 'binary'),
('Library/bin/libcrypto-1_1-x64.pdb', 'C:\\\\ci\\\\openssl_1571238319275\\\\_h_env', 'binary'),
('Library/bin/libssl-1_1-x64.pdb', 'C:\\\\ci\\\\openssl_1571238319275\\\\_h_env', 'binary'),
('Library/bin/openssl.pdb', 'C:\\\\ci\\\\openssl_1571238319275\\\\_h_env', 'binary'),
('Library/lib/engines-1_1/capi.pdb', 'C:\\ci\\openssl_1571238319275\\_h_env', 'binary'),
('Library/lib/engines-1_1/padlock.pdb', 'C:\\ci\\openssl_1571238319275\\_h_env', 'binary'),
('Library/misc/CA.pl', 'C:\\ci\\openssl_1571238319275\\_h_env', 'text'),
('Library/misc/tsget.pl', 'C:\\ci\\openssl_1571238319275\\_h_env', 'text'),
('Scripts/pronto-script.py', 'C:\\Users\\usuari\\p4p', 'text'),
('Scripts/pronto.exe', 'C:\\Users\\usuari\\p4p', 'binary'),
('Scripts/pygmentize-script.py', 'C:\\Users\\usuari\\p4p', 'text'),
('Scripts/pygmentize.exe', 'C:\\Users\\usuari\\p4p', 'binary'),
('Library/bin/pylupdate5.bat', 'C:\\ci\\pyqt_1537383866988\\_h_env', 'text'),
('Library/bin/pyrcc5.bat', 'C:\\ci\\pyqt_1537383866988\\_h_env', 'text'),
('Library/bin/pyuic5.bat', 'C:\\ci\\pyqt_1537383866988\\_h_env', 'text'),
('Lib/site-packages/tables-3.6.1-py3.7.egg-info/SOURCES.txt', 'C:/ci/pytables_1573079632272/_h_env', 'text'),
('python.pdb', 'C:\\ci\\python_1578510570019\\_h_env', 'binary'),
('pythonw.pdb', 'C:\\ci\\python_1578510570019\\_h_env', 'binary'),
('Lib/site-packages/zmq/utils/compiler.json', 'C:\\\\ci\\\\pyzmq_1565899382831\\\\_h_env', 'text'),
('Lib/site-packages/zmq/utils/config.json', 'C:\\\\ci\\\\pyzmq_1565899382831\\\\_h_env', 'text'),
('Library/bin/Qt5Core.dll', 'C:/qt64/qt_1544645195969/_h_env', 'binary'),
('Library/bin/repc.exe', 'C:/qt64/qt_1544645195969/_h_env', 'binary'),
('Library/lib/Qt5AccessibilitySupport.prl', 'C:\\\\qt64\\\\qt_1544645195969\\\\_h_env', 'text'),
('Library/lib/Qt5AxBase.prl', 'C:\\\\qt64\\\\qt_1544645195969\\\\_h_env', 'text'),
('Library/lib/Qt5AxContainer.prl', 'C:\\\\qt64\\\\qt_1544645195969\\\\_h_env', 'text'),
('Library/lib/Qt5AxServer.prl', 'C:\\\\qt64\\\\qt_1544645195969\\\\_h_env', 'text'),
('Library/lib/Qt5Bootstrap.prl', 'C:\\\\qt64\\\\qt_1544645195969\\\\_h_env', 'text'),
('Library/lib/Qt5DeviceDiscoverySupport.prl', 'C:\\\\qt64\\\\qt_1544645195969\\\\_h_env', 'text'),
('Library/lib/Qt5EglSupport.prl', 'C:\\\\qt64\\\\qt_1544645195969\\\\_h_env', 'text'),
('Library/lib/Qt5EventDispatcherSupport.prl', 'C:\\\\qt64\\\\qt_1544645195969\\\\_h_env', 'text'),
('Library/lib/Qt5FbSupport.prl', 'C:\\\\qt64\\\\qt_1544645195969\\\\_h_env', 'text'),
('Library/lib/Qt5FontDatabaseSupport.prl', 'C:\\\\qt64\\\\qt_1544645195969\\\\_h_env', 'text'),
('Library/lib/Qt5OpenGLExtensions.prl', 'C:\\\\qt64\\\\qt_1544645195969\\\\_h_env', 'text'),
('Library/lib/Qt5PacketProtocol.prl', 'C:\\\\qt64\\\\qt_1544645195969\\\\_h_env', 'text'),
('Library/lib/Qt5PlatformCompositorSupport.prl', 'C:\\\\qt64\\\\qt_1544645195969\\\\_h_env', 'text'),
('Library/lib/Qt5QmlDebug.prl', 'C:\\\\qt64\\\\qt_1544645195969\\\\_h_env', 'text'),
('Library/lib/Qt5QmlDevTools.prl', 'C:\\\\qt64\\\\qt_1544645195969\\\\_h_env', 'text'),
('Library/lib/Qt5ThemeSupport.prl', 'C:\\\\qt64\\\\qt_1544645195969\\\\_h_env', 'text'),
('Library/lib/Qt5UiTools.prl', 'C:\\\\qt64\\\\qt_1544645195969\\\\_h_env', 'text'),
('Library/lib/qtfreetype.prl', 'C:\\\\qt64\\\\qt_1544645195969\\\\_h_env', 'text'),
('Library/lib/qtmain.prl', 'C:\\\\qt64\\\\qt_1544645195969\\\\_h_env', 'text'),
('Library/mkspecs/qmodule.pri', 'C:\\\\qt64\\\\qt_1544645195969\\\\_h_env', 'text'),
('Scripts/jupyter-qtconsole-script.py', 'C:\\Users\\usuari\\p4p', 'text'),
('Scripts/jupyter-qtconsole.exe', 'C:\\Users\\usuari\\p4p', 'binary'),
('Lib/site-packages/scipy/__config__.py', 'C:/ci/scipy_1575910250707/_h_env', 'text'),
('Lib/site-packages/sipconfig.py', 'C:\\\\Miniconda3\\\\conda-bld\\\\sip_1533137953113\\\\_h_env', 'text'),
('Library/bin/tcl86t.dll', 'C:\\ci\\tk_1535487422822\\_h_env', 'binary'),
('Library/lib/tclConfig.sh', 'C:\\ci\\tk_1535487422822\\_h_env', 'text'),
('Library/lib/tdbc1.0.6/tdbcConfig.sh', 'C:\\ci\\tk_1535487422822\\_h_env', 'text'),
('Scripts/tqdm-script.py', 'C:\\Users\\usuari\\p4p', 'text'),
('Scripts/tqdm.exe', 'C:\\Users\\usuari\\p4p', 'binary'),
('Scripts/runxlrd.py', 'C:\\ci\\xlrd_1545084358935\\_h_env', 'text'),
('Library/lib/pkgconfig/libzmq.pc', 'C:/ci/zeromq_1549025594292/_h_env', 'text'),
('Library/share/cmake/ZeroMQ/ZeroMQTargets.cmake', 'C:/ci/zeromq_1549025594292/_h_env', 'text'),
('Library/share/pkgconfig/zlib.pc', 'C:/ci/zlib_1542815121147/_h_env', 'text'),
('P4P_Course\\images\\python_intro\\sets_2.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\images\\seaborn\\seaborn_logo.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('Shortcuts\\Jupyter QtConsole (iPython).lnk', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\images\\python_intro\\none_1.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\4.1 Beyond. Pyteomics Intro.ipynb', 'C:\\Users\\usuari\\p4p', 'text'),
('desktop.ini', 'C:\\Users\\usuari\\p4p', 'text'),
('P4P_Course\\images\\python_intro\\bools_1.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\data\\qualitative\\Excel.xlsx', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\0.0 First Notebook.ipynb', 'C:\\Users\\usuari\\p4p', 'text'),
('conda_requirements.txt', 'C:\\Users\\usuari\\p4p', 'text'),
('Shortcuts\\Anaconda Navigator.lnk', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\images\\beyond\\pyteomics_logo.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\Supplementary Course Materials\\conda Cheat Sheet - Anaconda.pdf', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\images\\python_intro\\sequences_3.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\images\\pandas\\pandas_logo.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('Menu\\ipython.ico', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\images\\python_intro\\dicts_1.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('Lib\\_nsis.py', 'C:\\Users\\usuari\\p4p', 'text'),
('P4P_Course\\images\\python_intro\\variables_2.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\images\\biopython\\biopython_logo.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\images\\python_intro\\objects_3.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\images\\python_intro\\variables_3.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\example_package\\sub_package\\__init__.py', 'C:\\Users\\usuari\\p4p', 'text'),
('Lib\\_system_path.py', 'C:\\Users\\usuari\\p4p', 'text'),
('P4P_Course\\images\\logo_p4p.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\images\\biopython\\go_term_lineage_0.jpg', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\images\\python_intro\\sequences_2.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\images\\python_intro\\dicts_3.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\images\\console_1.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\example_package\\sub_package\\module1.py', 'C:\\Users\\usuari\\p4p', 'text'),
('Menu\\qtconsole_menu.json', 'C:\\Users\\usuari\\p4p', 'text'),
('P4P_Course\\data\\qualitative\\uniprot-reviewed-Human-9606-w-isoforms.fasta', 'C:\\Users\\usuari\\p4p', 'text'),
('Add_Shortcuts_to_Win_Start_Menu.bat', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\.directory', 'C:\\Users\\usuari\\p4p', 'text'),
('P4P_Course\\images\\python_intro\\bools_2.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('JupyterNotebook.bat', 'C:\\Users\\usuari\\p4p', 'binary'),
('Shortcuts\\JupyterLab.lnk', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\images\\python_intro\\tuples_1.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\images\\python_intro\\dicts_2.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\data\\qualitative\\MQ\\acE77\\evidence.txt', 'C:\\Users\\usuari\\p4p', 'text'),
('P4P_Course\\Supplementary Course Materials\\Python 3 Cheat Sheet - Memento.pdf', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\data\\readme.md', 'C:\\Users\\usuari\\p4p', 'text'),
('Menu\\jupyterlab_menu.json', 'C:\\Users\\usuari\\p4p', 'text'),
('Shortcuts\\Anaconda Prompt (p4p).lnk', 'C:\\Users\\usuari\\p4p', 'binary'),
('Uninstall-Miniconda3.exe', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\images\\python_intro\\sets_1.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\images\\python_intro\\objects_5.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('JupyterQtConsole.bat', 'C:\\Users\\usuari\\p4p', 'binary'),
('JupyterLab.bat', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\data\\go-basic.obo.gz', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\Supplementary Course Materials\\matplotlib Cheat Sheet - DataCamp.pdf', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\Supplementary Course Materials\\Sheet of Python - Tiago Montes.pdf', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\example_package\\module2.py', 'C:\\Users\\usuari\\p4p', 'text'),
('P4P_Course\\images\\python_intro\\bools_3.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\images\\python_intro\\packages_1.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\1.1 Introduccion a Python.ipynb', 'C:\\Users\\usuari\\p4p', 'text'),
('P4P_Course\\images\\python_intro\\objects_2.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\images\\biopython\\go_term_lineage_1.jpg', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\images\\python_intro\\objects_1.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\images\\python_intro\\containers_1.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\images\\python_intro\\sequences_1.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\Supplementary Course Materials\\pip Cheat Sheet.pdf', 'C:\\Users\\usuari\\p4p', 'binary'),
('qt.conf', 'C:\\Users\\usuari\\p4p', 'text'),
('AnacondaNavigator.bat', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\images\\cell_types.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\1.2 Introduccion a Python.ipynb', 'C:\\Users\\usuari\\p4p', 'text'),
('P4P_Course\\data\\quantitative\\MQ\\3319_FE_MBR_U_02_proteinGroups.txt', 'C:\\Users\\usuari\\p4p', 'text'),
('AnacondaPrompt.bat', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\3.3 Pandas Quant Workflow.ipynb', 'C:\\Users\\usuari\\p4p', 'text'),
('P4P_Course\\images\\biopython\\numpy_logo.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\images\\python_intro\\strings_1.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\images\\python_intro\\variables_1.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('Menu\\ipython_menu.json', 'C:\\Users\\usuari\\p4p', 'text'),
('Shortcuts\\Anaconda Powershell Prompt (p4p).lnk', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\images\\python_intro\\lists_1.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\data\\noisoforms-uniprot-reviewed-Human-9606.fasta', 'C:\\Users\\usuari\\p4p', 'text'),
('P4P_Course\\2.1 Biopython.ipynb', 'C:\\Users\\usuari\\p4p', 'text'),
('P4P_Course\\data\\qualitative\\MQ\\acE23\\evidence.txt', 'C:\\Users\\usuari\\p4p', 'text'),
('P4P_Course\\3.1 Pandas Intro.ipynb', 'C:\\Users\\usuari\\p4p', 'text'),
('P4P_Course\\3.2 Pandas Qual Workflow.ipynb', 'C:\\Users\\usuari\\p4p', 'text'),
('P4P_Course\\images\\logo_p4p.ico', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\Supplementary Course Materials\\Numpy Basics Cheat Sheet - DataCamp.pdf', 'C:\\Users\\usuari\\p4p', 'binary'),
('Library\\bin\\qt.conf', 'C:\\Users\\usuari\\p4p', 'text'),
('P4P_Course\\4.0 Beyond. P4P and Beyond.pdf', 'C:\\Users\\usuari\\p4p', 'binary'),
('_conda.exe', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\images\\python_intro\\objects_4.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\example_package\\module1.py', 'C:\\Users\\usuari\\p4p', 'text'),
('P4P_Course\\data\\qualitative\\PD\\acE77\\acE77_isoforms_PSMs.txt', 'C:\\Users\\usuari\\p4p', 'text'),
('P4P_Course\\data\\qualitative\\PD\\acE23\\acE23_isoforms_PSMs.txt', 'C:\\Users\\usuari\\p4p', 'text'),
('P4P_Course\\data\\qualitative\\CommaSeparatedValues.csv', 'C:\\Users\\usuari\\p4p', 'text'),
('P4P_Course\\images\\console_2.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('ipython.bat', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\example_package\\__init__.py', 'C:\\Users\\usuari\\p4p', 'text'),
('P4P_Course\\p4p.py', 'C:\\Users\\usuari\\p4p', 'text'),
('Shortcuts\\iPython Console.lnk', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\images\\scipy\\scipy_logo.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\Supplementary Course Materials\\Clases y Funciones built-in de Python3 - P4P 2019.pdf', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\readme.md', 'C:\\Users\\usuari\\p4p', 'text'),
('P4P_Course\\images\\biopython\\matplotlib_logo.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\Supplementary Course Materials\\Pandas Data Wrangling Cheat Sheet - Irv Lustig.pdf', 'C:\\Users\\usuari\\p4p', 'binary'),
('Shortcuts\\Jupyter Notebook.lnk', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\data\\acE25_rep1_fs01.mgf', 'C:\\Users\\usuari\\p4p', 'text'),
('P4P_Course\\data\\goa_human.gaf.gz', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\images\\python_intro\\modules_1.png', 'C:\\Users\\usuari\\p4p', 'binary'),
('P4P_Course\\Supplementary Course Materials\\Biopython Extras.ipynb', 'C:\\Users\\usuari\\p4p', 'text')
]

if __name__ == '__main__':
    import os
    import argparse
    parser = argparse.ArgumentParser(
            prog='conda-unpack',
            description=('Finish unpacking the environment after unarchiving.'
                         'Cleans up absolute prefixes in any remaining files'))
    parser.add_argument('--version',
                        action='store_true',
                        help='Show version then exit')
    args = parser.parse_args()
    # Manually handle version printing to output to stdout in python < 3.4
    if args.version:
        print('conda-unpack 0.4.0')
    else:
        script_dir = os.path.dirname(__file__)
        new_prefix = os.path.abspath(os.path.dirname(script_dir))
        for path, placeholder, mode in _prefix_records:
            try:
                update_prefix(os.path.join(new_prefix, path), new_prefix,
                              placeholder, mode=mode)
            except Exception as e:
                #DEBUG:
                print("\nERROR!!:")
                print("path/file:", path)
                print("placeholder:", placeholder)
                print("mode:", mode)
                print()
                print(e, "\n")
                
