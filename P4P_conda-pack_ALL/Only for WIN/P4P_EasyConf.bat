@echo off

SETLOCAL EnableDelayedExpansion

REM set _cwd=%CD%
set _cwd=%~dp0
if "%_cwd:~-1%" neq "\" set "_cwd=%_cwd%\"
set _reentered=0
set _first_step=1
set _laststep_file="%_cwd%p4p_easyconf_laststep.log"
set _valid_steps=" 1 2 3 4 5 6 "
set _logging=1
call :UPDATE_DATATIME
set _logfile="%_cwd%p4p_easyconf_%_now%.log"
set _valid_yesno=" n no N No nO NO s si s� S Si sI SI S� s� S� y yes Y Yes YeS YEs YES "


:CHECK_CONDA
REM Check conda enviroment, or try to activate one:
if not DEFINED CONDA_PREFIX (
    if EXIST "%_cwd%Scripts\activate.bat" (
        call "%_cwd%Scripts\activate.bat"
    ) else (
        echo.
        echo Lo sentimos. Este script debe ejecutarse s�lo en un entorno conda.
        echo Ejecute Inicio ^> Todos los Programas ^> Anaconda3 ^> Anaconda Prompt
        echo y desde su l�nea de comandos vuelva a lanzar este script [ "%~f0" ]
        goto :W_ERRORS_END
    )
) else (
    set _old_title=Anaconda Promt
)


:CHECK_PARAMETERS
REM Check for optional supplied parameters 1 and 2:
set _param2=%~2
set _param1=%~1
REM Check parameter 2:
if "%_param2%" equ "--reentering" (
    set _reentered=1
    set _logging=0
    set "_param2="
)
if "%_param2%" equ "--nolog" (
    set _logging=0
    set "_param2="
)
if "%_param2%" neq "" (
    echo.
    echo ERROR. "%~2" no se reconoce como un par�metro v�lido.
    echo Como segundo par�metro s�lo se acepta "--nolog".
    goto :W_ERRORS_END
)
REM Check step supplied as parameter 1 or readed from log, if any and valid:
if "%_param1:~0,5%" equ "-step" (
    set _first_step=%_param1:~5%
    set "_param1="
) else (
    if "%_param1%" equ "" (
        if EXIST %_laststep_file% (
            set /p _first_step=<%_laststep_file%
            set _first_step=!_first_step:~5!
            echo La �ltima vez que se ejecuto este script, se detuvo en el paso !_first_step!
            :whileloop_usefilestep
            set _use_filestep=n
            set /p _use_filestep=Desea Reanudar la ejecuci�n desde el paso !_first_step! ^(s/[n]^)^?
            call set _validflbldans_wo_use=%%_valid_yesno: !_use_filestep! =%%
            if !_validflbldans_wo_use! equ !_valid_yesno! (
                echo "!_use_filestep!" no es una respuesta v�lida.
                echo   Por favor, responda �nicamente s� ^(s^) o no ^(n^)
                goto :whileloop_usefistep
            )
            if /i "!_use_filestep!" equ "n" set _first_step=1
            if /i "!_use_filestep!" equ "no" set _first_step=1
        )
    ) else (
        echo.
        echo ERROR. "%~1" no se reconoce como un par�metro v�lido.
        echo Como primer par�metro s�lo se acepta "-setepN", siendo N el n�mero del paso por el que continuar la ejecuci�n.
        goto :W_ERRORS_END
    )
)
call set _valid_wo_firststep=%%_valid_steps: %_first_step% =%%
if !_valid_wo_firststep! equ %_valid_steps% (
    echo.
    echo ERROR. Paso "%_first_step%" No valido.
    echo S�lo se aceptan los siguientes pasos para continuar la ejecuci�n:
    echo %_valid_steps:~1,-1%
    goto :W_ERRORS_END
)


:WELCOME
REM Welcome message:
if %_reentered% equ 0 (
    title %~nx0 - Preparando el entorno %CONDA_PREFIX% [ Bienvenida ]
    echo.
    echo ** Bienvenido/a a %~nx0 **
    echo.
    echo Este script configurar� a continuaci�n el entorno conda ubicado en
    echo  %CONDA_PREFIX% para el curso Python For Proteomics ^(P4P^).
    echo.
    echo Para ello, es necesaria una conexi�n a Internet y unos 3 GiB de espacio
    echo libre en la unidad %~d0
    echo.
    echo.
    if "%_first_step%" equ "1" (
        set _w_question=Desea Iniciar la ejecuci�n ^(s/[n]^)^?
    ) else (
        set _w_question=Desea Continuar la ejecuci�n desde el paso %_first_step% ^(s/[n]^)^?
    )
    :whileloop_continue
    set /p _continue=!_w_question! || set _continue=n
    call set _validans_wo_continue=%%_valid_yesno: !_continue! =%%
    if !_validans_wo_continue! equ %_valid_yesno% (
        echo "!_continue!" no es una respuesta v�lida.
        echo   Por favor, responda �nicamente s� ^(s^) o no ^(n^)
        goto :whileloop_continue
    )
    if /i "!_continue!" equ "n" goto :ABORT
    if /i "!_continue!" equ "no" goto :ABORT
)


:MAIN_SCRIPT_CONTROL
REM Flow Control and Logging:
if %_logging% equ 1 (
    REM Log output and errors, and Execute WorkFlow from supplied step:
    (
    echo -----------------------------------------------
    echo %~nx0 Log File
    echo %DATE% - %TIME%
    echo -----------------------------------------------
    echo %0 %* ^>  %_logfile%
    echo.
    echo Parameters: %*
    echo First Step: %_first_step%
    echo.
    echo Current Working Directory: %_cwd%
    echo Current Directory: %CD%
    echo Conda base environment: %CONDA_PREFIX%
    echo -----------------------------------------------
    echo.
    )> %_logfile%
    REM Log all:
    tee.exe --help >nul 2>&1
    if !ERRORLEVEL! neq 0 (
        echo AVISO. No se encontr� el comando "tee.exe" para poder guardar el registro de operaciones en el archivo %_logfile%
        echo.
        echo AVISO. No se encontr� el comando "tee.exe" para poder guardar el registro de operaciones en este archivo.>> %_logfile%
        echo.>> %_logfile%
        set _logging=2
        call :step%_first_step%
    ) else (
        REM Trick to put all the batch file output in one file (see https://superuser.com/a/1123378 plus https://stackoverflow.com/a/4237137):
        call %0 -step%_first_step% --reentering 2>&1 | tee -a %_logfile%
        echo.>> %_logfile%
        set _logging=2
    )
) else (
    REM Execute WorkFlow from supplied step:
    call :step%_first_step%
)
REM Return to the calling script, if needed:
if %_reentered% equ 1 exit /b


:SCRIPT_ENDINGS
REM Real/Final Main script Ending:
if DEFINED _old_title title %_old_title%

if NOT EXIST %_laststep_file% (
    goto :SUCCESSFUL_END
) else (
    goto :W_ERRORS_END
)

:SUCCESSFUL_END
set _msg=** Proceso de actualizaci�n e instalaci�n finalizado Correctamente. **
echo.
echo.
echo %_msg%
if %_logging% equ 2 echo %_msg%>> %_logfile%
echo.
echo.
echo Pulse una tecla para Terminar...
pause > nul
if EXIST "%_cwd%tee.exe" del /f /q "%_cwd%tee.exe" > nul
del /f /q %0 > nul & exit /b 0

:W_ERRORS_END
set _msg=*** ALERTA. Ha ocurrido alg�n ERROR. ***
echo.
echo.
echo %_msg%
if %_logging% equ 2 echo %_msg%>> %_logfile%
echo.
if %_logging% neq 0 (
    echo Revise el archivo %_logfile% para averiguar qu� puede haber fallado...
    echo.
)
echo.
echo Pulse una tecla para Terminar...
pause > nul
exit /b 1

:ABORT
if DEFINED _old_title title %_old_title%
set _msg=** Proceso Abortado por el usuario. **
echo.
echo.
echo %_msg%
if %_logging% equ 2 echo %_msg%>> %_logfile%
echo.
echo.
echo Pulse una tecla para Terminar...
pause > nul
exit /b 1


REM ------------------------------
REM   Sub-routines and functions  
REM ------------------------------


REM /-------------------------
:FULL_WORKFLOW
REM 6 steps WorkFlow:
:step1
REM Step 1: Add channel "anaconda" (just in case):
echo -step1> %_laststep_file%
title %~nx0 - Preparando el entorno %CONDA_PREFIX% [ Paso 1 ]
echo.
echo * Paso 1  [ -step1 ]:
echo * A�adiendo canal "anaconda" a los canales de instalaci�n disponibles ...
echo.
call conda config --append channels anaconda
if %ERRORLEVEL% neq 0 goto :workflow_w_error_end

:step2
REM Step 2: Update Python, conda and all the related packages:
echo -step2> %_laststep_file%
title %~nx0 - Preparando el entorno %CONDA_PREFIX% [ Paso 2 ]
echo.
echo * Paso 2  [ -step2 ]:
echo * Actualizando todo ^(conda, Python, paquetes, ...^) ...
echo.
call conda update --yes --all
if %ERRORLEVEL% neq 0 goto :workflow_w_error_end

:step3
REM Step 3: Install packages and dependencies needed for the course:
echo -step3> %_laststep_file%
title %~nx0 - Preparando el entorno %CONDA_PREFIX% [ Paso 3 ]
echo.
echo * Paso 3  [ -step3 ]:
echo * Instalando paquetes necesarios para el curso [ "conda_requirements.txt" ] ...
echo.
if EXIST "%_cwd%conda_requirements.txt" (
    call conda install --yes --file=conda_requirements.txt
    if !ERRORLEVEL! neq 0 goto :workflow_w_error_end
) else (
    echo.
    echo ERROR. No se encontr� el fichero con la lista de paquetes "%_cwd%conda_requirements.txt" .
    goto :workflow_w_error_end
)

:step4
REM Step 4: Add already build extensiones to installed JupyterLab:
echo -step4> %_laststep_file%
title %~nx0 - Preparando el entorno %CONDA_PREFIX% [ Paso 4 ]
echo.
echo * Paso 4  [ -step4 ]:
echo * Actualizando ficheros de Jupyter Lab con algunas extensiones ...
echo.

REM Verify source and destination folders:
if NOT EXIST "%_cwd%share\jupyter\lab" (
    echo.
    echo ERROR. No se encontr� la carpeta destino "%_cwd%share\jupyter\lab\" .
    echo Posiblemente No se instal� correctamente el paquete "jupyterlab" ...
    goto :workflow_w_error_end
)
if NOT EXIST "%_cwd%easyconf_tmp\lab\static" (
    echo.
    echo ERROR. No se encontr� la carpeta origen "%_cwd%easyconf_tmp\lab\static" .
    goto :workflow_w_error_end
)
if NOT EXIST "%_cwd%easyconf_tmp\lab\extensions" (
    echo.
    echo ERROR. No se encontr� la carpeta origen "%_cwd%easyconf_tmp\lab\extensions" .
    goto :workflow_w_error_end
)
if NOT EXIST "%_cwd%easyconf_tmp\lab\schemas" (
    echo.
    echo ERROR. No se encontr� la carpeta origen "%_cwd%easyconf_tmp\lab\schemas" .
    goto :workflow_w_error_end
)

call :UPDATE_DATATIME

REM If a destination with te same name as the source folder exist, move to another name
REM (to completelly overwrite late) or copy (schemas) to another name:
if EXIST "%_cwd%share\jupyter\lab\static" (
    move /Y "%_cwd%share\jupyter\lab\static" "%_cwd%share\jupyter\lab\static_%_now%" > nul
    if !ERRORLEVEL! neq 0 goto :workflow_w_error_end
)
if EXIST "%_cwd%share\jupyter\lab\extensions" (
    move /Y "%_cwd%share\jupyter\lab\extensions" "%_cwd%share\jupyter\lab\extensions_%_now%" > nul
    if !ERRORLEVEL! neq 0 goto :workflow_w_error_end
)
if EXIST "%_cwd%share\jupyter\lab\schemas" (
    REM The /I parameter ensures destination is treated correctly (as a folder in this case):
    xcopy "%_cwd%share\jupyter\lab\schemas" "%_cwd%share\jupyter\lab\schemas_%_now%" /H /E /Y /V /C /Q /K /I
    if !ERRORLEVEL! neq 0 goto :workflow_w_error_end
)

REM Copy all JupyterLab extensions data to installed configuration folder:
xcopy "%_cwd%easyconf_tmp\lab" "%_cwd%share\jupyter\lab" /H /E /Y /V /C /Q /K
if %ERRORLEVEL% neq 0 goto :workflow_w_error_end

:step5
REM Step 5: Add shortcuts to windows Start menu:
echo -step5> %_laststep_file%
title %~nx0 - Preparando el entorno %CONDA_PREFIX% [ Paso 5 ]
echo.
echo * Paso 5  [ -step5 ]:
echo * Creando entrada en el men� Inicio para JupyterLab ...
echo.
set _menu_file=jupyterlab_menu.json
if EXIST "%_cwd%%_menu_file%" (
    if EXIST "%_cwd%Menu" (
        move /Y "%_cwd%!_menu_file!" "%_cwd%Menu" > nul
        if !ERRORLEVEL! neq 0 goto :workflow_w_error_end
        set _menu_file=Menu^\!_menu_file!
    )
    call menuinst "%_cwd%!_menu_file!"
    if !ERRORLEVEL! neq 0 goto :workflow_w_error_end
) else (
    if EXIST "%_cwd%Menu\%_menu_file%" (
        call menuinst "%_cwd%Menu\%_menu_file%"
        if !ERRORLEVEL! neq 0 goto :workflow_w_error_end
    ) else (
        echo.
        echo ERROR. No se encontr� el fichero "%_cwd%%_menu_file%" .
        goto :workflow_w_error_end
    )
)
set _menu_file=qtconsole_menu.json
if EXIST "%_cwd%%_menu_file%" (
    if EXIST "%_cwd%Menu" (
        move /Y "%_cwd%!_menu_file!" "%_cwd%Menu" > nul
        if !ERRORLEVEL! neq 0 goto :workflow_w_error_end
        move /Y "%_cwd%ipython.ico" "%_cwd%Menu" > nul
        if !ERRORLEVEL! neq 0 goto :workflow_w_error_end
        call menuinst "%_cwd%Menu\%_menu_file%"
        if !ERRORLEVEL! neq 0 goto :workflow_w_error_end
    )
) else (
    if EXIST "%_cwd%Menu\%_menu_file%" (
        call menuinst "%_cwd%Menu\%_menu_file%"
        if !ERRORLEVEL! neq 0 goto :workflow_w_error_end
    ) else (
        echo.
        echo ERROR. No se encontr� el fichero "%_cwd%%_menu_file%" .
        goto :workflow_w_error_end
    )
)

:step6
REM Step 6: Cleaning conda environment, temporary installation files, ...:
echo -step6> %_laststep_file%
title %~nx0 - Preparando el entorno %CONDA_PREFIX% [ Paso 6 ]
echo.
echo * Paso 6  [ -step6 ]:
echo * Limpieza post-instalaci�n ...
echo.
call conda clean --yes --all
if %ERRORLEVEL% neq 0 goto :workflow_w_error_end

if EXIST %_cwd%easyconf_tmp (
    del /f /s /q "%_cwd%easyconf_tmp\*.*" > nul
    rd /s /q "%_cwd%easyconf_tmp" > nul
    if EXIST "%_cwd%easyconf_tmp" rd /s /q "%_cwd%easyconf_tmp"
    if !ERRORLEVEL! neq 0 goto :workflow_w_error_end
)
if EXIST %_laststep_file% del /f /q %_laststep_file% > nul
if %ERRORLEVEL% neq 0 goto :workflow_w_error_end

:successful_workflow_end
exit /b 0

:workflow_w_error_end
exit /b 1

goto :eof
REM -------------------------/


REM /-------------------------
:UPDATE_DATATIME
REM Update global variables _date , _time and _now :
set _date=%DATE:/=-%
set _date=%_date: =_%
set _time=%TIME::=%
set _time=%_time:.=_%
set _time=%_time:,=_%
set _now=%_date%_%_time%

goto :eof
REM -------------------------/
