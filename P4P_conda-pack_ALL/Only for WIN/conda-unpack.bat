@echo off

SETLOCAL EnableDelayedExpansion

set _cwd=%~dp0
if "%_cwd:~-1%" neq "\" set "_cwd=%_cwd%\"


:CHECK_CONDA
REM Check conda environment, or try to activate one:
if not DEFINED CONDA_PREFIX (
    echo Intentando activar el entorno conda...
    if EXIST "%_cwd%Scripts\activate.bat" (
        set "_old_title=%ComSpec%"
        call "%_cwd%Scripts\activate.bat"
        if !ERRORLEVEL! equ 0 (
            echo ... Activado^^!
            echo.
        ) else (
            echo.
            goto :W_ERRORS_END
        )
    ) else (
        echo.
        echo Lo sentimos. Este script debe ejecutarse s�lo en un entorno conda.
        echo Ejecute Anaconda Prompt, y desde su l�nea de comandos vuelva a 
        echo lanzar este script [ "%~f0" ]
        goto :W_ERRORS_END
    )
) else (
    echo Un entorno conda ya est� activado...
    echo Us�ndolo.
    echo.
    set "_old_title=Anaconda Promt %CONDA_PROMPT_MODIFIER% [%CONDA_PREFIX%]"
)


:CHECK_PARAMETERS
REM No checking: we will simply pass all parameters to target command:


:MAIN_SCRIPT_CONTROL
REM Execute command/s inside conda environment:
REM Execute conda-unpack in the new destination folder:
title conda-unpack %CONDA_PREFIX%
echo Ejecutando "conda-unpack" para actualizar el entorno conda a su nueva ubicaci�n en %_cwd% ...
cd %_cwd% || goto :W_ERRORS_END
"%_cwd%Scripts\conda-unpack.exe" || goto :W_ERRORS_END
echo ... Re-ubicaci�n del entorno conda terminada.
echo.

REM Add shortcuts to windows Start menu:
call "%_cwd%Add_Shortcuts_to_Win_Start_Menu.bat" || goto :W_ERRORS_END


:SUCCESSFUL_END
REM rename current script:
echo.
echo.
echo ** Proceso finalizado Correctamente. **
echo.
echo El actual archivo por lotes '%0' ser� renombrado a continuaci�n a '%0.BACK' para evitar su ejecuci�n por accidente.
echo.
echo Pulse una tecla para renombrar y terminar o Ctrl+C para interrumpir...
pause > nul
if DEFINED _old_title title %_old_title%
REM (move /Y "%_cwd%%~nx0" "%_cwd%%~nx0.BACK" & exit /b 0) > nul
REM Trick from https://stackoverflow.com/a/20333575:
(goto) 2>nul & move /Y "%_cwd%%~nx0" "%_cwd%%~nx0.BACK" > nul

:W_ERRORS_END
echo.
echo.
echo *** ALERTA. Ha ocurrido alg�n ERROR. ***
echo.
echo.
echo Pulse una tecla para Terminar...
pause > nul
if DEFINED _old_title title %_old_title%
exit /b 1

:ABORT
echo.
echo.
echo ** Proceso Abortado por el usuario. **
echo.
echo.
echo Pulse una tecla para Terminar...
pause > nul
if DEFINED _old_title title %_old_title%
exit /b 1
