@echo off

SETLOCAL EnableDelayedExpansion

set _cwd=%~dp0
if "%_cwd:~-1%" neq "\" set "_cwd=%_cwd%\"


:CHECK_CONDA
REM Check conda environment, or try to activate one:
if not DEFINED CONDA_PREFIX (
    echo Intentando activar el entorno conda...
    if EXIST "%_cwd%Scripts\activate.bat" (
        set "_old_title=%ComSpec%"
        call "%_cwd%Scripts\activate.bat"
        if !ERRORLEVEL! equ 0 (
            echo ... Activado^^!
            echo.
        ) else (
            echo.
            goto :W_ERRORS_END
        )
    ) else (
        echo.
        echo Lo sentimos. Este script debe ejecutarse s�lo en un entorno conda.
        echo Ejecute Anaconda Prompt, y desde su l�nea de comandos vuelva a 
        echo lanzar este script [ "%~f0" ]
        goto :W_ERRORS_END
    )
) else (
    echo Un entorno conda ya est� activado...
    echo Us�ndolo.
    echo.
    set "_old_title=Anaconda Promt %CONDA_PROMPT_MODIFIER% [%CONDA_PREFIX%]"
)


:CHECK_PARAMETERS
REM No checking: we will simply pass all parameters to target command:


:MAIN_SCRIPT_CONTROL
REM Execute command/s inside conda environment:
REM Add shortcuts to windows Start menu:
title A�adiendo entradas de programas del entorno conda [%CONDA_PREFIX%] al men� Inicio
echo.
echo * Creando entrada en el men� Inicio para JupyterLab ...
echo.
set _menu_file=jupyterlab_menu.json
if EXIST "%_cwd%%_menu_file%" (
    if EXIST "%_cwd%Menu" (
        move /Y "%_cwd%!_menu_file!" "%_cwd%Menu" > nul
        if !ERRORLEVEL! neq 0 goto :W_ERRORS_END
        set _menu_file=Menu^\!_menu_file!
    )
    call menuinst "%_cwd%!_menu_file!"
    if !ERRORLEVEL! neq 0 goto :W_ERRORS_END
    echo   ... Entrada creada.
) else (
    if EXIST "%_cwd%Menu\%_menu_file%" (
        call menuinst "%_cwd%Menu\%_menu_file%"
        if !ERRORLEVEL! neq 0 goto :W_ERRORS_END
        echo   ... Entrada creada.
    ) else (
        echo ERROR. No se encontr� el fichero "%_cwd%%_menu_file%" o "%_cwd%Menu\%_menu_file%" .
        goto :W_ERRORS_END
    )
)
echo.
echo * Creando entrada en el men� Inicio para Jupyter QtConsole ...
echo.
set _menu_file=qtconsole_menu.json
if EXIST "%_cwd%%_menu_file%" (
    if EXIST "%_cwd%Menu" (
        move /Y "%_cwd%!_menu_file!" "%_cwd%Menu" > nul
        if !ERRORLEVEL! neq 0 goto :W_ERRORS_END
        move /Y "%_cwd%ipython.ico" "%_cwd%Menu" > nul
        if !ERRORLEVEL! neq 0 goto :W_ERRORS_END
        call menuinst "%_cwd%Menu\%_menu_file%"
        if !ERRORLEVEL! neq 0 goto :W_ERRORS_END
        echo   ... Entrada creada.
    )
) else (
    if EXIST "%_cwd%Menu\%_menu_file%" (
        call menuinst "%_cwd%Menu\%_menu_file%"
        if !ERRORLEVEL! neq 0 goto :W_ERRORS_END
        echo   ... Entrada creada.
    ) else (
        echo ERROR. No se encontr� el fichero "%_cwd%%_menu_file%" o "%_cwd%Menu\%_menu_file%" .
        goto :W_ERRORS_END
    )
)
echo.
echo * Creando entrada en el men� Inicio para iPython ...
echo.
set _menu_file=ipython_menu.json
if EXIST "%_cwd%%_menu_file%" (
    if EXIST "%_cwd%Menu" (
        move /Y "%_cwd%!_menu_file!" "%_cwd%Menu" > nul
        if !ERRORLEVEL! neq 0 goto :W_ERRORS_END
        move /Y "%_cwd%ipython.ico" "%_cwd%Menu" > nul
        call menuinst "%_cwd%Menu\%_menu_file%"
        if !ERRORLEVEL! neq 0 goto :W_ERRORS_END
        echo   ... Entrada creada.
    )
) else (
    if EXIST "%_cwd%Menu\%_menu_file%" (
        call menuinst "%_cwd%Menu\%_menu_file%"
        if !ERRORLEVEL! neq 0 goto :W_ERRORS_END
        echo   ... Entrada creada.
    ) else (
        echo ERROR. No se encontr� el fichero "%_cwd%%_menu_file%" o "%_cwd%Menu\%_menu_file%" .
        goto :W_ERRORS_END
    )
)
echo.
echo * Creando entradas en el men� Inicio para Anaconda Prompt ...
echo.
set _menu_file=console_shortcut.json
if EXIST "%_cwd%Menu\%_menu_file%" (
    call menuinst "%_cwd%Menu\%_menu_file%"
    if !ERRORLEVEL! neq 0 goto :W_ERRORS_END
    echo   ... Entrada para CMD creada ...
) else (
    echo ERROR. No se encontr� el fichero "%_cwd%Menu\%_menu_file%" .
    goto :W_ERRORS_END
)
set _menu_file=powershell_shortcut.json
if EXIST "%_cwd%Menu\%_menu_file%" (
    call menuinst "%_cwd%Menu\%_menu_file%"
    if !ERRORLEVEL! neq 0 goto :W_ERRORS_END
    echo   ... Entrada para PowerShell creada.
) else (
    echo ERROR. No se encontr� el fichero "%_cwd%Menu\%_menu_file%" .
    goto :W_ERRORS_END
)
echo.
echo * Creando entrada en el men� Inicio para Jupyter Notebook ...
echo.
set _menu_file=notebook.json
if EXIST "%_cwd%Menu\%_menu_file%" (
    call menuinst "%_cwd%Menu\%_menu_file%"
    if !ERRORLEVEL! neq 0 goto :W_ERRORS_END
    echo   ... Entrada creada.
) else (
    echo ERROR. No se encontr� el fichero "%_cwd%Menu\%_menu_file%" .
    goto :W_ERRORS_END
)
echo.
echo * Creando entrada en el men� Inicio para Anaconda Navigator ...
echo.
set _menu_file=anaconda-navigator.json
if EXIST "%_cwd%Menu\%_menu_file%" (
    call menuinst "%_cwd%Menu\%_menu_file%"
    if !ERRORLEVEL! neq 0 goto :W_ERRORS_END
    echo   ... Entrada creada.
) else (
    echo ERROR. No se encontr� el fichero "%_cwd%Menu\%_menu_file%" .
    goto :W_ERRORS_END
)


:SUCCESSFUL_END
echo.
echo.
echo ** Proceso finalizado Correctamente. **
echo.
if DEFINED _old_title title %_old_title%
exit /b 0

:W_ERRORS_END
echo.
echo.
echo *** ALERTA. Ha ocurrido alg�n ERROR. ***
echo.
echo.
echo Pulse una tecla para Terminar...
pause > nul
if DEFINED _old_title title %_old_title%
exit /b 1
