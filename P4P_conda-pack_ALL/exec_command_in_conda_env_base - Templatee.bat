@echo off

SETLOCAL EnableDelayedExpansion

set _cwd=%~dp0
if "%_cwd:~-1%" neq "\" set "_cwd=%_cwd%\"


:CHECK_CONDA
REM Check conda environment, or try to activate one:
if not DEFINED CONDA_PREFIX (
    echo Intentando activar el entorno conda...
    if EXIST "%_cwd%Scripts\activate.bat" (
        set "_old_title=%ComSpec%"
        call "%_cwd%Scripts\activate.bat"
        if !ERRORLEVEL! equ 0 (
            echo ... Activado^^!
            echo.
        ) else (
            echo.
            goto :W_ERRORS_END
        )
    ) else (
        echo.
        echo Lo sentimos. Este script debe ejecutarse s�lo en un entorno conda.
        echo Ejecute Anaconda Prompt, y desde su l�nea de comandos vuelva a 
        echo lanzar este script [ "%~f0" ]
        goto :W_ERRORS_END
    )
) else (
    echo Un entorno conda ya est� activado...
    echo Us�ndolo.
    echo.
    set "_old_title=Anaconda Promt %CONDA_PROMPT_MODIFIER% [%CONDA_PREFIX%]"
)


:CHECK_PARAMETERS
REM No checking: we will simply pass all parameters to target command:


:MAIN_SCRIPT_CONTROL
REM Execute command/s inside conda environment:
REM title conda-unpack %CONDA_PREFIX%
REM "%_cwd%Scripts\conda-unpack.exe"
if !ERRORLEVEL! neq 0 (
    goto :W_ERRORS_END
)
REM call "%_cwd%Add_Shortcuts_to_Win_Start_Menu.bat"
if !ERRORLEVEL! neq 0 (
    goto :W_ERRORS_END
)

:SUCCESSFUL_END
echo.
echo.
echo ** Proceso finalizado Correctamente. **
echo.
if DEFINED _old_title title %_old_title%
exit /b 0

:W_ERRORS_END
echo.
echo.
echo *** ALERTA. Ha ocurrido alg�n ERROR. ***
echo.
echo.
echo Pulse una tecla para Terminar...
pause > nul
if DEFINED _old_title title %_old_title%
exit /b 1

:ABORT
echo.
echo.
echo ** Proceso Abortado por el usuario. **
echo.
echo.
echo Pulse una tecla para Terminar...
pause > nul
if DEFINED _old_title title %_old_title%
exit /b 1
