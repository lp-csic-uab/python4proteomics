# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     comment_magics: true
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %% [markdown]
# ![Python4Proteomics](images/logo_p4p.png) **(Python For Proteomics)**
#
# # INTRODUCCIÓN A PYTHON (Parte 1)
#
# En este apartado veremos los conceptos básicos del lenguaje de programación **Python**:
#
#  * **Parte 1** (este _notebook_):
#     - Variables.
#     - Objetos y clases de objetos.
#     - Tipos básicos (*built-in*): números (**int**, **float**), secuencias (**str**, **tuple**, **list**), y otros contenedores de objetos (**set**, **dict**).
#  * Parte 2 (_notebook_ [1.2 Introduccion a Python.ipynb](1.2%20Introduccion%20a%20Python.ipynb "Abrir el notebook de la parte 2")):
#     - Control de flujo de ejecución del código: condicionales (`if... elif... else...`), bucles (`for...`, `while...`, `continue`, `break`), y captura de errores/excepciones (`try... except... else...`).
#     - Algunas funciones básicas de Python (*built-in*), y funciones definidas por el usuario (`def...`).
#     - Módulos y paquetes (`import`).
#     - Referencias y material de ampliación (como [Memento, la _chuleta_ de Python 3](Supplementary%20Course%20Materials/Python%203%20Cheat%20Sheet%20-%20Memento.pdf "Chuleta de Python 3")).
#
# ---
#

# %% [markdown]
# **Índice de la Parte 1:**
#
#  * [I.- Variables](#I.--Variables)
#    * [Asignación y acceso](#Asignaci%C3%B3n-y-acceso)
#    * [Nombres de variables en Python](#Nombres-de-variables-en-Python)
#  * [II.- Tipos/Clases de Objetos](#II.--Tipos%2FClases-de-Objectos)
#    * [Objetos y Clases](#Objetos-y-Clases)
#    * [Tipos simples](#Tipos-simples)
#      * [Números ( *int*, *float* )](#N%C3%BAmeros)
#      * [Booleanos ( *bool* )](#Booleanos-(-bool-))
#      * [El tipo nulo ( *None* )](#El-tipo-nulo-(-None-))
#    * [Contenedores](#Contenedores)
#      * [Secuencias](#Secuencias)
#        * [Cadenas de caracteres ( *str* )](#Cadenas-de-caracteres-(-str%2C-''%2C-%22%22-))
#        * [Tuplas ( *tuple* )](#Tuplas-\(-tuple%2C-(-%2C)-\))
#        * [Listas ( *list* )](#Listas-(-list%2C-%5B-%5D-))
#      * [Conjuntos ( *set* )](#Conjuntos-(-set%2C-%7B-%7D-))
#      * [Diccionarios ( *dict* )](#Diccionarios-(-dict%2C-%7Bkey%3A-value%7D-))
#
# ---
#

# %% [markdown] toc-hr-collapsed=false
# # I.- Variables
#
# Tal como hemos visto, una de las cosas más sencillas que podemos hacer en Python son cálculos matemáticos, como si de una sencilla calculadora se tratara:

# %%
5 * 2 + 3

# %% [markdown]
# Vemos el resultado de la operación; y, al igual que en una sencilla calculadora sin memoria, si queremos utilizar este resultado en una nueva operación, deberemos teclearlo nuevamente:

# %%
13 + 2

# %% [markdown]
# Éstos números (13, 15) son fáciles de recordar y volver a escribir.
#
# Pero, ¿qué sucede si el resultado es diferente?:

# %%
15 / 7

# %% [markdown]
# Éste ya no es un número tan fácil de recordar y volver a escribir cada vez que lo necesitemos...
#
# Es una de las razones por las que utilizamos una **variable**, es decir, un _"nombre"_ para *referenciar* el valor que queremos recuperar más adelante.
#
# ![variable_value](images/python_intro/variables_1.png)

# %% [markdown] toc-hr-collapsed=false
# ## Asignación y acceso
#
# Para **asignar** una _variable_ a un _valor_ se utiliza _un signo igual_ (`=`):
# ```python
#     variable_name = value
# ```

# %%
x = 15 / 7

# %% [markdown]
# En esta ocasión no vemos el resultado de la operación, debido a que tras realizar la operación (15 / 7) su resultado ha sido _asignado_ o _enlazado_ a la variable `x`.

# %% [markdown]
# Para visualizar el **valor** al que _apunta_ una variable (para **acceder** a él), lo único que tenemos que hacer es escribir dicha variable como la *última* expresión de una celda de código del _notebook_:

# %%
x  # ←← 💡 el carácter almohadilla (#) indica el inicio de un "comentario" y todo lo que le procede no es interpretado por Python

# %%
x
'Ahora el valor de x no se muestra porque NO es la última sentencia de la celda'     # ← Última sentencia

# %% [markdown]
# También podemos utilizar la *función* `print()` para poder visualizar (_imprimir_) el valor al que apunta una variable:
# ```Python
#     print(contents[, more_contents, ...])
# ```
# La ventaja de `print()` es que podemos usarla en cualquier punto de la celda de código (no es necesario que sea al final):

# %%
# Uso de la función `print( )` para "imprimir" por pantalla el valor de la variable `x`
print(x)      

'A pesar de no ser la última sentencia de la celda, el valor de x se ha impreso'      # ← Última sentencia

# %% [markdown]
# <br />
#
# Además de visualizar el valor al que han sido asignadas, las variables se utilizan como _**substitutas**_ del **valor** real que referencían al realizar nuevas operaciones (como en matemáticas):

# %%
y = 2 * x + 2
z = y + x - 10

# %% [markdown] tags=["ejercicio"]
# <br />
#
# > 📝 **Práctica:**
# >
# > Ahora, asigna la variable `w` al valor `22`, y visualiza los _valores_ referenciados por las variables `w`, `y` , `z`; todo ello utilizando **una sola celda de código**:

# %% tags=["ejercicio"]



# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución:

w = 42

print(w)
print(y)
z

# %% [markdown] jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 Python permite además la _asignación_ de _múltiples variables_ a _múltiples valores_ en una única línea de código, usando comas (`,`) a ambos lados del signo igual (`=`) :
# ```python
#     first_variable, second_variable = first_value, second_value
# ```
# Ejemplo:

# %% tags=["sabermas"] jupyter={"source_hidden": true}
# 📌 Doble asignación en una única línea de código:
x, y = 4, 2

print(x)
print(y)

# %% tags=["sabermas"] jupyter={"source_hidden": true}
# 📌 Intercambio de valores usando la doble asignación en una única línea de código:
x, y = y, x

print(x)
print(y)

# %% [markdown]
# <br />
#
# _**Diferentes** variables_ pueden referenciar un _**mismo** valor_:
# ```python
#     second_variable = first_variable
# ```
# Usando esta sintaxis `second_variable` también pasará a referenciar el _mismo valor_ que ya es referenciado por `first_variable`.  
# Veámoslo con un _ejemplo_:

# %%
x = 42   # `x` apunta al valor 42
y = x    # `y` apunta al valor al que apunta x, o sea, también 42

print(x)
print(y)

# %% [markdown] jupyter={"source_hidden": true} tags=["sabermas", "alternativa"]
# 📌 🔄 En Python, se pueden _asignar_ también _multiples variables_ a un _mismo valor_ de la siguiente forma:
# ```python
#     second_variable = first_variable = value
# ```

# %% jupyter={"source_hidden": true} tags=["sabermas", "alternativa"]
# 📌 🔄 Múltiple asignación a un mismo objeto:
y = x = 42

print(x)
print(y)

# %% [markdown]
# La situación entre las variables `x` e `y` y su _valor común_ se puede representar de varias maneras, siendo la correcta aquella en que ambas apuntan / referencían el _mismo_ valor:
# ![variables_same_value](images/python_intro/variables_2.png)

# %% [markdown]
# De manera que si ahora asignamos la variable `x` a _otro valor_, aún podemos _acceder_ al _primer valor_ recurriendo a la variable `y`:

# %%
x = -1      # Ahora `x` apunta a -1

print(x)
print(y)

# %% [markdown]
# ![variables_same_value](images/python_intro/variables_3.png)

# %% [markdown] toc-hr-collapsed=true
# ## Nombres de variables en Python
#  - Pueden tener cualquier longitud.
#  - Pueden contener letras (**A-Z**, **a-z**), dígitos (**0-9**), y el carácter _"barra baja"_ ( **_** ).
#  - Python **diferencia** entre mayúsculas y minúsculas (es _case sensitive_), por lo que *x* (minúscula) y _X_ (mayúscula) son nombres de variable diferentes.
#  - El _primer_ carácter de un nombre **no** puede ser un dígito. Por lo tanto, *1st* o *4teen* no son nombres de variables válidos.
#  - Un nombre de variable **no** puede ser igual a alguna de las _palabras clave_ reservadas (_reserved keywords_) por el propio lenguaje.

# %%
# 💡 La función `help( )` permite obtener ayuda en Python (https://stackabuse.com/the-python-help-system/):

help('keywords') 

# %% [markdown]
# <br />
#
# _Ejemplos_ de nombres de variables **permitidos/válidos**:

# %%
X = 0            # `X` (mayúscula) es un nombre de variable diferente a `x` (minúscula)
pi = 3.141592653589793
pI = 7           # `pI` (I mayúscula) también es diferente a `pi` (todo minúsculas)

# Por supuesto las variables también pueden referenciar texto
text_1 = 'Hello' 

print(x)
print(X)
print(pI)
print(pi)
print(text_1)

# %%
# Por convención, en Python los nombres de variable con todas las letras en mayúscula
# se consideran "constantes", y por tanto su valor referenciado no debería alterarse:
CONSTANT = "I'm a CONSTANT: don't change me!"

# Por convención, los nombres de variables que empiezan por "barra baja" ( _ )
# se consideran "privadas", y debería evitarse su uso por otros desarrolladores:
_private = "I'm _private: avoid using me"

print(CONSTANT)
print(_private)

# %% [markdown] tags=["sabermas"] jupyter={"source_hidden": true}
# <br />
#
# 📌 _Ejemplos_ de nombres de variables **_no_ permitidos/*in*válidos**:

# %% jupyter={"source_hidden": true} tags=["error", "sabermas"]
2digits = 12 # Los nombres de variables no pueden comenzar por un dígito ➞ Error (SyntaxError)!

# %% jupyter={"source_hidden": true} tags=["error", "sabermas"]
text$ = "Hi!" # Los nombres de variables sólo pueden contener letras, dígitos y la "barra baja" ( _ ) ➞ Error (SyntaxError)!

# %% jupyter={"source_hidden": true} tags=["error", "sabermas"]
another-text = "Meeeec!" # Los nombres de variables sólo pueden contener letras, dígitos y la "barra baja" ( _ ) ➞ Error (SyntaxError)!

# %% jupyter={"source_hidden": true} tags=["error", "sabermas"]
and = -1 # Los nombres de variables no pueden coincidir con palabras clave reservadas por Python ➞ Error (SyntaxError)!

# %% [markdown] jupyter={"source_hidden": true} tags=["sabermas"]
# ## 📌 Inspeccionando y Borrando nombres de variables
#
# En los _notebooks_ como éste, podemos examinar fácilmente las variables definidas utilizando el comando `%whos`:

# %% jupyter={"source_hidden": true} tags=["sabermas"]
# %whos

# %% [markdown] jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 Para borrar un nombre de variable se utiliza la instrucción `del` seguida del nombre de la variable:

# %% jupyter={"source_hidden": true} tags=["sabermas"]
del x        # Elimina el nombre de la variable x de nuestro espacio de nombres
del CONSTANT # Elimina el nombre de la variable CONSTANT de nuesto espacio de nombres

# %% jupyter={"source_hidden": true} tags=["sabermas"]
# %whos

# %% [markdown] toc-hr-collapsed=false
# # II.- Tipos/Clases de Objectos

# %% [markdown]
# ## Objetos y Clases
#
# _"In Python everything is an **object**()"_
#
# Los **objetos** son una [abstracción de lenguajes de programación como Python, Java, ...](https://es.wikipedia.org/wiki/Programaci%C3%B3n_orientada_a_objetos) que resultan útiles a la hora de estructurar y organizar el código. Un objeto agrupa unos _valores_, referenciados por unas "variables" llamadas **atributos**, con unas _operaciones/funciones_ que están relacionadas con dichos atributos, llamadas **métodos**.
#
# ![objects](images/python_intro/objects_1.png)
#
# Los _objetos_ permiten agrupar _información / estado_ (`.atributos`) y _funcionalidad_ (`.metodos()`) en una sola entidad, facilitando el diseño de programas, la reutilización de código, y la manera en que nos hacemos una idea mental sobre dicho código, ya a que al fin y al cabo los humanos vivimos rodeados de _objetos_ de todo tipo en nuestro día a día.

# %% [markdown]
# Para _acceder_ al valor de un **atributo**, en Python se utiliza un *punto* (`.`) entre el _nombre del objeto_ y el nombre del _atributo_:
#
# ```python
#     object_name.attribute_name
# ```

# %%
# El atributo `.real` referencía la parte real (ℝ, no imaginaria) de un número
y.real        

# %% [markdown]
# Y de _igual_ manera (mediante un *punto* `.`) _accedemos_ a un **método** del objeto; pero en este caso, para conseguir así que se _ejecute_ el código asociado al método, además hay que _"llamarlo"_ utilizando _paréntesis_ `()` después del nombre del método:
#
# ```python
#     object_name.method_name()
# ```

# %%
# El método `.bit_length()` calcula el mínimo número de bits necesarios para representar el número en binario
y.bit_length() 

# %%
# Sin los paréntesis, el método no se ejecuta (sólo recuperamos una referencia a él)
y.bit_length 

# %% [markdown]
# Si en una celda de código de _notebook_, después de escribir un _punto_ tras el nombre del objeto pulsamos la **tecla** `Tab ↹` (tabulador), aparecerá una lista que contiene los _atributos y métodos_ de dicho objeto :
#
# ![attributes_methods_tab](images/python_intro/objects_2.png ". Tab ↹")

# %% [markdown] tags=["ejercicio"]
# <br />
#
# > 📝 **Práctica:**
# >
# > Prueba a ver los _atributos y métodos_ del objeto referenciado por la variable `y` usando la tecla `Tab ↹`:

# %% tags=["ejercicio"]
y.

# %% [markdown]
#   

# %% [markdown] jupyter={"source_hidden": true} tags=["sabermas", "alternativa"]
# 📌 🔄 Para obtener los nombres de los _atributos y métodos_ de un _objeto_, también se puede utilizar la función `dir( )`:
# ```python
#     dir(object)
# ```

# %% jupyter={"source_hidden": true} tags=["sabermas", "alternativa"]
dir(y)

# %% [markdown]
# <br />
#
# Al igual que en el mundo real, en Python hay _diferentes_ **tipos** o **clases** de objetos; siendo este tipo o clase al que pertenece un objeto lo que determinará sus _atributos y métodos_ específicos.
#
# ![class_objects](images/python_intro/objects_3.png)
#
# Así, por ejemplo, los objetos `42` (variable `y`) y `0` (variable `x`) pertenecen ambos a la clase _números enteros_ (**int**), y por tanto ambos poseen el atributo `.numerator`:

# %%
print(X)
print(X.numerator)

print(y)
print(y.numerator)

# %% [markdown]
# Sin embargo el texto `"Hello"` (variable `text_1`) pertenece a _otro_ tipo / clase (**str**) que _no_ posee dicho atributo:

# %% tags=["error"]
print(text_1)

text_1.numerator        # `.numerator` no es un atributo de str ➞ Error (AttributeError)!

# %% [markdown]
# <br />
#
# Para saber el tipo al que pertenece un objeto, se puede utilizar la _"llamada"_ a **type** de la siguiente manera, para que nos devuelva la *clase* a que pertenece el objeto:
# ```python
#     type(object)
# ```

# %%
type(X)

# %%
type(text_1)

# %% [markdown] tags=["sabermas"] jupyter={"source_hidden": true}
# ![introspection](images/python_intro/objects_4.png)

# %% [markdown]
# <br />
#
# En esta introducción veremos brevemente los siguientes **tipos/clases de objetos básicos** de Python:
#
# ![object_classes](images/python_intro/objects_5.png)

# %% [markdown]
# ### Operadores de Comparación
#
# Para comparar _objetos_, se utilizan los siguientes _**operadores de comparación**_:
#
# | Operador | Significado |
# |:--------:| ----------- |
# | `==`     | _Igual_ que (equivale al símbolo = en matemáticas) |
# | `!=`     | _Diferente_ que (≠) |
# | `<`      | _Menor_ que (<) |
# | `>`      | _Mayor_ que (>) |
# | `<=`     | _Menor o igual_ que (≤) |
# | `>=`     | _Mayor o igual_ que (≥) |
# | `is`     | _es_ el _mismo_ objeto que |
# | `is not` | _no es_ el _mismo_ objeto que |
#

# %%
1 == 1.0     # Iguales (==) ?

# %% [markdown]
# Nótese que la comparación de igualdad (`==`) es verdadera (`True`), ya que tienen igual **valor**, aunque son objetos de _tipos/clases diferentes_ (1 `int` y 1.0 `float`).

# %%
1 is 1.0       # Son el mismo objeto (is) ?

# %%
"a" != "b"     # Diferentes (!=) ?

# %%
1E-3 > 0.001   # Mayor que (>) ?

# %%
# -1 Menor que (<) 0.0 y 0.0 Menor o Igual a (<=) 0 ? (encadenamiento de operadores)
-1 < 0.0 <= 0 

# %% [markdown] toc-hr-collapsed=false
# ## Tipos simples

# %% [markdown] toc-hr-collapsed=false
# ### Números
#
# Veremos dos tipos numéricos básicos:
#
#  * **Enteros** (ℤ) o ***integers***  &nbsp; (clase **int**) : &nbsp; `1`, `-2`, `0`
#  * **Reales** (ℝ), representados en [coma flotante](https://es.wikipedia.org/wiki/Coma_flotante) (clase **float**) : `0.1`, `-10.3`, `9.67E6`, `0.0`, `inf`, [`NaN`](https://en.wikipedia.org/wiki/NaN)
#
# Ambos generan objetos _inmutables_ (no se puede alterar su valor una vez creados)

# %%
# Enteros (int):
a = 2
b = -3

print( type(a) )
print(a)

# %%
# Reales (float):
c = -1.5555
d = 1.420E4          # Notación científica
inf = float('inf')   # Infinito ( ∞ )
nan = float('nan')   # "Not a Number" (NaN) ➞ valor no definido o no representable

print( type(d) )
print(d)
print( type(nan) )
print(nan)

# %% [markdown]
# #### Operaciones matemáticas con números
#
# Los _**operadores matemáticos**_ son los estándar: `+` para _sumar_, `-` para _restar_ (o indicar un número _negativo_), `*` para _multiplicar_, `/` para _dividir_:

# %%
print(a, "+", b, "   ➞ ",  a + b )      # Suma ( + )

# %%
print(a, "-", c, " ➞ ",  a - c )        # Resta ( - )

# %%
print(b, "*", d, " ➞ ",  b * d )        # Multiplicación ( * )

# %%
print(a, "/", inf, "   ➞ ",  a / inf )  # División ( / )

# %% [markdown]
# Relacionados con la división, están los _operadores matemáticos_ `//` o "división suelo" ([_floor división_](https://docs.python.org/3/glossary.html#term-floor-division)), que equivaldría al _cociente_ de una división (si ambos números son positivos), y `%` para el [módulo](https://en.wikipedia.org/wiki/Modulo_operation), equivalente al _resto_ de una división (si ambos números son positivos):  

# %%
# División suelo ( // )
# Equivale al cociente de una división, si ambos números son del mismo signo

print(7, "//", a, " ➞ ",  7 // a ) 

# %%
# Módulo ( % )
# Equivale al resto de una división, si ambos números son positivos

print(7, "%", a, " ➞ ",  7 % a ) 

# %% [markdown]
# Para _elevar_ un número a otro ([_potenciación_](https://es.wikipedia.org/wiki/Potenciaci%C3%B3n), xʸ) se puede utilizar tanto el _operador matemático_ `**` como la _función_ `pow( )`:

# %%
print('Potenciación ( ** ó pow( ) ):')
print(a, "**", b, "      ➞",  a ** b)
print("pow(", a, ",", b, ") ➞",  pow(a, b) )     # 🔄 Alternativa al operador **

# %% [markdown] toc-hr-collapsed=false
# <br />
#
# También podemos utilizar las _**funciones**_ `abs( )` (valor absoluto), `max( )` (máximo), `min( )` (mínimo), y `round( )` (redondeo), de la siguiente manera:
#  * ```python
#    abs(number)
#    ``` 
#    Devuelve el _valor **abs**oluto_ de un número:

# %%
print("abs(", b, ")  ➞",  abs(b) )      # Valor abs()oluto

# %% [markdown]
#  * ```python
#    max(number1, number2[, number3, ...])
#    ```
#    Devuelve el _valor **máx**imo_ de una serie de números:

# %%
print("max(", a, ",", b, ",", c, ",", d, ")  ➞ ",  max(a, b, c, d) )     # Valor max()imo

# %% [markdown]
#  * ```python
#    min(number1, number2[, number3, ...])
#    ```
#    Devuelve el _valor **mín**imo_ de una serie de números:

# %%
print("min(", a, ",", b, ",", c, ",", d, ")  ➞ ",  min(a, b, c, d) )      # Valor min()imo

# %% [markdown]
#  * ```python
#    round(number[, decimal_places])
#    ```
#    Devuelve el número _redondeado_ al número de posiciones decimales indicado (*decimal_places*):

# %%
print("round(", c, ",", 2, ")  ➞ ",  round(c, 2) )    # Redondeo a 2 posiciones decimales

# %% [markdown]
# <br />
#
# Además, podemos utilizar **paréntesis** `()` para _alterar_ la _precedencia_ de los operadores y elaborar sentencias más complejas:

# %%
10 ** ((-1 + abs(-3)) * round(5.35, 1)) / (2 - max(1, 0, -1))

# %% [markdown] tags=["ejercicio"]
# <br />
#
# > 📝 **Práctica:**
# >
# > _Calcula_ los siguientes dos valores (`value1` y `value2`):
# >
# > * `value1` : El valor _redondeado_ a 1 decimal de:
# >
# >     23 _elevado_ a 10, _multiplicado_ por -9.59, _dividido_ entre -3.01.
# > * `value2` : El _valor absoluto_ de:
# >
# >     452 _más_ 10492, _dividido_ entre -0.5, _elevado_ todo a 3.

# %% tags=["error", "ejercicio"]
value1 = 





# %% jupyter={"source_hidden": true} tags=["solucion", "ejercicio"]
# 📎 Solución paso a paso:
value1 = 23 ** 10
value1 = value1 * -9.59
value1 = value1 / -3.01
value1 = round(value1, 1)
print(value1)

# 📎 Solución en una única línea, utilizando paréntesis:
value1 = round( ((23 ** 10) * -9.59) / -3.01, 1 )
print(value1)

# %% tags=["error", "ejercicio"]
value2 = 





# %% jupyter={"source_hidden": true} tags=["solucion", "ejercicio"]
# 📎 Solución paso a paso:
value2 = 452 + 10492
value2 = value2 / -0.5
value2 = value2 ** 3
value2 = abs(value2)
print(value2)

# 📎 Solución en una única línea, utilizando paréntesis:
value2 = abs( ((452 + 10492) / -0.5) ** 3 )
print(value2)

# %% [markdown] tags=["ejercicio"]
# > Por último, determina cuál de ellos (`value1` ó `value2`) es el _mayor_.

# %% tags=["ejercicio"]



# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución:
print( max(value1, value2) )

# %% [markdown]
# #### Conversión entre tipos numéricos
#
# Se utilizan directamente _llamadas_ a los nombres de clase (`int()` o `float()`), pasándoles como *argumento* el número a convertir:
#
#  * ```python
#    int(float_number)
#    ```
#    Devuelve sólo la _parte entera_ correspondiente al número real (*float_number*), _sin_ hacer redondeos:
#    

# %%
print("int", c)
      
int(c)       # Ojo! No redondea, sólo descarta los decimales

# %% [markdown]
# <ul style="list-style-type:none;"><li>Para convertir un número real a un entero, pero <em>redondeando</em>, debemos utilizar la función <code>round( )</code> <em>sin</em> indicar decimales (redondeamos a ninguna posición decimal):</li></ul>

# %%
print("round", c)

round(c)     # Redondeo a ningún decimal; o sea, a un entero

# %% [markdown]
#  * ```python
#    float(integer_number)
#    ```
#    Devuelve un _número real_ (en coma flotante) equivalente al número entero (*integer_number*):

# %%
print("float", b)  

float(b)     # Conversión a número real en coma flotante

# %% [markdown] jupyter={"source_hidden": true} tags=["sabermas"]
# ### 📌 Más sobre números...
#
# Python soporta otros tipos numéricos, tales como [imaginarios o complejos](https://docs.python.org/3/library/stdtypes.html#typesnumeric) (ℂ, clase **complex**), **Decimal** (módulo [*decimal*](https://docs.python.org/3/library/decimal.html)) y **Fraction** (módulo [*fractions*](https://docs.python.org/3/library/fractions.html)); además de más operaciones matemáticas como logaritmos, raíz cuadrada, factorial, coseno, ... que están disponibles a través del módulo [*math*](https://docs.python.org/3/library/math.html).

# %% [markdown]
# ### Booleanos ( bool )
#
# Los objetos _booleanos_ (clase **bool**) son dos:
#  * **True** (Verdadero)
#  * **False** (Falso)
#
# ![booleans](images/python_intro/bools_1.png)
#
# Y se referencían usando las _palabras reservadas_ `True` y `False`:

# %%
print(type(True), True)
print(type(False), False)

# %% [markdown]
# #### Evaluación a True o False
#
# Las _expresiones_ que usan los **operadores de comparación** (`==`, `<`, `>`, `!=`, `<=`, `>=`, `is`, `is not`), evalúan a uno de estos dos objetos booleanos: `True` o `False`:

# %%
1 == 1.0

# %%
1 is 1.0

# %% [markdown]
# <br />
#
# Adicionalmente, para saber si un **objeto** evalúa a `True` o `False` se puede utilizar la llamada a la clase **bool**:
# ```python
#     bool(object)
# ```
#
# Por defecto, evalúan a `False` los siguiente objetos:
#  * Cualquier **_tipo numérico_ igual a 0** ( `0`, `0.0`, `0j` ).
#  * Los **_contenedores_ vacíos** ( `""`, `tuple()`, `list()`, `set()`, `dict()` ).
#  * El objeto nulo (`None`).
#
# Y evalúan a `True` los siguientes objetos:
#  * Cualquier **_tipo numérico_ diferente a 0**.
#  * Los **_contenedores_ no vacíos**.
#  * En general, la _mayoría_ de otros _objetos_.

# %%
bool(0.0)

# %%
bool(-9)

# %%
bool("")

# %% [markdown]
# #### Operadores Lógicos
#
# Los objetos booleanos pueden utilizarse con los **operadores lógicos** `and` (y), `or` (o), `not` (no), según las siguientes reglas lógicas (tablas de verdad):
#
# ![logical_operators](images/python_intro/bools_2.png)

# %%
True and False

# %%
True or False

# %%
not False

# %% [markdown]
# Como con otros operadores se pueden utilizar **paréntesis** `()` para elaborar sentencias más complejas:

# %%
not ( (True and bool( [0] )) ) or (False or (not False))

# %% [markdown]
# #### Operadores Binarios
#
# Los **operadores binarios** ([_bitwise operators_](https://wiki.python.org/moin/BitwiseOperators)), operan sobre los _bits_ (0 y 1) que componen la base de todos los datos en un ordenador digital. En Python usaremos sobretodo los operadores binarios `&` (_and_), `|` (_or_):
#
# ![logical_operators](images/python_intro/bools_3.png)

# %%
1 & 0

# %%
1 | 0

# %% jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 Uso sobre valores binarios formados por varios bits:
print( bin(4) )
print( bin(5) )

print( bin(4 & 5) ) # El and binario ( & ) se evalúa bit a bit

# %% [markdown]
# Y dado que los booleanos `True` y `False` son _equivalentes_ a `1` y `0` (respectivamente), se pueden utilizar con ellos también algunos operadores binarios (`&`, `|`) para obtener evaluaciones booleanas:

# %%
True == 1

# %%
False == 0

# %%
True & False

# %%
True | False

# %% [markdown]
# ### El tipo nulo ( None )
#
# Sólo existe un _único_ objeto del tipo nulo (clase **NoneType**), el cual se referencia usando la palabra reservada `None`.
#
# ![none](images/python_intro/none_1.png)

# %%
type(None)

# %%
# 💡 `None` no se observa en la salida de las celdas de código del notebook...
None               

# %%
# ... Salvo que se imprima.
print(None) 

# %% [markdown] jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 El objeto `None` es comúnmente usado como el _valor por defecto_ de algunos parámetros de *funciones* y *métodos*.
#
# También es _retornado_ por _funciones o métodos_ que no devuelven explícitamente ningún objeto.

# %% [markdown]
# Recordar que el objeto `None` *evalúa* a `False`:

# %%
bool( None )

# %% [markdown]
# ## Contenedores
#
# Loas **contenedores** (_containers_) son _objetos_ que _contienen_ referencias a otros objetos:
#
# ![sequence](images/python_intro/containers_1.png)
#
# Los _objetos_ que forman parte de los _contenedores_, en algunos contenedores han de ser del _mismo tipo_ (como en las **cadenas de caracteres**), mientras en otros pueden ser de _diferentes tipos_ (**tuplas**, **listas**, **conjuntos** y **diccionarios**).

# %% [markdown] toc-hr-collapsed=false
# #### Operadores de pertenencia (`in` y `not in`) a contenedores
#
# Con cualquier tipo de _**contenedor**_, se pueden utilizar los **operadores de pertenencia** `in` (comprueba si un elemento **forma parte** del contenedor) y `not in` (comprueba si un elemento **_no_ forma parte** del contenedor):
#
# ```python
#     element in container
#     element not in container
# ```

# %%
# La cadena de caracteres 'AAA' forma parte de la lista ?
'AAA' in [12, True, 0.5, 'AAA', None] 

# %% [markdown]
# #### Funciones informativas (`len()`, `max()` y `min()`) sobre los contenedores
#
# Además, con cualquier tipo de ***contenedor*** se pueden utilizar las **funciones internas** (_built-in_) `len()`, `max()` y `min()` para obtener _información_ de dichos contenedores:
#
#  * ```python 
#    len(container)
#    ```
#    Devuelve la _longitud_, el _número de objetos_ referenciados por el contenedor (*container*).
#
#  * ```python 
#    max(container)
#    ```
#    Devuelve el objeto del contenedor (*container*) cuyo _valor_ es el _más elevado_ (_máximo_).
#    
#  * ```python 
#    min(container)
#    ```
#    Devuelve el objeto del contenedor (*container*) cuyo _valor_ es el _más bajo_ (_mínimo_).

# %%
len( (4, 8, 9, -1) )     # Longitud de la tupla:

# %%
max( [4, 8, 9, -1] )     # Máximo valor de la lista:

# %%
min( {4, 8, 9, -1} )     # Mínimo valor del conjunto:

# %% [markdown] toc-hr-collapsed=false
# ### Secuencias
#
# Las **secuencias** (_sequences_) son ***contenedores*** de otros objetos, en los que _además_ estos objetos están **"ordenados"** (presentan una _ordenación_) y por tanto pueden **accederse** mediante un **índice numérico**:
#
# ![sequence](images/python_intro/sequences_1.png)
#
# **Importante !!** Al *primer* elemento de la secuencia le corresponde el índice `0` (**no** el 1). Este índice va _incrementándose_ de 1 en 1 hasta el *último* elemento de la secuencia, al cual le corresponderá por tanto un índice igual al número de elementos de la secuencia menos uno (`longitud - 1`).  
# También se puede recurrir al _"índice reverso"_, que comienza en `-1` para el *último* elemento de la secuencia, y va _decrementando_ en una unidad hasta llegar al *primer* elemento, al cual le corresponderá un índice igual al número de elementos de la secuencia en negativo (`-longitud`).

# %%
s = (0, 1, 2, 3)      # Una tupla como ejemplo de secuencia

# %% [markdown] toc-hr-collapsed=true
# Para **acceder** a _cada_ uno de los _objetos_ de la secuencia se indica el *número de índice* entre corchetes `[]` de la siguiente manera:
#
# sequence_name<strong>[</strong>`index_number`<strong>]</strong>&nbsp;&nbsp; ➞ object_at_*index_number*
#
# ![sequence](images/python_intro/sequences_2.png)

# %%
print( s[0] , s[-4] )

print( s[3] , s[-1] )

# %% [markdown]
# <br />
#
# Además, podemos **acceder** a _**intervalos**_ (_slices_) de la secuencia, indicando dentro de los corchetes tanto el _índice inicial_ como el _índice final_, separados por `:`.  
# El _**intervalo** resultante_ es una _nueva secuencia_, del _mismo tipo/clase_ que la secuencia original, y que _contine_ los objectos desde la posición del _índice inicial_ (_incluido_) hasta la posición del _índice final_ (_no incluido_):
#
# sequence_name<strong>[</strong>`start_index`**:**`end_index`<strong>]</strong>&nbsp;&nbsp; ➞ new_sequence_w_objects_from_*start_index*\_(included)\_to_*end_index*\_(not\_included)
#
# ![sequence](images/python_intro/sequences_3.png)

# %%
print( s[1:3] , s[-3:-1] )     # Ojo!: el objeto con índices 3 y -1 no está incluido en las nuevas secuencias

# %% [markdown]
# Si al indicar un _intervalo_ (_slice_) se _**omite**_ el _índice inicial_, se presupone el índice correspondiente al _objeto inicial_, y si se _**omite**_ el _índice final_, se presupone el índice correspondiente al _último objeto_ + 1:

# %%
print( s[:3] )   # Índice inicial omitido ➞ La subsecuencia empieza desde el primer elemento (equivale a índice 0)
print( s[1:] )   # Índice final omitido   ➞ la subsecuencia se extiende hasta el final
print( s[:] )    # Ambos índices omitidos ➞ nueva secuencia con todos los objetos de la original (una copia)

# %% [markdown] jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 En los _intervalos_ (_slices_) de una _secuencia_ se puede especificar un _tercer número_ (_step_, separado de los anteriores por `:`) que indica cada cuántas posiciones se debe coger un elemento de la secuencia:
#
# sequence_name<strong>[</strong>`start_index`:`end_index`**:**`step`<strong>]</strong> ➞ Objetos en el intervalo [*start_index*, *end_index*) cada _step_ posiciones

# %% jupyter={"source_hidden": true} tags=["sabermas"]
print( s[::2] )  # Del inicial al último, de 2 en 2
print( s[::-1] ) # Del inicial al último, al revés

# %% [markdown]
# <br />
#
# Las _secuencias_ también soportan la **concatenación** mediante el operador `+`, para formar una _nueva_ secuencia cuyos elementos son la suma de los objetos de las secuencias concatenadas, en el _orden_ de la concatenación:

# %%
s + s[1:3]

# %%
s[1:3] + s

# %% [markdown]
# <br />
#
# Por último, las _secuencias_ poseen los **métodos** `.count( )` e `.index( )` :
#
#  * ```python 
#    sequence.count(object)
#    ```
#    Devuelve _las veces_ que el objecto (*object*) aparece (está contenido) en la secuencia.
#    

# %%
s.count(3)    # Cuentas/repeticiones del número 3 en la secuencia

# %% [markdown]
#  * ```python 
#    sequence.index(object)
#    ```
#    Devuelve el _primer_ número de _índice_ que le corresponde al objeto (*object*) en la secuencia.

# %%
s.index(3)   # Posición/índice del número 3 en la secuencia

# %% [markdown] toc-hr-collapsed=true
# <br />
#
# En Python hay _diferentes tipos básicos de secuencias_, de los cuales veremos:
#
#  * **Cadenas de caracteres** o ***strings*** (clase **str**)
#  * **Tuplas** de objectos (clase **tuple**)
#  * **Listas** de objectos (clase **list**)

# %% [markdown] toc-hr-collapsed=false
# #### Cadenas de caracteres ( str, '', "" )
#
# Los _textos_ que hemos visto en ejemplos anteriores, son **_secuencias_ inmutables** de **caracteres [Unicode](https://en.wikipedia.org/wiki/Unicode)**, llamadas _**cadenas de caracteres**_ (_strings_, clase **str**).  
# En Python se pueden definir tanto utilizando _comillas simples_ (`' '`) como _comillas dobles_ (`" "`):
# ```python
#     str_name = 'characters...'
#     str_name = "characters..."
# ```

# %%
txt_simples = 'Hello World!'    # Comillas simples '  '
txt_doubles = "Hello World!"    # Comillas dobles "  "

print(type(txt_simples), txt_simples)
print(type(txt_doubles), txt_doubles)

txt_simples == txt_doubles      # Iguales (==) ?

# %%
# Cadenas de caracteres vacías (sin caracteres):

txt_void1 = ''       # Comillas simples ''
txt_void2 = ""       # Comillas dobles ""
txt_void3 = str()    # Llamada a la clase str sin pasar ningún argumento

print(txt_void1)
print(txt_void2)
print(txt_void3)

txt_void1 == txt_void2 == txt_void3      # Iguales (==) ?

# %% [markdown]
# Para incluir algún carácter _comilla simple_ (`'`) en una cadena de caracteres, deberemos utilizar _comillas dobles_ para definir dicha cadena de caracteres, y al revés para incluir algún carácter _comilla doble_ (`"`):

# %%
txt1 = "I'm a string!"      # Comillas dobles para poder incluir comillas simples en la cadena
txt2 = 'He said: "Hello".'  # Comillas simples para poder incluir comillas dobles en la cadena

print(txt1)
print(txt2)

# %% tags=["error"]
txt1 = 'I'm a string!'      # Error!

# %% [markdown]
# Alternativamente, se pueden [_"**escapar**"_](https://en.wikipedia.org/wiki/Escape_character) los caracteres _comillas simples_ y _comillas dobles_, dentro de la cadena de caracteres, anteponiéndoles una _barra invertida (`\`):
#
# "`\'`" ➞ `'`
#
# "`\"`" ➞ `"`
#
# "`\\`" ➞ `\`

# %%
txt1 = 'I\'m a string!'         # \' (comillas simples "escapadas")
txt2 = "He said: \"Hello\"."    # \" (comillas dobles "escapadas")

print(txt1)
print(txt2)

# %% [markdown] tags=["ejercicio"]
# <br />
#
# > 📝 **Práctica:**
# >
# > Define una _cadena de caracteres_ que contenga simultáneamente _comillas simples_ (`'`), _comillas dobles_ (`"`) y una barra invertida (`\`):

# %% tags=["ejercicio"]
txt3 = 

print(txt3)


# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución:

txt3 = "Posible solución al 'ejercicio', incluyendo una \"barra invertida\": \\"

print(txt3)

# %% [markdown] jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 Existen además otros códigos de escape útiles:
#
# `\t` ➞ **t**abulador
#
# `\n` ➞ **n**ueva linea
#
# `\r` ➞ **r**etornno de linea

# %% jupyter={"source_hidden": true} tags=["sabermas"]
line1 = 'a \tb \tc '
line2 = '11\t75\t051'
print(line1)
print(line2)

# %% jupyter={"source_hidden": true} tags=["sabermas"]
twolines = line1 + '\n' + line2
twolines

# %% jupyter={"source_hidden": true} tags=["sabermas"]
print(twolines)

# %% [markdown] toc-hr-collapsed=false
# ##### **Cadenas de caracteres como _contenedores_:**
#
# Dado que las cadenas de caracteres son _**contenedores**_, podemos:
#
#  * Comprobar si un carácter individual **pertenece o no** a una cadena de caracteres mediante los _operadores de pertenencia_ `in` y `not in`:

# %%
txt = 'GARyCAtWEEK'

# %%
# "Y" (mayúscula) se encuentra en la cadena referenciada por la variable `txt` ?
'Y' in txt 

# %%
# "y" (minúscula) no se encuentra en la cadena referenciada por la variable `txt` ?
'y' not in txt 

# %% [markdown]
# <ul><il>En el caso particular de las <em>cadenas de caracteres</em>, <code>in</code> y <code>not in</code> sirven además para comprobar si una <strong><em>sub-cadena</em></strong> forma parte o no de otra cadena:</il></ul>

# %%
# "GARy" forma parte de la cadena referenciada por la variable `txt` ?
'GARy' in txt 

# %%
# "CAT" no forma parte de la cadena referenciada por la variable `txt` ?
'CAT' not in txt 

# %% [markdown]
#  * Utilizar las **funciones internas** `len( )`, `max( )` y `min( )`.

# %%
len(txt)      # Longitud (número de caracteres) de `txt`

# %% tags=["sabermas"]
print("Máximo ( max('" + txt + "') )   :", max(txt) )      # 📌 Carácter de `txt` cuyo valor numérico sea el máximo
print("Mínimo ( min('" + txt + "') )   :", min(txt) )      # 📌 Carácter de `txt` cuyo valor numérico sea el mínimo

# %% [markdown] tags=["ejercicio"]
# <br />
#
# > 📝 **Práctica:**
# >
# > Dada la siguiente _secuencia proteica_ `protein_seq`:

# %% tags=["ejercicio"]
protein_seq = 'MKWVTFISLLFLFSSAYSRGVFRRDAHKSEVAHRFKDLGEENFKALVLIAFAQYLQQCPFEDHVKLVNEVTEFAKTCVADESAENCDKSLHTLFGDKLCTVATLRETYGEMADCCAKQEPERNECFLQHKDDNPNLPRLVRPEVDVMCTAFHDNEETFLKKYLYEIARRHPYFYAPELLFFAKRYKAAFTECCQAADKAACLLPKLDELRDEGKASSAKQRLKCASLQKFGERAFKAWAVARLSQRFPKAEFAEVSKLVTDLTKVHTECCHGDLLECADDRADLAKYICENQDSISSKLKECCEKPLLEKSHCIAEVENDEMPADLPSLAADFVESKDVCKNYAEAKDVFLGMFLYEYARRHPDYSVVLLLRLAKTYETTLEKCCAAADPHECYAKVFDEFKPLVEEPQNLIKQNCELFEQLGEYKFQNALLVRYTKKVPQVSTPTLVEVSRNLGKVGSKCCKHPEAKRMPCAEDYLSVVLNQLCVLHEKTPVSDRVTKCCTESLVNRRPCFSALEVDETYVPKEFNAETFTFHADICTLSEKERQIKKQTALVELVKHKPKATKEQLKAVMDDFAAFVEKCCKADDKETCFAEEGKKLVAASQAALGL'

# %% tags=["ejercicio", "sabermas", "alternativa"] jupyter={"source_hidden": true}
# 📌 Una forma de representar cadenas muy largas es usar paréntesis incluyendo la cadena dividida en subcadenas
# Python lee esta representación como una única cadena:

protein_seq = ("MKWVTFISLLFLFSSAYSRGVFRRDAHKSEVAHRFKDLGEENFKALVLIAFAQYLQQCPFEDHVKLVNEVTEFAKTCVADE"
               "SAENCDKSLHTLFGDKLCTVATLRETYGEMADCCAKQEPERNECFLQHKDDNPNLPRLVRPEVDVMCTAFHDNEETFLKKYL"
               "YEIARRHPYFYAPELLFFAKRYKAAFTECCQAADKAACLLPKLDELRDEGKASSAKQRLKCASLQKFGERAFKAWAVARLSQ"
               "RFPKAEFAEVSKLVTDLTKVHTECCHGDLLECADDRADLAKYICENQDSISSKLKECCEKPLLEKSHCIAEVENDEMPADLP"
               "SLAADFVESKDVCKNYAEAKDVFLGMFLYEYARRHPDYSVVLLLRLAKTYETTLEKCCAAADPHECYAKVFDEFKPLVEEPQ"
               "NLIKQNCELFEQLGEYKFQNALLVRYTKKVPQVSTPTLVEVSRNLGKVGSKCCKHPEAKRMPCAEDYLSVVLNQLCVLHEKT"
               "PVSDRVTKCCTESLVNRRPCFSALEVDETYVPKEFNAETFTFHADICTLSEKERQIKKQTALVELVKHKPKATKEQLKAVMD"
               "DFAAFVEKCCKADDKETCFAEEGKKLVAASQAALGL")

# %% [markdown] tags=["ejercicio"]
# > Y los siguientes _péptidos_:

# %% tags=["ejercicio"]
pep1 = 'KYLYEIAR'
pep2 = 'AFKAWAVAR'
pep3 = 'AFKAWAVARLSQDFPK'
pep4 = 'SHCIAEVENDEMPADLPSLAADFVESK'

# %% [markdown] tags=["ejercicio"]
# > Determina cuales de ellos _pertenecen_ a la proteína con la secuencia `protein_seq`:

# %% tags=["ejercicio"]



# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución:

print(pep1 in protein_seq)
print(pep2 in protein_seq)
print(pep3 in protein_seq)
print(pep4 in protein_seq)

# %% [markdown] tags=["ejercicio"]
# > Determina el _número de aminoácidos_ de la secuencia `protein_seq`:

# %% tags=["ejercicio"]



# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución:

len(protein_seq)

# %% [markdown]
# ##### **Cadenas de caracteres como _secuencias_:**
#
# ![string](images/python_intro/strings_1.png)
#
# Dado que las cadenas de caracteres además de _contenedores_ son _**secuencias** de caracteres_:
#  * Se puede **acceder a sus elementos individuales** (a _cada carácter_ en este caso) usando la notación genérica que hemos descrito previamente para las secuencias en Python:
#
#    string_name<strong>[</strong>`index_number`<strong>]</strong>&nbsp;&nbsp; ➞ Carácter en la posición indicada por *index_number*

# %%
print('0 ➞', txt[0] )   # Recordar que al primer elemento de una secuencia le corresponde el índice 0
print('1 ➞', txt[1] )
print('5 ➞', txt[5] )

# %%
print('-1 ➞', txt[-1] )  # Y que al último elemento de una secuencia le corresponde el índice -1
print('-2 ➞', txt[-2] )
print('-6 ➞', txt[-6] )

# %% [markdown]
#  * Podemos **acceder** a _**intervalos**_ (_slices_) de la secuencia:
#
#    string_name<strong>[</strong>`start_index`**:**`end_index`<strong>]</strong>&nbsp;&nbsp; ➞ Nueva _string_ con los caracteres desde la posición *start_index* (incluido) hasta la posición *end_index* (_no_ incluido)

# %%
print('[0:4]  ➞', txt[0:4] )       # Ojo!: el carácter con índice 4 no está incluido
print('[4,-1] ➞', txt[4:-1] )      # Ojo!: el carácter con índice -1 no está incluido

# %%
print('[:4] ➞', txt[:4] )      # Índice inicial omitido ➞ 0
print('[4:] ➞', txt[4:] )      # Índice final omitido ➞ índice del último elemento + 1
print('[:]  ➞', txt[:] )       # Una copia de toda la cadena de caracteres

# %% jupyter={"source_hidden": true} tags=["sabermas"]
print('Del inicial al último, de 2 en 2 [::2] ➞', txt[::2] )  # 📌 De 2 en 2
print('Del inicial al último, al revés [::-1] ➞', txt[::-1] ) # 📌 Al revés

# %% [markdown]
#  * Soportan la **concatenación** mediante el operador `+` :

# %%
txt4 = "This is the sequence: " + txt
txt5 = txt[:3] + "I" + " is a cat"

print(txt4)
print(txt5)

# %%
# "\n" es un carácter "escapado" equivalente a un salto de línea
txt6 = "1. " + txt + "\n" + "2. " + txt 
print(txt6)

# %% [markdown]
#  * Poseen los **métodos** de _secuencia_ `.count( )` e `.index( )` :

# %%
txt.count('A')    # Cuentas/repeticiones del carácter/elemento "A" en `txt`

# %%
txt.count('CA')   # Cuentas/repeticiones de la sub-cadena "CA" en `txt`

# %%
txt.index('A')    # Posición/índice del carácter/elemento "A" en `txt`

# %%
txt.index('CA')   # Posición/índice inicial de la sub-cadena "CA" en `txt`

# %% [markdown] tags=["ejercicio"]
# <br />
#
# > 📝 **Práctica:**
# >
# > Usando la notación de _índices_, determina el _aminoacido **N-terminal**_ y el _**C-terminal**_ de la _secuencia proteica_ `protein_seq` vista antes:

# %% tags=["ejercicio"]



# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución:

print("N-term:", protein_seq[0])
print("C-term:", protein_seq[-1])

# %% [markdown] tags=["ejercicio"]
# > Cuántas lisinas (**K**) contiene?

# %% tags=["ejercicio"]



# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución:

print("Lysines:", protein_seq.count('K'))

# %% [markdown] tags=["ejercicio"]
# > En qué _posición biológica_ se encuentra el péptido `pep4` dentro de `protein_seq`, y hasta qué posición dentro de esta alcanza?

# %% tags=["ejercicio"]
start_pos = 




# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución:

start_pos = protein_seq.index(pep4) + 1 # Las posiciones biológicas empiezan en 1 (no en 0 como las secuencias en Python)
print("Start position of '" + pep4 + "' in 'protein_seq':", start_pos)

end_pos = start_pos + len(pep4) - 1
print("End position of '" + pep4 + "' in 'protein_seq':", end_pos)

# %% [markdown] tags=["ejercicio"]
# > Demuéstralo obteniendo el _intervalo_ (*slice* correspondiente directamente de `protein_seq`:

# %% tags=["ejercicio"]
protein_seq[ : ] == pep4

# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución:

protein_seq[start_pos-1:end_pos] == pep4

# %% [markdown]
# ##### **Algunos métodos útiles _propios_ de las cadenas de caracteres:**

# %% [markdown]
#  * ```python 
#    str.lower()
#    ```
#    Devuelve una _nueva_ cadena de caracteres con todas las letras en _minúsculas_.
#    
#  * ```python 
#    str.upper()
#    ```
#    Devuelve una _nueva_ cadena de caracteres con todas las letras en _mayúsculas_.

# %%
print('.lower() :', txt.lower() )
print('.upper() :', txt.upper() )

# %% [markdown]
#  * ```python 
#    str.replace(old_substring, new_substring)
#    ```
#    Devuelve una _nueva_ cadenas de caracteres que resulta de _reemplazar_ todas las coincidencias de *old_substring* por *new_substring*.

# %%
txt.replace('WEEK', 'YEAR')    # Nueva cadena con las sub-cadenas "WEEK" reemplazadas por "YEAR"

# %%
txt     # La cadena de caracteres original permanece inmutable

# %%
txt.replace('y', 'Y(Phos)')    # Nueva cadena con los caracteres "y" reemplazados por "Y(Phos)"

# %%
txt.replace('y', 'Y(Phos)').replace('t', 'T(Phos)')

# %% [markdown]
# * ```python 
#    str.strip([characters])
#    ```
#    Devuelve una _nueva_ cadenas de caracteres que resulta de _eliminar_ los caracteres de la cadena *characters* del inicio y del final de la cadena original.  
#    Si _no_ se especifica una cadena *characters*, se eliminan los _"espacios"_, termino que incluye a los *espacios* propiamente (`" "`), a los tabuladores (`"\t"`), a los saltos de línea (`"\n"`) y a los retornos de línea (`"\r"`).

# %%
' \t sobran "espacios" al principio y al final    \n\r'.strip()

# %%
'.ASKPSSTVS.'.strip('.')

# %%
'_[98]SYRPQ[121]WSM[35]_'.strip('[]_0123456789')

# %% [markdown]
#  * ```python 
#    str.split([separator])
#    ```
#    Devuelve la _lista_ de cadenas de caracteres que resultan de _dividir_ la cadena original por el *separator* (un carácter o una sub-cadena). Éste *separator* no formará parte de la lista devuelta.  
#    Si _no_ se especifica un *separator*, se divide la cadena por los _"espacios"_, término que incluye *espacios* (`" "`), tabuladores (`"\t"`), saltos de línea (`"\n"`) y retornos de línea (`"\r"`).

# %%
'\tUn texto\n dividido en sus palabras\n\r'.split()   # Dividir por los "espacios"

# %%
'uno, dos, tres, permanecen,juntos, '.split(', ')     # Dividir por ', ' (coma + espacio)

# %% [markdown]
#  * ```python 
#    str.join(container_of_strings)
#    ```
#    Devuelve una _nueva_ cadenas de caracteres que resulta de _unir_ las cadenas de caracteres del contenedor o iterable *container_of_strings*, usando como nexo entre cadenas la cadena de caracteres original (*str*).

# %%
"; ".join( ['uno', 'dos', 'tres', ''] )    # Unir usando "; " como nexo entre cadenas

# %%
"".join( ['uno', 'dos', 'tres', ''] )      # Equivale a: "uno" + "dos" + "tres" + "" (sin nexo entre cadenas)

# %% [markdown] jupyter={"source_hidden": true} tags=["sabermas"]
# 📌
# * ```python 
#    str.find(sub_string[, start_pos])
#    ```
#    Devuelve el _primer_ número de _índice_ que le corresponde a la sub-cadena (*sub_string*) en la cadena de caracteres.  
#    Es pues similar al resultado devuelto por el método `.index( )`, pero a _diferencia_ de éste, si la sub-cadena *no* se encuentra dentro de la cadena de caracteres, el valor devuelto es *-1* y no lanza un error.  
#    Además, al método `.find( )` se le puede pasar un segundo argumento (parámetro *start_pos*), que especifica la _posición_ de la cadena a partir de la cual se buscará la sub-cadena.

# %% jupyter={"source_hidden": true} tags=["sabermas"]
txt.find('A') # Busca el carácter "A" en `txt` y devuelve la posición de la primera coincidencia ➞ Similar a `txt.index("A")`

# %% jupyter={"source_hidden": true} tags=["sabermas"]
txt.find('A', 2) # Busca el carácter "A" en `txt`, empezando en la posición 2, y devuelve la posición de la siguiente coincidencia

# %% jupyter={"source_hidden": true} tags=["sabermas"]
txt.find('LALALA') # Busca la sub-cadena "LALALA" en `txt` ➞ No encontrado ➞ Devuelve -1

# %% jupyter={"source_hidden": true} tags=["error", "sabermas"]
txt.index('LALALA') # Posición/índice de la sub-cadena "LALALA" en `txt` ➞ No encontrado ➞ ERROR!

# %% [markdown] tags=["ejercicio"]
# <br />
#
# > 📝 **Práctica:**
# >
# > Dado el siguiente _registro de proteína_, `prot_reg`, leído desde un fichero FASTA:

# %% tags=["ejercicio"]
# Contiene meta-datos (primera línea), y la secuencia dividida en varias líneas y 
# con elementos no válidos en dicha secuencia (espacios, *, tabuladores, ...):
prot_reg = """\t >sp|P29762|RABP1_HUMAN Cellular retinoic acid-binding protein 1 OS=Homo sapiens OX=9606 GN=CRABP1 PE=1 SV=2 \t 
              \tMPNFAGTWKMRSSENFDELLKALGVNAMLRKVAVAAASKPHVEIRQDGDQFYIKTSTTVR 
              \tTTEINFKVGEGFEEETVDGRKCRSLATWENENKIHCTQTLLEGDGPKTYWTRELANDELI 
              \tLTFGADDVVCTRIYVRE* """
prot_reg

# %% [markdown] tags=["ejercicio"]
# > **Separar** los _metadatos_ de la _secuencia de aminoácidos_, como nuevas _cadenas_ de caracteres, en variables _independientes_ (`metadata` y `seq`).  
# > Además, estas nuevas cadenas de caracteres han de estar _**limpias**_: sin "espacios" al principio ni al final; y, en el caso de la _secuencia_, sin espacios ni caracteres de escape (`\t` `\n` `\r`) en medio.

# %% tags=["ejercicio"]
protreg_lines = 


# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución por pasos:

protreg_lines = prot_reg.split('\n')

protreg_lines

# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
metadata = protreg_lines[0].strip()[1:]

metadata

# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
seq_lines = protreg_lines[1:]
seq = "".join(seq_lines)
seq

# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
seq = seq.replace(" ", "").replace("\t", "")[:-1]
seq

# %% [markdown]
# ##### **"Conversiones" entre números y cadenas de caracteres:**
#
# Se utilizan directamente las _llamadas_ a los _nombres de clase_ a la cual se desea realizar la conversión (`str( )`, `int( )`, `float( )`) pasándoles como argumento el objeto a convertir, de manera similar a como ya vimos con las [conversiones entre tipos numéricos](#Conversi%C3%B3n-entre-tipos-num%C3%A9ricos):
#
#   
#  * ```python
#    str(object)
#    ```
#    Devuelve una cadena de caracteres a partir de cualquier tipo de objeto.

# %%
str(42)

# %%
"Conversión a string de números: " + ", ".join( [ str(-123), str(2.4E2), str(float('NaN')) ] )

# %% [markdown]
#  * ```python
#    int(string)
#    ```
#    
#    Devuelve un número entero a partir de la cadena de caracteres (_string_). Para ello, por defecto (*base*=10), la cadena de caracteres _sólo_ puede contener dígitos (`"0"`-`"9"`), _"espacios"_ (incluyendo los caracteres de "escape" `"\t"`, `"\n"` y `"\r"`) y el signo menos (`"-"`) delante del primer dígito (para indicar números negativos).

# %%
int('42')

# %%
int(' -123 \n')

# %% tags=["error"]
int('-123.456') # Contiene un punto ➞ Error!

# %% [markdown]
#   
#  * ```python
#    float(string)
#    ```
#    
#    Devuelve un número real en coma flotante a partir de la cadena de caracteres, la cual, además de los caracteres permitidos en el caso anterior, también puede contener el punto decimal (`"."`), la `"E"` para la notación científica, e `"inf"` para _infinito_ y `"nan"` para _Not a Number_.

# %%
float('-123.456')

# %%
float('1E6')

# %%
float('\t -123 ')

# %% [markdown] toc-hr-collapsed=false
# #### Tuplas ( tuple, ( ,) )
#
# Las [_tuplas_](https://es.wikipedia.org/wiki/Tupla) son objetos de la clase **tuple**, que en Python consisten en **_secuencias_ inmutables** de otros **objetos**, pudiendo contener _cualquier tipo de objeto_.  
# Generalmente, se definen utilizando _paréntesis_ y separando los diferentes elementos u objetos por _comas_ `( ,)`:
# ```python
#     tuple_name = (object0, object1, ...)
# ```
# Aunque la siguiente notación, utilizando _sólo comas_ `, ` para separa los objetos (_sin_ usar _paréntesis_), también es válida:
# ```python
#     tuple_name = object0, object1, ...
# ```

# %%
tpl_sngl = ('single',)          # Tupla de un único elemento. 👁 Ojo con la coma!
tpl_nums = 45, 7, 25, 25, 33    # Sin usar paréntesis, sólo comas
tpl_strs = ('ASDRPK', 'VMTTDEK', 'MsWPAy')

print(type(tpl_sngl), tpl_sngl)
print(type(tpl_nums), tpl_nums)
print(type(tpl_strs), tpl_strs)

# %%
tpl_objs = (0, 'Aa', -2.22E2, tpl_strs) # Tupla conteniendo objetos de diferentes tipos, incluyendo otra tupla

print(type(tpl_objs), tpl_objs)

# %% [markdown]
# ![tuple](images/python_intro/tuples_1.png)

# %% [markdown]
# Alternativamente, se puede crear una tupla a partir de otro _contenedor_ o _iterable_, _llamando_ a la clase **tuple**:
# ```python
#     tuple_name = tuple([container_or_iterable])
# ```

# %%
tpl_void = tuple() # Tupla vacía
tpl_chrs = tuple(txt) # Tupla de caracteres a partir de una cadena de caracteres

print(tpl_void)
print(tpl_chrs)

# %% [markdown]
# ##### **Tuplas como _contenedores_:**
#
# Dado que las tuplas son _**contenedores**_, podemos:
#
#  * Comprobar si un objeto **pertenece o no** a una tupla mediante los _operadores de pertenencia_ `in` y `not in`.
#  * Utilizar las **funciones internas** `len()`, `max()` y `min()`.

# %%
# Tests de pertenencia y no pertenencia:
print(tpl_strs)
print("'VMTTDEK' en `tpl_strs`?   ➞",  'VMTTDEK' in tpl_strs )
print("'MsWPAy' no en `tpl_strs`? ➞",  'MsWPAy' not in tpl_strs )
print("`tpl_strs` en `tpl_objs`?  ➞",  tpl_strs in tpl_objs )

# %%
# Funciones `len( )`, `max( )` y `min( )` :
print(tpl_nums)
print('len : ', len(tpl_nums) )   # Longitud (número de objetos) de la tupla
print('max : ', max(tpl_nums) )   # Objeto de la tupla con valor máximo
print('min : ', min(tpl_nums) )   # Objeto de la tupla con valor mínimo

# %% [markdown]
# ##### **Tuplas como _secuencias_:**
#
# Y como además las tuplas son _**secuencias** de objetos_, podemos:
#
#  * **Acceder a sus elementos individuales** mediante un **índice** ( tuple_name<strong>[</strong>`index_number`<strong>]</strong> ).
#  * **Acceder a intervalos** ( tuple_name<strong>[</strong>`start_index`**:**`end_index`<strong>]</strong> ).
#  * **Concatenar** diversas tuplas usando el operador `+`.
#  * Utilizar los **métodos** `.count( )` e `.index( )`.

# %%
tpl_objs

# %%
# Acceso a elementos individuales:
print(' 0 ➞', tpl_objs[0] )     # Al primer elemento de una secuencia le corresponde el índice 0
print('-1 ➞', tpl_objs[-1] )    # Al último elemento de una secuencia le corresponde el índice -1

# %%
# Accediendo a elementos de sub-secuencias:
print('-1 ➞ 2 ➞', tpl_objs[-1][2] )            # Acceso a la cadena en posición 2 del la tupla en posición -1
print('-1 ➞ 2 ➞ 5 ➞', tpl_objs[-1][2][5] )    # Acceso al carácter 5 de la cadena 2 del la tupla en -1

# %%
# Acceso a intervalos (slicing):
print('[0:3] ➞', tpl_objs[0:3] )      # Del 0 al 2
print('[:-1] ➞', tpl_objs[:-1] )      # Del inicial al penúltimo

# %%
# Concatenación:
print( tpl_strs + ('AAAAAA',) )
print( tpl_nums + tpl_strs )

# %%
# Métodos `.count( )` e `.index( )` :
print(tpl_objs)
print('count :', tpl_objs.count(0) )        # Veces que aparece el entero 0
print('index :', tpl_objs.index(tpl_strs) ) # Primera posición en que aparece la tupla `tpl_strs`

# %% [markdown]
# ##### **Inmutabilidad**:
#
# Las tuplas son como _listas inmutables_: no se pueden **ni añadir**, **ni eliminar** _elementos_ de una tupla, por lo que su _longitud_, el _orden_ de sus elementos y los _elementos_ que contiene son _inalerables_:

# %% tags=["error"]
tpl_objs[-1] = tpl_nums # Intento de re-asignación de un elemento ➞ Error!

# %% tags=["error"]
del tpl_objs[0] # Intento de eliminación de un elemento ➞ Error!

# %% [markdown] tags=["ejercicio"]
# <br />
#
# > 📝 **Práctica:**
# >
# > Las siguientes tuplas contienen los _péptidos_ de interés (`_peps`), y sus correspondientes _PSMs_ (`_psms`), que se han identificado en dos réplicas (`rpl1_` y `rpl2_` ) de un mismo experimento:

# %% tags=["ejercicio"]
# Replica 1:
rpl1_peps = ('KYLYEIAR', 'AFKAWAVAR', 'DVCKNYAEAR', 'LSQDFPK')
rpl1_psms = (         2,           4,            3,         7)
# Replica 2:
rpl2_peps = ('AFKAWAVARLSQDFPK', 'SHCIAEVENDEMPADLPSLAADFVESK', 'DVCKNYAEAR')
rpl2_psms = (                 3,                             2,            4)

# %% [markdown] tags=["ejercicio"]
# > Determina en _**cuántas** réplicas_ se han identificado cada uno de los péptidos de la siguiente tupla `check_peps`:

# %% tags=["ejercicio"]
check_peps = 'AFKAWAVAR', 'DVCKNYAEAR'


# %% tags=["ejercicio"]



# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución:

check_pep1, check_pep2 = check_peps

all_rpl_peps = rpl1_peps + rpl2_peps

# check_pep1 :
print("Péptido '" + check_pep1 + "' identificado en", all_rpl_peps.count(check_pep1), "réplicas.")

# check_pep2 :
print("Péptido '" + check_pep2 + "' identificado en", all_rpl_peps.count(check_pep2), "réplicas.")

# %% [markdown] tags=["ejercicio"]
# > En _**qué** réplicas_ se han identificado cada uno de estos dos péptidos (`check_peps`)? :

# %% tags=["ejercicio"]



# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución:

# check_pep1 :
print("Péptido '" + check_pep1 + "' en Replica 1? ➞", check_pep1 in rpl1_peps)
print("Péptido '" + check_pep1 + "' en Replica 2? ➞", check_pep1 in rpl2_peps)

# check_pep2 :
print("Péptido '" + check_pep2 + "' en Replica 1? ➞", check_pep2 in rpl1_peps)
print("Péptido '" + check_pep2 + "' en Replica 2? ➞", check_pep2 in rpl2_peps)

# %% [markdown] tags=["ejercicio"]
# > Y cuántas _PSMs suman en total_ (en _todo el experimento_) cada uno de estos dos péptidos (`check_peps`)? :

# %% tags=["ejercicio"]



# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución:

# check_pep1 :
pep1_in_rpl1 = rpl1_peps.index(check_pep1)

checkpep1_psms = rpl1_psms[pep1_in_rpl1]

print("PSMs totales del péptido '" + check_pep1 + "' ➞", checkpep1_psms)

# check_pep2 :
pep2_in_rpl1 = rpl1_peps.index(check_pep2)
pep2_in_rpl2 = rpl2_peps.index(check_pep2)

checkpep2_psms = rpl1_psms[pep2_in_rpl1] + rpl2_psms[pep2_in_rpl2]

print("PSMs totales del péptido '" + check_pep2 + "' ➞", checkpep2_psms)

# %% [markdown] tags=["ejercicio"]
# > Ahora, _en cada una de las réplicas_, qué _péptido_ de la réplica ha sido identificado con _mayor número de PSMs_? :

# %% tags=["ejercicio"]



# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución:

# Réplica 1 :
rpl1_maxpsm = max(rpl1_psms)
rpl1_maxpsm_idx = rpl1_psms.index(rpl1_maxpsm)
print("Péptido con más PSMs (" + str(rpl1_maxpsm) + ") en la réplica 1:", rpl1_peps[rpl1_maxpsm_idx] )

# Réplica 2 :
rpl2_maxpsm = max(rpl2_psms)
rpl2_maxpsm_idx = rpl2_psms.index(rpl2_maxpsm)
print("Péptido con más PSMs (" + str(rpl2_maxpsm) + ") en la réplica 2:", rpl2_peps[rpl2_maxpsm_idx] )

# %% [markdown] toc-hr-collapsed=false
# #### Listas ( list, [ ] )
#
# Los objetos de la clase **list** son **_secuencias_ mutables** de otros **objetos**, pudiendo contener _cualquier tipo de objeto_.  
# Podemos considerar las _listas_ como el equivalente _mutable_ (alterable) de las tuplas, ya que al contrario que estas últimas, si se pueden _añadir_, _eliminar_  y _substituir_ elementos de una lista.
#
# Se definen utilizando _corchetes_ (imprescindibles en este caso) y separando los diferentes elementos u objetos por _comas_ `[ ,]`:  
# ```python
#     list_name = [object0, object1, ...]
# ```

# %%
lst_void = []              # Lista vacía
lst_sngl = ['single']      # Lista de un elemento (no hace falta una coma final como con las tuplas de 1 elemento)
lst_nums = [3.3, -1, 2, 5, 7.0, 4, 6]
lst_strs = ['ASDRPK', 'VMTTDEK', 'MsWPAy']

print(type(lst_void), lst_void)
print(type(lst_sngl), lst_sngl)
print(type(lst_nums), lst_nums)
print(type(lst_strs), lst_strs)

# %%
# Lista conteniendo objetos de diferentes tipos, incluyendo otra lista
lst_objs = [0, 'aA', 2.0, tpl_strs, lst_strs] 

print(type(lst_objs))
print(lst_objs)

# %% [markdown] tags=["sabermas"]
# ![list](images/python_intro/lists_1.png)

# %% [markdown]
# Alternativamente, se puede _crear_ una lista a partir de otro _contenedor_ o _iterable_, _llamando_ a la clase **list**:
# ```python
#     list_name = list([container_or_iterable])
# ```

# %%
lst_void = list()         # Lista vacía ( 🔄 alternativa a usar [] )
lst_chrs = list(txt)      # Lista (mutable) de caracteres a partir de una cadena de caracteres (inmutable)
lst_ints = list(tpl_nums) # Lista (mutable) a partir de una tupla (inmutable)

print(lst_void)
print(lst_chrs)
print(lst_ints)

# %% [markdown]
# ##### **Listas como _contenedores_:**
#
# Al igual que con otros _**contenedores**_ que ya hemos visto, con las listas también podemos:
#
#  * Comprobar si un objeto **pertenece o no** a una lista (mediante los operadores de _pertenencia_ `in` y `not in`).
#  * Utilizar las **funciones internas** `len()`, `max()` y `min()`.

# %%
# Tests de pertenencia y no pertenencia:
print(lst_strs)
print("'VMTTDEK' en `lst_strs`?   ➞",  'VMTTDEK' in lst_strs )
print("'MSWPAY' no en `lst_strs`? ➞",  'MSWPAY' not in lst_strs )

# %%
# Funciones `len()`, `max()` y `min()` :
print(lst_nums)
print('len( ) : ', len(lst_nums) ) # Longitud (número de objetos) de la lista
print('max( ) : ', max(lst_nums) ) # Objeto de la lista con valor máximo
print('min( ) : ', min(lst_nums) ) # Objeto de la lista con valor mínimo

# %% [markdown]
# ##### **Listas como _secuencias_:**
#
# Como con otras _**secuencias**_ que ya hemos visto (cadenas de caracteres y tuplas), con las listas también podemos:
#
#  * **Acceder a sus elementos individuales** mediante un **índice** ( list_name<strong>[</strong>`index_number`<strong>]</strong> ).
#  * **Acceder a intervalos** ( list_name<strong>[</strong>`start_index`**:**`end_index`<strong>]</strong> ).
#  * **Concatenar** diversas listas usando el operador `+`.
#  * Utilizar los **métodos** `.count()` e `.index()`.

# %%
# Acceso a elementos individuales y de sub-secuencias:
print(lst_objs)
print(' 4 ➞',            lst_objs[4] )            # Acceso al elemento en posición 4
print(' 3 ➞ -1 ➞',      lst_objs[3][-1] )        # Acceso a la cadena en posición -1 de la tupla en posición 3
print('-1 ➞  1 ➞ 0 ➞', lst_objs[-1][1][0] )     # Acceso al carácter 0 de la cadena 1 de la lista en -1

# %%
# Acceso a intervalos (slicing):
print('[:-1] ➞', lst_objs[:-1] )    # Del inicial al penúltimo

# %%
# Concatenación:
print( lst_strs + ['AAAA'] )
print( ['AAAA'] + lst_strs )

# %%
# Métodos `.count()` e `.index()` :
print(lst_nums)
print('count :', lst_nums.count(25) )    # Veces que está presente el objeto 25 en la lista
print('index :', lst_nums.index(7.0) )   # Posición en que el objeto 7.0 aparece por primera vez

# %% [markdown]
# ##### **Mutabilidad:**
#
# Pero, al contrario de las secuencias vistas hasta ahora, las listas son _**mutables**_, por lo que la _longitud_, el _orden_ de los elementos, y los _elementos_ que contiene una lista pueden ser _alterados_.
#
# Así, se pueden **añadir** objetos a una lista, usando para ello los siguientes *métodos propios* de las listas `.append( )` `.extend( )` e `.insert( )`:
#
#  * ```python 
#    list.append(object)
#    ```
#    _Añade_ un objeto (*object*) al _final_ de la lista.
#    

# %%
print(lst_strs, len(lst_strs))    # Original

lst_strs.append('AAAA')         # Añadimos un nuevo péptido al final

print(lst_strs, len(lst_strs))    # 'appended'

# %% [markdown]
#  * ```python 
#    list.extend(container)
#    ```
#    _Añade_ los objetos que forman parte del contenedor (*container*) al _final_ de la lista.

# %%
lst_strs.extend( ['CRPHWK', 'AAAA', 'XXXX'] ) # Añadir varios péptidos al final
print(lst_strs, len(lst_strs))                  # 'extended

# %% [markdown]
#  * ```python 
#    list.insert(index, object)
#    ```
#    _Inserta_ el objeto (*object*) en la posición indicada (*index*).

# %%
lst_strs.insert(0, 'AAAA')     # Inserta un objeto al principio (índice 0) de la lista
print(lst_strs, len(lst_strs))

# %% [markdown]
# <br />
#
# También se pueden **eliminar** objetos de una lista, usando el comando `del`, o los métodos `.remove( )` y `.pop( )` de los objetos de tipo lista:
#  * ```python 
#    del list_name[index_number]
#    ```
#    _Elimina_ el objeto de la lista en la posición indicada (*index_number*).
#  * ```python 
#    del list_name[start_index:end_index]
#    ```
#    _Elimina_ los objeto de la lista comprendidos entre las posiciones *start_index* (incluido) y *end_index* (no incluido). Es como eliminar un intervalo/*slice* de la lista.

# %%
del lst_strs[4]       # Elimina el objeto en quinta posición (índice 4)
print(lst_strs, len(lst_strs))

# %% [markdown]
#  * ```python 
#    list.remove(object)
#    ```
#    _Elimina_ la primera aparición del objeto (*object*) de la lista, pero sólo si éste estaba presente.  
#    En caso de que el objeto no esté en la lista, se mostrará un error (*ValueError*).

# %%
lst_strs.remove('AAAA')     # Elimina la primera aparición de 'AAAAAA' usando el método `.remove( )`
print(".remove( )  :", lst_strs, len(lst_strs))

lst_strs.remove('AAAA')     # Elimina la siguiente aparición de 'AAAAAA'
print(".remove( )  :", lst_strs, len(lst_strs))

# %% tags=["error"]
lst_strs.remove('AAAA') # Ya no quedan más péptidos 'AAAAAA' ➞ Error al intentar eliminarlo!

# %% [markdown] jupyter={"source_hidden": true} tags=["sabermas"]
# 📌
#  * ```python 
#    list.pop([index_number])
#    ```
#    _Extrae_ el objeto de la lista en la posición indicada (*index_number*), y nos lo _devuelve_. En caso de no indicar ninguna posición, por defecto _extrae_ el último objeto de la lista (índice `-1`).

# %% jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 Extrae el objeto en segunda posición (índice 1):
poped_obj = lst_strs.pop(1)

print(".pop( )     :", lst_strs, len(lst_strs))
print("poped object:", poped_obj)

# %% jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 Extrae el objeto en última posición (índice -1):
poped_obj = lst_strs.pop()

print(".pop()      :", lst_strs, len(lst_strs))
print("poped object:", poped_obj)

# %% [markdown]
# <br />
#
# Y, se pueden **reasignar/substituir** elementos de una lista, usando la siguiente notación:
# ```python
#     list_name[index_number] = object
# ```

# %%
lst_strs[1] = lst_strs[1].lower()

print('Reasignación :', lst_strs, len(lst_strs))

# %% [markdown] tags=["ejercicio"]
# <br />
#
# > 📝 **Práctica:**
# >
# > Retomemos las tuplas de la _práctica anterior_, que contienen los _péptidos_ de interés (`_peps`), y sus correspondientes _PSMs_ (`_psms`), que se han identificado en dos réplicas (`rpl1_` y `rpl2_` ) de un mismo experimento:

# %% tags=["ejercicio"]
# Réplica 1:
rpl1_peps = ('KYLYEIAR', 'AFKAWAVAR', 'DVCKNYAEAR', 'LSQDFPK')
rpl1_psms = (         2,           4,            3,         7)
# Réplica 2:
rpl2_peps = ('AFKAWAVARLSQDFPK', 'SHCIAEVENDEMPADLPSLAADFVESK', 'DVCKNYAEAR')
rpl2_psms = (                 3,                             2,            4)

# %% [markdown] tags=["ejercicio"]
# > A partir de estas tuplas, generar dos _listas_:
# >  * `exp_peps` : con todos los *péptidos* de todas las réplicas.
# >  * `exp_psms` : con todas las *PSMs* correspondientes a los péptidos de `exp_peps`.

# %% tags=["ejercicio"]
exp_peps = 
exp_psms =

print(exp_peps)
print(exp_psms)


# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución:

exp_peps = list(rpl1_peps + rpl2_peps)
exp_psms = list(rpl1_psms + rpl2_psms)

print(exp_peps)
print(exp_psms)

# %% [markdown] tags=["ejercicio"]
# > **Añade** _al final_ de la lista `exp_peps` el péptido `MEEEEK`, y sus `3` PSMs al final de `exp_psms`:

# %% tags=["ejercicio"]



# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución:

exp_peps.append('MEEEEK')
exp_psms.append(3)

print(exp_peps)
print(exp_psms)

# %% [markdown] tags=["ejercicio"]
# > **Elimina** de las listas anteriores el péptido `SHCIAEVENDEMPADLPSLAADFVESK` y su correspondiente número de PSMs:

# %% tags=["ejercicio"]



# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución:

pep_idx = exp_peps.index('SHCIAEVENDEMPADLPSLAADFVESK')

exp_peps.remove('SHCIAEVENDEMPADLPSLAADFVESK') # 🔄 O bien: del exp_peps[pep_idx]
del exp_psms[pep_idx] # 🔄 O bien: exp_psms.pop(pep_idx)

print(exp_peps)
print(exp_psms)

# %% [markdown] tags=["ejercicio"]
# > **Añade** a las listas anteriores (`exp_peps` y `exp_psms`) todos los elementos de una nueva réplica (elementos de las tuplas `rpl3_peps` y `rpl3_psms`):

# %% tags=["ejercicio"]
# Réplica 3:
rpl3_peps = ('AWAVAR', 'PSLAADFVESK', 'DVCKNYAEAR', 'LSQDFPK')
rpl3_psms = (       3,            2,            4,         6)

# %% tags=["ejercicio"]



# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución:

exp_peps.extend(rpl3_peps)
exp_psms.extend(rpl3_psms)

print(exp_peps)
print(exp_psms)

# %% [markdown] tags=["ejercicio"]
# > **Reemplaza** las _PSMs_ correspondientes al péptido `AWAVAR` por el valor _mínimo_ de todas las PSMs:

# %% tags=["ejercicio"]



# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución:

pep_idx = exp_peps.index('AWAVAR')
exp_psms[pep_idx] = min(exp_psms)

print(exp_psms)

# %% [markdown] tags=["ejercicio"]
# > Cómo sería el péptido resultante de la unión de los _3 últimos péptidos_ de `exp_peps`? Cuántos _aminoácidos_ tendría?:

# %% tags=["ejercicio"]



# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución:

pep = "".join( exp_peps[-3:] )

print(pep, len(pep))

# %% [markdown] tags=["ejercicio"]
# > **Reemplaza** la _segunda_ `A` del _quinto péptido_ de la lista `exp_peps` por una `X`; usando *una* única línea de código:

# %% tags=["ejercicio"]



# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución:

exp_peps[4] = exp_peps[4][:3] + "X" + exp_peps[4][4:]

exp_peps

# %% [markdown] jupyter={"source_hidden": true} tags=["sabermas"] toc-hr-collapsed=true
# ##### 📌 **Otros métodos útiles propios de las listas:**
#
#    
#  * ```python 
#    list.sort(reverse=False)
#    ```
#    _Ordena_ los objetos de la lista _in situ_. Salvo que se indique lo contrario (*reverse*=True), por defecto ordena los objetos de _menor a mayor_.
#    

# %% jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 Ejemplos del método `.sort( )`:

print('Original            :', lst_nums)

lst_nums.sort() # Ordena la lista in situ, de menor a mayor
print('.sort()             :', lst_nums)

lst_nums.sort(reverse=True) # Ordena la lista in situ, de mayor a menor
print('.sort(reverse=True) :', lst_nums)

# %% [markdown] jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 
#  * ```python 
#    list.reverse()
#    ```
#    _Invierte_ el orden actual de los objetos de la lista _in situ_. De manera que el primer objeto pasa a ser el último, y el último pasa a ser el primero.

# %% jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 Ejemplo del método `.reverse()`:

print('Original   :', lst_strs)

lst_strs.reverse() # Invierte el orden actual de la lista in situ
print('.reverse() :', lst_strs)

# %% [markdown] jupyter={"source_hidden": true} tags=["sabermas"] toc-hr-collapsed=true
# 📌 
#  * ```python 
#    list.copy()
#    ```
#    Devuelve una _nueva_ lista que contiene los mismos objetos que la lista original.

# %% jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 Ejemplo del método `.copy()`:

lst_copy = lst_sngl.copy()

print('Original :', lst_sngl, id(lst_sngl))
print('.copy()  :', lst_copy, id(lst_copy))

# %% jupyter={"source_hidden": true} tags=["sabermas"]
lst_sngl == lst_copy # El contenido de las listas es el mismo...

# %% jupyter={"source_hidden": true} tags=["sabermas"]
lst_sngl is lst_copy # ...Pero son objetos diferentes.

# %% jupyter={"source_hidden": true} tags=["sabermas"]
# La lista original y su copia son independientes:
lst_copy.append('another')

print('Original :', lst_sngl, id(lst_sngl))
print('.copy()  :', lst_copy, id(lst_copy))

# %% [markdown] toc-hr-collapsed=false
# ### Conjuntos ( set, { } )
#
# Los objetos de la clase **set** son **_contenedores_ mutables no ordenados** de _objetos_ **únicos** (sin duplicados), pudiendo pues contener _sólo_ objetos que permitan calcular su _unicidad_ (generalmente objetos _inmutables_: ver [_hashable_](https://docs.python.org/3/glossary.html#term-hashable) para más información).
#
# Se definen utilizando _llaves_ y separando los diferentes elementos/objetos que lo forman por _comas_ `{ ,}`:
# ```python
#     set_name = {hashable_object0, hashable_object1, ...}
# ```

# %%
st_sngl = {'single'}        # Conjunto de un sólo elemento (no hace falta coma final como con las tuplas)
st_nums = {1, 2, 1, 4, 6, 4, 3, 3, 3}
st_objs = {0, 'Aa', -2.0, ('x', 'y'), 0.0, ('x', 'y')}    # Sólo objetos que permitan calcular su "unicidad"

print(type(st_sngl), st_sngl)
print(type(st_nums), st_nums)
print(type(st_objs), st_objs)

# %% [markdown]
# ![set](images/python_intro/sets_1.png)

# %% [markdown] toc-hr-collapsed=false
# Alternativamente, se pueden definir conjuntos a partir de otros _contenedores_ o _iterables_ (como las _secuencias_), llamando a la clase **set**:
# ```python
#     set_name = set([container_or_iterable])
# ```

# %%
st_void = set()    # Conjunto vacío
st_chrs = set('No duplicated characters')        # De cadena de caracteres a conjunto de caracteres
st_strs = set( lst_strs + ['MsWPAy', 'MsWPAy'] ) # De lista a conjunto

print(type(st_void), st_void)
print(type(st_chrs), st_chrs)
print(type(st_strs), st_strs)

# %% [markdown]
# <br />
#
# Es importante observar como, efectivamente, los conjuntos/_sets_ **no** contienen objetos **duplicados** (sólo objetos _únicos_), y **no** mantienen el **orden** de los objetos según como han sido introducidos ni ninguna otra ordenación (_**no** son **secuencias**_).

# %% [markdown]
# Adicionalmente, los conjuntos **no** pueden contener objetos como las _listas_ u otros _sets_, ya que éstos tipos de objetos son _mutables_ por lo que su contenido podría variar y por tanto lo que los hace o no únicos (su _unicidad_) podría variar también:

# %% tags=["error"]
st_lsts = {'0', 1, [2, 3]}     # Intento crear un set conteniendo listas ➞ Error!

# %% tags=["error"]
st_sets = {0, {1, 2, 1}, '3'}   # Intento crear un set conteniendo otros sets ➞ Error!

# %% [markdown]
# <br />
#
# Y, a la hora de realizar _**comparaciones**_ (mediante los _operadores de comparación_), mientras en las _**secuencias**_ importan tanto los _objetos_ que las forman, como la _ordenación_ de estos, en la _comparación_ de **conjuntos** _sólo_ importan los _objetos_ constituyentes y _no_ su ordenación (ya que _no_ poseen ordenación):

# %%
# Secuencias ➞ Importa la ordenación.
[1, 2, 3] == [3, 2, 1]                

# %%
# Conjuntos ➞ No importa la ordenación.
{1, 2, 3} == {3, 2, 1} == {2, 3, 1}   

# %% [markdown]
# #### Conjuntos como contenedores
#
# Al igual que con otros _**contenedores**_ con los conjuntos podemos:
#
#  * Comprobar si un objeto **pertenece o no** a un conjunto (mediante los operadores de _pertenencia_ `in` y `not in`).
#  * Utilizar las **funciones internas** `len( )`, `max( )` y `min( )`.

# %%
# Tests de pertenencia y no pertenencia:
print(st_strs)
print("'VMTTDEK'  en  `st_strs`? ➞",  'VMTTDEK' in st_strs )
print("'AsDRPK' no en `st_strs`? ➞",  'AsDRPK' not in st_strs )

# %%
# Funciones `len( )`, `max( )` y `min( )` :
print(st_nums)
print('len() :', len(st_nums) )   # Longitud (número de objetos) del conjunto
print('max() :', max(st_nums) )   # Objeto del conjunto con valor máximo
print('min() :', min(st_nums) )   # Objeto del conjunto con valor mínimo

# %% [markdown]
# #### Mutabilidad
#
# Podemos **añadir** objetos a un conjunto, usando para ello los _métodos propios_ de los conjuntos `.add()` y `.update()`; siempre y cuando los objetos a añadir _no_ formen parte de dicho conjunto (y además permitan calcular su unicidad):
#
#  * ```python 
#    set.add(object)
#    ```
#    _Añade_ un objeto (*object*) al conjunto, si éste aún no formaba parte del conjunto.   

# %%
print('Original  :', st_strs)

st_strs.add('AAAA') # Añadir un nuevo objeto usando el método `.add( )`

print('.add()    :', st_strs)

st_strs.add('MsWPAy') # Añadir un objeto ya contenido en el conjunto ➞ no se añade

print('.add()    :', st_strs)

# %% [markdown]
#  * ```python 
#    set.update(container)
#    ```
#    _Añade_ al conjunto aquellos objetos del contenedor (*container*) que aún _no_ formen parte del conjunto.

# %%
st_strs.update( ['AAAA', 'VMTTDEK', 'KLIsTAK'] ) # Sólo se añadirán los elementos nuevos (no 'AAAAAA').

print('.update()  :', st_strs)

# %% [markdown]
# <br />
#
# También podemos **eliminar** objetos de un conjunto, utilizando los métodos `.remove( )` o `.pop( )`:
#  * ```python 
#    set.remove(object)
#    ```
#    _Elimina_ el objeto indicado (*object*) del conjunto, si éste estaba presente. En caso de no estar presente, muestra un error (_KeyError_).

# %%
st_strs.remove('AAAA')    # Eliminar un objeto presente en el conjunto usando el método `.remove( )`

print('.remove()  :', st_strs)

# %% tags=["error"]
st_strs.remove('AAAA')   # Eliminar un objeto no presente en el conjunto ➞ Error!

# %% [markdown] jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 
#  * ```python 
#    set.pop()
#    ```
#    _Extrae_ un objeto arbitrario del conjunto (_eliminándolo_ del conjunto), y nos lo _devuelve_.  
#    En caso de que no queden objetos en el conjunto (el conjunto está _vacío_) muestra un error (_KeyError_).

# %% jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 Extraer un objeto arbitrario del conjunto usando el método `.pop()`:
poped_obj = st_strs.pop()

print(".pop()      :", st_strs)
print("poped object:", poped_obj)

# %% jupyter={"source_hidden": true} tags=["error", "sabermas"]
# 📌 Extraer un objeto de un conjunto vacío ➞ Error!
set().pop()

# %% [markdown]
# #### Otros métodos útiles propios de los conjuntos
#
#  * ```python 
#    set.union(container)
#    ```
#    Devuelve un _nuevo_ conjunto que contiene *todos* los elementos _únicos_ del conjunto original más los del contenedor (*container*).
#    
#  * ```python 
#    set.intersection(container)
#    ```
#    Devuelve un _nuevo_ conjunto que contiene sólo los elementos _únicos_ *comunes* entre el conjunto original y el contenedor (*container*).
#
#  * ```python 
#    set.difference(container)
#    ```
#    Devuelve un _nuevo_ conjunto que contiene sólo los elementos _únicos_ del conjunto original (*set*) que *no* estén presentes en el contenedor (*container*).
#    
#  * ```python 
#    set.symmetric_difference(container)
#    ```
#    Devuelve un _nuevo_ conjunto que contiene los elementos _únicos_ del conjunto original (*set*) que *no* estén presentes en el contenedor (*container*), más los elementos _únicos_ del contenedor que *no* estén presentes en el conjunto original. Es pues _complementario_ al conjunto intersección.
#
# ![set_methods](images/python_intro/sets_2.png)

# %%
{2, 1, 4, 6, 3}.union( [1, 4, 5, 6, 7] )        # Los elementos comunes no aparecen duplicados en el nuevo set


# %%
{2, 1, 4, 6, 3}.intersection( {1, 4, 5, 6, 7} ) # Sólo los elementos comunes a ambos

# %%
{2, 1, 4, 6, 3}.difference( (1, 4, 5, 6, 7) )

# %%
{1, 4, 5, 6, 7}.difference( (2, 1, 4, 6, 3) )

# %%
{2, 1, 4, 6, 3}.symmetric_difference( (1, 4, 5, 6, 7) ) # Sólo los elementos no comunes de ambos

# %% [markdown] jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 
#  * ```python 
#    set.copy()
#    ```
#    Devuelve un _nuevo_ conjunto que contiene los mismos objetos que el conjunto original.

# %% jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 Ejemplo del método `.copy()`:
st_copy = st_sngl.copy()

print('Original:', st_sngl, id(st_sngl))
print('.copy() :', st_copy, id(st_copy))

# %% jupyter={"source_hidden": true} tags=["sabermas"]
print(st_sngl == st_copy) # El contenido de los conjuntos es el mismo...
print(st_sngl is st_copy) # ...Pero son objetos diferentes.

# %% jupyter={"source_hidden": true} tags=["sabermas"]
# El conjunto original y su copia son independientes:
st_copy.add('another')

print('Original:', st_sngl, id(st_sngl))
print('.copy() :', st_copy, id(st_copy))
st_sngl == st_copy

# %% [markdown] tags=["ejercicio"]
# <br />
#
# > 📝 **Práctica:**
# >
# > Dada la siguiente _lista de PSMs_ `exp1_psms`:

# %% tags=["ejercicio"]
# PSMs del experimento 1:
exp1_psms = ['KYLYEIAR', 'AFKAWAVAR', 'DVCKNYAEAR', 'LSQDFPK', 'AFKAWAVARLSQDFPK', 'AFKAWAVAR', 
             'KYLYEIAR', 'SHCIAEVENDEMPADLPSLAADFVESK', 'DVCKNYAEAR', 'LSQDFPK']

# %% [markdown] tags=["ejercicio"]
# > Cuántos _PSMs_ totales contiene? Y cuántos _péptidos_?

# %% tags=["ejercicio"]
exp1_peps = 




# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución:

exp1_peps = set(exp1_psms)

print("PSMs totales:", len(exp1_psms))
print("Péptidos totales:", len(exp1_peps))

# %% [markdown] tags=["ejercicio"]
# > **Añade** el péptido `MEEEEK`, y **elimina** el péptido `SHCIAEVENDEMPADLPSLAADFVESK` del _conjunto_ de péptidos `exp1_peps`:

# %% tags=["ejercicio"]



# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución paso a paso:

exp1_peps.add('MEEEEK')
exp1_peps

# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
exp1_peps.remove('SHCIAEVENDEMPADLPSLAADFVESK')
exp1_peps

# %% [markdown] tags=["ejercicio"]
# > **Añade** al conjunto de péptidos (`exp1_peps`) todos los péptidos de la _nueva réplica_ `rpl3_psms`.  
# > Cuántos _nuevos péptidos_ se han añadido?

# %% tags=["ejercicio"]
# Replica 3:
rpl3_psms = ('AWAVAR', 'PSLAADFVESK', 'DVCKNYAEAR', 'LSQDFPK')

# %% tags=["ejercicio"]



# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución:

old_npeps = len(exp1_peps) # Número de péptidos antes de modificar el set

exp1_peps.update(rpl3_psms)

print(exp1_peps)
print("Número de nuevos péptidos:", len(exp1_peps) - old_npeps)

# %% [markdown] tags=["ejercicio"]
# > Dado los siguientes resultados de un nuevo _experimento_ (`exp2_psms`):

# %% tags=["ejercicio"]
# PSMs del experimento 2:
exp2_psms = ['METVKK', 'AFKAWAVARLSQDFPK', 'AFKAWAVAR', 'DVCKNYAEAR', 'AWAVARLSQDFPK', 
             'AFKAWAVAR', 'KYLYEIAR', 'SHCIAEVENDE', 'DVCKNYAEAR', 'KYLYEIAR']

# %% [markdown] tags=["ejercicio"]
# > **Compara** los péptidos de este nuevo experimento (`exp2_psms`) con el conjunto de péptidos  del experimento 1 (`exp1_peps`): péptidos _comunes_, péptidos _no comunes_ y péptidos _exclusivos_ de _cada experimento_.

# %% tags=["ejercicio"]



# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución paso a paso:

common_peps = exp1_peps.intersection(exp2_psms) # Péptidos comunes

print("Número de péptidos comunes:", len(common_peps))
common_peps

# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
uncommon_peps = exp1_peps.symmetric_difference(exp2_psms) # Péptidos no comunes (únicos de cada experimento)

print("Número de péptidos no comunes:", len(uncommon_peps))
uncommon_peps

# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
exclusive_exp1peps = exp1_peps.difference(exp2_psms)
exclusive_exp2peps = set(exp2_psms).difference(exp1_psms) # `exp2_psms` es una lista que hay que convertir primero en set para poder usar el método `.difference( )`

print("Número de péptidos exclusivos del experimento 1:", len(exclusive_exp1peps))
print(exclusive_exp1peps)
print("Número de péptidos exclusivos del experimento 2:", len(exclusive_exp2peps))
print(exclusive_exp2peps)

# %% [markdown]
# ### Diccionarios ( dict, {key: value} )
#
# Los _diccionarios_ (objetos de la clase **dict**) son **_contenedores_ mutables** de _pares_ de objetos **_key_ ➞ _value_**.
#
# Así, en los diccionarios se **asocian** objetos _únicos_ (generalmente _inmutables_) llamados _**claves**_ (_keys_) a otros objetos llamados _**valores**_ (_values_), pudiendo ser estos últimos objetos de _cualquier tipo_ o incluso estar asociados a diferentes _keys_. Cada uno de estos _pares_ de objetos _key_ ➞ _value_ de un diccionario se denomina _**item**_:
#
# ![dictionary](images/python_intro/dicts_1.png)

# %% [markdown]
# Se definen utilizando _llaves_, separando el objeto _key_ de su objeto _valor_ asociado mediante _dos puntos_, y separando los diferentes pares _key_ ➞ _value_ por _comas_ `{key: value,}`:
# ```python
#     dict_name = {key_object0: value_object0, key_object1: value_object1, ...}
# ```

# %%
# Diccionario que asocia aminoácidos con su masa monoisotópica:
aa2mass = {'S': 87.03203, 'R': 156.10111, 'K': 128.09496, 'T': 101.04768, 'Y': 163.06333}

print(type(aa2mass), aa2mass)

# %% [markdown]
# <br />
#
# Alternativamente, se pueden definir _llamando_ a la clase **dict** usando alguna de las siguientes notaciones:
#
#  * ```python
#    dict_name = dict( key_text0=value_object0, key_text1=value_object1, ...)
#    ```

# %%
# Diccionario que asocia PTMs con los aminoácidos que suelen modificar:
ptm2aas = dict( Phos={'S', 'T'}, Ub={'K', 'R'}, Ac={'K'} )

print(type(ptm2aas), ptm2aas)

# %% [markdown]
#  * ```python
#    dict_name = dict( [(key_object0, value_object0), (key_object1, value_object1), ...] )
#    ```

# %%
# Diccionario que asocia el código de 1 letra de aminoácidos con el correspondiente de 3 letras:
one2three = dict( [('S', 'Ser'), ('R', 'Arg'), ('K', 'Lys'), ('T', 'Thr'), ('Y', 'Tyr')] )

print(type(one2three), one2three)

# %% [markdown] jupyter={"source_hidden": true} tags=["sabermas"]
# <br />
#
# 📌 Además, los diccionarios permiten construir estructuras complejas de datos en _árbol_:

# %% jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 Diccionario de diccionarios: permite construir estructuras de datos en árbol:
name2dict = {'aa2mass': aa2mass, 
             'one2three': one2three,
             'ptm2aas': ptm2aas, 
             'exp2data': {'e001': {'id': 'e001', 
                                   'description': 'First attempt', 
                                   'replicates': 3,
                                   'conclusions': 'Total disaster!'}, 
                          'e002': {'id': 'e002', 
                                   'description': 'Second attempt', 
                                   'replicates': 4,
                                   'conclusions': 'Not bad...'},
                          'e003': {'id': 'e003', 
                                   'description': 'Definitive attempt', 
                                   'replicates': 3,
                                   'conclusions': 'Ready to analyse data.'}
                          }
             }

# %% jupyter={"source_hidden": true} tags=["sabermas"]
print(name2dict) # print: representación lineal de los diccionarios

# %% jupyter={"source_hidden": true} tags=["sabermas"]
name2dict # prettyprint: representación más 'legible' gracias a la kernel iPython usada por el notebook

# %% [markdown]
# #### Acceso
#
# Para **acceder** al objeto _value_ asociado a un objeto _key_ se utiliza la siguiente notación:
#
# dictionary_name<strong>[</strong>`key_object`<strong>]</strong>&nbsp;&nbsp;  ➞ `value_object`
#
# ![dictionary](images/python_intro/dicts_2.png)

# %%
aa2mass['R']    # Acceso al objeto value asociado a la key 'R' del diccionario `aa2mass`

# %% tags=["error"]
aa2mass['M']    # Intento de acceder a una key no presente en el diccionario ➞ Error (KeyError)!

# %%
# Acceso a sub-elementos de los objetos value:
print("'Y' ➞    ",  one2three['Y'] )      # Acceso a la cadena asociada a la key 'Y' de `one2three`
print("'Y' ➞ 1 ➞",  one2three['Y'][1] )  # Acceso al carácter 1 de la cadena asociada a la key 'Y'

# %%
# Modificación "directa" del conjunto asociado a la key 'Ac' de `ptm2aas`:
print(ptm2aas)

ptm2aas['Ac'].add('N-term')

print(ptm2aas)

# %%
# Acceso a objetos value por "referencia":

phos_aas = ptm2aas['Phos'] # Referencia al conjunto asociado a la key 'Phos' de ptm2aas
print(phos_aas)

phos_aas.add('Y')    # Modificación del conjunto `phos_aas` referenciado ➞ se modifica en el diccionario `ptm2aas`
print(phos_aas)
print(ptm2aas)

# %% [markdown]
# <br />
#
# Alternativamente, se puede utilizar el _método_ `.get( )` de los diccionarios para _acceder_ al _valor_ asociado a una _key_:
#
#  * ```python 
#    dict.get(key_object, default=None)
#    ```
#    Devuelve el objeto _value_ asociado al objeto _key_ (*key_object*), _si_ este último _existe_ en el diccionario.  
#    Pero si la _key_ (*key_object*) **no existe**, en lugar de mostrar un error, devuelve el _objeto por defecto_ (*default*) si éste se ha especificado, o `None` si éste _no_ se ha especificado.
#    

# %%
one2three

# %%
one2three.get('S')             # Existe la key ➞ Devuelve el valor asociado a la key

# %%
one2three.get('M')             # No existe la key ➞ Devuelve el valor "default" (no indicado ➞ None)

# %%
one2three.get('M', 'Not present!')    # No existe la key ➞ Devuelve el valor "default" indicado

# %% [markdown]
# #### Diccionarios como contenedores
#
# Al igual que con otros _**contenedores**_ con los diccionarios podemos:
#
#  * Comprobar si un objeto _key_ **pertenece o no** a un diccionario (mediante `in` y `not in`).
#  * Utilizar las **funciones internas** `len()`, `max()` y `min()`.

# %%
one2three

# %%
'K' in one2three       # Pertenencia de la key 'K'

# %%
'Lys' in one2three     # Sólo sirve para los objetos key, no para los objetos value

# %%
'Fail' not in one2three    # No pertenencia de la key 'Fail'

# %%
print('len : ', len(one2three) )     # Longitud (número de pares key ➞ value) del diccionario

# %% jupyter={"source_hidden": true} tags=["sabermas"]
print('max( ) : ', max(aa_one2three) ) # 📌 Objeto key del diccionario con valor máximo
print('min( ) : ', min(aa_one2three) ) # 📌 Objeto key del diccionario con valor mínimo

# %% [markdown]
# #### Mutabilidad
#
# En un diccionario, tanto para **añadir** _un_ nuevo par _key_ ➞ _value_ (un nuevo _item_), como para **reasignar** otro objeto _value_ a una _key_ ya existente, se utiliza la siguiente notación:
#
# dictionary_name<strong>[</strong>`key_object`<strong>]</strong> **=** `value_object`

# %%
print(one2three)

one2three['M'] = '---'     # Añade la nueva key 'M' y su objeto valor '---' al diccionario `one2three`

one2three

# %%
one2three['M'] = 'Met'     # Re-asignación del objeto valor asociado a la key existente 'M'

one2three

# %% [markdown]
# Para **añadir** o **reasignar** _uno o más_ pares _key ➞ value_ (_items_) desde otro diccionario, podemos utilizar el método `.update( )` de los diccionarios:
#  * ```python 
#    dict.update(another_dict)
#    ```
#    Este método _actualiza_ el diccionario (*dict*) con los pares _key ➞ value_ del diccionario *another_dict*: se _añaden_ los no presentes en el diccionario inicial, y se _"sobrescriben"_ los ya presentes.

# %%
# Otro diccionario con nuevas keys y una ('S') común.
another_dict = {'S': '---', 'E': 'Glu', 'P': 'Pro'} 

# Actualiza one2three con los items de `another_dict` (se sobrescribe el valor de 'S')
one2three.update(another_dict) 

one2three

# %% [markdown]
# <br />
#
# Para **eliminar** _un_ par _key ➞ value_ ya existente en un diccionario (un _item_) se puede utiliza el comando `del` o bien el método `.pop( )`:
#  * ```python
#     del dictionary_name[key_object]
#    ```
#    _Elimina_ el par _key ➞ value_ correspondiente a la _key_ indicada (*key_object*). Si ésta _key_ no existe, muestra un error (_KeyError_).

# %%
del one2three['S']     # Elimina el par 'S' ➞ '---' (tanto la key como su value asociado)

one2three

# %% tags=["error"]
del one2three['S']     # Intento eliminar un par key ➞ value no existente ➞ Error! 

# %% [markdown] tags=["sabermas"] jupyter={"source_hidden": true}
# 📌 
#  * ```python 
#    dict.pop(key_object[, default])
#    ```
#    _Elimina_ el par _key ➞ value_ correspondiente al *key_object* y devuelve su objeto _value_ asociado.  
#    Si no existe la _key_ indicada (*key_object*), devuelve el _objeto por defecto_ (*default*) si éste se ha especificado, o muestra un error (_KeyError_) si éste no se ha especificado.

# %% jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 Elimina el par 'P'➞'Pro' y devuelve 'Pro':
print(".pop( ):", aa_one2three.pop('P', 'Not present!') )

aa_one2three

# %% jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 Como ya no existe la key 'P' devuelve 'Not present!' (el valor `default`):
print(".pop( ):", aa_one2three.pop('P', 'Not present!') )

# %% jupyter={"source_hidden": true} tags=["error", "sabermas"]
# 📌 No existe la key 'P' y no se ha especificado un valor `default` ➞ Error!
print(".pop( ):", aa_one2three.pop('P') )

# %% [markdown]
# #### Diferentes _Vistas_ de los diccionarios
#
# Las **vistas** son objetos que actúan como _representaciones sincronizadas_ de los diferentes objetos que forman un diccionario; de manera que si los objetos del diccionario _cambian_, las _vistas también_ reflejan dichos cambios.  
# Se obtienen mediante los _métodos propios_ de los diccionarios `.keys()` `.values()` e `.items()`:
#
#  * ```python 
#    dict.keys()
#    ```
#    Devuelve una _vista_ (un objeto de tipo *dict_keys*) que representa sólo al conjunto de los objetos _key_ del diccionario.
#    
#  * ```python 
#    dict.values()
#    ```
#    Devuelve una _vista_ (un objeto de tipo *dict_values*) que representa sólo a los objetos _value_ del diccionario.
#
#  * ```python 
#    dict.items()
#    ```
#    Devuelve una _vista_ (un objeto de tipo *dict_items*) que representa a los pares de objetos _key ➞ value_ del diccionario como un conjunto de _tuplas_ (_key, value_).
#    
# ![dict_views](images/python_intro/dicts_3.png)

# %%
print("dict: ", aa2mass)

keys = aa2mass.keys()      # Vista únicamente de las keys
values = aa2mass.values()  # Vista únicamente de los values
items = aa2mass.items()    # Vista de los pares (key, value)

print(keys)
print(values)
print(items)

# %%
aa2mass['C'] = 103.00919   # Nuevo item ➞ Las vistas reflejan automáticamente los cambios

print("dict: ", aa2mass)

print(keys)
print(values)
print(items)

# %% [markdown]
# <br />
#
# Estas _vistas_ son una especie de _"contenedores virtuales"_ (ya que no son ellas quienes contienen los objetos, sino el diccionario al que _representan_), y como tales admiten el uso de los _operadores de pertenencia_ `in` y `not in`, y las _funciones internas_ `len()`, `max()`, `min()`.  
# Pero _no son secuencias_, por lo que *ni* se puede acceder a los elementos que _representan_ mediante índices, *ni* poseen los métodos `.count()` ni `.index()`:

# %%
# Vistas de un diccionario como "contenedores virtuales":
print("('C', 103.00919) en `items`?  ➞", ('C', 103.00919) in items)
print("'J' no en `keys`?             ➞", 'J' not in keys)
print('len : ', len(keys) )
print('max : ', max(values) )
print('min : ', min(values) )

# %% tags=["error"]
keys[0]    # Sin acceso a elementos mediante índices (no son secuencias)

# %% tags=["error"]
values.count(103.00919)   # Sin métodos `.count( )` ni `.index( )`

# %% [markdown] jupyter={"source_hidden": true} tags=["sabermas"] toc-hr-collapsed=true
# #### 📌 Otros métodos útiles propios de los diccionarios
#
#  * ```python 
#    dict.copy()
#    ```
#    Devuelve un _nuevo_ diccionario que contiene los mismos pares de objetos _key ➞ value_ que el diccionario original.

# %% jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 Ejemplo del método `.copy()`:
copy_ptm2aas = ptm2aas.copy()

print('Original:', ptm2aas, id(ptm2aas))
print('.copy() :', copy_ptm2aas, id(copy_ptm2aas))

# %% jupyter={"source_hidden": true} tags=["sabermas"]
print(ptm2aas == copy_ptm2aas) # El contenido de los diccionarios es el mismo...
print(ptm2aas is copy_ptm2aas) # ...Pero son objetos diferentes.

# %% jupyter={"source_hidden": true} tags=["sabermas"]
# El diccionario original y su copia son independientes:
phos_aas = copy_ptm2aas.pop('Phos')

print('Original:', ptm2aas, id(ptm2aas))
print('.copy() :', copy_ptm2aas, id(copy_ptm2aas))
ptm2aas == copy_ptm2aas

# %% jupyter={"source_hidden": true} tags=["sabermas"]
# Pero los objetos que contienen son los mismos (la copia es "superficial"):
ptm2aas['Ac'].remove('N-term')

print('Original:', ptm2aas, id(ptm2aas))
print('.copy() :', copy_ptm2aas, id(copy_ptm2aas))
ptm2aas['Ac'] is copy_ptm2aas['Ac']

# %% [markdown] tags=["ejercicio"]
# <br />
#
# > 📝 **Práctica:**
# >
# > Dado el siguiente _diccionario de experimentos_ `id2expdata`:

# %% tags=["ejercicio"]
# Identificador de experimento ➞ Diccionario de datos de experimento:
id2expdata = {'e001': {'id': 'e001', 
                       'description': 'First attempt', 
                       'replicates': 2,
                       'peptides': {'KYLYEIAR', 'AFKAWAVAR', 'DVCKNYAEAR'}, 
                       'conclusions': 'Total disaster!'}, 
              'e002': {'id': 'e002', 
                       'description': 'Second attempt', 
                       'replicates': 0,
                       'peptides': {'SHCIAEVENDE', 'METVKK', 'AWAVARLSQDFPK', 'AFKAWAVAR', 
                                    'AFKAWAVARLSQDFPK', 'DVCKNYAEAR', 'KYLYEIAR'}, 
                       'conclusions': 'Not bad...'},
              'e003': {'id': 'e003', 
                       'description': 'Definitive attempt', 
                       'replicates': 3,
                       'peptides': {'AFKAWAVAR', 'AFKAWAVARLSQDFPK', 'DVCKNYAEAR', 'KYLYEIAR', 'METVKK', 
                                    'LSQDFPK', 'PSLAADFVESK', 'LVESTARTREK', 'LKSKYWALKER', 'AWAVAR'}, 
                       'conclusions': 'Ready to analyse data.'}
              }

# %% [markdown] tags=["ejercicio"]
# > _Cuántos experimentos_ hay en el diccionario? Hay alguno de ellos cuyo _identificador_ sea `'E002'`? Cuáles son las _keys_ de _un_ diccionario de experimento?

# %% tags=["ejercicio"]



# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución:

print("Experimentos totales:", len(id2expdata))

print("Existe el ID 'E002'?:", 'E002' in id2expdata)

print("'Keys' de un diccionario de experimentos:")
list(id2expdata.values())[0].keys() # dict_values ➞ list ➞ primer elemento (un dict) ➞ dict_keys

# %% [markdown] tags=["ejercicio"]
# > _Cuántos péptidos_ se han obtenido en el experimento `'e002'`? Y _cuántos_ son _comunes_ a los obtenidos en los otros experimentos?

# %% tags=["ejercicio"]



# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución paso a paso:

exp2_peps = id2expdata['e002']['peptides']

print("Número de péptidos en 'e002':", len(exp2_peps))

# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
exp1_peps = id2expdata['e001']['peptides']
exp3_peps = id2expdata['e003']['peptides']

print("Número de péptidos comunes entre 'e002' y 'e001':", len( exp2_peps.intersection(exp1_peps) ))
print("Número de péptidos comunes entre 'e002' y 'e003':", len( exp2_peps.intersection(exp3_peps) ))

# %% [markdown] tags=["ejercicio"]
# > **Cambia** el _número de réplicas_ del experimento `'e002'` de 0 a _3_. Y **elimina** todo el experimento `'e001'` del diccionario.

# %% tags=["ejercicio"]



# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución:

id2expdata['e002']['replicates'] = 3

del id2expdata['e001']

id2expdata

# %% [markdown] tags=["ejercicio"]
# > **Crea** un _nuevo diccionario de experimento_ cuyo identificador ('id') sea `'f001'`, y que contenga los péptidos ('peptides') de la variable `expf1_peps`. Después **añade** éste nuevo experimento al diccionario `id2expdata`.

# %% tags=["ejercicio"]
expf1_peps = {'AFKAWAVAR', 'DVCKNYAEAR', 'KYLYEIAR', 'LSQDFPK', 'PSLAADFVESK', 'AFKAWAVARLSQDFPK', 'MEEEEK', 'AWAVAR'}

# %% tags=["ejercicio"]



# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución:

expf1_data = {'id': 'f001', 'peptides': expf1_peps}

id2expdata[ expf1_data['id'] ] = expf1_data
id2expdata

# %% [markdown]
# <br />
# <br />
# <br />
# <br />
#
# [**Continuar con la Parte 2 ...  >>>**](1.2%20Introduccion%20a%20Python.ipynb "Abrir el notebook de la parte 2")  
# (archivo de _notebook_ [1.2 Introduccion a Python.ipynb](1.2%20Introduccion%20a%20Python.ipynb))
