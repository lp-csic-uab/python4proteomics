# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     comment_magics: true
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %% [markdown]
# ![Python4Proteomics](images/logo_p4p.png) **(Python For Poteomics)**
#
# # Introducción a Pandas
#
# **Pandas** es una de las librerías de **Python** más populares para trabajar con tablas datos. Este paquete ofrece unos contenedores muy flexibles y potentes para manipular datos de manera sencilla: la *Series* y el *DataFrame*.
#
# **Índice:**
#
#  * [I.- La *Series* y el *DataFrame* en Pandas](#I.--La-Series-y-el-DataFrame-en-Pandas)
#  * [II.- Importando nuestros datos como *DataFrame*](#II.--Importando-nuestros-datos-como-DataFrame)
#  * [III.- Exploración básica de *DataFrames*](#III.--Exploración-básica-de-DataFrames)
#  * [IV.- Exploración visual de *DataFrames*](#IV.--Exploración-visual-de-DataFrames)
#  * [V.- Acceso a los *DataFrames*](#V.--Acceso-a-los-DataFrames)
#     * [Accediendo a columnas (como *DataFrames*)](#Accediendo-a-columnas-(como-DataFrames))
#     * [Accediendo a columnas (como *Series*)](#Accediendo-a-columnas-(como-Series))
#     * [Accediendo a filas](#Accediendo-a-filas)
#     * [Accediendo a filas y columnas simultáneamente](#Accediendo-a-filas-y-columnas-simultáneamente)
#     * [Accediendo mediante intervalos](#Accediendo-mediante-intervalos)
#  * [VI.- Indexación booleana y filtrado de *DataFrames*](#VI.--Indexación-booleana-y-filtrado-de-DataFrames)
#     * [Filtrando mediante indexación booleana](#Filtrando-mediante-indexación-booleana)
#  * [VII.- Transformando *DataFrames*](#VII.--Transformando-DataFrames)
#     * [Transformaciones numéricas](#Transformaciones-numéricas)
#     * [Transformaciones de texto](#Transformaciones-de-texto)
#  * [VIII.- Agrupando y agregando *DataFrames*](#VIII.--Agrupando-y-agregando-DataFrames)
#  * [IX.- Fundiendo *DataFrames*](#IX.--Fundiendo-DataFrames)
#  * [X.- Pivotando *DataFrames*](#X.--Pivotando-DataFrames)
# ---

# %% [markdown]
# # I.- La *Series* y el *DataFrame* en Pandas
#
# <img src="images/pandas/pandas_logo.png" width="150" style="float: left; margin-right: 10px;" />
#
# La "Serie" (*Series*) y el "Marco de Datos" (*DataFrame*) de **Pandas** son las estructuras de datos más utilizadas en esta librería de **Python**. Si pensamos en términos de hojas de cálculo, una *Series* podría entenderse como una columna cuyas filas están etiquetadas. En **Pandas**,  el conjunto de etiquetas de una *Series* se llama <u>índice</u>. Si varias *Series* comparten el mismo índice, podemos combinarlas para formar un *DataFrame* en el que cada *Series* formará una <u>columna</u> y cada uno de los índices será compartido por todas las columnas. Volviendo a la terminología de las hojas de cálculo, un *DataFrame* no es más que una tabla de datos etiquetados mediate <u>índices</u> y <u>columnas</u>.
#
# Para cargar la librería **Pandas** con el alias `pd` haremos:

# %%
# Loading the Pandas package and assign the 'pd' alias to it
import pandas as pd

# %% [markdown]
# # II.- Importando nuestros datos como *DataFrame*
#
# Para importar nuestros datos en forma de *DataFrame* utilizaremos una función u otra dependiendo de su formato de entrada:
#
# - [`pd.read_excel()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_excel.html) para formatos **\*.xlsx** y **\*.xls**.<br>
# - [`pd.read_csv()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_csv.html) para formato **\*.csv** y **\*.txt**.<br>
#
# > 💡 **Más información:**
# >
# > Si clicas en los nombres de estás funciones, un enlace te llevará a la correspondiente página de referencia de **Pandas**. Podrás usar estos enlaces para todas las funciones y métodos de **Pandas** que vayamos introduciendo en este bloque.
#
# Todas estas funciones tienen múltiples argumentos que permiten especificar con más detalle de qué manera queremos importar los datos (saltar filas iniciales, especificar el delimitador, importar una hoja en particular de un **Excel**...). Para empezar a practicar, importaremos el [siguiente](data/qualitative/Excel.xlsx) **Excel** de la carpeta `data/qualitative/` de nuestro repositorio.

# %%
# Reading an Excel SpreadSheet (*.xlsx or *.xls) and storing it in as a DataFrame: df
df = pd.read_excel('data/qualitative/Excel.xlsx')
#df = pd.read_csv('data/qualitative/CommaSeparatedValues.csv')

# %% [markdown]
# Una vez importados los datos, podemos visualizar nuestro *DataFrame* escribiendo el nombre de la variable en la que lo hemos guardado (`df`):

# %%
# Return the DataFrame
df

# %% [markdown]
# También podemos comprobar que nuestra variable `df` es realmente un *DataFrame* mediante la función `type()`:

# %%
# DataFrame data type
type(df)

# %% [markdown]
# # III.- Exploración básica de *DataFrames*
#
# Empezaremos introduciendo algunos métodos para explorar *DataFrames* de manera preliminar:
#
# - [`.shape`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.shape.html) para obtener las dimensiones del *DataFrame*.
# - [`.columns`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.columns.html) para obtener las etiquetas de las columnas del *DataFrame*.
# - [`.index`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.index.html) para obtener las etiquetas de las filas (índice) del *DataFrame*.

# %%
# DataFrame shape. Remember: (Rows, Columns)
df.shape

# %%
# DataFrame columns
df.columns

# %%
# DataFrame rows
df.index

# %% [markdown]
# Otros métodos muy útiles para la exploración básica de un *DataFrame* son los siguientes:
#
# - [`.head()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.head.html) para ver las primeras filas del *DataFrame*.
# - [`.tail()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.tail.html) para ver las últimas filas del *DataFrame*.
# - [`.describe()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.describe.html) para ver una descripción estadística simple del *DataFrame*.
# - [`.info()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.info.html) para ver información general del *DataFrame*.

# %%
# DataFrame head
df.head(1)

# %%
# DataFrame tail
df.tail(2)

# %%
# DataFrame (basic) statistical description
df.describe()

# %%
# DataFrame general information
df.info()

# %% [markdown]
# El método `.info()` resulta particularmente útil. Podemos ver el índice y las columnas, el tipo de variable que contiene cada columna y el espacio que ocupa nuestro *DataFrame* en memoria. También podemos ver el número de valores no nulos en cada columna, lo que nos permite estimar la cantidad de  valores nulos (`NaN`) presentes en nuestro *DataFrame*.

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > Visualiza sólo la primera fila de `df`.

# %% tags=["ejercicio"]
# DataFrame first head?


# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# SOLUTION
# DataFrame first head?
df.head(1)

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > Visualiza sólo la última fila de `df`.

# %% tags=["ejercicio"]
# DataFrame last tail?


# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# SOLUTION
# DataFrame last tail?
df.tail(1)

# %% [markdown]
# # IV.- Exploración visual de *DataFrames*
#
# Realizar una buena exploración visual <u>antes</u>  de analizar los datos es un práctica muy recomendable (por no decir <u>obligatoria</u>). El método [`.plot`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.plot.html) incluído en **Padas** sirve para construir visualiuzaciones simples y rápidas de nuestros *DataFrames*. A su vez, este método incluye otros métodos que permiten realizar la representación gráfica deseada en cada caso:
#
# Para empezar introduciremos algunos métodos para explorar *DataFrames* de manera preliminar:
#
# - [`.line()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.plot.line.html) para líneas.
# - [`.bar()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.plot.bar.html) para barras verticales.
# - [`.barh()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.plot.barh.html) para barras horizontales.
# - [`.hist()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.plot.hist.html) para histogramas.
# - [`.box()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.plot.box.html) para cajas.
# - [`.density()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.plot.density.html) para KDEs (*kernel density estimation*).
# - [`.area()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.plot.area.html) para areas apiladas.
# - [`.pie()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.plot.pie.html) para sectores.
# - [`.scatter()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.plot.scatter.html) para dispersiones.
# - [`.hexbin()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.plot.hexbin.html) para agrupaciones hexagonales.

# %%
# DataFrame line plot
df.plot.line()

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > Realiza alguna otra visualización de `df`. La que más te apetezca.

# %% tags=["ejercicio"]
# DataFrame arbitrary plot


# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# SOLUTION
# DataFrame arbitrary plot
df.plot.box()

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > Realiza la visualización de dispersión de `df` usando `.scatter()`.

# %% tags=["ejercicio"]
# DataFrame scatter plot


# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# SOLUTION
# DataFrame scatter plot
df.plot.scatter(x='Intensity', y='Amplitude')

# %% [markdown]
# # V.- Acceso a los *DataFrames*
#
# Al trabajar con un *DataFrame* necesitaremos acceder a parte de sus filas y columnas, especialmente si el *DataFrame* es muy extenso. En este apartado explicaremos la sintaxis más típica para acceder a determinadas filas y columnas de un *DataFrame*.

# %% [markdown]
# ## Accediendo a columnas (como *DataFrames*)
#
# Accederemos a las columnas de un *DataFrame* mediante los corchetes `df[]`. Debemos pasar una lista con las columnas que deseamos seleccionar al interior de los corchetes, así `df[['Raw', 'Intensity']]`. Fíjate que hay dos parejas de corchetes, una perteneciente a la lista de columnas que queremos seleccionar (interna), y otra que da acceso a las columnas del *DataFrame* (externa).

# %%
# Accessing DataFrame columns
df[['Raw', 'Intensity']]

# %% [markdown]
# También puedes crear una variable `cols` con las columnas a las que quieres acceder y luego pasar la lista `cols` al interior de los corchetes, así `df[cols]`, para seleccionar las columnas.

# %%
# Accessing DataFrame columns specifying first the list of columns we want
cols = ['Raw', 'Intensity']
df[cols]

# %% [markdown]
# ## Accediendo a columnas (como *Series*)
#
# Según lo explicado, cuando queramos acceder a una única columna haremos `df[['Intensity']]`. Alterenativamente, también podemos hacer `df['Intensity']` o `df.Intensity`. Ten en cuenta que esto sólo sirve para acceder a una única columna.

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > ¿El tipo de variable devuelto por `df[['Intensity']]` y `df['Intensity']` es el mismo?

# %% tags=["ejercicio", "solucion"]
# Printing the data type if accessing a column with df[[]]


# Printing the data type if accessing a column with df[]


# %% jupyter={"source_hidden": true} tags=["ejercicio"]
# SOLUTION
# Printing the data type if accessing a column with df[[]]
print(type(df['Intensity']))

# Printing the data type if accessing a column with df[]
print(type(df[['Intensity']]))

# %% [markdown]
# ## Accediendo a filas
#
# Accederemos a las filas de un *DataFrame* mediante el método [`.loc`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.loc.html) seguido de unos corchetes `df.loc[]`. Debemos pasar una lista con los índices que queremos seleccionar al interior de los corchetes, así `df.loc[[4, 1]]`. Nuevamente, fíjate que hay dos parejas de corchetes, una perteneciente a la lista de índices que queremos seleccionar (interna), y otra que da acceso a las columnas del *DataFrame* (externa).

# %%
# Accessing DataFrame rows
df.loc[[4, 1]]

# %% [markdown]
# Como hemos hecho antes con las columnas, puedes crear una variable `rows` con los índices a los que quieres acceder y luego pasar la lista `rows` al interior de los corchetes, así `df.loc[rows]`, para seleccionar las filas.

# %%
# Accessing DataFrame rows specifying first the list of rows we want
rows = [4, 1]
df.loc[rows]

# %% [markdown]
# ## Accediendo a filas y columnas simultáneamente
#
# Accederemos a múltiples filas y columnas de un *DataFrame* simultáneamente mediante el método `.loc` seguido de unos corchetes con una coma `,` en su interior `df.loc[, ]`. Esta vez, debemos pasar dos listas al interior de los corchetes: una para las filas (al lado izquierdo de la coma) y otra para las columnas (al lado derecho de la coma), así `df.loc[[4, 1], ['Raw', 'Intensity']]`.
#

# %%
# Accessing multiple DataFrame rows and columns simultaneously
df.loc[[4, 1],  ['Raw', 'Intensity']]

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > Accede a las filas `[4, 1]` y a las columnas `['Raw', 'Intensity']` del *DataFrame* `df` simultáneamente. Recuerda que las listas `rows` y `cols` ya han sido definidas y son variables que tienes a tu disposición.

# %% tags=["ejercicio"]
# Accessing DataFrame columns specifying first the list of indices and columns we want


# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# SOLUTION
# Accessing DataFrame columns specifying first the list of indices and columns we want
df.loc[rows, cols]

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > Accede a una única celda del *DataFrame* `df`, por ejemplo la celda con el valor de intensidad `112.643842`

# %% tags=["ejercicio"]
# Accessing a single DataFrame row and column simultaneously


# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# SOLUTION
# Accessing a single DataFrame row and column simultaneously
df.loc[[4], ['Intensity']]

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > ¿El tipo de variable devuelto por `df.loc[[4], ['Intensity']]`, `df.loc[4, ['Intensity']]`, `df.loc[[4], 'Intensity']` y `df.loc[4, 'Intensity']` es el mismo?

# %% tags=["ejercicio"]
# Printing the data type if accessing a DataFrame with df[[], []]


# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# SOLUTION
# Printing the data type if accessing a DataFrame with df[[], []]
print(type(df.loc[[4], ['Intensity']]))
df.loc[[4], ['Intensity']]

# %% tags=["ejercicio"]
# Printing the data type if accessing a DataFrame with df[, []]


# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# SOLUTION
# Printing the data type if accessing a DataFrame with df[, []]
print(type(df.loc[4, ['Intensity']]))
df.loc[4, ['Intensity']]

# %% tags=["ejercicio"]
# Printing the data type if accessing a DataFrame with df[[], ]


# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# SOLUTION
# Printing the data type if accessing a DataFrame with df[[], ]
print(type(df.loc[[4], 'Intensity']))
df.loc[[4], 'Intensity']

# %% tags=["ejercicio"]
# Printing the data type if accessing a DataFrame with df[, ]


# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# SOLUTION
# Printing the data type if accessing a DataFrame with df[, ]
print(type(df.loc[4, 'Intensity']))
df.loc[4, 'Intensity']

# %% [markdown]
# ## Accediendo mediante intervalos
#
# También podemos utilizar la notación de intervalos `:` (*slice notation*) para acceder a las filas y columnas de un *DataFrame* con un poco más de flexibilidad.

# %%
# Accessing SOME DataFrame rows and ALL columns
df.loc[[2, 3, 4, 5], ['Raw', 'Software', 'Node', 'Sequence', 'Intensity', 'Amplitude']]

# %%
# Accessing SOME DataFrame rows and ALL columns (using slicing)
df.loc[2:5, :]

# %%
# Accessing ALL DataFrame rows and SOME columns
df.loc[[0, 1, 2, 3, 4, 5, 6 ,7, 8, 9, 10, 11], ['Node', 'Sequence', 'Intensity']]

# %%
# Accessing ALL DataFrame rows and SOME columns (using slicing)
df.loc[:, 'Node': 'Intensity']

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > Utiliza *slicing* para acceder simultáneamente a todas las filas y todas las columnas de `df`.

# %% tags=["ejercicio"]
# Accessing ALL DataFrame rows and ALL columns (using slicing)


# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# SOLUTION
# Accessing ALL DataFrame rows and ALL columns (using slicing)
df.loc[:, :]

# %% [markdown]
# # VI.- Indexación booleana y filtrado de *DataFrames*
#
# Mediante el uso de los operadores de comparación, podemos "interrogar" a nuestro *DataFrame* para obtener respuestas booleanas (`True` o `False`). Algunos de los operadores de comparación más utilizados son:
#  
# - Es `A` **igual** a `B`? $\rightarrow$ `A == B` 
# - Es `A` **diferente** de `B`? $\rightarrow$ `A != B`
# - Es `A` **menor** que `B`? $\rightarrow$ `A < B`
# - Es `A` **mayor** que `B`? $\rightarrow$ `A > B`
# - Es `A` **menor o igual** que `B`? $\rightarrow$ `A <= B`
# - Es `A` **mayor o igual** que `B`? $\rightarrow$ `A >= B`
#
# Recuerda que también se pueden utilizar los operadores de pertenecia (`in`, `not in`), y los de identidad (`is`, `is not`) entre otros.

# %%
# Is the current index 'Intensity' greater than 100?
df['Intensity'] > 100

# %% [markdown]
# También podemos realizar múltiples preguntas simultáneamente utilizando los operadores binarios:
#
# - Conjunción $\rightarrow$ `&`
# - Disyunción $\rightarrow$ `|`
# - Negación $\rightarrow$ `~`
# - Disyunción exclusiva $\rightarrow$ `^`
#
# Ten en cuenta que para ello debemos rodear entre paréntesis `( )` cada una de las preguntas antes de conectarlas con los operadores binarios.

# %%
# Is the current index 'Intensity' greater than 100 AND 'Amplitude' smaller than 1.6?
(df['Intensity'] > 100) & (df['Amplitude'] < 1.6)

# %%
# Is the current index 'Software' not equal to 'PD' OR 'Node' equal to 'Amanda'?
(df['Software'] != 'PD') | (df['Node'] == 'Amanda')

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > Interrogar al *DataFrame* `df` sobre aquellas filas con intensidad menor de 90 **o** mayor de 140, **y**, nodo Andromeda **o** Amanda.

# %% tags=["ejercicio"]
# Is the current index 'Software' not equal to 'PD' OR 'Node' equal to 'Amanda'?
#( (df['Intensity'] _ ___) _ (df['Intensity'] _ ___) ) & ( (df['Node'] _ '___') _ (df['Node'] _ '___') )

# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# SOLUTION
# Is the current index 'Software' not equal to 'PD' OR 'Node' equal to 'Amanda'?
((df['Intensity'] > 140) | (df['Intensity'] < 90)) & ((df['Node'] == 'Andromeda') | (df['Node'] == 'Amanda'))

# %% [markdown]
# ## Filtrando mediante indexación booleana
#
# Al interrogar el *DataFrame* mediante un operador de comparación, **Python** nos devuelve una *Series*. Más concretamente, como esta *Series* sólo contiene valores booleanos, se trata de una Serie Booleana (*Boolean Series*). En este apartado veremos que una *Boolean Series* puede usarse como <u>máscara</u> para filtrar *DataFrames* de manera intuitiva y flexible.

# %%
# Create filter to get Proteome Discoverer software AND no to get Amanda search node.
first_filter = (df['Software'] == 'PD') & (df['Node'] != 'Amanda')
type(first_filter)

# %% [markdown]
# Ahora podemos usar la máscara `first_filter` para filtrar nuestro *DataFrame* `df`.

# %%
# Applying my (first) filter to my DataFrame
df[first_filter]

# %% [markdown] tags=["ejercicio"]
# En el siguiente bloque de código hemos implementado para ti un par de líneas para obtener el valor límite del cuantil 60% de la intensidad (`I_quantile`), y el valor límite del cuantil 40% de la amplitud (`A_quantile`). Tendrás que utilizar estas dos variables en el ejercio.

# %% tags=["ejercicio"]
# Retrieving the 60% quantile of the 'Intensity': I_quantile
I_quantile = df['Intensity'].quantile(0.60)
print(I_quantile)

# Retrieving the 40% quantile of the 'Amplitude': A_quantile
A_quantile = df['Amplitude'].quantile(0.40)
print(A_quantile)

# %% [markdown]
# > ✏️ **Práctica:**
# > 
# > Intenta construir una máscara (`first_filter`) para filtrar los valores de intensidad alta (` > I_quantile`)  **o** amplitud baja (` < A_quantile`). Luego aplica la máscara al *DataFrame* `df`.

# %% tags=["ejercicio"]
# Create filter to get high peak intensity (first quantile) OR low peak amplitude (last quantile)
#first_filter = 

# Applying first filter to DataFrame
#df[___]

# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# SOLUTION
# Create filter to get high peak intensity (first quantile) OR low peak amplitude (last quantile)
first_filter = (df['Intensity'] > I_quantile) | (df['Amplitude'] < A_quantile)

# Applying first filter to DataFrame
df[first_filter]


# %% [markdown]
# > ¿Qué cambiarías en la definición de `first_filter` si quisieras filtrar los valores de intensidad alta **y** amplitud baja? Hazlo y aplica la nueva máscara al *DataFrame* `df`.

# %% tags=["ejercicio"]
# Create filter to get high peak intensity (first quantile) AND low peak amplitude (last quantile)
#second_filter = 

# Applying second filter to DataFrame
#df[___]

# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# SOLUTION
# Create filter to get high peak intensity (first quantile) AND low peak amplitude (last quantile)
second_filter = (df['Intensity'] > I_quantile) & (df['Amplitude'] < A_quantile)

# Applying second filter to DataFrame
df[second_filter]

# %% [markdown]
# # VII.- Transformando *DataFrames*
#
# Prácticamente a diario necesitaremos procesar los datos almacenados en un *DataFrame*. Podríamos necesitar crear una columna a partir de otra, o aplicar una misma transformación a múltiples columnas, o construir una tabla dinámica...

# %% [markdown]
# ## Transformaciones numéricas
#
# A modo de ejemplo, intentaremos calcular el *Z-score* de la intensidad. Recuerda que la $i$-ésima observación de una magnitud $x$, $(x_i)$, tiene un *Z-score*, $(Z_i)$, dado por la siguiente ecuación:
#
# \begin{equation}
# Z_i = \frac{x_i - \mu(x)}{\sigma(x)}
# \end{equation}
#
# Aquí, $\mu(x)$ y $\sigma(x)$ representan la media y la desviación estándar de $x$, respectivamente.

# %%
# Computing the Z-score of the 'Intensity' column and storing it in a new 'Z-Intensity' column
df['Z-Intensity'] = ( (df['Intensity']) - (df['Intensity'].mean()) ) / ( df['Intensity'].std() )

df

# %% [markdown]
# >💡 **Más información:**
# >
# > Los métodos de **Pandas** [`.std()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.std.html) y [`.mean()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.mean.html) son muy útiles para hacer agragaciones estadísticas sencillas como la desviación estándar y la media. [Aquí](https://pandas.pydata.org/pandas-docs/stable/getting_started/basics.html#descriptive-statistics) puedes consultar todos los métodos disponibles.

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > Calcula la normalización de 0 a 1 para la amplitud. Recuerda que la $i$-ésima observación de una magnitud $x$, $(x_i)$, tiene una normalización de 0 a 1, $(N_i)$, dada por la siguiente ecuación:
# > 
# >\begin{equation}
# N_i = \frac{x_i - m(x)}{M(x) - m(x)}
# \end{equation}
# > 
# > Aquí, $m(x)$ y $M(x)$ representan los valores mínimo y máximo de $x$, respectivamente.
# > 
# > **Pista**: Los *DataFrames* de **Pandas** tienen dos métodos que te vendrán muy bien: [`.min()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.min.html) y [`.max()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.max.html)

# %% tags=["ejercicio", "error"]
# Computing the N-normalization of the 'Amplitude' column and storing it in a new 'N-Amplitude' column
#df['N-Amplitude'] = ___

# Return the DataFrame


# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# SOLUTION
# Computing the N-normalization of the 'Amplitude' column and storing it in a new 'N-Amplitude' column
df['N-Amplitude'] = (df['Amplitude'] - df['Amplitude'].min()) / (df['Amplitude'].max() - df['Amplitude'].min())

# Return the DataFrame
df

# %% [markdown]
# Ahora que tenemos el *Z-score* de la intensidad y la normalización de 0 a 1 de la amplitud en dos columnas independientes, podríamos descartar las columnas originales de intensidad y amplitud con el método [`.drop()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.drop.html).
#
# >💡 **Más información:**
# >
# > El parámetro `axis=` del método `.drop` especifica si la lista de etiquetas que queremos descartar hace referencia a los índices (`axis=0`) o a las columnas (`axis=1`). Alternativamente, se pueden utilizar los parámetros `index=` y `columns=`, los cuales resultan mucho más intuitivos.

# %%
# Dropping redundant columns 'Intensity' and 'Amplitude'
df = df.drop(['Intensity', 'Amplitude'], axis=1)
#df = df.drop(columns=['Intensity', 'Amplitude'])

# Return the DataFrame
df

# %% [markdown]
# Como norma general es buena práctica usar nombres cortos para las columnas de un *DataFrame*. El método [`.rename()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.rename.html) permite renombrar las columnas de un *DataFrame* mediante un diccionario. Las *keys* de este diccionario serán los nombres originales y los *values* serán los nombres nuevos.

# %%
# Creating a renaming dictionary for incomming column rename
rename_dic = {'Software': 'Soft',
              'Sequence': 'Seq',
              'Z-Intensity': 'I',
              'N-Amplitude': 'A'}

# Applying my (first) column rename to my DataFrame
df = df.rename(rename_dic, axis=1)
#df = df.rename(columns=rename_dic)

# Return the DataFrame
df

# %% [markdown]
# ## Transformaciones de texto
#
# Además de transformaciones numéricas podemos operar con cadenas de carácteres (*strings*). Esto resulta particularmente útil para trabajar con secuencias de aminoácidos. Por ejemplo, vemos que los *strings* de la columna `df['Raw']` tienen una estructura bien organizada, ya que constan de varios *substrings* separados por barras bajas `_`. Podemos encontrar un *substring* independiente para la fecha (`1985-04-06`), otro *substring* para el número de seguimento del trabajo (`0123`), otro para las iniciales del usuario (`GA`), otro para la condición experimental (`T` / `C`) y otro para la réplica (`R1` / `R2` / `R3` / `R4`). A veces puede resultar muy útil incorporar esta información en nuestro *DataFrame*.
#
# Las columnas (o *Series*) de un *DataFrame* poseen el método [`.str`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.Series.str.html). Este método da acceso a los *strings* almacenados en la *Series*. Una vez tenemos la *Series* bajo el efecto del método `.str` podemos utilizar cualquiera de los métodos disponibles para un *string*,  como por ejemplo `.split()`.

# %%
# Splitting the 'Raw' column by the underscore '_'
df['Split raw'] = df['Raw'].str.split('_')

# Return the DataFrame
df

# %%
# Taking the 4th element of the list in 'Split raw' as 'Cond'
df['Cond'] = df['Split raw'].str[3]

# Return the DataFrame
df

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > Crea una columna llamada `df['Repl']` para los replicados `R1`, `R2`, `R3` y `R4` tal y como aparecen en la columna `df['Raw']`. Luego descarta las columnas redundantes `df['Raw']` y `df['Split raw']`

# %% tags=["ejercicio"]
# Taking the 5th element of the list in 'Split raw' as 'Repl'
#df['Repl'] = ___


# Dropping redundant columns 'Raw' and 'Split raw'
#df = df.drop(___)


# Return the DataFrame


# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# SOLUTION
# Taking the 5th element of the list in 'Split raw' as 'Repl'
df['Repl'] = df['Split raw'].str[4]

# Dropping redundant columns 'Raw' and 'Split raw'
df = df.drop(['Raw', 'Split raw'], axis=1)

# Return the DataFrame
df

# %% [markdown]
# Para acabar con este capítulo, simplemente exportaremos nuestro *DataFrame* en forma de archivo **Excel**. Para ello sólo necesitaremos el método de *DataFrame* [`.to_excel()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.to_excel.html) de **Pandas**.

# %%
# Exporting the DataFrame as an Excel SpreadSheet
df.to_excel('data/qualitative/Excel_df.xlsx', sheet_name='Excel_df')

# %% [markdown]
# # VIII.- Agrupando y agregando *DataFrames*
#
# El método [`.groupby()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.groupby.html) de **Pandas** es uno de los más potentes y versátiles. Sirve para condensar los datos de un *DataFrame* mediante la <u>agrupación</u> de sus filas primero, y su posterior <u>agregación</u>. Por ejemplo, supongamos que queremos saber la $I$ y la $A$ <u>medias</u> del péptido `PEPTIDE` que nos proporcionan PD y MQ:

# %%
# Grouping by 'Soft' and aggregating with mean
g_df = df.groupby(['Soft']).mean()

# Return the DataFrame
g_df

# %% [markdown]
# También podríamos estar interesados en saber la $I$ y la $A$ <u>medias</u> del peptido `PEPTIDE` que nos proporcionan Sequest, Amanda y Andromeda:

# %%
# Grouping by 'Soft' and aggregating with mean
g_df = df.groupby(['Node']).mean()

# Return the DataFrame
g_df

# %% [markdown]
# Si quisiéramos, también podríamos agrupar por dos columnas. Por ejemplo, podemos agrupar por `'Soft'` y por `'Node'` para no tener que recordar a qué *Software* pertenece cada nodo.

# %%
# Grouping by 'Soft', 'Node' and aggregating with mean
g_df = df.groupby(['Soft', 'Node']).mean()

# Return the DataFrame
g_df

# %% [markdown]
# Por otro lado, si en nuestro *DataFrame* tubiéramos una lista extensa de péptidos (en vez de tener un solo péptido `PEPTIDE`), también podríamos incluír `'Seq'` en la agrupación.

# %%
# Grouping by 'Soft', 'Node', 'Seq' and aggregating with mean
g_df = df.groupby(['Soft', 'Node', 'Seq']).mean()

# Return the DataFrame
g_df

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > Agrupa ed *DataFrame* `df` por `'Soft'`, `'Node'`, `'Seq'` y agrega <u>sólo</u> la $A$ con el <u>mínimo</u>.

# %% tags=["ejercicio"]
# Grouping by 'Soft', 'Node', 'Seq' and aggregating 'A' with min


# Return the DataFrame


# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# SOLUTION
# Grouping by 'Soft', 'Node', 'Seq' and aggregating 'A' with min
g_df = df.groupby(['Soft', 'Node', 'Seq'])[['A']].min()

# Return the DataFrame
g_df

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > Agrupa el *DataFrame* `df` por `'Soft'`, `'Node'`, `'Seq'` y agrega <u>sólo</u> la $I$ con el <u>máximo</u>.

# %% tags=["ejercicio"]
# Grouping by 'Soft', 'Node', 'Seq'and aggregating 'I' with max


# Return the DataFrame


# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# SOLUTION
# Grouping by 'Soft', 'Node', 'Seq'and aggregating 'I' with max
g_df = df.groupby(['Soft', 'Node', 'Seq'])[['I']].max()

# Return the DataFrame
g_df

# %% [markdown]
# Dada una lista de agrupación (por ejemplo `['Soft', 'Node', 'Seq']`) podríamos agregar la $I$ y la $A$ con más de una función simultáneamente (por ejemplo `mean()` y `median()`). Para ello necesitaremos el método [`.agg()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.agg.html).

# %%
# Creating an aggrupation list with all columns we want to group
agg_column_list = ['Soft', 'Node', 'Seq']

# Creating an aggregating list with all functions we want to use
agg_function_list = ['mean', 'median']

# Grouping by "agg_column_list" and aggregating with multiple fuctions using "agg_function_list"
g_df = df.groupby(agg_column_list).agg(agg_function_list)

# Return the DataFrame
g_df

# %% [markdown]
# También es posible especificar con qué función queremos agregar cada columna. Para ello simplemente tenemos que definir un diccionario de agregación en el que las *key* son los nombres de las columnas que queremos agregar, y los *value* son las correspondientes funciones de agregación.

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > Agrupa el *DataFrame* `df` por `'Soft'`, `'Node'`, `'Seq'` agregando la $I$ con el <u>máximo</u> y la $A$ con el <u>mínimo</u>.

# %% tags=["ejercicio"]
# Creating an aggrupation dictionary
#agg_function_dict = {___}

# Grouping by "agg_column_list" and aggregating with multiple fuctions using "agg_function_list"
#g_df = df.groupby(___).agg(___)

# Return the DataFrame


# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# SOLUTION
# Creating an aggrupation dictionary
agg_function_dict = {'I': 'max', 'A': 'min'}

# Grouping by "agg_column_list" and aggregating with multiple fuctions using "agg_function_list"
g_df = df.groupby(agg_column_list).agg(agg_function_dict)

# Return the DataFrame
g_df

# %% [markdown]
# # IX.- Fundiendo *DataFrames*
#
# La función [`melt()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.melt.html) de **Pandas** permite convertir un *DataFrame* típico en un *Tidy-DataFrame* de manera muy fácil. Los dos argumentos más importantes de esta función son `id_vars=` y `value_vars=`.
#
# Por un lado, `id_vars=` pide las columnas que harán el papel de índice en el *Tidy-DataFrame*. Por otro lado, `value_vars=` pide las columnas que harán el papel de valores. Los <u>índices</u> y los <u>valores</u> de las columnas a fundir aparecerán en sendas nuevas columnas designadas con los nombres pasados a los argumentos `var_name=` y `value_name=`, respectivamente.

# %%
# Return the DataFrame (BEFORE melting)
df

# %% [markdown]
# Por ejemplo, supongamos que queremos combinar en una única columna los valores de $I$ y de $A$, manteniendo todas las otras columnas como índice:

# %%
# Melting 'I' and 'A', keeping 'Soft', 'Node', 'Seq', 'Cond' and 'Repl'
t_df = pd.melt(frame=df,
               id_vars=['Soft', 'Node', 'Seq', 'Cond', 'Repl'],
               value_vars=['I', 'A'],
               var_name='Variable',
               value_name='Value')

# Return the DataFrame (AFTER melting)
t_df

# %% [markdown]
# # X.- Pivotando *DataFrames*
#
# El método de **Pandas** [`.pivot_table()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.pivot_table.html) permite reorganizar el contenido de un *DataFrame* en forma de tabla pivotada de manera sencilla. Los argumentos más importantes de esta función son `index=`, `columns=` y `values=`.
#
# Por un lado `index=` pide las columnas que harán el papel de índice en la tabla pivotada. Por otro lado, `values=` y `columns=` piden los valores (`values=`) que seran desglosados en nuevas columnas (`columns=`).

# %%
# Return the just melted DataFrame (BEFORE pivoting)
t_df

# %% [markdown]
# Por ejemplo, supongamos que queremos separar en columnas distintas los valores de $I$ y de $A$ de habíamos derretido en el apartado anterior, manteniendo todas las otras columnas como índice:

# %%
# Pivoting tidy_df
p_df = t_df.pivot_table(index=['Soft', 'Node', 'Seq', 'Cond', 'Repl'],
                        columns=['Variable'],
                        values=['Value'])

# Return the DataFrame (AFTER pivoting)
p_df

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > Vuelve a pivotar el *Tidy-DataFrame* `t_df`, pero trasladando las columnas `'Soft'` y `'Node'` del argumento `index=` al argumento `columns=`.

# %%
# Pivoting tidy_df
#p_df = t_df.pivot_table(index=[___],
#                        columns=[___],
#                        values=[___])

# Return the DataFrame (AFTER pivoting)
#p_df

# %% jupyter={"source_hidden": true}
# SOLUTION
# Pivoting tidy_df
p_df = t_df.pivot_table(index=['Seq', 'Cond', 'Repl'],
                        columns=['Variable', 'Soft', 'Node'],
                        values=['Value'])

# Return the DataFrame (AFTER pivoting)
p_df

# %% [markdown]
# El método de **Pandas** `.pivot_table()` no sólo permite recolocar los valores de una *DataFrame* en forma de tabla pivotada, también podemos agregarlos:

# %%
# Pivoting tidy_df and aggregating the replicates 'Repl' with the mean
p_df = t_df.pivot_table(index=['Seq', 'Cond'],
                        columns=['Variable', 'Soft', 'Node'], 
                        values=['Value'],
                        aggfunc='mean')
p_df

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > Vuelve a pivotar el *Tidy-DataFrame* `t_df`, pero agregando `'Soft'`, `'Node'` y `'Repl'` con la mediana.

# %%
# Pivoting tidy_df and aggregating 'Soft', 'Node' and 'Repl' with the median
#p_df = t_df.pivot_table(index=[___],
#                        columns=[___],
#                        values=[___],
#                        aggfunc='median')

# Return the DataFrame (AFTER pivoting and aggregating)
#p_df

# %% jupyter={"source_hidden": true}
# SOLUTION
# Pivoting tidy_df and aggregating 'Soft', 'Node' and 'Repl' with the median
p_df = t_df.pivot_table(index=['Seq', 'Cond'],
                        columns=['Variable'],
                        values=['Value'],
                        aggfunc='median')

# Pivoting tidy_df and aggregating 'Soft', 'Node' and 'Repl' with the median
p_df
