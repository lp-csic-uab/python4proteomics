Python for Proteomics. Temario
==============================

Version 0.3

## Introducción al curso

- Objetivos del curso
- Python (lenguaje de programación y versiones)
- Instalación/comprobación del entorno de desarrollo

## Introducción a Python

- REPL/*shell*, *scripts*, y *Jupyter Notebooks*
- Variables
- Objetos y clases
- Tipos/clases básicas (*built-in*): números (**int**, **float**), secuencias (**str**, **tuple**, **list**), y otros contenedores de objetos (**set**, **dict**)
- Control de flujo de ejecución del código: condicionales (`if... elif... else...`), bucles (`for...`, `while...`, `continue`, `break`), y captura de errores/excepciones (`try... except... else...`)
- Funciones básicas de Python (*built-in*) y funciones definidas por el usuario (`def...`)
- Módulos y paquetes (`import`)
