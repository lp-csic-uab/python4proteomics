# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     cell_metadata_json: true
#     comment_magics: true
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %% [markdown]
# ![Python4Proteomics](images/logo_p4p.png) **(Python For Poteomics)**
#
# # BEYOND: INTRODUCCIÓN A PYTEOMICS
#
# En este apartado veremos como utilizar algunas herramientas del paquete [_**Pyteomics**_](https://pyteomics.readthedocs.io), un paquete de Python específico para trabajar con datos proteómicos.
#
# ---
#

# %% [markdown]
# **Índice:**
#
#  * [I.- El paquete *Pyteomics*](#I.--El-paquete-Pyteomics)
#  * [II.- Tratamiento de ficheros MGF](#II.--Tratamiento-de-ficheros-MGF)
#  * [III.- Trabajando con masas](#III.--Trabajando-con-masas)
#
# ---
#

# %% [markdown] {"toc-hr-collapsed": false}
# # I.- El paquete *Pyteomics*
#
# <img src="images/beyond/pyteomics_logo.png" style="float: right; margin-left: 15px;" /> 
#
# [_**Pyteomics**_](https://pyteomics.readthedocs.io) es un paquete de Python formado por una colección de herramientas versatiles, fiables, y bien documentas, que ayudan a trabajar con diferentes tipos de datos proteómicos.  
# Pyteomics proporcionar módulos de código reutilizable que facilitan las tareas más comunes en el análisis de datos proteómico, tales como:
#   * Cálculo de propiedades fisico-químicas básicas de los polipéptidos:
#       * Masa y distribución isotópica.
#       * Carga y pI.
#       * Tiempo de retención.
#   * Acceso a archivos comunes de datos en proteómica:
#       * Datos de MS o LC-MS: archivos MGF, mzML, mzXML.
#       * Bases de datos FASTA: lectura, escritura, generación de _decoys_.
#       * Resultados de búsquedas proteómicas: archivos pepXML, X!Tandem, mzIdentML.
#   * Estimación y filtrado de resultados por FDR.
#   * Fácil manipulación de secuencias modificadas (tanto de péptidos como de proteinas).

# %% [markdown]
# ## II.- Tratamiento de ficheros MGF

# %% [markdown]
# ### Lectura de datos de espectros de masas
#
# Para leer un fichero en [formato **MGF**](http://www.matrixscience.com/help/data_file_help.html#GEN) utilizaremos la función `read( )` del módulo *pyteomics.mgf* para ir obteniendo los diferentes espectros de masas del archivo MGF:
# ```python
# mgf.read(source, convert_arrays=2, use_index=True)  ➞ IndexedMGF_or_MGF_object  ➞➞ dictionaries
# ```
#

# %% [markdown] {"jupyter": {"source_hidden": true}, "tags": ["sabermas"]}
# 📌 Algunos parámetros a destacar de esta función:
#  * *source* : suele ser una _cadena de carácteres_ indicando el _nombre del fichero_ (y la localización) a ir leyendo (o, alternativamente, un objeto de tipo _input/output_ (entrada/salida o _io_) que actue como *source*).  
#  * *convert_arrays* : ha de ser igual a `0`, `1` ó `2` (por _defecto_). Define el tipo de objeto contenedor de los datos de _carga_ (_z_), _masa/carga_ (_m/z_) e intensidad de los espectros: `0` para que sean listas estándard de Python; `1` ó `2` para que sean objetos de la clase **ndarray** de NumPy (el paquete _**numpy**_ deberá estar instalado en estos casos).
#  * *use_index* : es un parámetro _booleano_ que permite definir el tipo de objeto devuelto por la función: `True` (por _defecto_) para que éste sea de la clase **IndexedMGF**, y por tanto los espectros puedan accederse rápidamente mediante diferentes _índices_; o `False` para que sea de la clase **MGF** (mucho más simple, sin posibilidad de usar múltiples indices, y más lento si se quiere acceder a los espectros por su único indice: el _título_). En ambos casos el objeto devuelto es _iterable_.

# %%
# Importamos el módulos `mgf` del paquete `pyteomics`:
from pyteomics import mgf

# %%
# Preparamos la localización (ruta + nombre de fichero) del fichero MGF a leer:
import os # Importamos el paquete os ("Operating System")

data_relpath = "data" # Ruta relativa del fichero MGF
mgf_filename = "acE25_rep1_fs01.mgf" # Nombre del fichero MGF

mgf_file = os.path.join(os.path.abspath(data_relpath), mgf_filename)
mgf_file

# %% [markdown]
# El objeto devuelto por la función `mgf.read( )` (sea este de la tipo **IndexedMFG** o de tipo **MGF**) puede ser _iterado_ para ir obteniendo secuencialmente todos los espectros del archivo indicado:

# %%
# Obtenemos un objeto iterable utilizando mgf.read( ), pasándole un nombre de fichero (`mgf_file`):
spectra = mgf.read(mgf_file)

type(spectra)

# %% [markdown]
# Cada uno de los espectros que obtengamos a partir de éste objeto *iterable* será un diccionario, a través de cuyas _keys_ podremos acceder a toda la información disponible sobre dicho espectro en el archivo MGF:

# %%
# Obtenemos el siguiente espectro del objeto iterable `spectra` utilizando la 
# función `next( )` de Python (o, alternativamente el método `.next()` del propio objeto):
spectrum = next(spectra)

type(spectrum)

# %%
spectrum.keys() # keys presentes en un diccionario de espectro:

# %%
# La key 'params' permite acceder a un diccionario con meta-datos sobre el espectro:
spectrum['params']

# %% {"jupyter": {"outputs_hidden": true, "source_hidden": true}, "tags": ["sabermas", "alternativa"]}
# 📌 🔄 Utilizando el método `.next()` también obtenemos el siguiente espectro:
spectrum = spectra.next() 

spectrum['params']

# %% [markdown]
# ### Representación gráfica de un espectro de masas
#
# Utilizaremos el paquete _**matplotlib**_ para la representación gráfica de los datos de _intensidad_ y _masa/carga_ del espectro contenidos en las matrices (contenedores **ndarray** de NumPy) asociadas a las _keys_ `'intensity array'` y '`'m/z array'` de dicho espectro (diccionario `spectrum`):

# %%
# Importamos el módulo `pyplot` del paquete `matplotlib`:
import matplotlib.pyplot as plt

# %%
# Utilizamos un gráfico de barras para dibujar el espectro:
plt.bar(spectrum['m/z array'], # La key 'm/z array' referencía los valores de masa/carga de cada ión
        spectrum['intensity array'], # 'intensity array' referencía los valores de intensidad correspondientes
        edgecolor='black')

plt.title( spectrum['params']['title'] ) # Utilizamos el título del espectro como título del gráfico
plt.xlabel("m/z")
_ = plt.ylabel("Intensity")

# %% [markdown]
# ### Inspección gráfica de los meta-datos de todos los espectros de masas de un MGF
#
# Para cada espectro de masas contenido en el archivo MGF vamos a leer la _carga_, la _masa peptídica_ y el _tiempo de retención_ (_keys_ `'charge'`, `'pepmass'` y `'rtinseconds'` de diccionario de meta-datos asociado a la _key_ `'params'` del diccionario de cada espectro, que obtendremos al iterar el objeto devuelto por la función `mgf.read( )`):

# %%
# Volvemos a obtener un nuevo objeto iterable desde el inicio del fichero MGF:
spectra = mgf.read(mgf_file)

# %% {"jupyter": {"source_hidden": true}, "tags": ["sabermas", "alternativa"]}
# 🔄 Alternativamente, podemos utilizar el método `.reset()` (valido tanto para los 
# objetos IndexedMGF como para los de tipo MGF), para que al iterador del objeto se
# rinicie:
spectra.reset() 

# %%
# Listas que contendrán los valores de los metadatos:
pep_zs = list() # Cargas (z)
pep_mzs = list() # Masa/Carga (m/z) correspondientes
pep_rts = list() # Tiempos de retención (RT) correspondientes

# Iteramos todos los espectros del archivo MGF:
for spectrum in spectra:
    params = spectrum['params'] # Metadatos del espectro actual (key 'params')
    # Añadimos los datos a la lista correspondiente:
    pep_zs.append( params['charge'][0] )
    pep_mzs.append( params['pepmass'][0] )
    pep_rts.append( params['rtinseconds'] )

# %% [markdown]
# #### Péptidos por cada valor de carga:
#
# Para ésto, primero necesitamos calcular cuántos péptidos corresponden a cada valor de carga.  
# Una manera rápida y sencilla de hacerlo es recurriendo a un objeto de la clase **Counter** (del paquete _**collections**_ de la _librería estándar_ de Python):

# %%
from collections import Counter

charge2counts = Counter(pep_zs) # Creamos un objeto de typo Counter a partir de la lista de cargas

charge2counts

# %% {"jupyter": {"outputs_hidden": true, "source_hidden": true}, "tags": ["sabermas", "alternativa"]}
# 🔄 El uso de un objeto Counter sería equivalente a:

charge2counts = dict()
for z in pep_zs:
    charge2counts[z] = charge2counts.get(z, 0) + 1

charge2counts

# %% [markdown]
# Para observar gráficamente como se distribuyen los péptidos en base a su carga, lo mejor es un _histograma_:

# %%
plt.bar(charge2counts.keys(), charge2counts.values(), edgecolor='black') # Cargas (`.keys()`) vs. cuentas (`.values()`)

plt.title("Charges Frequencies")
plt.xlabel('Charge (z)')
_ = plt.ylabel('Peptides')

# %% [markdown]
# #### Relación entre la masa y el tiempo de retención:
#
# Primero hemos de calcular la masa de cada péptido (la lista `pep_masses`), a partir de su _masa/carga_ (`pep_mzs`) y su _carga_ (`pep_zs`):

# %%
pep_masses = list() # Lista que contendrá las masas (m) de los péptidos.
# Iteramos simultáneamente 2 listas (los valores de masa/carga y los de carga, para un mismo péptido),
# usando la función zip( ):
for mz, z in zip(pep_mzs, pep_zs):
    # Cálculo de la masa (m):
    m = (mz * z) - 1.008 * z # 1.008 → masa aproximada de un protón (H⁺)
    pep_masses.append(m)

# %% [markdown]
# Para observar gráficamente la relación entre la masa y el tiempo de retención, recurriremos a una _gráfica de disperción_:

# %%
# Usando la función plt.plot( ) sin líneas entre marcadores:
plt.plot(pep_rts, pep_masses, 'b.', markersize=1) # 'b.' → 'b': blue, '.': tipo de marcador, nada: sin líneas entre marcadores

plt.title('Retention Time ~ Mass')
plt.xlabel('Retention Time (seconds)')
_ = plt.ylabel('Peptide Mass')

# %% [markdown]
# Si además queremos observar la relación entre la carga y la masa y el tiempo de retención (_3 variables_), podemos recurrir igualmente a una _gráfica de disperción_, pero utilizando en este caso la función `plt.scatter( )`, ya que nos permite colorear cada punto en función de una tercera variable, pasada como argumento del parámetro *c* (de _c_olor):

# %%
# Usando la función plt.scatter( ) :
scatter = plt.scatter(pep_rts, pep_masses, s=0.5, cmap='hsv', c=pep_zs) # `s`: tamaño del marcador; `c`: color de cada punto.

plt.title('Retention Time ~ Mass ~ Charge')
plt.xlabel('Retention Time (seconds)')
plt.ylabel('Peptide Mass')
_ = plt.legend( *scatter.legend_elements() ) # This should work with matplotlib 3.3.x

# %% {"jupyter": {"outputs_hidden": true, "source_hidden": true}, "tags": ["alternativa"]}
# 🔄 Alternativa para versiones de matplotlib < 0.3.3 :
for z_of_interest in sorted(charge2counts.keys()):
    # Listas que contendrán los valores de los metadatos:
    pep_masses_z = list() # Masas para péptidos con carga 5 (z=5)
    pep_rts_z = list() # Tiempos de retención (RT) correspondientes

    # Iteramos tres listas a la vez gracias a la función zip( ):
    for m, z, rt in zip(pep_masses, pep_zs, pep_rts):
        if z == z_of_interest: # Si la carga es igual a la carga elegida (`z_of_interest`):
            # Añadimos los datos a la lista correspondiente:
            pep_masses_z.append( m )
            pep_rts_z.append( rt )

    plt.plot(pep_rts_z, pep_masses_z, '.', markersize=1, alpha=0.5, label="z = " + str(z_of_interest))

plt.title("Retention Time ~ Mass ~ Charge")
plt.xlabel('Retention Time (seconds)')
plt.ylabel('Peptide Mass')
_ = plt.legend(markerscale=5)

# %% [markdown]
# O bien podemos ir filtrando los péptidos para una determinada carga, e ir graficando:

# %%
# Indicar la carga elegida:
z_of_interest = 5

# Listas que contendrán los valores de los metadatos:
pep_masses_z = list() # Masas para péptidos con carga 5 (z=5)
pep_rts_z = list() # Tiempos de retención (RT) correspondientes

# Filtrado de datos por carga (iteramos tres listas a la vez gracias a la función zip( )):
for m, z, rt in zip(pep_masses, pep_zs, pep_rts):
    if z == z_of_interest: # Si la carga es igual a la carga elegida (`z_of_interest`):
        # Añadimos los datos a la lista correspondiente:
        pep_masses_z.append( m )
        pep_rts_z.append( rt )

# %%
# Representación gráfica:
plt.plot(pep_rts_z, pep_masses_z, 'b.', markersize=1)

plt.title("Retention Time ~ Mass (Only for z = " + str(z_of_interest) + ")")
plt.xlabel('Retention Time (seconds)')
_ = plt.ylabel('Peptide Mass')

# %% [markdown]
# ## III.- Trabajando con masas
#
# Utilizaremos la función `calculate_mass( )` del módulo *pyteomics.mass* para obtener la _masa_ de aminoácidos, secuencias peptídicas, e incluso fórmnulas químicas:
# ```python
# mass.calculate_mass(sequence_or_formula="", charge=0, average=False)  ➞ mass_as_a_float
# ```

# %% [markdown] {"jupyter": {"source_hidden": true}, "tags": ["sabermas"]}
# 📌 Algunos _**parámetros**_ a destacar de esta función:
#  * *primer parámetro* : acepta como argumento una _cadena de carácteres_ que contenga una _secuéncia peptídica_ o una _fórmula química_. Generalmente la función identificará si se trata de una o de otra y devolverá la massa correctamente.  
#    Sin embargo, existen _casos ambiguos_, que no permiten diferenciar si se trata de una secuéncia peptídica o una fórmula química. En estos casos, se puede indicar de qué se trata (_desambiguación_), anteponiendo al argumento pasado el nombre de parámetro *sequence* (para _secuencias peptídicas_) o *formula* (para _fórmulas químicas_).  
#  * *charge* : puede pasarse como argumento de este parámetro un _número entero_ que indique la _carga_ de la _secuencia peptídica_, y así la función devuelva un valor de _masa/carga_ (_m/z_).  
#    Por defecto su valor es `0`, devolviendose un valor de masa (_m_) y no de masa/carga.
#  * *average* : es un parámetro _booleano_ que permite definir si se devuelve la _masa isotópica_, `False` (por _defecto_); o bien la _masa promedio_ `True`.

# %%
# Importamos el módulos `mass` del paquete `pyteomics`:
from pyteomics import mass

# %%
mass.calculate_mass('M') # AA (Metionina)

# %%
mass.calculate_mass('MATYLDEGGK') # Secuencia peptídica (average=False → masa monoisotópica)

# %%
mass.calculate_mass('MATYLDEGGK', average=True) # Secuencia peptídica (average=True → masa promedio)

# %%
mass.calculate_mass('MATYLDEGGK', charge=2) # Secuencia peptídica (charge=2 → cálculo de masa/carga)

# %%
mass.calculate_mass("H+") # Elemento químico (protón)

# %%
mass.calculate_mass("C8H10N4O2") # Fórmula (cafeína)

# %%
mass.calculate_mass("HCOOH") # ¿Secuencia o fórmula? → Por defecto es considerada una secuencia peptídica

# %%
mass.calculate_mass(formula="HCOOH") # Desambiguación → Forzamos a que sea interpretada como una fórmula química

# %% [markdown]
# ### Representación gráfica de masas/cargas de péptidos

# %% [markdown]
# Adicionalmente, el módulo *pyteomics.mass* posee algunas variables (_"atributos"_), como *mass*`.std_aa_mass` (un diccionario que asocia el código de una letra de cada [aminoacido proteogénico](https://en.wikipedia.org/wiki/Proteinogenic_amino_acid) a su masa menos 1 molécula de H₂O), que nos pueden resultar de utilidad para un pequeño experimento teórico:

# %%
aa2mass = mass.std_aa_mass # Diccionario aminoacido → (masa - H₂O)

print(len(aa2mass))
aa2mass

# %% [markdown]
# Para el experimento vamos a crearnos una función (`randompeps_sample_mzs( )`) que genere todos los posibles péptidos de la longitud que le indiquemos (parámetro *pep_length*), seleccione una muestra aleatoria del tamaño que le indiquemos (parámetro *sample_size*), y nos retorne su _masa/carga_ (parámetro *charge*):

# %%
import random # Módulo para la generación o selección de datos aleatorios.
from itertools import combinations_with_replacement as comb_w_rplc # Función de combinatoria,

# %%
AAs = aa2mass.keys() # Los códigos de 1 letra de los 22 aminoácidos proteogénicos.

def randompeps_sample_mzs(pep_length, charge=0, sample_size=None):
    # Combinatoria para generar los posibles péptidos de la longitud indicada (`pep_length`):
    population = tuple( comb_w_rplc(AAs, pep_length) )
    # Muestreo aleatorio (`sample_size`):
    sample = random.sample(population, sample_size or len(population))
    # Calcular m/z de la muestra:
    sample_mzs = list()
    for seq_aas in sample:
        seq_mz = mass.calculate_mass(sequence="".join(seq_aas), charge=charge)
        sample_mzs.append(seq_mz)
    return sample_mzs


# %% [markdown]
# Ahora utilizaremos la función `randompeps_sample_mzs( )` para generar 2 conjuntos de datos:

# %%
# 200.000 péptidos diferentes de 6 AAs y carga 2:
mzs_6aas_z2 = randompeps_sample_mzs(6, 2, 200000)

# %%
# 200.000 péptidos diferentes de 8 AAs y carga 3:
mzs_8aas_z3 = randompeps_sample_mzs(8, 3, 200000)

# %% [markdown]
# Y representaremos estos datos de _masa/carga_ en forma de histogramas, para estudiar su distribución:

# %%
import matplotlib.pyplot as plt

# %%
plt.figure( figsize=(15,5) )

plt.hist(mzs_6aas_z2, bins=1000, alpha=0.5, label='6 AAs; z=2')
plt.hist(mzs_8aas_z3, bins=1000, alpha=0.5, label='8 AAs; z=3')

plt.xlabel('m/z')
plt.ylabel('Peptides')
_ = plt.legend()

# %% [markdown]
# Y, si ampliamos el rango de _masa/carga_ entre 350 y 360, podemos observar la existencia de patrones de _"masa/cargas prohibidas"_ para los péptidos, en función de su longitud:

# %%
plt.figure( figsize=(15,5) )

# Zoom (uso del parámetro `range`):
plt.hist(mzs_6aas_z2, bins=500, range=(350, 360), alpha=0.5, label='6 AAs; z=2')
plt.hist(mzs_8aas_z3, bins=500, range=(350, 360), alpha=0.5, label='8 AAs; z=3')

plt.xlabel('m/z')
plt.ylabel('Peptides')
_ = plt.legend()
