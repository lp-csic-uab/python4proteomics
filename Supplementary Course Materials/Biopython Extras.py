# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     comment_magics: true
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %% [markdown]
# ![Python4Proteomics](../images/logo_p4p.png) **(Python For Proteomics)**
#
# # INTRODUCCIÓN A BIOPYTHON (_**Extras**_)
#
# En este apartado veremos como utilizar algunas herramientas **extras** del paquete [**Biopython**](https://biopython.org/), y otros paquetes como [pronto](https://pronto.readthedocs.io/) para la lectura de ontologías (_Gene Ontology_):
#
#  * Biopython (_notebook_ [2.1 Biopython.ipynb](../2.1%20Biopython.ipynb)):
#     - Breve introducción a Biopython.
#     - Tratamiento de ficheros [FASTA](https://en.wikipedia.org/wiki/FASTA) (lectura, análisis y manipulación).
#     - Funciones estadísticas _básicas_ de Python (módulo [statistics](https://docs.python.org/3/library/statistics.html)) y del paquete [numpy](http://www.numpy.org/).
#     - Gráficas sencillas utilizando el paquete [matplotlib](https://matplotlib.org/).
#     - Filtrado de proteínas de un archivo FASTA.
#     - Búsquedas con [Blastp](https://blast.ncbi.nlm.nih.gov/Blast.cgi) en base de datos de proteínas.
#     - Referencias y material de ampliación.
#
#
#  * **Extras** (este _notebook_):
#     - Obtención de información sobre proteínas desde [UniProt](https://www.uniprot.org/).
#     - Lectura de Anotaciones de Gene Ontology ([GOA](https://geneontology.github.io/docs/go-annotations/)) para proteínas y filtrado según estas anotaciones.
#     - Referencias y material de ampliación.
#
# ---
#

# %% [markdown]
# **Índice de los Extras:**
#
#  * [I.- Información extra desde UniProt](#I.--Informaci%C3%B3n-extra-desde-UniProt)
#    * [Obteniendo la información _on-line_](#Obteniendo-la-información-on-line)
#    * [Procesando la información obtenida](#Procesando-la-informaci%C3%B3n-obtenida)
#    * [Poniéndolo todo junto](#Poni%C3%A9ndolo-todo-junto)
#  * [II.- Gene Ontology Annotations](#II.--Gene-Ontology-Annotations)
#    * [Lectura de datos _GOA_ desde un fichero local](#Lectura-de-datos-GOA-desde-un-fichero-local)
#    * [Lectura de términos _GO_ desde un fichero local](#Lectura-de-términos-GO-desde-un-fichero-local)
#    * [Filtrado de proteínas usando *GOA*s y términos _GO_](#Filtrado-de-proteínas-usando-GOAs-y-términos-GO)
#  * [III.- Referencias y material de ampliación 🔗](#III.--Referencias-y-material-de-ampliaci%C3%B3n-🔗)
# ---
#

# %% [markdown]
# # I.- Información extra desde UniProt
#
# En este apartado veremos como obtener información extra sobre proteínas desde [UniProt](https://www.uniprot.org/) utilizando su [API Web](https://en.wikipedia.org/wiki/Web_API "Web Application Programming Interface"), y cómo procesarla después.

# %% [markdown]
# ## Obteniendo la información _on-line_
#
# Recurriremos a la función `urlopen( )`, del módulo _**request**_ en el paquete _**urllib**_ (de la _librería estándard_ de Python):

# %%
from urllib import request # Importamos el módulo `request` del paquete `urllib`

# %% [markdown]
# La función `request.urlopen( )` requiere como mínimo un argumento: la [URL](https://en.wikipedia.org/wiki/URL "Uniform Resource Locator") en la que se encuentra el archivo o página web a recuperar:
# ```python
#     request.urlopen(url[, ...])  ➞ response_object
# ```
# Al ser llamada con una URL válida, la función se _conectará_ al servidor indicado en la URL y le enviará una _petición_ para acceder al recurso indicado en dicha URL (un archivo, una carpeta, ...). Tras obtener una _respuesta_ válida del servidor, la función retornará, devolviendo (salvo que ocurra algún error en la conexión) un objeto que suele ser de tipo **HTTPResponse**.  

# %%
# Ejemplo de petición a una página web:

response = request.urlopen('http://proteomica.uab.cat')

print(type(response))
vars( response )

# %% tags=["sabermas"] jupyter={"source_hidden": true}
# 📌 Información básica de la respuesta recibida del servidor:
print("- Url :", response.geturl() ) # También disponible a través del atributo `.url`
print("- Status code :", response.getcode() ) # También a través del atributo `.status`
print("- Encabezados (meta-información) :\n", response.info(), sep='') # También a través de `.headers`

# %% [markdown]
# A pesar de sus diferencias, estos objetos _Response_ son, en cierta manera, como los objetos _input/output_ que ya hemos visto. Así, poseen un método `.read()` para _leer_ el contenido de la respuesta dada por el servidor , y un método `.close()` para _cerrar_ la conexión después:

# %%
index_html = response.read() # Lectura del contenido de la respuesta (en forma de datos binarios).

response.close() # Una vez leído el contenido, ya podemos cerrar el objeto input/output.

print(type(index_html), "\n") # Datos binarios (tipo/clase bytes).
print( index_html[:1000] )

# %% [markdown] jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 Como podemos ver, los datos devueltos por el método `.read()` de un objeto _Response_ son _datos binarios_: una _cadena de bytes (clase **bytes**).
#
# Para _convertir_ estos datos binarios en una _cadena de caracteres_ (texto en formato Unicode o UTF-8), utilizaremos el método `.decode( )` pasándole el argumento `'utf8'` como formato de decodificación a emplear:

# %% jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 Datos binarios (bytes) ➞ Texto Unicode UTF-8 (str):
index_html = index_html.decode('utf8')

print(type(index_html), "\n") # Datos de texto en Unicode (clase str)
print( index_html[:1000] ) # Visualizamos parte del código HTML devuelto:

# %% [markdown]
# <br/>
#
# Por lo que respecta a la [**API web de UniProt**](https://www.uniprot.org/help/api_retrieve_entries "UniProt Programmatic Access - Retrieving individual entries"), para obtener información sobre _una_ única proteína, sólo hemos de acceder a la dirección `https://www.uniprot.org/uniprot/`, añadiéndole el `Accession number` de la proteína seguido de un _punto_ (`.`) y el `formato` en que queremos recuperar la información (XML, FASTA, RDF, ...):  
# > [https://www.uniprot.org/uniprot/**P11217._xml_**](https://www.uniprot.org/uniprot/P11217.xml "Ejemplo para la proteína P11217, recuperando los datos en formato XML")

# %% [markdown]
# Así, usando la función `request.urlopen( )`, que acabamos de ver:

# %%
# Construimos la URL según la API Web de UniProt:
prot_ac = 'P11217' # Accession number
prot_url = "https://www.uniprot.org/uniprot/" + prot_ac + ".xml" # ".xml" ➞ formato XML

# Recuperamos la información sobre la proteína:
response = request.urlopen(prot_url) # Petición web y obtención de la respuesta de UniProt
prot_xml = response.read() # Lectura del contenido binario de la respuesta
response.close() # Cerramos el objeto de entrada/salida.

# Visualizamos parte del XML obtenido:
print( prot_xml[:443] )

# %% tags=["sabermas"] jupyter={"source_hidden": true}
# 📌 Datos binarios (bytes) ➞ Texto Unicode UTF-8 (str):
prot_xml = prot_xml.decode('utf8')

print( prot_xml[:443] ) # Visualizamos parte del código XML devuelto:

# %% [markdown]
# ## Procesando la información obtenida
#
# En lugar de leer directamente el contenido del objeto _Response_ utilizando el método `.read()` para obtener un texto en XML que resulta bastante _difícil_ de leer e interpretar por una persona; podemos pasar este objeto _Response_ como primer argumento (*handle*) de la función `SeqIO.read( )` de *Biopython* (vista [previamente](2.1%20Biopython.ipynb#Lectura-de-datos "En el notebook '2.1 Biopython.ipynb', apartado 'Lectura de datos FASTA'")) para obtener un objeto de tipo **SeqRecord**, que nos haga mucho _más fácil_ el leer e interpretar la información que hemos obtenido sobre la proteína:

# %%
from Bio import SeqIO # Importamos el sub-paquete `SeqIO` de Biopython

# %% [markdown]
# ```python
# SeqIO.read(handle, format, alphabet=None)  ➞ 1 seqrecord_object
# ```
# A esta función, además del objeto _Response_ obtenido desde UniProt (para hacer de *handle*), es necesario pasarle también el tipo de formato de los datos que debe interpretar (parámetro *format*), que en este caso ha de ser `'uniprot-xml'`:

# %%
# Hay que volver a realizar la petición a UniProt, ya que los objetos de tipo Response se
# "consumen" al ser leídos, cosa que ya hemos hecho al invocar antes el método .read():
uprot_response = request.urlopen(prot_url) # Petición web y obtención de la respuesta de UniProt

# Obtención de un objeto SeqRecord a partir del XML contenido en el objeto Response:
prot_record = SeqIO.read(uprot_response, 'uniprot-xml') # format = 'uniprot-xml'

uprot_response.close() # Cerramos el objeto Response.

prot_record # Información obtenida en forma de objeto SeqRecord:

# %% [markdown]
# Como podemos ver, un objeto **SeqRecord** contiene _mucha más información_ al generarse desde un registro XML de UniProt que cuándo se genera desde un registro FASTA.  

# %% [markdown]
# Posee, pues, los _atributos que ya conocíamos_ de este tipo de objetos: `.id` (en este caso sólo el _Accession number_ de la proteína), `.description` (su _descripción_) y `.seq` (que apunta a un _objeto de tipo **Seq**_):

# %%
print("ID de la proteína :", prot_record.id)
print("Descripción :", prot_record.description)
print("Secuencia :", prot_record.seq[:100] + "... ", prot_record.seq.alphabet)

# %% [markdown]
# Pero _además_, posee los atributos `.features` y `.annotations`, de interés por la gran cantidad de información que pueden proporcionarnos:
#  * ```python
#    SeqRecord.features  ➞  [ list_of_seqfeature_objects ... ]
#    ```
#    Apunta a una _lista_ de objetos de tipo **SeqFeature**, cada uno de los cuales representa una _característica_ (_feature_) de uno o varios aminoácidos de la secuencia proteica (atributo `.location` de cada objeto _SeqFeature_); pudiendo ser estas características de diferente _tipo/naturaleza_ (atributo `.type` de cada objeto _SeqFeature_): cadenas proteicas, residuos modificados, sitios de unión a substrato u otras proteínas, variantes de la secuencia (isoformas, alelos, mutaciones, ...), estructuras secundarias, ...
#

# %%
prot_record.features

# %%
print( prot_record.features[10] ) # Objeto SeqFeature con índice 10 dentro de la lista:

# %% [markdown]
#  * ```python
#    SeqRecord.annotations  ➞  annotations_dictionary
#    ```
#    Referencía un _diccionario_ con una gran cantidad de _meta-información_ sobre la proteína: todos sus _Accesion numbers_ (_key_ `'accessions'`), o los de sus productos alternativos e isoformas (_key_ `'comment_alternativeproducts_isoform'`), sus funciones (_key_ `'comment_function'`), el gen que la codifica (_key_ `'gene_name_primary'`), las referencias bibliográficas (_key_ `'references'`), ...

# %%
prot_record.annotations # Diccionario de anotaciones sobre la proteína:

# %%
prot_record.annotations['organism'] # Valor de la key 'organism':

# %% [markdown]
# ## Poniéndolo todo junto
#
# Podemos aprovechar todo lo visto en este apartado para construir una función (`get_uniprot_data4ac( )`) que nos devuelva algo de información extra de una proteína a partir del _Accesssion number_:

# %%
import time # Importamos el módulo time.

from Bio import SeqIO # Importamos el sub-paquete `SeqIO` de Biopython.

from urllib import request # Importamos el módulo `request` del paquete `urllib`.
from urllib.error import HTTPError, URLError # Importamos las excepciones para errores de conexión.

# %%
def get_psites4protrecord(prot_record):
    """Devuelve los sitios de fosforilación (p-sites) en la secuencia de la proteína, 
    a partir de la información contenida en algunos de los objetos SeqFeature del 
    objeto SeqRecord proporcionado (parámetro `prot_record`)"""
    psites = list() # La lista de phospho-sites (p-sites).
    # Recorre los objetos `SeqFeature` de la lista asociada al atributo `SeqRecord.features` 
    # de `prot_record` para seleccionar los posibles AAs fosforilables (p-sites):
    for feature in prot_record.features:
        # Selecciona p-sites: `SeqFeature.type` == "modified residue" 
        #                     `SeqFeature.qualifiers`["description"] == "phosphoserine" o "phosphotyrosine" o "phosphothreonine"
        if ( feature.type.lower() == 'modified residue' and 
             'phospho' in feature.qualifiers['description'].lower() ):
            aa_pypos = int(feature.location.start) # Posición del AA fosforilable (Ej. 203). `SeqFeature.location.start`
            aa = prot_record.seq[aa_pypos] # AA fosforilable a partir de la secuencia (`SeqRecord.seq`) y la posición (Ej. 'Y')
            psites.append( aa + str(aa_pypos + 1) ) # AA + Posición Biológica (Ej.: 'Y204')
    return psites

# %%
def get_uniprot_data4ac(ac):
    """Devuelve el nombre (`name`, str), las funciones (`functions`, list) y los `psites` (list) 
    de una proteína, a partir de su Accesssion number (parámetro `ac`)"""
    # Esperar un segundo, para no saturar el servidor (http error 503) en caso de
    # múltiples llamadas consecutivas a esta función:
    time.sleep(1) # Función `sleep(seconds)` del módulo `time`
    
    # Recuperar la información sobre la proteína:
    # - Construir la URL para usar la API Web de UniProt:
    prot_url = "https://www.uniprot.org/uniprot/" + ac + ".xml"
    # - Petición al servidor web de UniProt:
    try:
        uprot_response = request.urlopen(prot_url) # Petición web y obtención de la respuesta de UniProt.
    except (ConnectionError, HTTPError, URLError) as error: # En caso de errores de conexión:
        print("Error de conexión", error, ". No se pudo recuperar información para", ac, "\n")
        return None
    # - Conversión de datos XML en un objeto SeqRecord:
    prot_record = SeqIO.read(uprot_response, 'uniprot-xml') # Formato de la información: 'uniprot-xml'.
    # - Cerrar la conexión (el objeto Response):
    uprot_response.close()
    
    # Procesar la información obtenida:
    # - Obtener el nombre y la lista de funciones de la proteína:
    name = prot_record.description
    functions = prot_record.annotations.get('comment_function', list() )
    # - Obtener los sitios de fosforilación en la secuencia de la proteína:
    psites = get_psites4protrecord(prot_record)
            
    return name, functions, psites # Retorna los datos como una tupla de 3 elementos

# %%
get_uniprot_data4ac('P11217')

# %% [markdown]
# <br/>
#
# Y ahora **probaremos** la función `get_uniprot_data4ac( )` con algunos _Accession numbers_ del archivo FASTA _**sin** isoformas_ que hemos generado previamente en la [segunda parte](2.2%20Biopython.ipynb "En el notebook '2.2 Biopython.ipynb'")).

# %% [markdown] tags=["ejercicio"]
# > 📝 **Práctica:**
# >
# > Para ello, primero **reconstruye** el diccionario `noisoac2seqrecord` (_AC no isoforma ➞ SeqRecord_) a partir del archivo FASTA _sin isoformas_ que habíamos guardamos en disco:

# %% tags=["ejercicio"]
# Localización y nombre del fichero FASTA sin isoformas:
NOISOFORMS_FASTA_FILE = '../data/noisoforms-uniprot-reviewed-Human-9606.fasta'
# Este fichero contiene 20409 registros de proteínas humanas (sin isoformas), y 
# fue generado en la Parte 1 de esta Introducción a Biopython.

# %% [markdown] tags=["ejercicio"]
# > 💡 _Recuerda:_ necesitarás utilizar las _funciones_ `parse( )` y `to_dict( )` del sub-paquete `SeqIO` de Biopython, además de definir una función propia (`def`) para obtener sólo *AC*s como _keys_ del diccionario:

# %% tags=["ejercicio"]
from Bio import SeqIO # Importamos el sub-paquete `SeqIO` de Biopython,si no lo hemos importado antes
from Bio.Alphabet import generic_protein # Importamos el alfabeto para secuencias proteicas genéricas



# %% tags=["ejercicio", "solucion"] jupyter={"source_hidden": true}
# 📎 Solución:

from Bio import SeqIO # Importamos el sub-paquete `SeqIO` de Biopython,si no lo hemos importado antes
from Bio.Alphabet import generic_protein # Importamos el alfabeto para secuencias proteicas genéricas

# Obtenemos un objeto generador de SeqRecords a partir del fichero FASTA:
records_reader = SeqIO.parse(NOISOFORMS_FASTA_FILE, 'fasta', alphabet=generic_protein)

# Función `get_accession( )` para obtener el ACcession number de un objeto SeqRecord:
def get_accession(seq_record):
    """Retorna el ACcession number de un objeto SeqRecord (parámetro `seq_record`)"""
    parts = seq_record.id.split('|') # Divide la cadena de caracteres `id` del objeto SeqRecord.
    return parts[1] # Retorna el segundo elemento (el Accession number)

# Obtenemos el diccionario AC no isoforma ➞ SeqRecord a partir del generador anterior, utilizando la 
# función `get_accession( )` que acabamos de definir:
noisoac2seqrecord = SeqIO.to_dict(records_reader, key_function=get_accession)

print("Registros leídos de '" + NOISOFORMS_FASTA_FILE + "' : ", len(noisoac2seqrecord))

# %% [markdown]
# Ahora, ya podemos obtener la información de UniProt para los 10 primeros _Accession Numbers_ del diccionario `noisoac2seqrecord`:

# %%
# Ejemplo de uso de la función `get_uniprot_data4ac( )`:

ACs_TO_GET = 10 # Número de Accession Numbers a obtener

print('Obteniendo información desde UniProt ...\n')

for counter, ac in enumerate( noisoac2seqrecord.keys() ):
    if counter >= ACs_TO_GET: # Sólo procesar los primeros `ac`, después salir del bucle
        break
    # Obtener la información devuelta por la función `get_uniprot_data4ac( )`:
    uprot_data = get_uniprot_data4ac(ac)
    if uprot_data is None: # Si ha habido un fallo de conexión pasar al siguiente `ac`
        continue
    # Mostrar la información obtenida por pantalla:
    name, functions, psites = uprot_data
    print("AC :", ac, "  Nombre :", name)
    if functions:
        print("Funciones :", "\n".join(functions))
    if psites:
        print("Sitios de fosforilación :", ", ".join(psites))
    print()
    
print('... Proceso Finalizado!')

# %% [markdown] toc-hr-collapsed=false
# # II.- _Gene Ontology Annotations_


# %% [markdown] toc-hr-collapsed=false
# ## Lectura de datos _GOA_ desde un fichero local
#
# Para leer los registros de Anotaciones _Gene Ontology_ ([_GOA_](http://geneontology.org/docs/go-annotations/ "GO Annotations")) de proteínas desde un _fichero local_, utilizaremos la función `gafiterator( )` del módulo *GOA* del sub-paquete *Bio.UniProt*:

# %%
# Importamos la función `gafiterator( )` del módulo `GOA` del sub-paquete `UniProt` de Biopython:
from Bio.UniProt.GOA import gafiterator

# %% [markdown]
# ```python
#     GOA.gafiterator(input_output_object)  ➞ generator_object  ➞➞ dictionary_objects
# ```
#
# Esta función necesita como argumento un _objeto de tipo input/output_, el cual obtendremos al abrir el _fichero en [formato GAF](http://geneontology.org/docs/go-annotation-file-gaf-format-2.1/ "GO Annotation File (GAF) format 2.1") (Gene Association File)_ que contiene las _anotaciones GO_ de proteínas (en nuestro caso, el fichero `goa_human.gaf` de la _UniProtKB-GOA Database_ previamente descargado desde http://ftp.ebi.ac.uk/pub/databases/GO/goa/HUMAN ):

# %%
# Localización y nombre del fichero GAF:
GAF_GZ_FILE = '../data/goa_human.gaf.gz'
# Este fichero contiene 476061 registros de anotaciones GO de la UniProtKB-GOA Database
# release 2019-02-13:
# http://ftp.ebi.ac.uk/pub/databases/GO/goa/HUMAN

# %% [markdown]
# Pero, como podemos ver, nuestro fichero de _anotaciones GO_ está _comprimido_ con [_gzip_](http://www.gzip.org/), una utilidad típica de compresión en el mundo [Unix](https://en.wikipedia.org/wiki/Unix).
# Así pues, podemos descomprimir _a mano_ el fichero GZ para obtener el archivo GAF sin comprimir (en texto plano) y poder así leerlo con la función _built-in_ `open(filename, mode)` que ya hemos visto.
#
# O podemos utilizar la función `open(gzip_file, mode)` del módulo _**gzip**_ (de la _librería estándar_ de Python), que nos permitirá _abrir_ directamente el archivo comprimido, e irá _descomprimiendo_ los datos en memoria _automáticamente_ conforme sea requerido, sin que nosotros tengamos que hacer nada más (de forma _transparente_), y sin tener que ocupar más espacio en disco duro con la versión no comprimida del archivo:

# %%
import gzip # Importamos el módulo `gzip` de la librería estándar de Python.

# Abrimos el fichero GAF comprimido ('data/goa_human.gaf.gz') utilizando la función `gzip.open( )`, 
# indicando un modo de lectura de texto (mode='rt' ➞ 'r' por read, 't' por text):
iofile = gzip.open(GAF_GZ_FILE, 'rt')

iofile # Objeto input/output. A efectos prácticos como si fuera de un archivo descomprimido normal:

# %% [markdown]
# Ahora, con la función `GOA.gafiterator( )` obtendremos un objeto de tipo **generator** a partir del objeto _input/output_ anterior. Y al ir iterando, este _generador_ irá devolviendo los diferentes _registros de anotaciones GO_ como _diccionarios_ (uno por cada una de las anotaciones _GO_):

# %%
# Obtenemos un generador de registros GOA, utilizando `gafiterator( )` pasándole el objeto input/output:
goa_generator = gafiterator(iofile)

goa_generator # Objeto generador:

# %%
# Obtenemos un registro GOA del generador (función `next( )` de Python):
go_annotation = next(goa_generator)

go_annotation # Registro GOA obtenido como diccionario (objeto de tipo dict):

# %% [markdown] tags=["sabermas"]
# _Información útil_ que encontramos en estos diccionarios de _anotaciones GO_: 
#  * _Key_ `'DB_Object_ID'`: el _Accesion number_ de la _proteína_ a la que se refiere la anotación (sin indicativo de isoformas u otras variantes).
#  * _Key_ `'GO_ID'`: el _identificador_ ('GO:NUMERO') del _término GO_ con el que se anota dicha proteína.
#  * _Key_ `'Qualifier'`: es una _lista de [calificadores / modificadores](http://geneontology.org/docs/go-annotations/#annotation-qualifiers)_ que _modifican_ el significado de la anotación (del _término GO_ en relación con la _proteína_ anotada). Puede contener _ninguno_, _uno_ o _varios_ de los siguientes términos _modificadores_: `'NOT'`, `'contributes_to'`, `'colocalizes_with'`.
#  * _Key_ `'Aspect'`: el [_dominio_ / _aspecto_ o _"namespace"_](http://geneontology.org/docs/ontology-documentation/) al que pertenece el término _GO_. Sus _valores_ pueden ser: `'F'` ➞ _**F**unción_ biológica, `'C'` ➞ _**C**omponente_ celular, o `'P'` ➞ _**P**roceso_ biológico.
#  * [**...**](http://geneontology.org/docs/go-annotation-file-gaf-format-2.1/#gaf-fields "GAF fields")

# %%
iofile.close() # Cerramos el fichero (método `.close()` del objeto io) una vez leídos los registros.


# %% [markdown] toc-hr-collapsed=false
# <br />
#
# A continuación **leeremos todos** los registros _GOA_ del _fichero GAF_ para tener todos los _identificadores_ de términos _GO_ (*GO Term ID*s) de todas las anotaciones _asociadas_ a cada una de las _proteínas_ (a sus *AC*s) de nuestro fichero FASTA, en forma de un sencillo _diccionario de sets_ (diccionario `ac2goids`):
#
# ```python
# Accession number ➞  { GO Term IDs .... }
# ```

# %% [markdown]
# Para ello iremos iterando (bucle `for`) sobre el generador devuelto por la función `GOA.gafiterator( )`, leeremos los campos (_keys_) `'DB_Object_ID'` (_Accesion number_ de la proteína) y `'GO_ID'` (_identificador_ del término _GO_) de cada registro _GOA_, e iremos almacenando estos datos en el diccionario `ac2goids` (el _diccionario de sets_) _sólo_ si el _AC_ de la proteína anotada coincide con _alguno_ de los *AC*s de nuestro diccionario `noisoac2seqrecord` (re-generado en el apartado anterior desde el fichero FASTA sin isoformas):

# %%
ac2goids = dict() # El diccionario de sets: Accession number ➞  { GO Term IDs .... }

goas_read = 0 # Número de anotaciones GO leídas del archivo GAF.
goas_processed = 0 # Número de anotaciones GO utilizadas para generar nuestro diccionario.
goids_stored_qualified = set() # GO IDs modificados por "Qualifiers".


iofile = gzip.open(GAF_GZ_FILE, 'rt') # Abrimos el fichero .GAF.GZ

goa_generator = gafiterator(iofile) # Obtenemos un generador de registros GOA.

# Construimos el diccionario a medida que leemos los registros GOA:
print("Working, please wait ...\n")
for go_annotation in goa_generator:
    goas_read += 1
    ac = go_annotation['DB_Object_ID'] # Obtenemos el Accession Number de la proteína (`ac`).
    # Evita almacenar datos de GOAs para proteínas que no estaban en nuestro archivo FASTA inicial:
    if ac not in noisoac2seqrecord:
        continue
    goas_processed += 1
    go_id = go_annotation['GO_ID'] # Obtenemos el identificador del término GO (`go_id`).
    # Marcar los GO ID de anotaciones GO con modificadores("qualifiers"):
    go_qualifiers = go_annotation['Qualifier']
    if go_qualifiers != ['']: # Algún modificador del significado del término GO asociado:
        go_id = ",".join(go_qualifiers) + " " + go_id # Marcar este GO ID con dichos modificadores
        goids_stored_qualified.add(go_id)
    # Añadir el GO ID del registro GOA (`go_id`) al conjunto de GO IDs asociado al AC actual (`ac`):
    try:
        ac2goids[ac].add(go_id) # Este AC ya está en el diccionario (ya presenta algún GO ID asociado) ➞ no da error
    except KeyError as error: # Error debido a que el AC aún no se había añadido al diccionario:
        ac2goids[ac] = {go_id} # Añadir el AC y su primer GO ID asociado

iofile.close() # Cerramos el fichero .GAF.GZ


# Información sobre el proceso y el resultado:
print("GOAs leídas:", goas_read)
print("GOAs utilizada para crear el diccionario `ac2goids`:", goas_processed)
print("Proteínas con anotaciones GO:", len(ac2goids))
print("Proteínas del archivo FASTA no anotadas:", len(noisoac2seqrecord) - len(ac2goids))
allgoids = set()
for goids in ac2goids.values():
    allgoids.update(goids)
print("Términos GO diferentes en `ac2goids`:", len(allgoids))
print("De lo cuales, presentan modificadores:", len(goids_stored_qualified))

# %%
list( ac2goids.keys() )[-10:] # Visualización de los 10 últimos Accession numbers anotados:

# %%
ac2goids['P00156'] # Acceso a todos los GO IDs anotados para un Accession number:


# %% [markdown]
# ### Análisis exploratorio del número de términos _GO_ por proteína

# %%
import statistics as st
import numpy as np
import matplotlib.pyplot as plt

# %%
# Número de términos GO (GO IDs) por proteína:
ngoids4ac = list( map(len, ac2goids.values()) )

# %%
# Datos estadísticos:
print("Máximo número de GO IDs por proteína     :", max(ngoids4ac))
print("Mínimo número de GO IDs por proteína     :", min(ngoids4ac))
print("Mediana del número de GO IDs por proteína:", np.median(ngoids4ac))
print("Moda del número de GO IDs por proteína   :", st.mode(ngoids4ac))

# %%
# Histograma:
plt.hist(ngoids4ac, bins=50, edgecolor='b', label='Number of proteins')

plt.grid(True, axis='y')
plt.title('Distribution of GO IDs numbers by protein')
plt.xlabel('Number of GO IDs by protein')
_ = plt.legend()

# %%
# BoxPlot:
plt.boxplot(ngoids4ac, sym='.r', vert=False, showmeans=True, meanline=True, labels=['Proteins'])

plt.grid(True, axis='x')
plt.title('Distribution of GO IDs numbers by protein')
_ = plt.xlabel('Number of GO IDs by protein')

# %% [markdown]
# ## Lectura de términos _GO_ desde un fichero local
#
# Hasta ahora hemos conseguido asociar _identificadores_ de términos de _Gene Ontology_ ([_GO_](http://geneontology.org/docs/ontology-documentation/ "Gene Ontology") *ID*s) a _Accession Numbers_ de proteínas.  
# Pero un _identificador GO_ es básicamente un número que por si mismo no nos dice nada sobre la proteína, salvo que podamos recuperar el _término GO_ completo al que identifica.
#
# Para poder _leer_ y _explorar_ los términos de una [ontología](https://en.wikipedia.org/wiki/Ontology_(information_science)) utilizaremos el paquete [_**pronto**_](https://github.com/althonos/pronto "pronto code repository and documentation"):

# %%
import pronto # Importamos el paquete pronto para el tratamiento de ontologías.

pronto.__version__ # Examinamos la versión instalada del paquete pronto (debería ser 1.0 o superior)

# %% [markdown]
# Para _leer_ los términos de una ontología desde un fichero (_local_ o _remoto_), utilizaremos crearemos un nuevo objeto de la clase **Ontology** del paquete _**pronto**_:
# ```python
#     pronto.Ontology(handle=None)  ➞ ontology_object
# ```
#
# Para crear un nuevo objeto *Ontology*, sólo hay que llamar a la clase **Ontology** pasándole como argumento (parámetro *handle*) el _nombre del fichero_ a ir leyendo (o, alternativamente, una _URL_ o un objeto _input/output_ que actúen como *handle*).

# %% [markdown]
# En nuestro caso el _fichero_ que contiene los _términos GO_ está en _[formato OBO](http://purl.obolibrary.org/obo/oboformat/spec.html "OBO Flat File Format") (Open Biological and Biomedical Ontology)_, y ya lo hemos descargado previamente desde http://geneontology.org/docs/download-ontology/ :

# %%
# Localización y nombre del fichero OBO:
GO_GZ_FILE = '../data/go-basic.obo.gz'
# Este fichero contiene 47358 registros de términos GO (release 2019-02-21): 
# http://geneontology.org/docs/download-ontology/

# %% [markdown]
# Además, lo hemos comprimido con [_gzip_](http://www.gzip.org/) para que ocupe menos espacio en disco.   Sin embargo, en este caso _no_ será necesario recurrir a la función `open( )` del módulo _gzip_, ya que podemos pasarle directamente el _nombre del archivo comprimido_ a la clase *pronto*.**Ontology**, y ella se encargará de todo para crear un objeto _Ontology_ a partir de él:

# %%
# Obtenemos un objeto Ontology utilizando la llamada a la clase pronto.Ontology, pasándole 
# directamente el nombre del archivo OBO comprimido con gzip:
go = pronto.Ontology(GO_GZ_FILE)

go # Objeto de tipo Ontology:

# %% [markdown]
# Los objetos de tipo **Ontology** son _**contenedores**_ de _entidades ontológicas_, tanto de _términos ontológicos_ (objetos de tipo **Term**) como de las _relaciones_ entre ellos (objetos de tipo **Relationship**). En los objetos *Ontology*, las _entidades_ que contienen se encuentran _indexadas_ por su _identificador_ (o _ID_).  
# Así, para _acceder_ a un _término concreto_ (o una _relación concreta_) de la ontología (del objeto _Ontology_) podemos hacerlo a través de su _identificador_ (el cual es una _cadena de caracteres_), bien utilizando la notación típica de los _diccionarios_:
# ```python
#     ontology_object[term_or_relationship_ID_str]  ➞ term_or_relationship_object
# ```
# Bien utilizando el método `.get( )` (también similar al de los _diccionarios_) del objeto _Ontology_:
# ```python
#     Ontology.get(term_or_relationship_ID_str, default=None)  ➞ term_or_relationship_object_or_default
# ```

# %%
# Acceso a un termino o una relación de la ontología por su ID. Notación de diccionarios:
kinase_term = go['GO:0004672']

print(type(kinase_term))
kinase_term # Objeto Term:

# %%
# Acceso a un termino o una relación de la ontología por su ID. Método `.get( )`:
go.get('GO:9999999', 'El GO ID no existe!') # default = 'El GO ID no existe!'

# %% [markdown]
# Pero si _sólo_ estamos buscando _terminos ontológicos_ (y _no_ relaciones), podemos recurrir a una notación más precisa (y rápida), mediante el uso del método `.get_term( )` del objeto _Ontology_:
# ```python
#     Ontology.get_term(term_ID_str)  ➞ term_object
# ```

# %%
# Acceso a un termino GO por su ID. Método `.get_term( )`:
go.get_term('GO:0004672') # Si el ID no correspondiera a ninguno de los términos se generaría un error (KeyError)

# %% [markdown]
# Además, los objetos _Ontology_ también se pueden _iterar_ para ir obteniendo los objetos _Term_ que contienen utilizando el método `.terms()`:
# ```python
#     ontology_object.terms()  ➞  iterator_object  ➞➞ term_objects
# ```

# %%
# Iteramos sobre el objeto Ontology.terms() para obtener todos los términos GO:
for go_term in go.terms(): 
    if 'kinase' in go_term.name: # Pero sólo mostramos aquellos con 'kinase' en su nombre:
        print(go_term)

# %% [markdown] jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 Los objetos de tipo **Ontology** poseen además otros _atributos_ y _métodos_ que pueden sernos útiles:
# * ```python
#   Ontology.get_relationship(relationship_ID_str)  ➞  relationship_object
#   ```
# * ```python
#   Ontology.relationships()  ➞  iterator_object  ➞➞ relationship_objects
#   ```
#
#
# * ```python
#   Ontology.path  ➞  path_to_the_ontology_file
#   ```
# * ```python
#   Ontology.metadata  ➞  Metadata_object
#   ```

# %% jupyter={"source_hidden": true}
# 📌 Acceso a una relación por su ID. Método `.get_relationship( )`:
go.get_relationship('is_a')

# %% jupyter={"source_hidden": true}
# 📌 Iteramos sobre el objeto Ontology.relationships() para obtener las relaciones.
# Nótese que, sin embargo, la relación 'is_a' no aparece al iterar:
for relationship in go.relationships(): 
    print(relationship)

# %% jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 Meta-información contenida en el objeto Ontology:
print("Fichero:", go.path)
print("Ontología:", go.metadata.ontology )
print("Versión de la ontología:", go.metadata.format_version )
print("Dominio/namespace por defecto:", go.metadata.default_namespace )

# %% [markdown]
# <br/>
#
# Cada objeto de tipo **Term** contiene la _información_ de cada uno de los [_términos de la ontología_](http://geneontology.org/docs/ontology/ "GO Term Elements"), información que es accesible a través de múltiples _atributos_ y _métodos_:

# %% [markdown]
#  * ```python
#    Term.id
#    ```
#  * ```python
#    Term.name
#    ```
#  * ```python
#    Term.definition
#    ```
#    Permiten acceder al _identificador_, el _nombre_ y la _definición_ del termino, respectivamente:

# %%
print("Identificador (.id)      :", kinase_term.id )
print("Nombre (.name)           :", kinase_term.name )
print("Definición (.definition) :", kinase_term.definition )

# %% [markdown]
#  * ```python
#    Term.namespace
#    ```
#    Permite acceder al _dominio_ al que pertenece el termino.
#  * ```python
#    Term.alternate_ids
#    ```
#    Permite acceder a un *conjunto* con los posibles _identificadores alternativos_ (generalmente antiguos IDs) del termino.
#  * ```python
#    Term.comment
#    ```
#    Permite acceder a los posibles _comentarios_ del termino
#  * ```python
#    Term.xref
#    ```
#    Permite acceder a un *conjunto* con las posibles _referencias cruzadas_ (objetos pronto.Xref) en otras ontologías (_key_ `'xref'`)

# %%
print("Dominio (.namespace)              :", kinase_term.namespace )
print("IDs alternativos (.alternate_ids) :", kinase_term.alternate_ids )
print("Comentarios (.comment)            :", kinase_term.comment )

# %% [markdown]
# [![linage](../images/biopython/go_term_lineage_0.jpg)](../images/biopython/go_term_lineage_1.jpg)

# %% [markdown]
#  * ```python
#    Term.subclasses(distance=None)
#    ```
#    Devuelve un objeto de tipo *generador* que podemos iterar para ir obteniendo los _terminos descendientes_ (*sub*clases) del termino; _incluyendo_ dicho _termino_ .  
#    Si **no** pasamos ningún argumento para el parámetro *distance*, el generador irá devolviendo **todas** las *sub*clases: tanto los términos "hijos" o _descendientes **directos**_ como los _**indirectos**_ (los hijos de los hijos directos, los hijos de los hijos de éstos, ... y así, _recursivamente_, hasta llegar al último descendiente).  
#    Si, por el contrario, pasamos un _entero_ como argumento de *distancia*, el iterador sólo irá devolviendo *sub*clases hasta el _nivel de distancia_ indicado (desde el propio término, incluido, hasta los términos del nivel indicado, incluidos también). Así, si pasamos **1** como *distancia*, iteraremos _sólo_ hasta llegar a los términos "hijos" o _descendientes **directos**_ (de primer nivel) del termino; si pasamos un 2 hasta los descendientes de segundo nivel ("nietos"), ...

# %%
# Los hijos/descendientes directos del termino en GO:
kinase_subclss = list( kinase_term.subclasses(1) )
kinase_subclss.remove(kinase_term) # Eliminamos de la lista el propio término

print(len(kinase_subclss), "objetos Term descendientes directos.")
kinase_subclss # Descendientes directos:

# %%
# Todos los hijos/descendientes (directos e indirectos) del termino en GO:
kinase_allsubclss = list( kinase_term.subclasses() )
kinase_allsubclss.remove(kinase_term) # Eliminamos de la lista el propio término

print(len(kinase_allsubclss), "objetos Term descendientes totales.")
kinase_allsubclss[:10] # 10 primeros descendientes totales:

# %% [markdown]
#  * ```python
#    Term.superclasses(distance=None)
#    ```
#    Devuelve un objeto de tipo *generador* que podemos iterar para ir obteniendo los _terminos ascendientes_ (*super*clases) del termino; _incluyendo_ dicho _termino_ .  
#    Si **no** pasamos ningún argumento para el parámetro *distance*, el generador irá devolviendo **todas** las *super*clases: tanto los términos "padres" o _ascendientes **directos**_ como los _**indirectos**_ (los padres de los padres directos, los padres de los padres de éstos, ... y así, _recursivamente_, hasta llegar al primero de los ascendientes).  
#    Si, por el contrario, pasamos un _entero_ como argumento de *distancia*, el iterador sólo irá devolviendo *super*clases hasta el _nivel de distancia_ indicado (desde el propio término, incluido, hasta los términos del nivel indicado, incluidos también). Así, si pasamos **1** como *distancia*, iteraremos _sólo_ hasta llegar a los términos "padres" o _descendientes **directos**_ (de primer nivel) del termino; si pasamos un 2 hasta los ascendientes de segundo nivel ("abuelos"), ...

# %%
# Los padres/ascendientes directos del termino en GO:
kinase_superclss = list( kinase_term.superclasses(1) )
kinase_superclss.remove(kinase_term) # Eliminamos de la lista el propio término

print(len(kinase_superclss), "objetos Term ascendientes directos.")
kinase_superclss # Descendientes directos:

# %%
# Todos los padres/ascendientes (directos e indirectos) del termino en GO:
kinase_allsuperclss = list( kinase_term.superclasses() )
kinase_allsuperclss.remove(kinase_term) # Eliminamos de la lista el propio término

print(len(kinase_allsuperclss), "objetos Term ascendientes totales.")
kinase_allsuperclss # Ascendientes totales:

# %% [markdown] toc-hr-collapsed=false
# ## Filtrado de proteínas usando *GOA*s y términos _GO_
#
# Filtraremos las proteínas del archivo FASTA ya leído, para quedarnos sólo con aquellas asociadas, mediante sus _anotaciones GO_, con _términos GO_ de **kinasas**.


# %% [markdown]
# Primero obtenemos todos los _GO_ IDs _relacionados_ con **kinasas**: el ID del termino _GO_ para _"protein kinase activity"_ (referenciado por `kinase_term`), su ID _alternativo_ (atributo `.alternate_ids`), y los IDs de _todos_ sus _términos descendientes_ (lista `kinase_allchildren`) así como sus posibles IDs _alternativos_:

# %%
kinase_allgoids = set()

# Añadimos los GO IDs de 'protein kinase activity' y todas sus subclases, 
# y también los posibles IDs alternativos de éstas:
for go_term in kinase_term.subclasses():
    kinase_allgoids.add(go_term.id)
    kinase_allgoids.update(go_term.alternate_ids)

print(len(kinase_allgoids), "GO IDs de Kinasa y descendientes en el set.")

# %% [markdown]
# Y ahora utilizaremos estos _GO_ IDs (_set_ `kinase_allgoids`) para **filtrar** el diccionario que creamos con los _GO_ IDs anotados para cada de proteína (_diccionario_ de sets `ac2goids`): nos quedaremos sólo con aquellos _Accession Numbers_ que estén asociados a _algún_ _GO_ ID _relacionado_ con _kinasas_:

# %%
# Construimos un diccionario en el que incluiremos como keys sólo los AC de proteínas anotadas con 
# algún GO ID de kinasas, y como values lo GO IDs de kinasas de dichos AC:
kinaseac2kinasegoids = dict() # AC de proteínas anotadas como kinasas ➞ GO IDs de estas anotaciones
for ac, goids in ac2goids.items():
    kinase_goids4ac = kinase_allgoids.intersection(goids)
    if kinase_goids4ac:
        kinaseac2kinasegoids[ac] = kinase_goids4ac

print("Proteínas anotadas como kinasas:", len(kinaseac2kinasegoids))

# %% [markdown]
# <br/>
#
# Este valor (_560_ proteínas) es _menor_ (un 22% menor) que el valor que obtuvimos al clasificar las proteínas del archivo FASTA según _palabras clave_ presentes en su descripción (_717_ proteínas con la palabra _kinase_, pero no _subsrate_, en su descripción).  
# Posiblemente ésto es debido a que el filtrado mediante _anotaciones GO_ es mucho más fino, excluyéndose del resultado aquellas proteínas que aún estando asociadas y/o relacionadas de alguna manera con kinasas _no_ son kinasas.  
# Además recordad que se han excluido las _anotaciones GO_ con modificadores (_qualifiers_), ya que estos podrían generar falsos positivos; y que _1206_ proteínas en nuestro archivo FASTA _no_ tienen _anotaciones GO_.

# %% [markdown]
# ### Análisis del resultado del filtrado de kinasas mediante *GOA*s y términos _GO_
#
# Primero echaremos un vistazo al **número de términos _GO_ asociados a kinasas por proteína**, para las proteínas que han superado el filtrado (recurriendo al _diccionario_ `kinaseac2kinasegoids` que hemos generado en el apartado anterior):

# %%
import statistics as st
import numpy as np
import matplotlib.pyplot as plt

# %%
# Número de términos GO (GO IDs) asociados a kinasas por proteína:
nkingoids4ac = list( map(len, kinaseac2kinasegoids.values()) )

# %%
# Datos estadísticos:
print("Máximo número de GO IDs de kinasas por proteína     :", max(nkingoids4ac) )
print("Mínimo número de GO IDs de kinasas por proteína     :", min(nkingoids4ac) )
print("Media del número de GO IDs de kinasas por proteína  :", np.mean(nkingoids4ac) )
print("Mediana del número de GO IDs de kinasas por proteína:", np.median(nkingoids4ac) )
print("Moda del número de GO IDs de kinasas por proteína   :", st.mode(nkingoids4ac) )

# %%
# Histograma:
plt.hist(nkingoids4ac, bins=8, label='Number of proteins')

plt.grid(True, axis='y')
plt.title('Distribution of Kinase GO IDs numbers by protein')
plt.xlabel('Number of Kinase GO IDs by protein')
_ = plt.legend()

# %%
# BoxPlot:
plt.boxplot(nkingoids4ac, sym='.r', vert=False, showmeans=True, meanline=True, labels=['Kinases'])

plt.grid(True, axis='x')
plt.title('Distribution of Kinase GO IDs numbers by protein')
_ = plt.xlabel('Number of Kinase GO IDs by protein')

# %% [markdown]
# <br/>
#
# A continuación calcularemos **cuántos** de los términos _GO_ relacionados con _kinasas_ (_135_, set `kinase_allgoids`) hemos encontrado en nuestras proteínas FASTA anotadas:

# %%
# Construimos un diccionario inverso a `kinaseac2kinasegoids`, que asocie a cada GO ID
# los AC de las proteínas en que éste se ha encontrado como anotación:
kinasegoid2kinaseacs = dict() # GO ID de kinasas encontrado ➞ AC de proteínas anotadas con él
for ac, go_ids in kinaseac2kinasegoids.items():
    for kinase_goid in go_ids:
        kinase_acs = kinasegoid2kinaseacs.get(kinase_goid, list())
        kinase_acs.append(ac)
        kinasegoid2kinaseacs[kinase_goid] = kinase_acs

print("GO IDs de kinasas encontrados anotando proteínas:", len(kinasegoid2kinaseacs))

# %%
# GO IDs de kinasas no encontrados anotando proteínas:
kinasegoids_notfound = kinase_allgoids.difference( kinasegoid2kinaseacs.keys() )

print("GO IDs de kinasas No encontrados anotando proteínas:", len(kinasegoids_notfound), "\n")

altgoids_notfound = set()
for go_id in kinasegoids_notfound:
    try:
        print( go[go_id] )
    except KeyError as e: # GO ID no presente en la ontología:
        altgoids_notfound.add(go_id)
print("\nY otros", len(altgoids_notfound), 
      "IDs, que deben ser IDs alternativos de otros términos GO de kinasa:")
print(altgoids_notfound)

# %% [markdown]
# <br/>
#
# Por último, calcularemos y visualizaremos las **frecuencias** de los términos _GO_ de _kinasas_ en las anotaciones de nuestras proteínas FASTA:

# %%
# Cálculo del número de proteínas anotadas con cada uno de los GO ID de kinasas encontrados:
counts_kinasegoids = list()
for goid, acs in kinasegoid2kinaseacs.items():
    counts = len(acs)
    counts_kinasegoids.append( (counts, goid) )

# Ordenación de los GO IDs de kinasas según el número de proteínas en que aparecen:
counts_kinasegoids.sort(reverse=True) # Ordenar de más a menos (reverse=True)

# %% jupyter={"source_hidden": true} tags=["sabermas", "alternativa"]
# 🔄 Alternativa usando las funciones built-in `map( )` y `sorted( )`:

# Cálculo del número de proteínas en que aparece cada GO ID de kinasas:
def counts_acs_goid4item(item):
    goid, acs = item
    return len(acs), goid

counts_kinasegoids = sorted(map( counts_acs_goid4item, kinasegoid2kinaseacs.items() ), reverse=True)

# %%
IDS_TO_SHOW = 15 # Número máximo de IDs de términos GO de kinasas a mostrar.

# Muestra una tabla con los términos GO de kinasas más frecuentes, y 
# obtiene los datos necesarios para la posterior gráfica:
counts = list()
terms = list()
subclasses = list()
print("Counts", "\t", "Go Term")
for count, goid in counts_kinasegoids[:IDS_TO_SHOW]:
    term = go[goid]
    term_subclasses = len(set( term.subclasses() )) - 1
    print(count, "\t", term)
    counts.append(count)
    terms.append(term)
    subclasses.append(term_subclasses)
print("...", "\t", "...\n")

# Gráfica de barras de las frecuencias de estos términos GO, y
# del número de subclasses que poseen:
y_positions = range(IDS_TO_SHOW, 0, -1)
plt.barh(y_positions, counts, tick_label=terms, label='Number of annotated proteins')
plt.barh(y_positions, subclasses, tick_label=terms, label='Number of term\'s subclasses')

plt.grid(True, axis='x')
plt.title("The " + str(IDS_TO_SHOW) + " most common GO terms in Kinases")
_ = plt.legend()

# %% [markdown] tags=["sabermas"]
# # III.- Referencias y material de ampliación 🔗
#
# * ["A Gene Ontology Tutorial in Python"](https://nbviewer.jupyter.org/urls/dessimozlab.github.io/go-handbook/GO%20Tutorial%20in%20Python%20-%20Solutions.ipynb), por Alex Warwick Vesztrocy y Christophe Dessimoz.
# * [Pronto (Python frontend to ontologies) Official Documentation](https://pronto.readthedocs.io/en/latest/).
#
#
# * [The Gene Ontology Handbook](http://gohandbook.org/doku.php), de Christophe Dessimoz y Nives Skunka (editores).
# * [GOATOOLS: A Python library for Gene Ontology analyses. D.V. Klopfenstein _et al_. Nature Scientific Reports, 2018](https://doi.org/10.1038/s41598-018-28948-z).
