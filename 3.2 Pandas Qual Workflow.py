# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     comment_magics: true
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %% [markdown]
# ![Python4Proteomics](images/logo_p4p.png) **(Python For Poteomics)**
#
# # Ejemplo de flujo de trabajo ***cualitativo*** en Pandas: Cobertura proteica 
#
# En las carpetas `data/qualitative/PD/` y `data/qualitative/MQ/` de nuestro repositorio hay dos subcarpetas llamadas `acE23/` y `acE77/` con un archivo de salida de *peptide spectrum matches* (PSMs) cada una (dos para *Proteome Discoverer* y dos para *MaxQuant*, cuatro en total).
#
# Tenemos pues dos experimentos diferentes (`acE23` y `acE77`) cuyas búsquedas de proteínas se han realizado con dos *softwares* diferentes. ¿Se obtienen los mismos resultados cualitativos con *Proteome Discoverer* (PD) y *MaxQuant* (MQ)? En el presente ejemplo de flujo de trabajo (*workflow*) pondremos en práctica los conceptos básicos de **Pandas** que hemos visto en capítulo anterior e introduciremos algunas herramientas adicionales.
#
# Importaremos dos conjuntos de datos (*datasets*) completamente diferentes (uno de PD y otro de MQ) para fusionarlos en en único *dataset* con información sobre dos experimentos y dos *softwares*. Después de aplicar todos los filtros necesarios, calcularemos (con nuestras propias líneas de código) la <u>cobertura proteica</u> (*coverage*) de cada una de las identificaciones. Con esta información haremos una comparativa del *coverage* obtenido por dos *softwares* diferentes en dos experimentos independientes.
#
# **Índice:**
#
#  * [I.- Importando datos de PSMs](#I.--Importando-datos-de-PSMs)
#  * [II.- Filtrando datos de PSMs](#II.--Filtrando-datos-de-PSMs)
#  * [III.- Combinando datos de PSMs de PD y MQ](#III.--Combinando-datos-de-PSMs-de-PD-y-MQ)
#  * [IV.- Trabajando con FASTAs en **Pandas**](#IV.--Trabajando-con-FASTAs-en-Pandas)
#  * [V.- Manipulando secuencias en **Pandas**](#V.--Manipulando-secuencias-en-Pandas)
#  * [VI.- Calculando el *coverage*](#VI.--Calculando-el-coverage)
#  * [VII.- Visualizando los datos de *coverage*](#VII.--Visualizando-los-datos-de-coverage)
#  * [VIII.- Conclusiones y *take home message*](#VIII.--Conclusiones-y-take-home-message) 
# ---

# %% [markdown]
# # I.- Importando datos de PSMs
#
# Como siempre, empezaremos importando la librería **Pandas** con el alias `pd`. También necesitaremos la librería **Numpy** (alias `np`) y la clase `SeqIO` de la librería **Biopython** para algunos pasos del *workflow*.

# %%
# Importing the Pandas/Numpy package and assign the 'pd'/'np' alias to it
import pandas as pd
import numpy as np

# Importing the SeqIO class from the Bio package
from Bio import SeqIO

# %% [markdown]
# Sabiendo dónde están y cómo se llaman nuestros archivos con las PSMs, es sencillo importar cada archivo en un *DataFrame* individual. Sólo debemos tener claro el formato de entrada de nuestros datos (en nuestro caso `.txt`) y otros detalles como el sepadador que delimita las columnas dentro de cada archivo (en nuestro caso tabulador `\t`).

# %%
# Reading our PSMs data files from the corresponding folders
pd_ace23_df = pd.read_csv('data/qualitative/PD/acE23/acE23_isoforms_PSMs.txt', sep='\t')
pd_ace77_df = pd.read_csv('data/qualitative/PD/acE77/acE77_isoforms_PSMs.txt', sep='\t')
mq_ace23_df = pd.read_csv('data/qualitative/MQ/acE23/evidence.txt', sep='\t')
mq_ace77_df = pd.read_csv('data/qualitative/MQ/acE77/evidence.txt', sep='\t')

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > Realiza una exploración preliminar de estos cuatro *DataFrames*. Recuerda los métodos `.shape`, `.columns`, `.index`, `.head()`, `.tail()`, `.info()` y `.describe()`.

# %%
# DataFrame general information
#pd_ace23_df
#pd_ace77_df
#mq_ace23_df
#mq_ace77_df

# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# SOLUTION
# DataFrame general information
pd_ace23_df.info()
pd_ace77_df.info()
mq_ace23_df.info()
mq_ace77_df.info()

# %% [markdown]
# Si nos fijamos en la columna `df['Identifying Node Type']` de los *DataFrames* de PD, veremos que sus búsquedas se han realizado con dos nodos de búsqueda paralelamente (MS Amanda 2.0 y Sequest HT).
#
# > 💡 **Más información:**
# >
# > Puedes aplicar el método de **Pandas** [`.unique()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.unique.html) a la columna de la que quieras conocer sus valores únicos. Este método va muy bien para saber qué categorías (o factores) tenemos en una determina columna.

# %%
# Looking for unique values in the 'Identifying Node Type' column of pd_ace23_df
pd_ace23_df['Identifying Node Type'].unique()

# %%
# Looking for unique values in the 'Identifying Node Type' column of pd_ace77_df
pd_ace77_df['Identifying Node Type'].unique()

# %% [markdown]
# Para tener en cuenta posibles diferencias entre nodos, añadiremos una columna a los *DataFrames* de MQ con su correspodiente nodo (Andromeda).

# %%
# Adding an 'Identifying node' column called 'Andromeda' to MQ DataFrames
mq_ace23_df['Identifying node'] = 'Andromeda'
mq_ace77_df['Identifying node'] = 'Andromeda'

# %% [markdown]
# Los nombres "MS Amanda 2.0" y "Sequest HT" son un poco largos. Los podríamos simplificar utilizando el método de **Pandas** [`.map()`](https://pandas.pydata.org/pandas-docs/version/0.25/reference/api/pandas.Series.map.html) o, alternativamente el método de **Pandas** [`.replace()`](https://pandas.pydata.org/pandas-docs/version/0.25/reference/api/pandas.Series.replace.html).

# %%
# Creating a renaming dictionary for incomming replace
rename_dic = {'MS Amanda 2.0': 'Amanda',
              'Sequest HT': 'Sequest'}

# Applying my (first) replace mapping to my pd_ace23_df
pd_ace23_df['Identifying Node Type'] = pd_ace23_df['Identifying Node Type'].map(rename_dic)

# Applying my (second) replace mapping to my pd_ace7_df
pd_ace77_df['Identifying Node Type'] = pd_ace77_df['Identifying Node Type'].map(rename_dic)

# Looking for unique values in the 'Identifying Node Type' column of pd_ace77_df
pd_ace77_df['Identifying Node Type'].unique()


# %% [markdown]
# > 💡 **Más información:**
# >
# > El método `.map()` tiene la ventaja de ser mucho más rápido que el método `.replace()`. De todas formas, debemos tener cuidado al usarlo, ya que `.map()` reemplaza con un `nan` si no encuentra el *string* de entrada entre las *keys* del diccionario de renombrado.

# %% [markdown]
# # II.- Filtrando datos de PSMs
#
# Una de las manipulaciones más repetidas a la hora de trabajar con PSMs es el fitrado. Normalmente se suelen descartar péptidos provenientes de proteínas contaminantes o de proteínas de secuencia reversa. También se suelen descartar aquellos péptidos no únicos. 
#
# Al ser una tarea muy utilizada y repetitiva, el filtrado merece la pena implementarlo como una función de **Python**. Hacer esto nos ahorrará mucho tiempo y aligerará un poco el número de lineas de nuestros *scripts*. Empezaremos filtrando las PSMs de PD con el filtro `pd_psm_filter`.

# %%
# Defining a fuction to filter out contaminat and non-unique peptides in PD
def pd_psm_filter(in_psms):
    '''Filters-out contaminant and non-unique peptides in PD PSMs'''
    cont_mask = in_psms['Contaminant']
    unique_mask = in_psms['# Protein Groups'] == 1
    out_psms = in_psms[~cont_mask & unique_mask]
    return out_psms

# Applying filters to PD PSMs DataFrames
pd_ace23_df = pd_psm_filter(pd_ace23_df)
pd_ace77_df = pd_psm_filter(pd_ace77_df)

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > Comprueba que el filtrado se haya realizado correctamente. Utiliza la técnica que prefieras.

# %% tags=["ejercicio"]
# Checking if the pd_psm_filter worked properly
#print(__)
#print(__)
#print(__)
#print(__)

# %% jupyter={"source_hidden": true} tags=["solucion", "ejercicio"]
# SOLUTION
# Checking if the pd_psm_filter worked properly
print(pd_ace23_df['Contaminant'].unique())
print(pd_ace77_df['Contaminant'].unique())
print(pd_ace23_df['# Protein Groups'].unique())
print(pd_ace77_df['# Protein Groups'].unique())


# %% [markdown]
# Ahora filtraremos las PSMs de MQ. Si no estás familiarizada con el contenido de las columnas de MQ no te preocupes. Verás que no es crucial para entender qué hace el filtro `mq_psm_filter`.

# %%
# Defining a fuction to filter out contaminat, reverse and non-unique peptides in MQ
def mq_psm_filter(in_psms):
    '''Filters-out contaminant, reverse and non-unique peptides in MQ PSMs'''
    cont_mask = (in_psms['Potential contaminant'] == '+') | (in_psms['Leading razor protein'].str.contains('CON__'))
    rev_mask = (in_psms['Reverse'] == '+') | (in_psms['Leading razor protein'].str.contains('REV__'))
    nonunique_mask = in_psms['Protein group IDs'].str.contains(';')
    out_psms = in_psms[~cont_mask & ~rev_mask & ~nonunique_mask]
    return out_psms

# Applying filters to MQ PSMs DataFrames
mq_ace23_df = mq_psm_filter(mq_ace23_df)
mq_ace77_df = mq_psm_filter(mq_ace77_df)

# %% [markdown]
# Siempre que se pueda, debemos aligerar al máximo nuestro código. No es muy agradable retomar un *script* de más de 1000 líneas sin ningún `# comentario` pasados unos meses... Ahora estamos trabajando sólo con cuatro *DataFrames*, pero si trabajásemos con un número más grande podría volverse todo muy farragoso. Para solventar esto podríamos crear un diccionario de *DataFrames* y trabajar con bucles (recuerda las parejas *key* ➞ *value* de los diccionarios).

# %%
# Creating a df_dict with our four DataFrames
df_dict = {'PD_acE23': pd_ace23_df,
           'PD_acE77': pd_ace77_df,
           'MQ_acE23': mq_ace23_df,
           'MQ_acE77': mq_ace77_df}

# %%
# Looping each key (DataFrame name) and value (DataFrame) in "df_dict" to...
for key, value in df_dict.items():
    # ... print each DataFrame name
    print(key)
    # ... and also print each DataFrame shape
    print(value.shape)

# %% [markdown]
# # III.- Combinando datos de PSMs de PD y MQ
#
# En este *workflow* pretendemos comparar dos *softwares* y tres nodos de búsqueda. Por un lado tenemos PD (con los nodos Amanda y Sequest) y por el otro tenemos MQ (con el nodo Andromeda). Estaría muy bien combinar las PSMs de ambos *softwares* en un único *DataFrame*, pero las tablas de salida de PD y MQ son muy diferentes (columnas con la misma información tienen nombres diferentes y algunas columnas son exclusivas de uno de los dos *softwares*). Combinar diversos *DataFrames* en un único *DataFrame* es muy fácil con **Pandas**, solo es cuestión de proceder con un poco de cuidado.
#
# Con la intención de mantener la trazabilidad, primero añadiremos a cada *DataFrame* una columna con su respectivo nombre .

# %%
# Looping each key (DataFrame name) and value (DataFrame) in "df_dict" to...
for name, df in df_dict.items():
    # ... add a column with its corresponding name to each DataFrame
    df['DF'] = name

# %% [markdown]
# Comprobemos que nuestros cuatro *DataFrames* hayan incorporado correctamente la columna con su respectivo nombre:

# %%
# Looping each key (DataFrame name) and value (DataFrame) in "df_dict" to...
for name, df in df_dict.items():
    # ... print each DataFrame name
    print(name)
    # ... and also print the unique values in the 'DF' column
    print(df['DF'].unique())

# %% [markdown]
# Por otro lado, en nuestros cuatro *DataFrames* encontramos columnas con nombres diferentes a pesar de referirse a un mismo concepto. Es recomendable arreglar esto antes de combinar los datos.

# %%
# Initiating renaming dictionary to unify MQ and PD column nomenclature
rename_dic = {'Raw file': 'File',                              # MQ
              'Spectrum File': 'File',                         # PD
              'Scan number': 'Scan',                           # MQ
              'Master Scan(s)': 'Scan',                        # PD
              'Sequence': 'Seq',                               # MQ & PD
              'Modified sequence': 'Mod seq',                  # MQ
              'Annotated Sequence': 'Mod seq',                 # PD
              'Leading razor protein': 'AC',                   # MQ
              'Master Protein Accessions': 'AC',               # PD
              'Identifying node': 'Node',                      # MQ
              'Identifying Node Type': 'Node'}                 # PD

# Looping each key (DataFrame name) and value (DataFrame) in "df_dict" to...
for name, df in df_dict.items():
    # ... create a "renamed_df" by using the renaming dictionary
    renamed_df = df.rename(columns=rename_dic)
    # ... and also update the "df_dict" DataFrame with its renamed version
    df_dict[name] = renamed_df

# %% [markdown]
# Veamos si el renombrado ha funcionado imprimiendo el nombre de las columnas presentes en nuestros cuatro *DataFrames*. Deberían aparecer los nombre `'File'`, `'Scan'`, `'Mod seq'`, `'AC'` y `'Node'`.

# %%
# Looping each key (DataFrame name) and value (DataFrame) in "df_dict" to...
for name, df in df_dict.items():
    # ... print each DataFrame name
    print(name)
    # ... and also print each DataFrame columns
    print(df.columns)

# %% [markdown]
# Ahora que hemos uniformizado los nombres de las columnas que nos interesan, podemos proceder con la concatenación de nuestros cuatro *DataFrames* en un único *DataFrame* más grande. Para ello necesitamos tener lista de *DataFrames*, `df_list`, y utilizar la función de **Pandas** [`pd.concat()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.concat.html). También, resetearemos el índice para que nuestro *DataFrame* único tenga un índice secuencial.

# %%
# Preparing a list with the DataFrames to concatenate
df_list = list(df_dict.values())

# Concatenating the list of DataFrames into a single (huge!) DataFrame
full_df = pd.concat(df_list, sort=False)

# Resetting the "full_df" index to get a new index with sequential order
full_df = full_df.reset_index(drop=True)

# DataFrame general information
full_df.info()

# %% [markdown]
# > 💡 **Más información:**
# >
# > El parámetro `sort=False` de la función `pd.concat()` lo hemos incluído simplemente para silenciar un *FutureWarning*.

# %% [markdown]
# Como nuestro *DataFrame* concatenado `full_df` es un poco grande (mira la entrada `memory usage` de la información general), podríamos seleccionar sólo las columnas necesarias para nuestro análisi.

# %%
# Creating a list with the columns we want to work with
colum_list = ['File', 'DF', 'Node', 'AC', 'Seq']

# Setting aside a DataFrame with just our "colum_list"
df = full_df[colum_list].copy()

# Dataframe general information
df.info()

# %% [markdown]
# Ahora tenemos un *Dataframe* más manejable (mira la entrada `memory usage` de la información general), pero tenemos un problema con la columna `df['AC']`: hay valores nulos. El método de **Pandas** [`.isnull()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.isnull.html) pregunta qué filas de una determinada *Series* son nulas, devolviendo las respuestas en forma de *Boolean Series*. 

# %%
# Create filter to get null (nan) rows
mask_null = df['AC'].isnull()

# Filtering-in null rows of "df" to see what happends with such rows
df[mask_null]

# %% [markdown]
# Nótese que unas pocas PSMs no llevan un Número de Acceso (AC) asociado. Debemos descartarlas.

# %%
# Dropping the 'DF' column (here axis=1 means columns and axis=0 means rows)
df = df.dropna(axis=0)

# Resetting the "df" index to get a new index with sequential order
df.reset_index(drop=True, inplace=True)

# DataFrame general information
df.info()

# %% [markdown]
# > 💡 **Más información:**
# >
# > El parámetro `axis=` del método `.dropna` especifica si lo que queremos descartar son las filas con `NaN`s (`axis=0`) o a las columnas con `NaN`s (`axis=1`).
# >
# > El parámetro `inplace=True` del método `.reset_index` permite resetear el índice del *DatFrame* `df` "al vuelo". Esto significa que no nos hará falta "machacar" el antiguo *DataFrame* `df` (sin reseatear) con el nuevo *DataFrame* `df` (reseteado): `df = df.reset_index(drop=True)`.

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > Crea una columna con el *Software* utilizado (PD o MQ) y otra con el experimento (acE23 o acE77). Te puede ir muy bien el contenido de la columna `df['DF']`.

# %% tags=["ejercicio"]
# Adding a column with the software
#df['Soft'] = 

# Adding a column with the experiment tag
#df['Exp'] = 

# DataFrame general information


# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# SOLUTION
# Adding a column with the software
df['Soft'] = df['DF'].str.split('_').str[0]

# Adding a column with the experiment tag
df['Exp'] = df['DF'].str.split('_').str[1]

# DataFrame general information
df.info()

# %% [markdown]
# Veamos el aspecto de nuestro *DataFrame*.

# %%
# DataFrame head?
df.head()

# %%
# DataFrame tail?
df.tail()

# %% [markdown]
# Finalmente, podríamos descartar la columna `df['DF']` por ser redundate.

# %%
# Dropping the redundant 'DF' column (here 1 means column axis and 0 row axis)
df = df.drop('DF', axis=1)

# DataFrame general information
df.info()

# %% [markdown] toc-hr-collapsed=false
# # IV.- Trabajando con FASTAs en **Pandas**
#
# Para calcular el *coverage* de una determinada proteína necesitamos dos datos:
#    
# * La longitud total de su secuencia, $L_T$
# * La longitud cubierta de su secuencia, $L_C$.
#
# La covertura, $C$, no será más que: $C = \frac{L_C}{L_T}$.
#
# Por una lado, $L_T$ es fácil de obtener. Se puede sacar directamente a partir de la secuencia de amino ácidos de la propia proteína. En cambio, no es del todo trivial obtener $L_C$. Lo más normal es que los péptidos asociados a una proteína reportados en los archivos de *PSMs* tengan solapamientos, por lo que $L_C$ no coincide con la suma de las longitudes de los distintos péptidos.

# %% [markdown]
# La FASTA con la que se han realizado las búsquedas se encuentra en la carpeta `data/quatitative/` de nuestro repositorio. Tendremos que importarla para poder trabajar con las secuencias que archiva. Las entradas de una FASTA comienzan con una descripción del tipo: <br>
# `>sp|P11217|PYGM_HUMAN Glycogen phosphorylase, muscle form OS=Homo sapiens OX=9606 GN=PYGM PE=1 SV=6`
#
# Y justo a contunuación encontramos la secuencia de la proteína: <br>
# `MSRPLSDQEKRKQISVRGLAGVENVTELKKNFNRHLHFTLVKDRNVATPRDYYFALAHTV
# RDHLVGRWIRTQQHYYEKDPKRIYYLSLEFYMGRTLQNTMVNLALENACDEATYQLGLDM
# EELEEIEEDAGLGNGGLGRLAACFLDSMATLGLAAYGYGIRYEFGIFNQKISGGWQMEEA
# DDWLRYGNPWEKARPEFTLPVHFYGHVEHTSQGAKWVDTQVVLAMPYDTPVPGYRNNVVN
# TMRLWSAKAPNDFNLKDFNVGGYIQAVLDRNLAENISRVLYPNDNFFEGKELRLKQEYFV
# VAATLQDIIRRFKSSKFGCRDPVRTNFDAFPDKVAIQLNDTHPSLAIPELMRILVDLERM
# DWDKAWDVTVRTCAYTNHTVLPEALERWPVHLLETLLPRHLQIIYEINQRFLNRVAAAFP
# GDVDRLRRMSLVEEGAVKRINMAHLCIAGSHAVNGVARIHSEILKKTIFKDFYELEPHKF
# QNKTNGITPRRWLVLCNPGLAEVIAERIGEDFISDLDQLRKLLSFVDDEAFIRDVAKVKQ
# ENKLKFAAYLEREYKVHINPNSLFDIQVKRIHEYKRQLLNCLHVITLYNRIKREPNKFFV
# PRTVMIGGKAAPGYHMAKMIIRLVTAIGDVVNHDPAVGDRLRVIFLENYRVSLAEKVIPA
# ADLSEQISTAGTEASGTGNMKFMLNGALTIGTMDGANVEMAEEAGEENFFIFGMRVEDVD
# KLDQRGYNAQEYYDRIPELRQVIEQLSSGFFSPKQPDLFKDIVNMLMHHDRFKVFADYED
# YIKCQEKVSALYKNPREWTRMVIRNIATSGKFSSDRTIAQYAREIWGVEPSRQRLPAPDE
# AI`

# %% [markdown]
# La librería `Biopython` nos será de gran ayuda en esta parte del *workflow* al facilitar el parseo de la FASTA. Empecemos cargando la FASTA usando la clase `SeqIO` y su método `.parse()`.

# %%
# Importing the FASTA file
fasta = SeqIO.parse('data/qualitative/uniprot-reviewed-Human-9606-w-isoforms.fasta', 'fasta')

# FASTA parsed by SeqIO data type
type(fasta)

# %% [markdown]
# Ahora que tenemos la FASTA importada, iteraremos a lo largo de todas sus entradas para construir un diccionario `seq_dict` en el que los AC serán las llaves y las correspondientes secuencias serán los valores.

# %%
# Initializing an empty dictionary to store our AC-sequence mapping dictionary
seq_dict = {}

# Looping each record in the FASTA object...
for record in fasta:
    # ... to get the record accession number and the record sequence
    ac = record.description.split('|')[1]
    sequence = str(record.seq)
    # ... to store the accession-sequence pair as a dictionary key-value pair
    seq_dict[ac] = sequence

# %% [markdown]
# Ahora tenemos un diccionario con todas las parejas AC ➞ Secuencia almacenadas en forma de parejas *key* ➞ *value*. Utilizaremos el diccionario `seq_dict` para insertar la secuencia de cada proteina en nuestro *DataFrame* `df`.

# %%
# Duplicating 'AC' column for incomming AC-sequence mapping
df['Prot seq'] = df['AC']

# DataFrame head?
df.head()

# %%
# Mapping AC-sequence
df['Prot seq'] = df['Prot seq'].map(seq_dict)

# DataFrame head?
df.head()

# %% [markdown]
# Fíjate que hemos prodecido en dos pasos: primero hemos duplicado la columna `df['AC']` llamándola `df['Prot seq']` para luego mapear con el diccionario AC ➞ Secuencia que habíamos preparado antes.

# %% [markdown]
# # V.- Manipulando secuencias en **Pandas**
#
# Llegados a este punto, podemos calcular la longitud de las secuencias con la función `len()` junto con el método de **Pandas** `.str`. Almacenaremos los resultados en una nueva columna `df['Prot seq len']`.

# %%
# Computing the protein length
df['Prot seq len'] = df['Prot seq'].str.len()

# DataFrame head?
df.head()


# %% [markdown]
# Para saber el *coverage* de una determinada proteína, además de su longitud, también necesitamos saber qué posiciones de su secuencia están ocupadas por el conjunto de péptidos identificados asociados. Para llegar a ello, primero calcularemos las posiciones inicial y final de cada péptido dentro de su correspondiente proteína. Definiremos una función adecuada en cada caso (`start_fun()` y `stop_fun()`) y se la pasaremos al método [`.apply()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.apply.html). Fíjate estas dos funciones reciben como entrada un *DataFrame* (`in_df`) y devuelven una *Series* (`out_ser`).

# %%
# Defining function to compute the start position of a peptide within a protein
def start_fun(in_df):
    '''Given protein and peptide sequences, returns the start position of the peptide within the protein'''
    out_ser = in_df['Prot seq'].find(in_df['Seq']) + 1
    return out_ser

# Applying "start_fun" to (only) 'Prot seq' and 'Seq' columns of "df" and storing the resulting series in 'Start'
df['Start'] = df[['Prot seq', 'Seq']].apply(start_fun, axis=1)

# DataFrame head?
df.head()


# %%
# Defining function to compute the stop position of a peptide within a protein
def stop_fun(in_df):
    '''Given protein and peptide sequences, returns the stop position of the peptide within the protein'''
    out_ser = in_df['Prot seq'].find(in_df['Seq']) + len(in_df['Seq'])
    return out_ser

# Applying "stop_fun" to (only) 'Prot seq' and 'Seq' columns of "df" and storing the resulting series in 'Stop'
df['Stop'] = df[['Prot seq', 'Seq']].apply(stop_fun, axis=1)

# DataFrame head?
df.head()


# %% [markdown]
# > 💡 **Más información:**
# >
# > El método `apply()` se usa para para aplicar una función (en nuestro caso `start_fun()` y `stop_fun()`) a todos y cada uno de los elementos (secuencias) de las columnas `df['Seq']` y `df['Prot seq']`, uno a uno.

# %% [markdown]
# Sabiendo las posiciones inicial y final de cada péptido dentro de su correspondiente proteína, es inmediato calcular el rango de posiciones ocupado con la función `range()`. Procederemos análogamente a como hemos hecho antes, definiendo una función (`range_fun`) y usando el método `.apply()`.

# %%
# Defining function to compute the range between start and stop positions and applying it
def range_fun(in_df):
    '''Given a start-stop positions pair, returns the range of  "from start to stop" positions'''
    out_ser = range(in_df['Start'], in_df['Stop'] + 1)
    return out_ser

# Applying "range_fun" to (only) 'Start' and 'Stop' columns of "df" and storing the resulting series in 'Range'
df['Range'] = df[['Start', 'Stop']].apply(range_fun, axis=1)

# DataFrame head? (only for 'AC', Start', 'Stop', 'Range')
df[['AC', 'Start', 'Stop', 'Range']].head()


# %% [markdown]
# Finalmente, nos interesa convertir los rangos de la columna `df['Range']` en conjuntos. Este truco nos permetirá calcular la unión ($\cup$) de todas las posiciones ocupadas por todos los péptidos asociados a una determinada proteína. Por tercera vez, procederemos con el binomio función (`set_fun`) método `.apply()`.

# %%
# Defining function to transform a range into a set and applying it
def set_fun(in_df):
    '''Given a range "from start to stop" positions, returns the set of range of "from start to stop" positions'''
    out_ser = set(in_df['Range'])
    return out_ser

# Applying "set_fun" to (only) 'Range' column of "df" and storing the resulting series in 'Set'
df['Set'] = df[['Range']].apply(set_fun, axis=1)

# DataFrame shape?
print(df.shape)

# DataFrame head? (only for 'AC', 'Start', 'Stop', 'Range', 'Set')
df[['AC', 'Start', 'Stop', 'Range', 'Set']].head()


# %% [markdown]
# # VI.- Calculando el *coverage*
#
# Ya estamos preparados para agrupar y agregar nuestro *DataFrame*. Agruparemos por las columnas que queremos mantener desglosadas dentro del *DataFrame* agrupado `g_df` (`'Software'`, `'Experiment'`, `'Node'`, `'Prot'` y `'Prot seq'`) y agregaremos las columnas deseadas (`'Set'`) con la función de agregación que necesitemos, en este caso `union_fun`.

# %%
# Defining function to aggregate a Series of sets of ranges of "from start to stop" positions using the union of sets
def union_fun(in_df):
    '''Given a Series of sets of ranges of "from start to stop" positions, returns the union of the sets'''
    out_ser = set.union(*in_df)
    return out_ser

# Grouping by 'Soft', 'Exp', 'Node', 'AC', 'Prot seq' and aggregating 'Set' with union_fun
column_list = ['Soft', 'Exp', 'Node', 'AC', 'Prot seq']
g_df = df.groupby(column_list)[['Set']].agg(union_fun)

# Resetting index to recover grouped columns
g_df = g_df.reset_index(drop=False)

# DataFrame shape?
print(g_df.shape)

# DataFrame head?
g_df.head()

# %% [markdown]
# Finalmente, calcularemos el *coverage* usando el truco del método `.str` y el método `.len()`, y exportaremos el *DataFrame* `g_df` resultante:

# %%
# Computing the protein coverage
g_df['Coverage'] = g_df['Set'].str.len() / g_df['Prot seq'].str.len()

# Exporting the DataFrame as an Excel SpreadSheet
g_df.to_excel('data/qualitative/Coverage_df.xlsx')

# DataFrame head? (only for 'Coverage')
g_df.head()

# %% [markdown]
# Con este último paso ya hemos conseguido un único *DataFrame*, `g_df`, que combina los datos de *coverage* para dos experimentos (acE23 y acE77) y dos *softwares* de adquisición (PD y MQ).

# %% [markdown]
# # VII.- Visualizando los datos de *coverage*
#
# <img src="images/seaborn/seaborn_logo.png" width="150" style="float: left; margin-right: 10px;" />
#
# Ahora ya podemos empezar la exploración gráfica de nuestro *DataFrame* agregado `g_df`. La librería **Seaborn** permite hacer gráficos estadísticos de altas prestaciones de manera muy sencilla. En este último apartado mostraremos un aperitivo de lo que tres de las funciones de **Seaborn** son capaces:
# <br>
# <br>
#
# - [`seaborn.boxplot()`](https://seaborn.pydata.org/generated/seaborn.boxplot.html)<br>
# - [`seaborn.violinplot()`](https://seaborn.pydata.org/generated/seaborn.violinplot.html)<br>
# - [`seaborn.srtipplot()`](https://seaborn.pydata.org/generated/seaborn.stripplot.html)<br>
#
# Para cargar la librería **Seaborn** con el alias `sns` haremos:

# %%
# Importing the Seaborn package and assign the 'sns' alias to it
import seaborn as sns

# %% [markdown]
# Veamos cómo se distribuyen los valores de *coverage* de cada experimento:

# %%
# Plotting a boxplot of the coverage for each experiment
sns.boxplot(x="Exp", y="Coverage", data=g_df)

# %% [markdown]
# Incluyamos el efecto de *software* aprovechando el parámetro `hue=` (matiz):

# %%
# Plotting a boxplot of the coverage for each experiment by software
sns.boxplot(x="Exp", y="Coverage", hue="Soft", data=g_df)

# %% [markdown] tags=["ejercicio"]
# > ✏️ **Práctica:**
# >
# > Utiliza **Seaborn** para representar un *boxplot* del *coverage* en función del experimento para cada nodo de búsqueda (recuerda utilizar argumento `hue=`).

# %% tags=["ejercicio"]
# Plotting a boxplot of the coverage for each experiment by node
#sns.boxplot(x="___", y="___", hue="___", data=g_df

# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# SOLUTION
# Plotting a boxplot of the coverage for each experiment by node
sns.boxplot(x="Exp", y="Coverage", hue="Node", data=g_df)

# %% [markdown]
# Los *boxplots* están bien, pero los *violinplots* también son muy útiles. Al igual que los *boxplots*, los *violinplots* nos muestran el rango intercuartil de los datos. Pero además, nos informan de cómo se distribuyen nuestros datos de manera más precisa. Repitamos el último plot pero cambiando la función `sns.boxplot()` por la función `sns.violinplot()`:

# %%
# Plotting a violinplot of the coverage for each experiment
sns.violinplot(x="Exp", y="Coverage", hue="Node", data=g_df)

# %% [markdown]
# Si *boxplots* y *violinplots* no te parecen suficientemente explícitos, también puedes utilizar el *stripplot*. Fíjate que ahora utilizamos algunos parámetros adicionales (`alpha=`, `dodge=` y `jitter=`):


# %%
# Plotting a stripplot of the coverage for each experiment
sns.stripplot(x="Exp", y="Coverage", hue="Node", data=g_df,
              alpha=0.02, dodge=True, jitter=1/4)

# %% [markdown]
# Podemos incluso superponer un *boxplot* y un *stripplot* y, si rizamos un poco más el rizo...

# %%
# Initiating generalized plotting variables
my_x, my_y, my_h, my_d = "Exp", "Coverage", "Node", g_df

# Plotting a boxplot of the coverage for each experiment by node
ax = sns.boxplot(x=my_x, y=my_y, hue=my_h, data=my_d,
                 boxprops=dict(alpha=1/3), fliersize=0)

# overlaying a stripplot of the coverage for each experiment by node
ax = sns.stripplot(x=my_x, y=my_y, hue=my_h, data=my_d,
                   alpha=0.02, dodge=True, jitter=1/4)

# Setting logarithmic y-scale 
ax.set(yscale="log")

# Setting custom y-limits matching the lowermost/uppermost -1/+1 oders of magnitude
ax.set_ylim(10 ** (int(np.log10(my_d[my_y].min()) - 1)),
            10 ** (int(np.log10(my_d[my_y].max()) + 1)))

# Saving this nice plot
ax.figure.savefig('data/qualitative/Coverage.pdf')

# %% [markdown]
# A partir de este gráfico resulta muy sencillo sacar algunas conclusiones con bastante claridad:
#
# * El proteoma del experimento `acE23` presenta más *coverage* que el de `acE77`.
#
# * Los proteomas obtenidos por los nodos de búsqueda `Amanda` y `Sequest` prácticamente no presentan diferencias en términos de *coverage* (tanto en el experimento `acE23` como en el `acE77`).
#
# * El proteoma obtenido por el nodo de búsqueda `Andromeda` presenta un *coverage* ligeramente mayor que los obtenidos por `Amanda` y `Sequest` (tanto en el experimento `acE23` como en el experimento `acE77`).

# %% [markdown]
# # VIII.- Conclusiones y *take home message*
#
# A pesar de que el interés del presente *workflow* sea meramente didáctico, nos ha servido para ir cogiendo soltura a la hora de manipular *DataFrames* con grandes listados de péptidos. Hemos importado cuatro archivos de PSMs distintos y, después de filtrar y organizar un poco su contenido, hemos sido capaces de combinarlos en un único *DataFrame*. Luego hemos realizado una serie de manipulaciones que al final del día nos han permitido sacar el *coverage* de cada proteína.
#
# Quizá, el *take home message* sea que la parte más importante de cualquier análisis son todos los preliminares que hacemos hasta llegar a unos datos bien ordenados y claros. Una vez tenemos un buen *DataFrame*, hacer una exploración gráfica digna y sacar un par de conclusiones es los más sencillo.
