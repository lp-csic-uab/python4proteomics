# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     comment_magics: true
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %% [markdown]
# ![Python4Proteomics](images/logo_p4p.png) **(Python For Proteomics)**
#
# # INTRODUCCIÓN A BIOPYTHON
#
# En este apartado veremos como utilizar algunas herramientas del paquete [**Biopython**](https://biopython.org/), y otros paquetes como [matplotlib](https://matplotlib.org/):
#
#   * Breve introducción a Biopython.
#   * Tratamiento de ficheros [FASTA](https://en.wikipedia.org/wiki/FASTA) (lectura, análisis y manipulación).
#   * Funciones estadísticas _básicas_ de Python (módulo [statistics](https://docs.python.org/3/library/statistics.html)) y del paquete [numpy](http://www.numpy.org/).
#   * Gráficas sencillas utilizando el paquete [matplotlib](https://matplotlib.org/).
#   * Filtrado de proteínas de un archivo FASTA.
#   * Búsquedas con [Blastp](https://blast.ncbi.nlm.nih.gov/Blast.cgi) en base de datos de proteínas.
#   * Referencias y material de ampliación.
#
# ---
#

# %% [markdown]
# **Índice:**
#
#  * [I.- El paquete Biopython](#I.--El-paquete-Biopython)
#    * [Secuencias Biológicas](#Secuencias-Biol%C3%B3gicas)
#  * [II.- Tratamiento de ficheros FASTA](#II.--Tratamiento-de-ficheros-FASTA)
#    * [Lectura de datos](#Lectura-de-datos)
#    * [FASTA como un diccionario de registros](#FASTA-como-un-diccionario-de-registros)
#  * [III.- Estadística descriptiva](#III.--Estad%C3%ADstica-descriptiva)
#    * [Masas de las proteínas](#Masas-de-las-prote%C3%ADnas)
#    * [Longitudes de las proteínas](#Longuitudes-de-las-prote%C3%ADnas)
#    * [Funciones *built-in* para estadística descriptiva](#Funciones-built-in-para-estad%C3%ADstica-descriptiva)
#    * [Módulo *statistics*](#M%C3%B3dulo-statistics)
#    * [Paquete *numpy*](#Paquete-numpy)
#  * [IV.- Gráficas con *matplotlib*](#IV.--Gr%C3%A1ficas-con-matplotlib)
#  * [V.- Filtrado de las proteínas](#V.--Filtrado-de-las-prote%C3%ADnas)
#    * [Filtrado de isoformas](#Filtrado-de-Isoformas)
#    * [Clasificación de kinasas y fosfatasas](#Clasificaci%C3%B3n-de-kinasas-y-fosfatasas)
#  * [VI.- Blastp](#VI.--Blastp)
#    * [Realizando la búsqueda](#Realizando-la-b%C3%BAsqueda)
#    * [Procesando el resultado](#Procesando-el-resultado)
#  * [VII.- Referencias y material de ampliación 🔗](#VII.--Referencias-y-material-de-ampliaci%C3%B3n-🔗)
# ---
#

# %% [markdown] toc-hr-collapsed=false
# # I.- El paquete *Bio*python
#
# <img src="images/biopython/biopython_logo.png" width="150" style="float: left; margin-right: 10px;" /> 
#
# *Bio*python es un paquete de Python formado por un conjunto de herramientas para el trabajo bioinformático.  
# El objetivo de Biopython es proporcionar bibliotecas (librerías/módulos) de código reutilizable, de manera que el usuario pueda centrarse en los objetivos de su trabajo sin tener que preocuparse por tareas rutinarias de programación.

# %% [markdown] tags=["sabermas"] jupyter={"source_hidden": true}
# 📌 _Biopython_ **capabilities**:
#
# - Work with DNA, RNA, Protein sequences (`Bio.SeqIO`, `Bio.SeqUtils`):
#     - Translate
#     - 6-frame translation
#     - calculate complement
#     - calculate mass
#     - 1🡘3 letter code conversion
#
#
# - Parse sequence databases:
#     - FASTA
#     - GenBank
#
#
# - Parsing or Reading Sequence Alignments.
#
#
# - Performing alignments:
#     - CrustaW
#     - MUSCLE
#     - EMBOSS's water and needle
#     - `Bio.pairwise2`
#
#
# - Local and net BLAST+.
#
#
# - Search info databases:
#     - Entrez (PubMed, Medline...)
#     - ExPASy (swissprot)
#     - SCOP
#
#
# - 3D Structures (PDB).
# - Population Genetics.
# - Phylogenetics (`Bio.Phylo`).
# - Sequence motif analysis (`Bio.motifs`).
# - Cluster Analysis (`Bio.Cluster`).
# - Supervised machine learning methods.
# - Graphics.
#

# %%
import Bio         # Importamos el paquete Biopython (que se llama `Bio`)

Bio.__version__    # Debería ser >= 1.75 para evitar errores introducidos en versiones previas

# %% [markdown]
# El paquete base *Bio* importado de esta manera sólo contiene algunas subclases de **Exception** (tratamiento de errores), y poco más.  
# Para poder trabajar con Biopython, habrá que ir importando sus diferentes _módulos_ y _sub-paquetes_.

# %% [markdown] toc-hr-collapsed=false
# ## Secuencias Biológicas
#
# Para el almacenamiento y manipulación de secuencias biológicas (ADN, ARN y proteínas), la principal clase incluida en Biopython es la clase **Seq**, del módulo _Bio.Seq_:
# ```python
#     seq_object = Seq( sequence_data, alphabet=Bio.Alphabet.Alphabet() )
# ```

# %%
from Bio.Seq import Seq      # Importamos la clase `Seq` perteneciente al módulo `Seq` del paquete `Bio`

dna_seq = Seq('GATTACA')     # Creamos una secuencia a partir de una cadena de caracteres (parámetro `data`)

print(type(dna_seq), dna_seq)

# %% [markdown]
# A primera vista, los objetos de la clase **Seq** _parecen similares_ a las _cadenas de caracteres_ (clase **str**) que ya hemos visto en la introducción:
#
#  * Son también **_secuencias_ ordenadas** (y por tanto con un _índice posicional_) de *caracteres*:

# %%
# Comportamiento como contenedores:
print('Pertenencia (in)     :',  'TAC' in dna_seq )     # Pertenencia de una sub-cadena a la secuencia
print('Longitud ( len() )   :',   len(dna_seq) )        # Longitud de la secuencia

# Comportamiento como secuéncias de elementos:
print('Primer carácter [0]  :',  dna_seq[0], type(dna_seq[0]) )       # Obtenemos una `string` de un carácter.
print('Sub-secuencia [0,3)  :',  dna_seq[0:3], type(dna_seq[0:3]) )   # Obtenemos un nuevo objeto `Seq`.
print('Número de adeninas ( .count() ) :',  dna_seq.count('A') )

# %% [markdown]
#  * Ésta _secuencia_ de carácteres es _**inmutable**_:

# %% tags=["error"]
dna_seq[-1] = 'G'     # Intento de modificar un nucleótido ➞ Error!

# %% [markdown]
#  * Y poseen _algunos_ de los _métodos más comunes_ de las cadenas de caracteres:

# %%
print('.lower()  :',  dna_seq.lower() )     # Nueva secuencia en minúsculas.
print('.find( )  :',  dna_seq.find('TAC') ) # Busca una secuencia dentro de otra, devolviendo su posición (-1 si no la encuentra).
print('.split( ) :',  dna_seq.split('A') )  # Devuelve una lista de nuevos objetos Seq.

# %% [markdown]
# <br/>
#
# Sin embargo, presentan _interesantes diferencias_ con las _cadenas de caracteres_:
#
#  * Los objetos de tipo **Seq** tienen un _**alfabeto** asociado_ que determina qué tipo de secuencia biológica es (ADN, ARN o Proteína).  
#    Este _alfabeto_ es un objeto de alguna de las clases del sub-paquete *Bio.Alphabet* o del módulo *Bio.Alphabet.IUPAC*; es tenido en cuenta por los diferentes métodos de la clase **Seq** para decidir como tratar la secuencia; y es accesible a través del atributo `.alphabet` de cualquier objeto _Seq_:

# %%
# Como al crear la secuencia no lo definimos, el alfabeto de nuestra secuencia es genérico:
print(type(dna_seq.alphabet),  dna_seq.alphabet )

# %%
# Podemos cambiar el alfabeto para que coincida con el tipo de secuencia biológica (DNA):

# Importamos el módulo `IUPAC` del sub-paquete `Bio.Alphabet`
from Bio.Alphabet import IUPAC         

# Comprobamos qué letras admite el alfabeto IUPAC.unambiguous_dna
print('Alphabet.letters:',  IUPAC.unambiguous_dna.letters ) 

# %%
# Cambiamos al nuevo alfabeto (👁 Ojo el alfabeto si es mutable!).
dna_seq.alphabet = IUPAC.unambiguous_dna 

dna_seq

# %% [markdown]
# <ul style="list-style-type:none;"><li>Si queremos crear directamente una secuencia con un alfabeto determinado (lo deseable si conocemos el tipo de secuencia biológica que es), sólo hay que especificar el <em>segundo</em> argumento, <em>alphabet</em>, a la hora de llamar a la clase <strong>Seq</strong> para crear la secuencia:</li></ul>

# %%
# Creamos una secuencia proteica (con alfabeto de proteína):

# Utilizaremos un alfabeto extendido de Proteína (incluye códigos para AA ambiguos):
print('Alphabet.letters:', IUPAC.extended_protein.letters)

# Pasamos éste alfabeto como 2º parámetro (`alphabet`):
prot_seq = Seq('MMDCKNALSETNGDLLREKGLGKA', IUPAC.extended_protein)

prot_seq    # Secuencia resultante con un alfabeto extendido de Proteína

# %% [markdown]
#  * _Sólo_ podemos _concatenar_ (operador `+`) objetos _Seq_ si tienen ***alfabetos compatibles*** (ADN + ADN, ARN + ARN, Proteína + Proteína, o bien alfabetos lo suficientemente genéricos):

# %%
long_dnaseq = dna_seq + dna_seq + dna_seq

long_dnaseq     # Secuencia resultante (con el mismo alfabeto)

# %% tags=["error"]
# NO podemos concatenar secuencias con alfabetos incompatibles:
dna_seq + prot_seq      # ADN + Proteína ➞ Error!

# %% [markdown]
#  * Además, los objetos _Seq_ poseen _**métodos propios**_ para el tratamiento de secuencias biológicas:   
#  `.complement()`  
#  `.reverse_complement()`   
#  `.transcribe()`   
#  `.translate( )`   
#  `.tomutable()`   
#    ...  
#   Estos métodos funcionarán o no dependiendo del tipo de secuencia biológica (de su _alfabeto_), y devolverán un _nuevo_ objeto Seq con el _alfabeto adecuado_:

# %%
print("Original              : 5'",  long_dnaseq  , "3'")
print(".complement()         : 3'",  long_dnaseq.complement()  , "5'")
print(".reverse_complement() : 5'",  long_dnaseq.reverse_complement()  , "3'")

# %%
# 💡 Truco para obtener sólo la reversa de una secuencia:  reversa = secuencia[::-1]
print("Reversa [::-1] :", "5'", long_dnaseq[::-1], "3'")

# %%
# Transcripción de DNA a RNA:
rna_seq = long_dnaseq.transcribe()

print('.transcribe() :')
rna_seq                    # Secuencia resultante con un alfabeto de RNA:

# %% tags=["error"]
rna_seq.transcribe()      # Intento de transcribir RNA ➞ Error!

# %%
# Traducción RNA a Proteína:
prot_seq4rna = rna_seq.translate()

print('rna_seq.translate( ) :')
prot_seq4rna          # Secuencia resultante con un alfabeto de Proteína:

# %%
# "Traducción" directa de DNA a Proteína:
prot_seq4dna = long_dnaseq.translate()

print('dna_seq.translate( ) :')
prot_seq4dna         # Misma secuencia resultante con un alfabeto de Proteína:

# %% tags=["error"]
prot_seq.complement() # Una proteína no tiene secuencia complementaria ➞ Error!

# %% [markdown]
# ### Conversión a secuencias mutables
#
# Los objetos _inmutables_ de tipo **Seq** pueden "convertirse" en _nuevos_ objetos **mutables** de tipo **MutableSeq** (clase incluida en el módulo _Bio.Seq_ de Biopython), utilizando el método `.tomutable()`:

# %%
mut_protseq = prot_seq.tomutable()    # Seq ➞ MutableSeq

mut_protseq      # Secuencia mutable con alfabeto de Proteína:

# %% [markdown]
# A _diferencia_ de los objetos de tipo **Seq**, los objetos de tipo **MutableSeq** pueden ser _alterados_ (mutados) utilizando: la _asignación_ a posiciones de la secuencia (`=`); la instrucción `del` para _eliminar_ elementos; y diferentes _métodos_ como `.append(character)`, `.extend(sub-sequence)`, `.insert(character, position)`:

# %%
mut_protseq[1] = 'V'        # Cambio de un carácter por asignación a una posición
print('Cambiar AA en posición 1 (=)    :', mut_protseq)

del mut_protseq[-1]         # Eliminación de un carácter mediante `del`
print('Borrar último AA (del)          :', mut_protseq)

mut_protseq.append('I')     # `.append(character)` para adjuntar un carácter al final
print('Añadir AA al final (.append())  :', mut_protseq)

mut_protseq.extend('AAAAAA') # `.extend(sub-sequence)` para adjuntar una sub-secuencia al final
print('Añadir sub-secuencia al final (.extend()) :', mut_protseq)

mut_protseq.insert(1, 'J')   # `.insert(character, position)` para añadir un carácter en una posición determinada
print('Añadir AA en posición 1 (.insert())       :', mut_protseq)

# %% [markdown]
# Y dado que `mut_protseq` es un _nuevo_ objeto, el objeto inmutable original `prot_seq` (a partir del cual se creo usando el método `.tomutable()`) continua _sin_ sufrir _cambios_:

# %%
prot_seq

# %% [markdown]
# ### Conversión a cadenas de caracteres
#
# Usamos la llamada a la clase **str**, pasándole como argumento el _objeto Seq_ (o _MutableSeq_), para obtener una _nueva_ cadena de caracteres que contenga la secuencia biológica:

# %%
str(prot_seq)     # Seq ➞ str

# %%
str(mut_protseq)  # MutableSeq ➞ str

# %% [markdown] toc-hr-collapsed=false
# # II.- Tratamiento de ficheros FASTA
#
# Una de las cosas que con frecuencia se realiza en proteómica es el tratamiento de ficheros [FASTA](https://en.wikipedia.org/wiki/FASTA) de proteínas.  
# En este apartado usaremos _Biopython_ y otros módulos y paquetes de Python, para **leer**, **manipular** y **analizar los datos** de ficheros FASTA.

# %% [markdown]
# ## Lectura de datos
#
# Si supiéramos de antemano que el archivo de datos a leer (en formato FASTA, [GenBank](https://www.ncbi.nlm.nih.gov/Sitemap/samplerecord.html), [EMBL](https://www.ebi.ac.uk/ena/submit/data-formats), [PIR](http://emboss.sourceforge.net/docs/themes/seqformats/NbrfFormat.html), [UniProt XML](https://www.uniprot.org/docs/uniprot.xsd), ...) _sólamente_ contiene _un único registro_ (_record_), utilizaríamos la función `read( )` del sub-paquete *Bio.SeqIO* para obtener este único registros como un objeto de tipo **SeqRecord**:
# ```python
# SeqIO.read(handle, format, alphabet=None)  ➞ 1 seqrecord_object
# ```
#
# Pero puesto que la base de datos FASTA que queremos leer contiene _múltiples registros_ (lo más habitual), utilizaremos la función `parse( )` del mismo sub-paquete *Bio.SeqIO* para ir leyendolos secuencialmentes:
# ```python
# SeqIO.parse(handle, format, alphabet=None)  ➞ generator_object  ➞➞ seqrecord_objects
# ```

# %% [markdown]
# Como podemos observar, ambas funciones requieren los mismos _parámetros_:
#   * El primer argumento (parámetro *handle*) es el _nombre del fichero_ a leer (o, alternativamente, un objeto de tipo _input/output_ (entrada/salida o _io_) que actúe como *handle*).  
#   * Hay que indicar el _formato_ de dicho fichero como segundo argumento (parámetro *format*). En el caso de un fichero FASTA debe ser `'fasta'` (ver _formatos soportados_ en [esta página web](https://biopython.org/wiki/SeqIO#file-formats)).  
#   * _Opcionalmente_ podemos indicar el _alfabeto_ (parámetro *alphabet*) que resulte más adecuado para la/s secuencia/s a leer desde el fichero.  
#     Si no se especifica _ningún alfabeto_ (valor por defecto `None`) se elegirá uno automáticamente. El problema es que, para los archivos con formato FASTA se elige automáticamente un alfabeto _demasiado genérico_ que _no_ identifica el tipo de secuencia biológica, por lo es **muy recomendable** indicar un _alfabeto más específico_ (importado del sub-paquete *Bio.Alphabet*).

# %%
# Importamos el sub-paquete `SeqIO` de Biopython
from Bio import SeqIO

# Importamos el alfabeto para secuencias proteicas genéricas
from Bio.Alphabet import generic_protein 

# %%
# Localización y nombre del fichero FASTA:
FASTA_FILE = 'data/qualitative/uniprot-reviewed-Human-9606-w-isoforms.fasta'

# Este fichero contiene 42425 registros de proteínas humanas (incluyendo isoformas: canonical & isoform) 
# y fue obtenido de UniProtKB/SwissProt: 
# https://www.uniprot.org/uniprot/?query=reviewed%3Ayes+taxonomy%3A%22Homo+sapiens+%28Human%29+%5B9606%5D%22

# %% [markdown]
# La función `SeqIO.parse( )` retorna un objeto de tipo **generator** (_generador_), que podremos iterar para ir obteniendo cada una de las entradas/registros (_records_) del _fichero_ especificado:

# %%
# Obtenemos un generador utilizando SeqIO.parse() pasándole un nombre de fichero (`dbase`), 
# indicando como formato de fichero 'fasta', y un alfabeto de proteínas:
records_reader = SeqIO.parse(FASTA_FILE, 'fasta', alphabet=generic_protein) 

records_reader      # Objeto generador:

# %% [markdown]
# Cada uno de los registros (_record_) que obtengamos a partir de éste *generador* será un objeto de tipo **SeqRecord**, el cual poseerá un  atributo `.seq` que referenciará un _objeto Seq_, junto a otros atributos que permitirán acceder a _metadatos_ de dicha secuencia, como por ejemplo el atributo `.id` para acceder a su _identificador_ o `.description` para obtener su _descripción_:

# %%
# Obtenemos el siguiente registro del generador utilizando la función `next( )` de Python:
prot_record = next(records_reader)

prot_record       # Registro obtenido como objeto de tipo SeqRecord:

# %%
# Atributo `.seq` del objeto SeqRecord:
protrecord_seq = prot_record.seq
protrecord_seq    # Es un objeto de tipo Seq que contiene la secuencia de la proteína:

# %%
# Dada su longitud, hay que usar `print()` para observarla entera:
print(protrecord_seq)

# %%
# Atributo `.id` del objeto SeqRecord:
protrecord_id = prot_record.id
protrecord_id       # Identificador del "record": la primera parte del encabezado FASTA de la proteína:

# %%
# Separamos las partes del identificador:
# base_de_datos('sp' por SwissProt, 'tr' por TrEMBL)|ACcession_number-Isoforma|NOMBRE_ORGANISMO(Entry Name)
protrecord_id.split('|')

# %%
# Atributo `.description` del objeto SeqRecord:
prot_record.description   # Se corresponde con todo el encabezado FASTA de la proteína:

# %% [markdown] tags=["sabermas"]
# Adicionalmente, el _método_ `.format(format)` del objeto _SeqRecord_ nos permite obtener una representación (como cadena de caracteres) de dicho registro en el _formato_ (parámetro *format*) que especifiquemos:

# %% tags=["sabermas"]
print( prot_record.format('fasta') )    # Formatear el "record" como 'fasta' (el formato de origen):

# %% tags=["sabermas"]
print( prot_record.format('genbank') )    # Formatear el "record" como un registro 'genbank':

# %% [markdown]
# <br />
#   
# A continuación leeremos **todos** los registros de la base de datos FASTA para tenerlos disponibles en memoria RAM (mucho más rápido que ir leyéndolos cada vez del disco duro).  
# Para ello iremos _iterando_ (bucle `for`) sobre el _generador_ `records_reader`, e iremos almacenando los registros que devuelva (_objetos SeqRecord_) en una _lista_:

# %%
# Lista para almacenar los registros leídos y "parseados" desde el archivo FASTA.
fasta_records = [] 

# Iteramos todo el generador `records_reader` para leer todos los objetos SeqRecord:
for record in records_reader:
    fasta_records.append(record)    # Añadir cada objeto SeqRecord a la lista

print("Registros leídos de '" + FASTA_FILE + "' : ",  len(fasta_records) )

# %% [markdown]
# Aquí hay un **problema**: si recordáis, sabemos de antemano que nuestro fichero FASTA debería contener 42425 registros; pero la lista `fasta_records` sólo contiene 42424.  
# Falta uno! Dónde está?

# %% [markdown]
# La _explicación_ es que los objetos _generadores_ se van _"consumiendo"_ conforme obtenemos objetos de ellos.  
# Así, al utilizar el bucle iterativo `for` hemos _consumido_ **todos** los registros del generador `records_reader` (es decir, se han ido leyendo todos, uno detrás de otro, desde el fichero FASTA, hasta llegar al final), y por tanto ya no podemos obtener más del generador...

# %% tags=["error"]
next(records_reader)       # Siguiente "record" ➞ Error!

# %% [markdown]
# Y, dado que ya habíamos _obtenido/leído_ previamente al bucle `for` el _primer registro_ (la variable `prot_record`), ésto ha provocado que éste _no_ se haya incluido en la lista `fasta_records`: lo habíamos _consumido previamente_ del generador `records_reader` y ya no estaba disponible al iterar sobre él con el bucle `for` :

# %%
prot_record.id == fasta_records[0].id

# %% [markdown]
# Para _evitar_ este tipo de "problemas", hemos de asegurarnos de _volver a obtener_ el objeto *generator* cada vez que queramos leer registros de él desde el principio hasta el final:

# %%
records_reader = SeqIO.parse(FASTA_FILE, 'fasta', alphabet=generic_protein) # Nuevo objeto generator.

fasta_records = []

for record in records_reader:
    fasta_records.append(record)
print("Registros leídos: ",  len(fasta_records) )

# %% tags=["sabermas", "alternativa"] jupyter={"source_hidden": true}
# 🔄 Alternativamente podemos llamar a la clase list pasándole el generador, para que lo itere y
# defina la nueva lista con los elementos que éste vaya leyendo del archivo FASTA:
records_reader = SeqIO.parse(FASTA_FILE, 'fasta', alphabet=generic_protein) # Nuevo objeto generator

fasta_records = list(records_reader) # Lista a partir de un objeto iterable

print("Registros leidos: ",  len(fasta_records) )

# %% [markdown]
# Ahora sí tenemos los 42425 registros que deberíamos tener en la lista `fasta_records`.  

# %% [markdown]
# ## FASTA como un diccionario de registros
#
# Una _lista_ como `fasta_records` sólo nos permite acceder a los registros leídos que contiene usando un _índice numérico_ según el orden en que los elementos se han ido añadiendo a la lista; y esto no es muy útil para _indexar_ proteínas.
#
# Lo ideal sería acceder a cada registro a partir del [_Accession number_](https://www.uniprot.org/help/accession_numbers) de la proteína. Hemos de crear pues un _diccionario_ de pares   
#   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; _Accession number ➞ Protein Record_ .  
# Para ello recurriremos a la función `to_dict( )` del sub-paquete *Bio.SeqIO*:
# ```python
#     SeqIO.to_dict(generator_object, key_function=None)  ➞ dictionary
# ```

# %%
records_reader = SeqIO.parse(FASTA_FILE, 'fasta', alphabet=generic_protein)   # Nuevo objeto generator.

id2seqrecord = SeqIO.to_dict(records_reader)                                  # Sin pasar argumento para `key_function`.

print("Registros leídos: ",  len(id2seqrecord) )

# %%
list( id2seqrecord.keys() )[:10]      # Visualizamos las primeras 10 keys del diccionario obtenido:


# %% [markdown]
# Como vemos, el diccionario que obtenemos así _no_ contienen solamente los _Accession number_ (_AC_) como _keys_, sino que cada _key_ incluye _todo el identificador_ (atributo *SeqRecord*`.id`) de cada registro. _No_ es pues lo que nos habíamos planteado.

# %% [markdown]
# Si queremos que las _keys_ del diccionario sean tal como nosotros queremos (sólo el _AC_), hemos de pasarle a la función `SeqIO.to_dict( )` un segundo argumento (parámetro *key_function*), consistente en una _función_ que devuelva el _ACcession number_ correspondiente a cada registro (a cada objeto **SeqRecord**) que dicha función reciba como único _argumento_:

# %%
# Definimos la función `get_accession( )`, que pasaremos después a la función `Bio.SeqIO.to_dict( )`:
# Ha de aceptar un argumento, que debería ser un objeto SeqRecord

def get_accession(seq_record): 
    """Retorna el ACcession number de un objeto SeqRecord (parámetro `seq_record`)"""
    parts = seq_record.id.split('|')      # Divide la cadena de caracteres `id` del objeto SeqRecord
    return parts[1]                       # Retorna el segundo elemento (el Accession number)


# %%
records_reader = SeqIO.parse(FASTA_FILE, 'fasta', alphabet=generic_protein)   # Nuevo generator

# Pasamos la función `get_accession` como argumento para el parámetro `key_function` 
ac2seqrecord = SeqIO.to_dict(records_reader, key_function=get_accession)

print("Registros leídos: " , len(ac2seqrecord))

# %%
list( ac2seqrecord.keys() )[:10] # Visualizamos las primeras 10 keys del diccionario obtenido:

# %%
ac2seqrecord['Q9H9K5']        # Acceso a un registro a partir del ACcession Number:

# %%
ac2seqrecord['Q9H9K5'].seq    # Acceso la secuencia de otro registro:

# %% [markdown] jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 En caso de tratar con archivos *mucho más grandes*, que no pudieran contenerse en la memoria RAM disponible en el PC, podríamos recurrir a las funciones `SeqIO.index( )` o `SeqIO.index_db( )` para obtener _pseudo-diccionarios_ que no estarían contenidos en memoria e irían _leyendo_ los registros del disco _bajo demanda_ (en función de la _key_ que quisiéramos acceder).  
# Más información en la documentación oficial de [SeqIO.index( )](http://biopython.org/DIST/docs/api/Bio.SeqIO-module.html#index) y de [SeqIO.index_db( )](http://biopython.org/DIST/docs/api/Bio.SeqIO-module.html#index_db).

# %% [markdown] toc-hr-collapsed=false
# # III.- Estadística descriptiva
#
# A partir de las _masas_ y las _longitudes_ de secuencia de las proteínas del fichero FASTA realizaremos unos _cálculos estadísticos_ sencillos que nos ayuden a entender las distribuciones de estos valores.

# %% [markdown]
# ## Masas de las proteínas
#
# Para calcular la masa de las proteínas que hemos leído del fichero FASTA, utilizaremos la función `molecular_weight( )` del sub-paquete Bio.SeqUtils:
# ```python
#     SeqUtils.molecular_weight(Seq_or_str, seq_type=None, monoisotopic=False)  ➞ sequence_mass
# ```

# %% [markdown]
# Si como primer argumento pasamos una _cadena de caracteres_, debemos indicar el tipo de secuencia de que se trata (`'DNA'`, `'RNA'` o `'protein'`) mediante el parámetro *seq_type*.  
# En el caso de pasar como primer argumento un objeto de tipo **Seq**, tan sólo debemos pasar también el tipo de secuencia (*seq_type*) si el _alfabeto_ de la secuencia fuera _tan genérico_ que no identificase de qué tipo de secuencia biológica se trata (o sea, si se tratara de un alfabeto como Bio.Alphabet.generic_alphabet, Bio.Alphabet.single_letter_alphabet, ... ).  
# En el cálculo de la masa de una _secuencia proteica_, la función utiliza _por defecto_ el valor de masa promedio (_average_) para cada residuo aminoacídico, salvo que se pase `True` para el parámetro *monoisotopic*, en cuyo caso la masa utilizada para cada residuo será el valor de su [masa monoisotópica](https://en.wikipedia.org/wiki/Monoisotopic_mass).  

# %%
# Importamos la función `molecular_weight( )` de `SeqUtils`
from Bio.SeqUtils import molecular_weight 

# %%
# Ejemplos de uso de la función `molecular_weight( )` :

# Usando strings ➞ Hay que pasar siempre un argumento para `seq_type` ('DNA', 'RNA' o 'protein'):
prot_seq = 'MAEGEITTFTALTEKFNLPPGNYKKPKLL'
print("From string, average  :",  molecular_weight(prot_seq, seq_type='protein') )

# Usando objetos Seq ➞ Han de tener definido el alfabeto adecuado:
prot_seq = Seq('MAEGEITTFTALTEKFNLPPGNYKKPKLL', alphabet=generic_protein)
print("From Seq, average     :",   molecular_weight(prot_seq) )
print("From Seq, monoisotopic:",   molecular_weight(prot_seq, monoisotopic=True) )

# %% [markdown]
# Un **problema** que podemos encontrar es que la secuencia proteica _contenga_ caracteres que representen _nucleótidos_ o _aminoácidos_ _**ambiguos**_, como por ejemplo `'J'` (Leu o Ile), `'X'` (cualquier AA), `'N'` (cualquier nucleótido), ... :

# %% tags=["error"]
print( molecular_weight('AAAAJAAA', seq_type='protein', monoisotopic=True) )   # 'J' ➞ Error!

# %% [markdown]
# <br/>
#
# Ahora calcularemos la masa *monoisotópica* de cada una de las proteínas de nuestro diccionario `ac2seqrecord` usando la función `molecular_weight( )`; pero teniendo en cuenta que podemos encontrarnos _aminoácidos ambiguos_ en las secuencias, usaremos un bloque `try... except... else...` para "capturar" las posibles _excepciones_ ("errores") que éstos aminoácidos ambiguos puedan provocar:

# %%
ac2protmass = dict()   # Diccionario de pares ACcession Number ➞ Protein Mass.
ac2error = dict()      # Diccionario para almacenar las proteínas con AA ambiguos (ACcession Number ➞ Error).

for ac, record in ac2seqrecord.items():
    try:
        monoiso_mass = molecular_weight(record.seq, monoisotopic=True)
    except ValueError as error:        # Si se produce un error/excepción de tipo ValueError:
                                       # No tenemos ningún valor de masa a añadir al diccionario `ac2protmass`...
        ac2error[ac] = error           # ...Pero nos guardamos el AC de la proteína y el error en `ac2error`.
    else:                              # Si no se ha producido ningún error al calcular la masa:
        ac2protmass[ac] = monoiso_mass # Añadimos la masa obtenida al diccionario `ac2protmass`.

print('Masas calculadas:', len(ac2protmass) ) # De cuántas proteínas hemos calculado la masa?

# %%
# Qué proteínas han provocado errores, y qué errores?
ac2error

# %% [markdown]
# <br/>
#
# Por último, para poder realizar los cálculos _estadísticos_ y las _gráficas_ posteriores, obtendremos una _lista_ (`protmasses`) únicamente con los _valores_ de _masas_ del diccionario `ac2protmass`:

# %%
protmasses = list( ac2protmass.values() )  # Lista de masas de las proteínas del archivo FASTA

protmasses[:10]                            # Visualizamos las 10 primeras masas de la nueva lista:

# %% [markdown]
# ## Longitudes de las proteínas

# %% [markdown] tags=["ejercicio"]
# > 📝 **Práctica:**
# >
# > Calcular las **longitudes** de todas las secuencias de las proteínas que hemos leído del fichero FASTA, y almacenarlas en el _diccionario_ `ac2length`:

# %% tags=["ejercicio"]
ac2length = dict() # Diccionario de pares ACcession Number ➞ Protein Length



# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución del Ejercicio:

# Diccionario de pares ACcession Number ➞ Protein Length
ac2length = dict()     

for ac, record in ac2seqrecord.items():
    ac2length[ac] = len(record.seq)       # Utilizamos la función `len()` de Python

# %% tags=["ejercicio"]
print('Longitudes proteicas calculadas:', len(ac2length) ) # De cuántas proteínas hemos calculado la longitud?
list( ac2length.items() )[:10]         # Visualización de los 10 primeros pares AC ➞ Protein Length:

# %% [markdown] tags=["ejercicio"]
# > Por último, para los cálculos _estadísticos_ y las _gráficas_ posteriores, obtener una _lista_ (`protlengths`) unicamente con los _valores_ de _longitudes_:

# %% tags=["ejercicio"]



# %% tags=["ejercicio", "solucion"] jupyter={"source_hidden": true}
# 📎 Solución:

protlengths = list( ac2length.values() ) # Lista de longitudes de las proteínas del archivo FASTA

# %% tags=["ejercicio"]
protlengths[:10] # Visualizamos las 10 primeras longitudes de la lista:

# %% [markdown]
# ## Funciones _built-in_ para estadística descriptiva
#
# Básicamente podemos usar las funciones `min( )`, `max( )` y `len( )`:

# %%
print('Masa más baja ( min() ) :',  min(protmasses) )
print('Masa más alta ( max() ) :',  max(protmasses) )
print('Número total de valores de masa ( len() ) :',  len(protmasses) )


# %% [markdown] tags=["ejercicio"]
# > 📝 **Práctica:**
# >
# > Define una _función_ `mean( )` que permita obtener el **promedio** (la [media aritmética](https://en.wikipedia.org/wiki/Arithmetic_mean#Definition)) de un contenedor iterable de valores numéricos (como es el caso de la lista `protmasses`).  
#
# > _💡 Apunte:_ para ello _puedes_ utilizar la función `sum( )` de Python.

# %% tags=["ejercicio"]
def mean(iterable):
    "Calcula la media aritmética del contenedor `iterable`"



# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución del Ejercicio:

def mean(iterable):
    "Calcula la media aritmética del contenedor `iterable`"
    return sum(iterable)/len(iterable)


# %% tags=["ejercicio"]
# Utilizamos la nueva función `mean()` que has definido, para obtener el promedio de las masas:
print('Promedio de las masas ( mean() ) :',  mean(protmasses) )

# %% [markdown] tags=["ejercicio"]
# > 📝 **Práctica:**
# >
# > Obtener el valor **máximo**, el **mínimo**, el **número** de valores, y la **media** de las _longitudes_ de las proteínas (variable `protlengths`) utilizando las funciones anteriores.

# %% tags=["ejercicio"]



# %% tags=["ejercicio", "solucion"] jupyter={"source_hidden": true}
# 📎 Solución:

print('Longitud más baja ( min( ) ) :',  min(protlengths) )
print('Longitud más alta ( max( ) ) :',  max(protlengths) )
print('Promedio de las massas ( mean( ) ) :',  mean(protmasses) )
print('Número total de valores de Longitud ( len( ) ) :',  len(protlengths) )

# %% [markdown]
# ## Módulo *statistics*
#
# El módulo [_**statistics**_](https://docs.python.org/3/library/statistics.html) es un módulo de la _librería estándard_ de Python 3 que posee algunas _funciones_ para obtener medidas estadísticas _básicas_:
#
#
#  * ```python
#    statistics.mean(iterable)
#    ```
#    Devuelve la [media aritmética](https://en.wikipedia.org/wiki/Arithmetic_mean#Definition) de los valores numéricos del *iterable* pasado como argumento.
#    
#  * ```python
#    statistics.median(iterable)
#    ```
#    Devuelve la [mediana](https://en.wikipedia.org/wiki/Median) (el valor central) de los valores numéricos del *iterable* pasado como argumento.
#    
#  * ```python
#    statistics.mode(iterable)
#    ```
#    Devuelve la [moda](https://en.wikipedia.org/wiki/Mode_(statistics)) (el valor más frecuente) de los valores numéricos del *iterable* pasado como argumento.
#    
#  * ```python
#    statistics.pstdev(iterable)
#    ```
#    Devuelve la [desviación estándar](https://en.wikipedia.org/wiki/Standard_deviation) _poblacional_ (la raíz cuadrada de la [varianza](https://en.wikipedia.org/wiki/Variance) poblacional) de los valores numéricos del *iterable* pasado como argumento.

# %%
import statistics as st          # Usaremos el alias `st` para abreviar.

# %%
print('Media ( mean() )             :',  st.mean(protmasses) )
print('Mediana ( median() )         :',  st.median(protmasses) )
print('Desv. estándar ( pstdev() )  :',  st.pstdev(protmasses) )

# %% jupyter={"source_hidden": true} tags=["sabermas"]
# %%timeit # ← 📌 Permite calcular el tiempo que tarda en ejecutarse el código de la celda
st.mean(protmasses), st.median(protmasses), st.pstdev(protmasses)

# %% [markdown] tags=["ejercicio"]
# > 📝 **Práctica:**
# >
# > Obtener la **media**, la **mediana** y la **desviación estándar** de las _longitudes_ de las proteínas (variable `protlengths`) utilizando las _funciones_ del módulo _statistics_ (`st`):

# %% tags=["ejercicio"]



# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución:

print('Media ( mean( ) )             :',  st.mean(protlengths) )
print('Mediana ( median( ) )         :',  st.median(protlengths) )
print('Desv. estándar ( pstdev( ) ) :',  st.pstdev(protlengths) )

# %% [markdown]
# ## Paquete *numpy*
#
# <img src="images/biopython/numpy_logo.png" width="150" style="float: left; margin-right: 10px;" />
#
# El paquete [_**numpy**_](http://www.numpy.org/) es un paquete _fundamental_ para el cálculo numérico en Python (muchos otros paquetes de cálculo científicos se basan en NumPy). Entre otras cosas, define: un tipo para crear matrices N-dimensionales (clase **ndarray**); funciones de cálculo vectorial (paralelizable), rápidas y eficientes; funciones de álgebra lineal, transformadas de Fourier, generación de números aleatorios, ...
#
# Entre las funciones incluidas en NumPy podemos encontrar las siguientes _funciones estadísticas_ básicas:
#
#  * ```python
#    numpy.mean(iterable_or_ndarray)
#    ```
#    Devuelve la _media arirmética_ de los valores numéricos del *iterable* pasado como argumento.
#    
#  * ```python
#    numpy.median(iterable_or_ndarray)
#    ```
#    Devuelve la _mediana_ (el valor central) de los valores numéricos del *iterable* pasado como argumento.
#    
#  * ```python
#    numpy.std(iterable_or_ndarray)
#    ```
#    Devuelve la _desviación estándar poblacional_ (la raíz cuadrada de la _variancia_ poblacional) de los valores numéricos del *iterable* pasado como argumento.

# %%
# Comúnmente el paquete `numpy` se importa como `np` (alias) para abreviar:
import numpy as np

# %%
print('Media ( mean() )          :', np.mean(protmasses) )
print('Mediana ( median() )      :', np.median(protmasses) )
print('Desv. estándard ( std() ) :', np.std(protmasses) )

# %% jupyter={"source_hidden": true} tags=["sabermas"]
# %%timeit # ← 📌 Permite calcular el tiempo que tarda en ejecutarse el código de la celda
np.mean(protmasses), np.median(protmasses), np.std(protmasses)

# %% [markdown] tags=["ejercicio"]
# > 📝 **Práctica:**
# >
# > Obtener la **media**, la **mediana** y la **desviación estándar** de las _longitudes_ de las proteínas (variable `protlengths`) utilizando las _funciones_ del paquete _numpy_ (`np`):

# %% tags=["ejercicio"]



# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución:

print('Media ( mean( ) )          :',  np.mean(protlengths) )
print('Mediana ( median( ) )      :',  np.median(protlengths) )
print('Desv. estándard ( std( ) ) :',  np.std(protlengths) )

# %% [markdown] toc-hr-collapsed=false
# # IV.- Gráficas con *matplotlib*
#
# <img src="images/biopython/matplotlib_logo.png" width="150" style="float: left; margin-right: 10px;" />
#
# El paquete [_**matplotlib**_](https://matplotlib.org/ "Web de matplotlib") es la principal librería para la creación y representación de gráficas 2D (también 3D) en Python.  
# A pesar de estar formado por una extensa y completa [API (_Application Programming Interface_)](https://en.wikipedia.org/wiki/Application_programming_interface "Definición de API: Application Programming Interface") orientada a objetos que permite personalizar hasta el más mínimo detalle de una gráfica, es mucho más habitual utilizar el módulo _pyplot_, el cual permite simplificar la creación de gráficas utilizando un estilo imperativo (a base de funciones que van definiendo el aspecto final de la gráfica) muy similar al del programa [MATLAB](https://en.wikipedia.org/wiki/MATLAB).
#
# Nosotros utilizaremos este módulo _pyplot_:

# %%
# Comúnmente el módulo `matplotlib.pyplot` se importa como `plt` (alias) para abreviar:
import matplotlib.pyplot as plt

# %% [markdown]
# La forma más sencilla de crear una gráfica utilizando _pyplot_ es llamando a la función  `plot()` para dibujar una _línea_ y/o los puntos (_marcadores_) de los valores pasados:
#
# ```python
# pyplot.plot( [xdata,] ydata[, format_str], color=None, linestyle='-', marker='', label='', zorder=1 )  
# ```

# %% [markdown]
#  * *ydata* es el único parámetro obligatorio, y ha de ser una secuencia o matriz que contenga los valores del _eje Y_ de la _línea_ o _marcadores_ a dibujar.  

# %%
# Llamada simple a la función `plot()`, únicamente con los valores del eje Y:
plt.plot( [2, 6, 4, 5] )


# %% [markdown]
#  * *xdata* ha de ser una secuencia o matriz con los valores del _eje X_ de la _línea_ o _marcadores_ a dibujar.  
#    Si no se especifica, estos valores serán extrapolados a los valores de indice/posición para cada valor de *ydata*.
#  * *color* (o <em>c</em>) permite definir el _color_ para dibujar la _línea y/o los marcadores_ (_markers_) de cada punto. El valor para este parámetro suele ser o bien una _cadena de caracteres_ conteniendo el _nombre_ del color (en inglés, ej.: `'red'`, `'green'`, `'darkblue'`) o su _inicial_ (ej.: `'g'` para _green_, `'y'` para _yellow_), o bien una _tupla_ de números entre 0 y 1, como representación aritmética del formato [RGB](https://en.wikipedia.org/wiki/RGB_color_model#Numeric_representations) (_red, green, blue_ ➞ tupla de 3 valores) o RGBA (_red, green, blue, alpha_ ➞ tupla de 4 valores).  
#    Si no se especifica (o su valor es `None`), _por defecto_ el color va cambiando para cada línea dibujada por `plot( )` entre una serie de colores predefinidos.
#  * *linestyle* (o *ls*) permite indicar un _estilo_ para la _línea_ a dibujar. Ha de ser una cadena de caracteres: `'-'` ó `'solid'` para una línea _continua_; `'--'` ó `'dashed'` para una línea _discontinua_; `':'` ó `'dotted'` para una línea de _puntos_; `'-.'` ó `'dashdot'` para una linea de _rayas_ y _puntos_; `'None'`, `' '` ó `''` para indicar que _no_ se dibuje la línea. Especialmente esta última opción es muy útil ya que permite utilizar la función `plot( )` para obtener _gráficos de dispersión_ (o [_scatterplot_](https://en.wikipedia.org/wiki/Scatter_plot)).  
#    Si no se indica (o su valor es `None`), _por defecto_ se dibuja una línea _continua_.
#  * *marker* permite indicar el tipo de _marcadores_ a usar para representar los _puntos (x, y)_ de los valores pasados.  
#    Ha de ser un carácter: `'o'` (_circulos_); `'.'` (_circulos pequeños_); `'v'`, `'^'`, `'<'` ó `'>'` (diferentes tipos de _triángulos_); `'s'` (_cuadrados_); `'p'` (_pentagonos_); `'h'` ó `'H'` (para _hexágonos_);  `'*'` (_estrellas_); `'+'` (símbolos _más_); `'x'` (_equis_); `'D'` ó `'d'` (para _rombos_); `' '` ó `''` (_no_ dibujar _marcadores_); ...  
#    Si no se especifica (o su valor es `None`), _por defecto_ no se dibuja _ningún marcador_.

# %%
# Como un ejemplo más completo del uso de la función plot(), vamos a representar gráficamente
# la función matemática  y = f(x) = 2x² - 20 :
def f(x):
    return 2 * x**2 - 20


# %%
# Valores de X (del eje X) entre -5 y 5:
data1_x = range(-5, 6)        

# %%
# Para obtener los valores de Y, aplicamos la función `f()` a cada uno de los valores X, 
# utilizando para ello la función `map()` de Python:
# `map()` devuelve un iterador con los resultados, que pasamos a lista:
data1_y = list( map(f, data1_x) )     
data1_y

# %% tags=["sabermas", "alternativa"] jupyter={"source_hidden": true}
# 🔄 La línea anterior en que usamos la función `map( )` sería equivalente al siguiente código:
data1_y = []
for x in data1_x:
    data1_y.append( f(x) )

data1_y

# %%
# Llamada más completa a la función `plot()` para representar los valores `data1_x` y `data1_y`:
plt.plot(data1_x, data1_y, color='red', linestyle='--', marker='s') # → Ésto resulta un poco largo, veamos como abreviarlo:

# %% [markdown]
#  * *format_str* permite _abreviar_ y definir en _una única_ cadena de caracteres el _color_ (usando las *iniciales* del color), el _estilo de línea_ (usando los *símbolos* de estilo de línea vistos para el parámetro *linestyle*) y el _tipo de marcadores_ (usando los *caracteres* vistos para el parámetro *marker*):  
#    ```python
#     '[color][linestyle][marker]'
#    ```
#    _Ejemplo_: `'b:o'` ➞ color azul (`'b'` por _blue_), línea discontinua de _puntitos_ (`':'` por _dotted_) y marcadores _circulares_ (`'o'` por _circulo_).  
#    Si no se indica la letra inicial del color, se usará el _color por defecto_; si no se indica el estilo de línea, _no_ se dibujará la línea; y si no se indica el carácter del tipo de marcador, _no_ se dibujaran marcadores.

# %%
# Llamada "abreviada" a la función `plot()` usando el argumento `format_str`:
plt.plot(data1_x, data1_y, 'r--s')    # ➞ color='r', linestyle='--', marker='s'

# %% [markdown]
#  * *label* permite indicar el _nombre_ o _etiqueta_ de la serie de datos en la _leyenda_ de la gráfica.  
#    Si no se especifica (o su valor es `None`) la serie de datos no aparecerá en la _leyenda_.
#    
#  * *zorder* ha de ser un valor numérico que indique la _profundidad_ de la línea o el conjunto de marcadores dibujados: cuanto menor sea más \"al fondo\" estará, y podrá ser tapado por otros elementos gráficos con un valor de *zorder* mayor. Si no se pasa ningún argumento para el parámetro *zorder*, su valor por defecto es 2."

# %% [markdown]
# <br/>
#
# Después de una llamada a la función `pyplot.plot()` podemos hacer más llamadas a esta función con conjuntos de datos distintos para ir dibujando más líneas y/o puntos/marcadores en la misma gráfica; siempre y cuando las llamadas sean dentro de la _misma celda_ del _notebook_. 
#
# Si matplotlib lo utilizáramos en _modo no interactivo_ tanto en _scripts_ como en el _REPL estándar_, tendríamos que invocar la función `pyplot.show()` como última instrucción. Sin embargo, tal como hemos ido viendo, en los _notebooks_ esto no es necesario porque, al _ejecutar una celda_ que utiliza el módulo _pyplot_,se invoca _automáticamente_  `pyplot.show()`.

# %%
# Definimos un nuevo "dataset":
data2_x = [-5, 3, -1,  0, 0,  1, 5, -3,  5]   # Valores X
data2_y = [ 0, 8,  4, 10, 8, -1, 2,  1, 10]   # Valores Y correspondientes

# %%
# "Dataset" 1 como una línea con marcadores:
plt.plot(data1_x, data1_y, 'r--s')

# "Dataset" 2 como un gráfico de dispersión (sin líneas que unan los marcadores):
plt.plot(data2_x, data2_y, 'bo')   # ➞ 'b' por color='blue', nada por linestyle='', 'o' por marker='o'

# En un script hay que llamar a `.show()` para que se dibuje el gráfico.
# En un notebook `.show()` es llamada automáticamente al ejecutar la celda de código.
plt.show() 

# %% [markdown] tags=["sabermas"]
# <br />
#
# Para hacer la gráfica **más _vistosa_ y _completa_** (de cara a una publicación, una presentación ...) podemos usar, entre otras, las _funciones_ `grid()`, `legend()`, `title()`, `xlabel()`, e `ylabel()` del módulo *pyplot*:

# %% [markdown] tags=["sabermas"]
#  * ```python
#    pyplot.grid(boolean=None, axis='both')
#    ```
#    _Muestra_ (`boolean=True`) u _oculta_ (`boolean=False`) la _rejilla_ de la gráfica.  
#    El segundo parámetro, *axis*, permite indicar qué _líneas de la rejilla_ mostrar u ocultar: `'x'`, `'y'`, o `'both'` (por _defecto_).
#
#  * ```python
#    pyplot.legend()
#    ```
#    _Muestra_ la _leyenda_ de la gráfica, usando para ello los valores del argumento *label* de los diferentes elementos que hayamos dibujado.
#
#  * ```python
#    pyplot.title(graph_title)
#    ```
#    _Define_ el _título general_ a mostrar en la gráfica.
#
#  * ```python
#    pyplot.xlabel(x_axis_label)
#    ```
#    _Define_ la etiqueta a mostrar junto al _eje X_ de la gráfica.
#
#  * ```python
#    pyplot.ylabel(y_axis_label)
#    ```
#    _Define_ la etiqueta a mostrar junto al _eje Y_ de la gráfica.

# %% tags=["sabermas"]
# "Dataset" 1 como una línea con marcadores y "Dataset" 2 como un gráfico de dispersión:
plt.plot(data1_x, data1_y, 'r--s', label='Dataset 1')
plt.plot(data2_x, data2_y, 'bo', label='Dataset 2')

# Completamos la gráfica:
plt.grid(True)              # Muestra la rejilla completa
plt.legend()                # Muestra una leyenda (los 'labels')
plt.title('Graphic Title')  # Título de la gráfica
plt.xlabel('x  values')     # Etiqueta del eje X

# Asignamos el valor retornado de la última función a `_` para que éste no aparezca en pantalla:
_ = plt.ylabel('y  values') # Etiqueta del eje Y

# %% [markdown] tags=["sabermas"] jupyter={"source_hidden": true}
# <br/>
#
# 📌 Además, podemos usar las _funciones_ `xlim()` e `ylim()` para averiguar los valores límite (_mínimo y máximo_) de los _ejes X e Y_ respectivamente, y poder pasárselos a las _funciones_ `vlines( )` y `hlines( )` para añadir **líneas _horizontales_ y _verticales_** a la gráfica, con las que _marcar_ determinados valores en la gráfica:

# %% [markdown] jupyter={"source_hidden": true} tags=["sabermas"]
# 📌
#  * ```python
#    pyplot.xlim([xmin, xmax]) ➞ (xmin, xmax)
#    ```
#    Si se pasan valores para los parámetros opcionales *xmin* y *xmax*, permite _definir_ los valores _mínimo_ y _máximo_ del _eje X_ de la gráfica en función de éstos.  
#    En cualquier caso, siempre _devuelve_ los valores _mínimo_ y _máximo_ del _eje X_ de la gráfica.
#    
#  * ```python
#    pyplot.hlines(y, xmin, xmax, colors='k', linestyles='solid', label='', zorder=2)
#    ```
#    Dibuja una línea horizontal a la *altura y* indicada del _eje Y_, que va del valor *xmin* al valor *xmax* del _eje X_.  
#    El resto de parámetros son equivalentes a los de la función `pyplot.plot( )` con el mismo nombre.
#    
#  * ```python
#    pyplot.ylim([ymin, ymax]) ➞ (ymin, ymax)
#    ```
#    Si se pasan los valores para los parámetros opcionales *ymin* y *ymax*, permite _definir_ los valores _mínimo_ y _máximo_ del _eje Y_ de la gráfica en función de éstos.  
#    En cualquier caso, siempre _devuelve_ los valores _mínimo_ y _máximo_ del _eje Y_ de la gráfica.
#    
#  * ```python
#    pyplot.vlines(x, ymin, ymax, colors='k', linestyles='solid', label='', zorder=2)
#    ```
#    Dibuja una línea vertical en la *posición x* indicada del _eje X_, que va de la altura *ymin* a la *ymax* del _eje Y_.  
#    El resto de parámetros son equivalentes a los de la función `pyplot.plot( )` con el mismo nombre.
#    

# %% jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 "Dataset" 2 como un gráfico de dispersión:
plt.plot(data2_x, data2_y, 'bo', label='Dataset 2', zorder=1)

# Línea horizontal (`hlines( )`) para representar el valor médio de los valores y:
xmin, xmax = plt.xlim() # Obtine los valores máximo y mínimo del eje X.
plt.hlines(np.mean(data2_y), xmin, xmax, 'darkred', 'dashed', label='y-mean', zorder=2)

# Línea vertical (`vlines( )`) para representar el valor medio de los valores x:
ymin, ymax = plt.ylim() # Obtiene los valores máximo y mínimo del eje Y.
plt.vlines(np.mean(data2_x), ymin, ymax, 'r', '-.', label='x-mean', zorder=2)

# Completamos la gráfica:
plt.grid(True)
plt.legend()
plt.title('Graphic with H. lines and V. lines')
plt.xlabel('x  values')
_ = plt.ylabel('y  values')

# %% [markdown]
# <br />
#
# Por último, si deseamos **guardar la gráfica en un _fichero_**, deberemos usar la _función_ `savefig()`:
# ```python
#     pyplot.savefig(file_name)
# ```
# El _formato_ del fichero en que se guardará la figura dependerá de la _extensión_ del nombre de archivo que pasemos a la función como argumento del parámetro *file_name*: 
#   * Algunas extensiones para formatos de [imágenes vectoriales](https://en.wikipedia.org/wiki/Vector_graphics) son `'.svg'`, `'.pdf'`, `'.ps'` o `'.eps'`.
#   * Algunas extensiones para formatos de [imágenes de mapas de bits](https://en.wikipedia.org/wiki/Raster_graphics) son `'.jpg'`, `'.png'` o `'.tiff'`.

# %%
plt.plot(data1_x, data1_y, 'b:.', label='2x^2 - 20')

plt.grid(True)
plt.legend()
plt.title('Test Plot to PDF')
plt.xlabel('x  values')
plt.ylabel('y  values')

# Guardamos la gráfica como un archivo .PDF:
plt.savefig('test_plot.pdf')

# %% [markdown]
# Puedes comprobar como en la carpeta dónde se encuentra este _notebook_ ha aparecido un nuevo archivo llamado [test_plot.pdf](test_plot.pdf "Ver archivo 'test_plot.pdf'") que contiene la gráfica anterior.

# %% [markdown] toc-hr-collapsed=true
# ## Inspección gráfica de los datos de masas

# %% [markdown]
# ### - Gráficas de Dispersión:
#
# Empezaremos por representar los datos de masas que obtuvimos previamente de las proteínas mediante una **gráfica de _dispersión_** (recurriendo a la función `pyplot.plot()` para dibujar los _valores de masa_ como _puntos_, _sin líneas_ que los unan):

# %%
# Dibujar los valores de masas como puntos/marcadores (gráfica de dispersión):
plt.plot(protmasses, '.', label='Masses')   # ➞ nada por color=None, nada por linestyle='', '.' por marker='.'

# Completamos la gráfica:
plt.grid(True, axis='y')      # Sólo muestra las líneas x de la rejilla.
plt.legend()
plt.title('All Protein Masses')
plt.xlabel('Index/position')
_ = plt.ylabel('Protein Mass (monoisotopic)')

# %% [markdown]
# ### - Histogramas:
#
# Otra de las funciones del módulo _pyplot_ que más se utilizan para la inspección de un conjunto de datos es `hist()`, ya que permite obtener un [**_histograma_**](https://en.wikipedia.org/wiki/Histogram) de los datos analizados, permitiéndonos observar como se _distribuyen_ los elementos de una muestra según intervalos de los valores de una variable cuantitativa:
#
# ```python
# pyplot.hist(x, bins=None, range=None, histtype='bar', orientation='vertical', 
#             color=None, edgecolor=color, label=None, zorder=1 )
# ```

# %% [markdown] jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 
#  * *x* ha de ser una secuencia o matriz con los valores a partir de los cuales se quiere generar el histograma.
#  * *bins* permite determinar el número de _intervalos_ o "compartimentos" en que se dividirán los valores que hemos pasado; y por tanto indica el número de barras o escalones del histograma.  
#    _Ejemplo_: si hemos pasado valores que están en un rango [1, 40] y *bins* vale 4, el rango de valores se dividirá en 4 _intervalos_ de 10 (40/4) de la siguiente manera: [1, 10), [10, 20), [20, 30), [30, 40].  
#    Si no se especifica, o es igual a `'auto'` o `None`, el valor de *bins* será determinado automáticamente por la función.
#  * *range* permite definir el rango de valores de <em>x</em> a los que debe restringirse la función para calcular los elementos de cada *bin* y representar el histograma. Ha de ser una tupla que contenga el margen inferior y el superior del rango que se desea: `(min_value, max_value)`.  
#    Si no se especifica (o su valor es `None`), el rango de valores irá del valor mínimo de *x* al valor máximo de _x_.
#  * *histtype* permite indicar el _estilo_ de histograma dibujado. Ha de ser una cadena de caracteres: `'bar'`  para un histogramas de _barras_ tradicional; `'barstacked'`  para un histogramas de _barras apiladas_ cuando <em>x</em> contiene varias series de datos; `'step'` para una _línea_ que representa los "escalones" del histograma; o `'stepfilled'` para una _línea_ rellena de *color* que representa los "escalones" del histograma.  
#    Si no se especifica, su valor _por defecto_ es `'bar'`.
#  * *orientation* permite especificar la _orientación_ (`'vertical'` u `'horizontal'`) del histograma.  
#    Si no se especifica, su valor _por defecto_ es `'vertical`'.
#  * *color* define el _color_ que se debe utilizar para el relleno del histograma. Su valor ha de ser alguno de los valores _admitidos_ para el parámetro *color* de la función `pyplot.plot( )` (ver más arriba).
#  * *edgecolor* (o *ec*) permite definir el _color_ de la línea de _borde_ de las barras del histograma "de barras" (*histtype* igual a `'bar'` o `'barstacked'`), o de la línea de un histograma "escalonado" (*histtype* igual a `'step'` o `'stepfilled'`).  
#    Si no se especifica, su valor _por defecto_ es igual al del parámetro *color*.
#  * *label* indica el _nombre_ o _etiqueta_ de la serie de datos en la _leyenda_ de la gráfica.  
#    Si no se especifica (o su valor es `None`) la serie de datos no aparecerá en la _leyenda_.
#  * *zorder* ha de ser un valor numérico que indique la "profundidad" del histograma dibujado respecto a otros elementos de la gráfica.  
#    Si no se especifica ningún argumento para *zorder*, el valor _por defecto_ es `1`, de manera que las líneas de la rejilla se superondrán al histograma.

# %%
# Histograma que cubre todo el rango de valores de las masas de las proteínas
# en 50 intervalos (50 bins ➞ barras)
plt.hist(protmasses, bins=50, edgecolor='darkblue', label='Masses', zorder=2)

# Completamos la gráfica:
plt.grid(True)
plt.legend()
# plt.xticks(rotation=45)
plt.title('Protein Mass Frequencies')
plt.xlabel('Protein Mass (monoisotopic)')
_ = plt.ylabel('Number of Proteins')

# %% [markdown]
# Tal como podemos observar en el histograma, la mayoría de las proteínas de nuestro archivo FASTA poseen masas entre 0 y 500000 Da.
#
# Para _observar mejor_ cómo se distribuyen las proteínas en esta región de masas, haremos un nuevo histograma restringido a dicha zona (un "zoom" en el *rango* de masas elegido), utilizando para ello el parámetro *range* de la función `hist( )`:

# %%
# Usamos el argumento `range` para indicar el rango de valores del eje X (masas) que queremos
plt.hist(protmasses, bins=50, range=(0, 500000), edgecolor='darkblue', label='Masses', zorder=2)

# Completamos la gráfica:
plt.grid(True)
plt.legend()
plt.title('Protein Mass Frequencies (0, 500.000 Da)')
plt.xlabel('Protein Mass (monoisotopic)')
_ = plt.ylabel('Number of Proteins')

# %% [markdown]
# ### - Diagramas de Caja y Bigotes (_BoxPlot_):
#
# Otra forma de observar la _distribución_ de valores es mediante un [_**boxplot**_](https://en.wikipedia.org/wiki/Box_plot) (o _**diagrama de caja y bigotes**_), que podemos obtener utilizando la función `boxplot()` :
# ```python
# pyplot.boxplot(x, sym='o', vert=True, showmeans=False, meanline=False, labels=None)
# ```

# %% [markdown] jupyter={"source_hidden": true} tags=["sabermas"]
# 📌
#  * *x* ha de ser una secuencia o matriz con los valores a partir de los cuales se quiere generar el _boxplot_.
#  * *sym* permite definir el tipo de _símbolos_ a utilizar para representar los _**valores atípicos**_ (_**outliers**_): valores que están más allá de los límites marcados por los _bigotes_.  
#    Ha de ser cualquiera de los caracteres válidos para el parámetro *marker* de la función `pyplot.plot( )`: `'o'` ➞ _circulo_; `'.'` ➞ _circulo pequeño_; `'v'`, `'^'`, `'<'` ó `'>'` ➞ _triángulos_; `'s'` ➞ _cuadrado_; `'p'` ➞ _pentagono_; `'h'` ó `'H'` ➞ _hexágonos_; `'*'` ➞ _estrella_; `'+'` ➞ _más_; `'x'` ➞ _equis_; `'D'` ó `'d'` ➞ _rombos_; `' '` ó `''` ➞ _no_ dibujar _outliers_; ...  
#    Adicionalmente (y a diferencia del parámetro *marker* de `pyplot.plot( )`), se puede _anteponer_ una _inicial de color_ que determine el color del símbolo. Ejemplo: `'rd'` ➞ _outliers_ como _rombos_ de color _rojo_.  
#    Si no se especifica (o su valor es `None`), _por defecto_ se dibujaran los _outliers_ como círculos (`'o'`) de borde negro.
#  * *vert* permite indicar si la _orientación_ del _boxplot_ será o no vertical: `True` ➞ vertical; `False` ➞ horizontal.  
#    Si no se especifica, su valor _por defecto_ es `True` (vertical).
#  * *showmeans* permite determinar si se ha de mostrar (`True`) o no (`False`) la media de cada _boxplot_.  
#    Si no se especifica, por defecto su valor es `False` (no se muestran las medias).
#  * *meanline* permite indicar si la media de cada _boxplot_ se representará como una línea (`True`) o como un símbolo (`False`).  
#    Si no se especifica, su valor _por defecto_ es `False` (representar la media como símbolo).
#  * *labels* ha de ser una _serie_ que contenga los _nombres_ (o _etiquetas_) de cada _boxplot_ de la gráfica.  
#    Si no se especifica (o su valor es `None`), _por defecto_ cada _boxplot_ será identificado por un número.

# %%
# Boxplot horizontal de las masas de las proteínas:
plt.boxplot(protmasses, vert=False, labels=['Proteins by Mass'])

# Completamos la gráfica:
plt.grid(True, axis='x')
plt.title('BoxPlot of Protein Masses')
_ = plt.xlabel('Protein Mass (monoisotopic, u.m.a.)')

# %% [markdown]
# Dado que hay muchas proteínas con masas _atípicas_ (_outliers_), no podemos ver bien el _boxplot_, por lo que pasaremos a representarlo _sin outliers_ (`sym=''`):

# %%
# Usamos el argumento `sym` para eliminar los outliers:
plt.boxplot(protmasses, sym='', vert=False,    # sym='' ➞ sin outliers
            showmeans=True, meanline=True,     # Muestra la media (línea verde discontinua)
            labels=['Proteins by Mass'])

# Completamos la gráfica:
plt.grid(True, axis='x')
plt.title('BoxPlot of Protein Masses (no outliers)')
_ = plt.xlabel('Protein Mass (monoisotopic)')

# %% [markdown]
# ## Inspección gráfica de los datos de longitudes

# %% [markdown] tags=["ejercicio"]
# > 📝 **Práctica:**
# >
# > Aprovecha todo lo aprendido sobre gráficas con _matplotlib_ para representar los datos de **longitudes** que obtuvimos previamente de las proteínas (variable `protlengths`):  
# >  * Haz una **gráfica de _dispersión_** (recurriendo a la función `pyplot.plot()` para dibujar los valores de longitud como puntos). 
# >  * Muestra uno o varios _**histogramas**_ (función `pyplot.hist()`, haciendo uso del parámetro *range* si fuera necesario).
# >  * Y dibuja uno o varios _**boxplots**_ (función `pyplot.boxplot()`, usando el parámetro *sym* si fuera necesarios para ocultar los posibles _outliers_).

# %% tags=["ejercicio"]
# Gráfica de dispersión de los valores de longitud como puntos/marcadores:



# %% tags=["solucion", "ejercicio"] jupyter={"source_hidden": true}
# 📎 Solución: Gráfica de dispersión de los valores de longitud como puntos/marcadores:

plt.plot(protlengths, '.', label='Lengths') # '.' ➞ color=None, linestyle='', marker='.'

# Completamos la gráfica:
plt.grid(True, axis='y') # Sólo muestra las líneas x de la rejilla.
plt.legend()
plt.title('All Protein Lengths')
plt.xlabel('Index/position')
_ = plt.ylabel('Protein Length (number of AAs)')

# %% tags=["solucion"]
# Histograma de las longitudes de las proteínas:



# %% tags=["solucion", "ejercicio"] jupyter={"source_hidden": true}
# 📎 Solución: Histograma de las longitudes de las proteínas:

# - Prueba a 50 bins (compartimentos ➞ barras) en todo el rango de valores:
plt.hist(protlengths, bins=50, edgecolor='darkblue', label='Lengths')

# Completamos la gráfica:
plt.grid(True)
plt.legend()
plt.title('Protein Length Frequencies')
plt.xlabel('Protein Length (number of AAs)')
_ = plt.ylabel('Number of Proteins')

# %% tags=["solucion", "ejercicio"] jupyter={"source_hidden": true}
# 📎 Solución: Histograma de las longitudes de las proteínas:

# - Usamos el parámetro `range` para pasar el rango de valores del eje X (masas)
#   sobre los que queremos generar el histograma:
plt.hist(protlengths, bins=50, range=(0, 4500), edgecolor='darkblue', label='Lengths')

# Completamos la gráfica:
plt.grid(True)
plt.legend()
plt.title('Protein Length Frequencies (0, 4500 AAs)')
plt.xlabel('Protein Length (number of AAs)')
_ = plt.ylabel('Number of Proteins')

# %% tags=["ejercicio"]
# Boxplot vertical de las longitudes de las proteínas:



# %% jupyter={"source_hidden": true} tags=["solucion", "ejercicio"]
# 📎 Solución: Boxplot vertical de las longitudes de las proteínas:

# - Con outliers:
plt.boxplot(protlengths, sym='r.', vert=False, labels=['Proteins by Length'])

# Completamos la gráfica:
plt.grid(True, axis='x')
plt.title('BoxPlot of Protein Lengths')
_ = plt.xlabel('Protein Length (number of AAs)')

# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución: Boxplot vertical de las longitudes de las proteínas:

# - Sin outliers:
plt.boxplot(protlengths, sym='', vert=False, # sym='' ➞ sin outliers
            showmeans=True, meanline=True,  # Muestra la media y lo hace en forma de línea
            labels=['Proteins by Length'])

plt.grid(True, axis='x')
plt.title('BoxPlot of Protein Lengths (no outliers)')
_ = plt.xlabel('Protein Length (number of AAs)')

# %% [markdown] toc-hr-collapsed=true
# ## Inspección gráfica de la posible relación masa - longitud
#
# Antes de nada, echemos un vistazo muy por encima a los datos que queremos "comparar":

# %%
print("Masas en la lista `protmasses`: ", len(protmasses) )
print("Longitudes en `protlengths`   : ", len(protlengths) )

# %% [markdown]
# Ambas listas no son de la misma longitud. Esto es debido a que al calcular las _masas_, se descartaron las proteínas con _aminoácidos ambiguos_. Esto hace que muchos de los elementos de stas listas estén "desfasados"/"desemparejados" (la masa y la longitud para una misma posición o *index* en las dos listas no corresponden a la misma proteína). 
#
# Por este motivo, para poder graficar las _masas_ de las proteínas _en función_ de sus _longitudes_, tendremos que **rehacer** las listas de masas (`protmasses`) y longitudes (`protlengths`):

# %%
# Cálculo conjunto de las masas y de las longitudes (número de AAs), sólo de aquellas 
# proteínas de las cuales podamos calcular su masa (sin AAs ambiguos) mediante la función
#`SeqUtils.molecular_weight( )`:
from Bio.SeqUtils import molecular_weight

prot_masses = []    # Nueva lista de masas
prot_lenghts = []   # Nueva lista de longitudes

for record in ac2seqrecord.values():
    try:
        monoiso_mass = molecular_weight(record.seq, monoisotopic=True)
    except ValueError as error: # Si se produce un error/excepción de tipo ValueError:
         continue       # No tenemos ningún valor de masa ➞ Pasamos al siguiente, sin añadir a las listas ni masa ni longitud
    # Si no se ha producido ningún error al calcular la masa, añadimos 
    # masa y longitud a sus correspondientes listas, en la misma posición:
    prot_masses.append(monoiso_mass)
    prot_lenghts.append(len(record.seq))

print('Masas en la nueva lista `prot_masses`      :', len(prot_masses))
print('Longitudes en la nueva lista `prot_lengths`:', len(prot_lenghts))

# %% [markdown]
# <br/>
#
# Ahora ya podemos realizar la **gráfica de _dispersión_** de la _masa_ (`prot_masses`) en función de la _longitud_ (`prot_lengths`), recurriendo a la función `pyplot.plot()` para dibujar los _valores_ como _puntos_ (`'.'`), _sin líneas_ que los unan:

# %%
# Gráfica de dispersión de la masa en función de la longitud:
plt.plot(prot_lenghts, prot_masses, '.')

# Completamos la gráfica:
plt.grid(True)
plt.title('Mass ~ Length')
plt.xlabel('Protein Length (number of AAs)')
_ = plt.ylabel('Protein Mass (monoisotopic)')

# %% [markdown]
# Tal como era de esperar, existe una _clara relación lineal directa_ entre la longitud y la masa de las proteínas (a mayor longitud de la proteína, mayor será su masa).

# %% [markdown] jupyter={"source_hidden": true} tags=["sabermas"]
# ### 📌 Regresión lineal masa - longitud
#
# <img src="images/scipy/scipy_logo.png" width="65" style="float: left; margin-right: 10px;" />
#
# Usaremos la función `linregress( )` (ver [aquí](https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.linregress.html "Documentación de 'linregress( )'") la documentación) del sub-paquete _estadístico_ `stats` de la librería de _cálculo científico_ [_**SciPy**_](https://docs.scipy.org/doc/scipy/reference/ "Documentación de SciPy"):

# %% jupyter={"source_hidden": true} tags=["sabermas"]
from scipy.stats import linregress # Importamos `linregress( )` de `scipy.stats`

# Cálculo del modelo de regresión lineal, utilizando la función `linregress( )`:
linreg = linregress(prot_lenghts, prot_masses)

print(linreg)

fit_func_str = "y = " + str(round(linreg.slope, 2)) + "x" + " + " + str(round(linreg.intercept, 2))
print("\nLínea de regresión lineal : " + fit_func_str)
print("R² :", linreg.rvalue ** 2)


# %% tags=["sabermas"] jupyter={"source_hidden": true}
# Función a partir del modelo de regresión lineal obtenido (`linreg`):
def fit_func(length_or_lengths):
    """y = bx + a  ➞  mass = slope * length + intercept"""
    return linreg.slope * np.array(length_or_lengths) + linreg.intercept


# %% tags=["sabermas"] jupyter={"source_hidden": true}
# Gráfica de dispersión de las masa en función de la longitud:
plt.plot(prot_lenghts, prot_masses, '.', label='Original values')

# Línea de regresión:
plt.plot(prot_lenghts, fit_func(prot_lenghts), 'r--', 
         alpha=0.4,     # Un 40% de opacidad (o 60% de transparencia)
         label="Fitted line (" + fit_func_str + ")")

# Completamos la gráfica:
plt.grid(True)
plt.legend()
plt.title('Mass-Length')
plt.xlabel('Protein Length (number of AA)')
_ = plt.ylabel('Protein Mass (monoisotopic)')

# %% [markdown] toc-hr-collapsed=false
# # V.- Filtrado de las proteínas

# %% [markdown] toc-hr-collapsed=true
# ## Filtrado de Isoformas
#
# La _eliminación_ de _isoformas_ es uno de los filtrados más típicos; y además es muy sencillo de hacer a partir del diccionario `ac2seqrecord` (_AC ➞ SeqRecord_) que ya tenemos, pues sólo hemos de quedarnos con aquellos registros asociados a un _ACcession number_ que no tenga notación de isoforma (sin '-numero'):

# %%
# Filtrado de Isoformas:

# Nuevo diccionario sin isoformas (AC no isoforma ➞ SeqRecord).
noisoac2seqrecord = dict() 

for ac, seqrecord in ac2seqrecord.items():
    if '-' not in ac:                       # Si el ACcession number no tiene el separador del número de isoforma:
        noisoac2seqrecord[ac] = seqrecord   # Añadir al diccionario sin isoformas.

# Información del resultado:
print("Entradas Con Isoformas:",  len(ac2seqrecord) )
print("Entradas Sin Isoformas:",  len(noisoac2seqrecord) )

# %% [markdown]
# #### Escritura en disco del nuevo fichero FASTA filtrado
#
# Una vez nos hemos quedado sólo con aquellas proteínas cuyo _Accession Number_ no tiene notación de isoforma, guardaremos los registros seleccionados como un nuevo archivo FASTA para usarlo en los motores de búsqueda, ...  
# Para la escritura de los registros (objetos **SeqRecord**) en un nuevo _fichero_ con _formato_ FASTA utilizaremos la función `write()`, del sub-paquete *Bio.SeqIO*:
# ```python
#     SeqIO.write(sequences, handle, format)  ➞ number_of_saved_records
# ```

# %% [markdown]
#  * *sequences* : ha de ser una _secuencia_ o un _iterador_ de tipo **SeqRecord** (los _registros_ a guardar).
#  * *handle* : suele ser una _cadena de caracteres_ indicando la localización y nombre de fichero en el que se guardarán los _registros_.
#  * *format* : _Formato_ del fichero, que en el caso concreto de los ficheros FASTA debe ser `'fasta'`.

# %%
# Localización y nombre del fichero FASTA sin isoformas:
NOISOFORMS_FASTA_FILE = 'data/noisoforms-uniprot-reviewed-Human-9606.fasta'

# %%
# Guardamos los registros SeqRecord que han superado el filtro en un archivo, en formato FASTA:
SeqIO.write(noisoac2seqrecord.values(), NOISOFORMS_FASTA_FILE, 'fasta')

# %% [markdown]
# Puedes comprobar como en la sub-carpeta `data/` hay un nuevo archivo llamado [noisoforms-uniprot-reviewed-Human-9606.fasta](data/noisoforms-uniprot-reviewed-Human-9606.fasta "data/noisoforms-uniprot-reviewed-Human-9606.fasta") que contiene únicamente las variantes _sin notación de isoforma_ de las proteínas del archivo original ([data/qualitative/uniprot-reviewed-Human-9606-w-isoforms.fasta](data/qualitative/uniprot-reviewed-Human-9606-w-isoforms.fasta "data/qualitative/uniprot-reviewed-Human-9606-w-isoforms.fasta")).

# %% [markdown] jupyter={"source_hidden": true} tags=["sabermas"]
# #### 📌 Análisis de las isoformas por proteínas
#
# Adicionalmente, vamos a crear también un nuevo diccionario que asocie a cada _Accession Number_ sin notación de isoforma todas sus posibles notaciones (con y sin indicador de isoforma) presentes en la FASTA (_AC no isoform ➞ All related ACs_).  
# Este diccionario nos permitirá hacer un sencillo _análisis_ del número de isoformas por proteína:

# %% jupyter={"source_hidden": true} tags=["sabermas"]
# Obtención de las isoformas por cada proteína:

noisoac2allrelacs = dict() # Nuevo diccionario de pares AC no isoform ➞ All related ACs.

for ac in ac2seqrecord.keys():
    noiso_ac = ac.split('-')[0] # Para el AC actual (sea o no isoforma), hallamos su versión sin isoformas
    # Obtenemos el conjunto de todos los ACs (con y sin notación de isoforma) asociado el AC sin 
    # isoforma actual; en caso de no estar éste en el diccionario, obtenemos un conjunto vacío nuevo:
    all_rel_acs = noisoac2allrelacs.get(noiso_ac, set())
    all_rel_acs.add(ac)                       # Añadimos el AC actual (sea o no isoforma) al conjunto.
    noisoac2allrelacs[noiso_ac] = all_rel_acs # Reasignamos/asignamos al AC sin isoforma el conjunto.

# Información del resultado:
print("Entradas Sin Isoformas:",  len(noisoac2allrelacs) ) # Comprobamos la longitud del nuevo diccionario:

# %% jupyter={"source_hidden": true} tags=["sabermas"]
list( noisoac2allrelacs.items() )[:10] # Inspección de los 10 primeros "items" (AC no isoform, All related ACs):

# %% [markdown] tags=["ejercicio", "sabermas"] jupyter={"source_hidden": true}
# > 📝 **Práctica:**
# >
# > Haz un pequeño **análisis descriptivo** del _número de isoformas por proteína_ presente en el archivo FASTA, haciendo uso tanto de las funciones _estadísticas_ que hemos visto como de algunas _gráficas_ (histograma y boxplot):

# %% tags=["ejercicio", "sabermas"] jupyter={"source_hidden": true}
# 1.- Obtener una lista con el número de isoformas para cada proteína:

numisoforms =

# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion", "sabermas"]
# 📎 Solución: 1.- Obtener una lista con el número de isoformas para cada proteína:

numisoforms = list( map(len, noisoac2allrelacs.values()) )

# %% tags=["ejercicio", "sabermas"] jupyter={"source_hidden": true}
# 2.- Obtener algunos valores estadísticos:



# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion", "sabermas"]
# 📎 Solución: 2.- Obtener algunos valores estadísticos:

print('Mínimo ( min( ) )          :', min(numisoforms) )
print('Máximo ( max( ) )          :', max(numisoforms) )
print('Media ( mean( ) )          :', np.mean(numisoforms) )
print('Mediana ( median( ) )      :', np.median(numisoforms) )
print('Moda ( mode( ) )           :', st.mode(numisoforms))
print('Desv. estándar ( std( ) ) :', np.std(numisoforms) )

# %% tags=["ejercicio", "sabermas"] jupyter={"source_hidden": true}
# 3.1- Inspección gráfica de los datos (Histograma):



# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion", "sabermas"]
# 📎 Solución: 3.1- Inspección gráfica de los datos (Histograma):

plt.hist(numisoforms, bins=40, edgecolor='darkblue', label='Isoforms')

# Completamos la gráfica:
plt.grid(True)
plt.legend()
plt.title('Histogram of Protein Isoforms')
plt.ylabel('Number of Proteins')
_ = plt.xlabel('Isoform number')

# %% tags=["ejercicio", "sabermas"] jupyter={"source_hidden": true}
# 3.2- Inspección gráfica de los datos (BoxPlot):



# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion", "sabermas"]
# 📎 Solución: 3.2- Inspección gráfica de los datos (BoxPlot):

plt.boxplot(numisoforms, sym='r.', vert=False, 
                showmeans=True, meanline=True, 
                labels=['Isoforms by Protein'])

# Completamos la gráfica:
plt.grid(True, axis='x')
plt.title('BoxPlot of Protein Isoforms')
_ = plt.xlabel('Isoform number')

# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion", "sabermas"]
# 📎 Extra: como el rango de los valores es pequeño, y las proteínas tienen mayoritariamente 
# pocas isoformas, exploramos los valores numéricos reales:
print("Isoforms", "# Proteins")
for num in range(1, 10):
    print(num, "\t", numisoforms.count(num))

# %% [markdown] toc-hr-collapsed=true
# ## Clasificación de kinasas y fosfatasas
#
# _Clasificaremos_ las proteínas que hemos leído del archivo FASTA en **4 _categorías_**: kinasas (`kinases`), fosfatasas (`phosphatases`), kinasas y fosfatasas dudosas (`uncertain`), y otras (`others`).  
# Para esta clasificación, recurriremos a la presencia o no de determinadas _palabras clave_ (`'kinase'`, `'phosphatase'` y `'substrate'`) en la _descripción_ del registro correspondiente a cada secuencia (`SeqRecord.description`):

# %%
# Clasificación de proteínas en 4 categorías (listas) según palabras clave en su descripción:

kinases = []      # Kinasas: contienen la palabra 'kinase' en su descripción.
phosphatases = [] # Fosfatasas: contienen la palabra 'phosphatase' en su descripción.
uncertain = []    # Kinasas y fosfatasas dudosas: contienen la palabra 'kinase' o ' phosphatase', pero también 'substrate' en su descripción.
others = []       # Otras proteínas: no contienen ni 'kinase' ni 'phosphatase' en su descripción.

for record in ac2seqrecord.values():
    entry_name = record.id.split('|')[2]     # Entry Name ('NAME_ORGANISM')
    description = record.description.lower() # La descripción del registro, en minúsculas(!)
    # Clasificación:
    if 'kinase' in description:
        if 'substrate' in description:
            uncertain.append(entry_name)   # Añadir a Dudosas
            continue
        kinases.append(entry_name)         # Añadir a Kinasas
    elif 'phosphatase' in description:
        if 'substrate' in description:
            uncertain.append(entry_name)   # Añadir a Dudosas
            continue
        phosphatases.append(entry_name)    # Añadir a Fosfatasas
    else:
        others.append(entry_name)          # Añadir a Otras

# Información del resultado de la clasificación:
print('Frecuencias Absolutas:\n')
print("Kinasas   :", len(kinases))
print("Fosfatasas:", len(phosphatases))
print("Dudosas   :", len(uncertain))
print("Otras     :", len(others))

# %% [markdown]
# <br/>
#
# _Peeeero_.... :

# %%
kinases.sort()  # Ordenamos la lista alfabéticamente "in-situ" (método `.sort()` de las listas)
kinases[:20]    # Visualización de los 20 primeros Entry Name de kinasas  ➞  Hay repeticiones!!

# %% [markdown]
# Como se observan **repeticiones** del _Entry Name_ que hemos almacenado para cada clase (kinasas, fosfatasas, ...), lo que toca hacer es _eliminar estas repeticiones_, y quedarnos sólo con los _valores únicos_.  
# Para ello utilizaremos los **set** (que, tal como [ya hemos visto](1%20Introduccion%20a%20Python.ipynb#Conjuntos-(-set%2C-%7B-%7D-)), son colecciones de objetos _únicos_):

# %%
# Conversión de la lista en set: sólo se conservan los Entry Name únicos:
uniq_kinases = set(kinases) 
uniq_phosphatases = set(phosphatases)
uniq_uncertain = set(uncertain)
uniq_others = set(others)

print('Kinasas diferentes:', len(uniq_kinases))
sorted(uniq_kinases)[:20]      # Visualización de 20 Entry Name de kinasas ➞ No hay repeticiones.

# %% [markdown]
# <br />
#
# Una vez eliminadas las repeticiones del _Entry Name_, volvemos a calcular las _frecuencias absolutas_ (el número de proteínas) en cada _categoría_ (en cada _set_):

# %%
# Cálculo del número de kinasas, fosfatasas, dudosas, y otras, únicas:

kinase_n = len(uniq_kinases)
phosphatase_n = len(uniq_phosphatases)
uncertain_n = len(uniq_uncertain)
other_n = len(uniq_others)

# Información de los resultados:
print('Frecuencias Absolutas:\n')
print("Kinasas   :", kinase_n)
print("Fosfatasas:", phosphatase_n)
print("Dudosas   :", uncertain_n)
print("Otras     :", other_n)

# %% [markdown]
# ### - Gráficas de Barras:
#
# Estos valores se pueden representar utilizando una **gráfica de _barras_** ([_bar chart_](https://en.wikipedia.org/wiki/Bar_chart)).  
# Para ello podemos usar las funciones `bar()` o `barh()` del modulo *pyplot* de *matplotlib*, según si queremos que las barras sean _verticales_ u _horizontales_, respectivamente:
#
# ```python
# pyplot.bar(x, height, log=False, color=None, edgecolor=color, zorder=1)
# pyplot.barh(y, width, log=False, color=None, edgecolor=color, zorder=1)
# ```

# %% [markdown] jupyter={"source_hidden": true} tags=["sabermas"]
# 📌
#  * *x* (`bar`) ó <em>y</em> (`barh`) ha de ser, o bien una secuencia o matriz con las _posiciones_ de cada una de las barras sobre el eje X (`bar`) o Y (`barh`), o bien una secuencia o matriz con los _nombres_ o _categorías_ que aparecerán como _etiquetas_ bajo/al lado de las barras de la gráfica.
#  * *height* (`bar`) o *witdh* (`barh`) ha de ser una secuencia o matriz con los valores de cada una de las barras (_altura_ (`bar`) o _longitud_ (`barh`)).
#  * *log* permite determinar si se ha de usar (`True`) o no (`False`) una _escala logarítmica_ para representar los valores de las barras.  
#    Si no se especifica, su valor por defecto es `False` (escala normal, _no_ logarítmica).
#  * *color* define el _color_ que se debe utilizar para el _relleno_ de las barras. Su valor ha de ser alguno de los valores *permitidos* para el parámetro *color* de la función `pyplot.plot( )` (ver más arriba). Puede ser _un único valor_ de color para todas las barras, o una _secuencia de colores_, en cuyo caso cada uno de los colores de la secuencia definirá el color de una barra (según sus indices).
#  * *edgecolor* (o *ec*) define el _color_ de la línea de _borde_ de las barras. Puede ser _un único valor_ de color para todos los bordes de las barras, o una _secuencia de colores_, en cuyo caso cada uno de los colores de la secuencia definirá el color del borde de una barra (según sus indices).  
#    Si no se especifica, su valor por defecto es igual al del parámetro *color*.
#  * *zorder* ha de ser un valor numérico que determine la "profundidad" del conjunto de barras dibujado respecto a otros elementos de la gráfica.  
#    Si no se especifica ningún valor para *zorder*, el valor por defecto es 1, de manera que las líneas de la rejilla (si ésta se dibuja) se superpondrían a las barras de la gráfica.

# %%
# Gráfica de barras vertical simple:

plt.bar(['Kinases', 'Phosphatases', 'Uncertain', 'Others'],   # Posiciones/categorías sobre el eje X
        [ kinase_n,  phosphatase_n, uncertain_n,  other_n],   # Valores (frecuencias) para cada barra en el eje vertical
        zorder=2)

# Completamos la gráfica:
plt.grid(True, axis='y')
plt.title('Vertical Bar Chart')
_ = plt.ylabel('Number of Proteins')

# %% jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 Gráfica de barras horizontal, con escala logarítmica y "colorines":

plt.barh(['Kinases', 'Phosphatases', 'Uncertain', 'Others'], # Posiciones/categorías sobre el eje Y
         [ kinase_n,  phosphatase_n, uncertain_n,  other_n], # Valores (frecuencias absolutas) para cada barra en el eje horizontal
         log=True,                                           # log=True ➞ Escala logarítmica
         color=['blue', 'orange', 'grey', 'green'],          # Lista de colores para cada barra
         edgecolor='k',                                      # Color de la línea del borde para todas las barras ('k' ➞ black)
         zorder=2)

# Completamos la gráfica:
plt.grid(True, axis='x')
plt.title('Horizontal Logarithmic Bar Chart')
_ = plt.xlabel('Number of Proteins (log)')

# %% [markdown]
# ### - Gráficas de Pastel:
#
# Otra forma de representar gráficamente estos valores es utilizando una **gráfica de _pastel_** ([_pie chart_](https://en.wikipedia.org/wiki/Pie_chart)).  
# Para ello utilizaremos la función `pie()` del módulo *pyplot* de *matplotlib*:
# ```python
# pyplot.pie(x, colors=None, labels=None, autopct=None, radius=1)
# ```

# %% [markdown] jupyter={"source_hidden": true} tags=["sabermas"]
# 📌
#  * *x* ha de ser una secuencia o matriz con los valores de cada uno de los sectores en que se dividirá el círculo de la gráfica.
#  * *colors*, si se pasa como argumento, ha de ser una _secuencia de colores_ dónde cada uno de los colores de la secuencia definirá el color de _relleno_ de cada uno de los sectores (según sus indices). El valor de cada color de la secuencia ha de ser alguno de los valores *permitidos* para el parámetro *color* de la función `pyplot.plot( )` (ver más arriba).  
#    Si no se especifica (o su valor es `None`), a cada sector se le asignará un color automáticamente. 
#  * *labels* ha de ser una secuencia con los _nombres_ o _etiquetas_ que aparecerán al lado de cada uno de los sectores de la gráfica circular (según sus indices).  
#    Si no se especifica (o su valor es `None`), no aparecerá ningún nombre al lado de cada sector.
#  * *autopct* ha de ser una cadena de caracteres que determine cómo dar formato al valor de _porcentaje_ calculado automáticamente para cada sector, de manera que pueda aparecer superpuesto a éste.  
#    Si no se especifica ningún formato (o su valor es `''` o `None`), no apareceran los valores de porcentaje de cada sector.
#  * *radius* ha de ser un valor numérico que indique la longitud del _radio_ del círculo de la gráfica.  
#    Si no se especifica (o su valor es `None`), el valor por defecto es 1.

# %%
# Gráfica de pastel:

_ = plt.pie([kinase_n, phosphatase_n, uncertain_n + other_n],         # Valores de cada uno de los sectores
            labels=['Kinases', 'Phosphatases', 'Uncertain + Others'], # Etiquetas de los sectores
            autopct="%4.1f %%", # Formato del %: 4 dígitos máximo, de los cuales 1 será un decimal + %
            radius=1.4)

# %% [markdown] toc-hr-collapsed=false
# # VI.- Blastp
#
# En este apartado veremos como utilizar _Biopython_ (concretamente módulos del sub-paquete _Bio.Blast_) para hacer búsquedas de secuencias de proteínas con [BLAST](https://en.wikipedia.org/wiki/BLAST_(biotechnology)).
#
#
#
#



# %% [markdown] toc-hr-collapsed=false
# ## Realizando la búsqueda

# %% [markdown]
# ### Blastp _on-line_
#
# La búsqueda se realiza en servidores BLAST disponibles en Internet (por defecto en los del [NCBI](https://www.ncbi.nlm.nih.gov/)) o en nuestra red local.  
# Se utiliza la función `qblast( )` del módulo *NCBIWWW* del sub-paquete *Bio.Blast*:

# %%
# Importamos el módulo `NCBIWWW` del sub-paquete `Blast` de Biopython
from Bio.Blast import NCBIWWW

# %% [markdown]
# Para poder hacer búsquedas BLAST _on-line_, la función `NCBIWWW.qblast()` requiere como _mínimo_ 3 argumentos:
# ```python
#     NCBIWWW.qblast(program, database, sequence[, ...])  ➞ input_output_object
# ```
#
#  * *program* será el tipo de programa blast que queremos utilizar (`'blastn'` para nucleótidos, `'blastp'` para proteínas, ...).
#  * *database* ha de indicar la base de datos a utilizar de entre las disponibles en la [web del NCBI BLAST](https://blast.ncbi.nlm.nih.gov/Blast.cgi):     
#  
#     `'nr'` para las proteínas no redundantes de varios repositorios de proteínas (GenBank, SwissProt, PDB, PIR, ...).       
#     `'swissprot'` para la base de datos no redundante de UniProtKB/SwissProt.   
#    `'refseq_protein'` para las proteínas de referencia del NCBI.   
#     [**...**](https://blast.ncbi.nlm.nih.gov/Blast.cgi?PROGRAM=blastp&PAGE_TYPE=BlastSearch#dblist)
#  * *sequence* debe ser una _cadena de caracteres_ con la secuencia(s) que queremos buscar (en formato FASTA, y sin líneas en blanco entre diferentes secuencias, si queremos indicar más de una secuencia a buscar).
#  
# Al llamar a la función `NCBIWWW.qblast( )`, se _enviará_ la petición de búsqueda a los servidores del NCBI, en los que se realizará la búsqueda.  
# La función no retornará hasta que los servidores del NCBI no completen la búsqueda y envíen la respuesta. Y cuándo retorne, devolverá un objeto de tipo _input/output_ (entrada/salida o _io_) que nos permitirá recuperar el resultado de la búsqueda, resultado que por defecto estará en formato [_XML_](https://en.wikipedia.org/wiki/XML).

# %% [markdown] jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 Otros parámetros interesantes de la función `qblast( )` son:
#
#  * *descriptions* : número máximo de descripciones a recuperar. Por defecto `500`.
#  * *alignments* : número máximo de alineamientos a recuperar. Por defecto `500`.
#  * *expect* : permite definir el ["_expectation threshold_"](https://blast.ncbi.nlm.nih.gov/Blast.cgi?CMD=Web&PAGE_TYPE=BlastDocs&DOC_TYPE=BlastHelp#expect), o valor umbral _máximo_ para los _E-value_ de los alineamientos resultantes de la búsqueda. Por defecto `10.0` (lo cual puede resultar demasiado _permisivo_).
#  * *matrix_name* : permite indicar la [matriz de substitución](https://www.ncbi.nlm.nih.gov/books/NBK279684/#_appendices_BLAST_Substitution_Matrices_) a utilizar por el programa (`'PAM30'`, `'PAM70'`, `'BLOSUM80'`, `'BLOSUM45'` o `'BLOSUM62'`). Por defecto `'BLOSUM62'`.
#  * *format_type* : permite definir el formato que tendrá el resultado de la búsqueda (`"HTML"`, `"Text"`, `"ASN.1"` o `"XML"`). Por defecto `"XML"`.
#

# %% [markdown]
# Nosotros enviaremos una búsqueda de una única secuencia de 42 AA de la región N-terminal de la proteína humana P11217 (PYGM_HUMAN, una glicógeno fosforilasa de músculo), contra la _base de datos_ de proteínas de UniProtKB/SwissProt (*database* = `'swissprot'`):

# %%
# Busqueda BLAST on-line sencilla:

# Secuencia peptídica a buscar (`query`) en formato FASTA:
query_seq = """>MySeq01
MSRPLSDQEKRKQISVRGLAGVENVTELKKNFNRHLHFTLVK"""

print("Búsqueda 'blastp' on-line en 'swissprot':\n" + query_seq + "\n\n...(esperando respuesta)...\n")

# Envía la búsqueda a los servidores del NCBI y no retorna hasta obtener respuesta:
ionet = NCBIWWW.qblast(program='blastp', database='swissprot', sequence=query_seq)

print("Respuesta recibida:",  ionet)           # Objeto input/output (io) recibido

# %% [markdown]
# Una vez hemos obtenido el objeto _input/output_, **leeremos** _todo_ el resultado devuelto por el servidor usando para ello el método `.read()` de este objeto ... :

# %%
result_xml = ionet.read()   # Leemos todo el resultado devuelto por el servidor BLAST como objeto io.

print( result_xml[:262] )   # Primeras líneas del resultado, en formato XML:

# %% [markdown]
# ... Y a continuación **guardaremos** este resultado en disco como un _fichero de texto_; utilizando para ello la _función built-in_ `open(filename, mode)` y el _método_ `.write(data)` del nuevo _objeto input/output_ devuelto por ésta, tal como se muestra a continuación:

# %%
# Abrimos un fichero en disco ('blastp_result.xml') utilizando la función `open(filename, mode)`, 
# indicando un modo de escritura de texto (mode='wt' ➞ 'w' por write, 't' por text):
iofile = open('blastp_result.xml', 'wt')    # 👁 Ojo!: Si el fichero ya existe se sobrescribirá!

# Escribimos el texto XML (con el resultado devuelto por el servidor) en el fichero representado 
# por `iofile` (objeto input/output devuelto por `open( )`) usando su método `.write( )`:
iofile.write(result_xml)

# Cerramos el fichero, utilizando el método `.close()` del objeto input/output:
iofile.close()

# %% [markdown]
# Puedes comprobar como en la carpeta dónde se encuentra este notebook ha aparecido un nuevo archivo llamado [blastp_result.xml](blastp_result.xml) que contiene el resultado de la búsqueda _on-line_ en formato XML.

# %% tags=["sabermas", "alternativa"] jupyter={"source_hidden": true}
# 📌🔄 Alternativamente, podemos usar el "context manager" del objeto input/output devuelto 
# por `open( )` de la siguiente manera:

with open('blastp_result.xml', 'wt') as iofile: # Sentencia `with ... as ...` para usar el "context manager"
    iofile.write(result_xml) # Escribimos el texto XML del resultado como antes (método `.write( )`)

# Nos ahorramos así tener que cerrar el fichero (además de parte de la gestión de errores)
# Esta (el uso de "context manager") es la forma recomendada en Python para abrir y operar con archivos en Python

# %% [markdown] jupyter={"source_hidden": true} tags=["sabermas"]
# ### 📌 Blastp local
#
# En este caso, la búsqueda se realiza en nuestro propio ordenador, gracias al programa [NCBI BLAST+](https://blast.ncbi.nlm.nih.gov/Blast.cgi?CMD=Web&PAGE_TYPE=BlastDocs&DOC_TYPE=Download) que debemos haber _instalado y configurado_ previamente (además de haber preparado y formateado adecuadamente los ficheros de bases de datos en que realizar las búsquedas).  
# Como éste _no_ es el caso, las celdas siguientes están sólo a **modo informativo**, y si se ejecutan pueden devolver diversos _errores_.
#
# Se utiliza la clase **NcbiblastpCommandline** del módulo *Applications* del sub-paquete *Bio.Blast*:

# %% jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 Importamos la clase NcbiblastpCommandline del módulo `Bio.Blast.Applications` con el alias `BlastpCmd`:
from Bio.Blast.Applications import NcbiblastpCommandline as BlastpCmd

# %% jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 Creación de un objeto NcbiblastpCommandline con argumentos de ejemplo para los parámetros 
# necesarios para realizar una búsqueda sencilla:
blastp_cmd = BlastpCmd(query='seqs_to_search.fasta', # Fichero con la(s) secuencia(s) a buscar (en formato FASTA)
                       db='local_protein_db', # Base de datos local en la que buscar (y que debe haber sido adecuadamente formateada para su uso con BLAST+)
                       evalue=0.001, # Valor umbral máximo para los E-value de los alineamientos resultantes de la búsqueda (por defecto sería 10, muy permisivo)
                       outfmt=5, # Formato de salida de los resultados de búsqueda (5 ➞ XML)
                       out='blastp_result.xml') # Archivo en el que almacenar los resultados

# %% jupyter={"source_hidden": true} tags=["sabermas", "error"]
# 📌 Llamada al objeto NcbiblastpCommandline (sí, hay objetos que pueden ser llamados 
# como si fueran funciones), para que se ejecute el programa NCBI BLAST+ local 
# (según los parámetros definidos al crear antes el objeto `blast_cmd`), y que 
# éste realice la búsqueda y guarde además los resultados (en forma de fichero XML):
stdout, stderr = blastp_cmd()

# %% [markdown]
# ## Procesando el resultado
#
# Una vez tenemos unos resultados de búsqueda BLAST en formato XML, es momento de _procesarlos_ para obtener de ellos los **alineamientos** generados por BLAST.
#
# Para ello podemos utilizar las funciones del módulo *NCBIXML* del sub-paquete *Bio.Blast* :  
# `read()`, si _sólo_ hemos enviado _una búsqueda_    
# `parse()`, si hemos enviado _más de una_ secuencia para buscar

# %%
# Importamos el módulo `NCBIXML` del sub-paquete `Blast` de Biopython
from Bio.Blast import NCBIXML 

# %% [markdown]
# ```python
#     NCBIXML.read(input_output_object)  ➞ 1 recordblast_object
#     
#     NCBIXML.parse(input_output_object)  ➞ generator_object  ➞➞ recordblast_objects
# ```
#
# Ambas funciones necesitan como argumento un _objeto de tipo input/output_ que represente los resultados de BLAST en _formato XML_.  
# Este objeto _input/output_ puede ser directamente el que devuelve la función `NCBIWWW.qblast( )` vista antes, o un objeto _input/output_ resultado de abrir un fichero (función `open(filename, mode)`) de resultados en formato XML almacenado en disco (`blastp_result.xml` en nuestro ejemplo).
#
# La función `NCBIXML.read( )` devuelve _un único_ objeto de tipo **Record.Blast** (ya que interpreta los resultados para una única secuencia buscada). Mientas que la función `NCBIXML.parse( )` devuelve un objeto de tipo **generator**, que al ser _iterado_ irá devolviendo los _diferentes_ registros de resultados como objetos de tipo **Record.Blast** (uno por cada una de las secuencias buscadas).

# %% [markdown]
# En nuestro ejemplo, hemos buscado _sólo una_ secuencia, por lo que sólo esperamos _un_ único registro con los resultado, y podemos por tanto utilizar la función `NCBIXML.read()` directamente:

# %%
# Abrimos un fichero en disco ('blastp_result.xml') utilizando la función `open(filename, node)`, 
# indicando un modo de lectura de texto (mode='rt' ➞ 'r' por read, 't' por text):
iofile = open('blastp_result.xml', 'rt') # Obtenemos el objeto input/output que necesitamos

# Obtenemos resultados del fichero como un único objeto Record.Blast, utilizando para ello 
# la función `NCBIXML.read( )`:
record_blast = NCBIXML.read(iofile)

iofile.close() # Cerramos el fichero (método .close() del objeto input/output)

record_blast

# %% [markdown] tags=["sabermas"] jupyter={"source_hidden": true}
# 📌 Un objeto de tipo **Record.Blast** contiene diferentes _atributos_ con información sobre la _búsqueda_ realizada, como:  
#  * ```python
#    Blast.application
#    ```
#    El nombre de la _aplicación BLAST_ utilizada para la búsqueda.
#  * ```python
#    Blast.version
#    ```
#    _Versión_ de BLAST con el cual se realizó la búsqueda.
#  * ```python
#    Blast.query
#    ```
#    _Nombre_ dado a la secuencia buscada.
#  * ```python
#    Blast.query_length
#    ```
#    _Longitud_ de la secuencia buscada.
#  * ```python
#    Blast.database
#    ```
#    _Nombre_ de la base de datos en que se hizo la búsqueda BLAST.
#  * ```python
#    Blast.database_sequences
#    ```
#    _Número de secuencias_ de esta base de datos.
#  * ```python
#    Blast.expect
#    ```
#    [_Valor umbral_ máximo](https://blast.ncbi.nlm.nih.gov/Blast.cgi?CMD=Web&PAGE_TYPE=BlastDocs&DOC_TYPE=BlastHelp#expect) usado para los _E-value_ de los alineamientos resultantes de la búsqueda.
#  * ```python
#    Blast.matrix
#    ```
#    La [_matriz de substitución_](https://www.ncbi.nlm.nih.gov/books/NBK279684/#_appendices_BLAST_Substitution_Matrices_) utilizada en la búsqueda).

# %% tags=["sabermas"] jupyter={"source_hidden": true}
# 📌 Algunos atributos del objeto Record.Blast con información sobre la búsqueda realizada:  

attribs = ('application', 'version', 'query', 'query_length', 'database', 'database_sequences', 
           'expect', 'matrix')
for attrib in attribs:
    print("." + attrib + " : ", getattr(record_blast, attrib) )

# %% [markdown]
# De entre los _atributos_, con información sobre la búsqueda, de un objeto **Record.Blast** _destacan_ `.descriptions` y `.alignments`, por ser los atributos que contienen la información sobre los _**resultados**_ de BLAST:  
# * ```python
#   Blast.descriptions  ➞  [ list_of_description_objects ... ]
#   ```
#    Referencía una _lista_ de objetos de tipo **Record.Description**; conteniendo ésta tantos objetos _Description_ como _alineamientos_ BLAST resultantes de la búsqueda:

# %%
print("Resultados encontrados:", len(record_blast.descriptions))

record_blast.descriptions # Lista de objetos de tipo Blast.Record.Description:

# %% [markdown]
# <ul><il>Y cada uno de estos objetos <em>Description</em> contiene <em>atributos</em> con <em>información básica</em> sobre el resultado particular (el <em>alineamiento</em> BLAST particular) al que corresponden. Atributos tales como <code>.accession</code>, <code>.e</code>, <code>.title</code>, ... :</il></ul>

# %%
# Para inspeccionar los atributos y sus valores de uno de estos objetos Description, 
# podemos utilizar la función built-in `vars(object)`, que retorna un diccionario 
# atributo ➞ valor del objeto indicado (object):
vars( record_blast.descriptions[-1] )       # Atributos y valores para el último objeto Description de la lista:

# %%
# Resumen de los resultados de BLAST usando sólo los objetos Description 
# contenidos en la lista referenciada por `record_blast.descriptions`:

print("Hit\t  AC\tEntry Name\t  E-value") # Encabezado.

# Iteramos sobre la lista de objetos Description para obtener información descriptiva
# de cada alineamiento Blast:
for hit, description in enumerate(record_blast.descriptions, start=1): # `enumerate with start=1)` ➞➞ (1, description0), (2, description1), ... :
    prot_ids = description.title.split(' ')[0] # Identificadores varios para la proteína encontrada
    entry_name = prot_ids.split('|')[1]        # Entry Name en SwissProt de la proteína encontrada
    print(hit, description.accession,          # `.accession` ➞ Accession Number
          entry_name, description.e,           # `.e` ➞ E-value (mejor cuanto menor sea su valor)
          sep='\t')                            # sep='\t' ➞ separa elementos por tabuladores en lugar de espacios

# %% [markdown]
# * ```python
#   Blast.alignments  ➞  [ list_of_alignments_objects ... ]
#   ```
#    Referencía una _lista_ de objetos de tipo **Record.Alignment**. Y contendrá tantos objetos *Alignment* como _alineamientos_ BLAST resultantes de la búsqueda:  

# %%
print("Resultados encontrados:", len(record_blast.alignments))

record_blast.alignments

# %% [markdown]
# <ul><il>Y cada uno de estos objetos <em>Alignment</em> contiene <em>atributos</em> en los que está contenida <em>toda la información</em> sobre el resultado particular (el <em>alineamiento</em> BLAST) al que corresponden:</il></ul>

# %%
alignment = record_blast.alignments[-1]    # Variable que apunta al último Alignment de `.alignments`

vars(alignment)    # Diccionario de pares atributo ➞ valor para el objeto Alignment concreto:

# %% [markdown]
# <ul><il>Como podemos ver los atributos <code>.accession</code> y <code>.title</code> de un objeto <em>Alignment</em> contienen la misma información que los correspondientes atributos del objeto <em>Description</em> asociado, y el resto de atributos añade poca información extra (salvo la longitud (<code>.length</code>) de la proteína coincidente encontrada) ... <br />
# Dónde está pues la información que falta sobre cada alineamiento? <br />
# <br />
#     Pues está en los objetos <strong>Record.HSP</strong> asociados a cada objeto <em>Alignment</em>, y contenidos en una lista a la que apunta el atributo <code>.hsps</code> de cada objeto <em>Alignment</em>:</il></ul>
#
# ```python
#         Alignment.hsps  ➞  [ list_of_hsp_objects ... ]
# ```

# %%
# Lista de objetos High-Scoring Pairs. Habitualmente ésta contiene un único objeto HSP:
alignment.hsps 

# %% [markdown]
# <br/>
#
# Los objetos de tipo **Record.HSP** reciben su nombre de [_"High-scoring Segment Pair"_](https://www.ncbi.nlm.nih.gov/books/NBK62051/#hsp "BLAST glossary") : cada uno de los objetos _HSP_ de un objeto _Alignment_ representa una _región de alineamiento significativo entre la secuencia a buscar (_query_) y la secuencia proteica encontrada en el alineamiento en cuestión (_hit sequence_ o _subject_):

# %%
# Acceso al único HSP del alineamiento.
hsp = alignment.hsps[0] 

print(hsp) # Representación por pantalla del sumario de los atributos de un HSP:

# %% [markdown]
# Algunos atributos de un objeto _HSP_ con información de interés:
#  * ```python
#    HSP.query
#    ```
#    _Sub-cadena_ de la _secuencia buscada_ (_query_) que _alinea de manera significativa_ con la secuencia proteica encontrada.
#  * ```python
#    HSP.query_start
#    ```
#    Posición _inicial_ de dicha sub-cadena dentro de la secuencia buscada.
#  * ```python
#    HSP.query_end
#    ```
#    Posición _final_ de dicha sub-cadena dentro de la secuencia buscada.

# %%
hsp.query_start, hsp.query, hsp.query_end

# %% [markdown]
#  * ```python
#    HSP.sbjct
#    ```
#    _Sub-cadena_ de la _secuencia proteica encontrada_ (_subject_) que _alinea de manera significativa_ con la secuencia buscada.
#  * ```python
#    HSP.sbjct_start
#    ```
#    Posición _inicial_ de dicha sub-cadena dentro de la secuencia proteica encontrada.
#  * ```python
#    HSP.sbjct_end
#    ```
#    Posición _final_ de dicha sub-cadena dentro de la secuencia proteica encontrada.

# %%
hsp.sbjct_start, hsp.sbjct, hsp.sbjct_end

# %% [markdown]
#  * ```python
#    HSP.match
#    ```
#    _Cadena de caracteres_ que indica: qué AAs de la _secuencia buscada_ son iguales a los de la _secuencia proteica encontrada_ (identidades: letras de AA), qué AAs son diferentes pero con las mismas características bioquímicas (cambios conservativos: signos `+`), y qué AAs son totalmente diferentes (espacios).
#  * ```python
#    HSP.positives
#    ```
#    Número de AAs de la _secuencia buscada_ que son iguales a los de la _secuencia proteica encontrada_ más los que son diferentes pero con las mismas características bioquímicas.
#  * ```python
#    HSP.identities
#    ```
#    Sólo el número de AAs de la _secuencia buscada_ que son idénticos a los de la _secuencia proteica encontrada_.

# %%
hsp.match

# %%
hsp.positives, hsp.identities

# %% [markdown]
# Atributos que contienen los diferentes [_estadísticos del HSP_](https://www.ncbi.nlm.nih.gov/BLAST/tutorial/Altschul-1.html):
#  * ```python
#    HSP.expect
#    ```
#  * ```python
#    HSP.score
#    ```
#  * ```python
#    HSP.bits
#    ```

# %%
hsp.expect, hsp.score, hsp.bits

# %%
# Resultados de BLAST más significativos, usando los objetos Alignment de 
# `record_blast.alignments`, y los objetos HSP de estos objetos Alignment:

MAX_EVALUE = 1E-19 # Máximo valor de E-value que aceptaremos

# Iteramos sobre la lista de objetos Alignment para obtener información detallada 
# de cada alineamiento BLAST:
for idx, alignment in enumerate(record_blast.alignments):   # ➞➞ (0, alignment0), (1, alignment1), ... 
    hit = idx + 1                                           # Número de alineamiento (Hit)
    matching_description = record_blast.descriptions[idx]   # Obtener el objeto Description del actual objeto Alignment.
    
    # Filtramos por E-value:
    if matching_description.e < MAX_EVALUE:
        print("\n* Hit", hit, ": ", alignment.hit_id, 
              alignment.hit_def[14:49] + "... ", alignment.length, "AAs")
        
        # Iteramos sobre la lista de objetos HSP:
        for hsp_idx, hsp in enumerate(alignment.hsps, start=1):
            print("| > HSP n.", hsp_idx, "of hit", hit, " ➞", 
                  hsp.identities, "out of", query_len, "AAs are identical:")
            print(hsp)

# %% [markdown] tags=["ejercicio"]
# >  📝 **Práctica:**
# >
# > Basándote en el código de la celda anterior, y las variables que se definen a continuación, **filtrar** los objetos *Alignment* de `record_blast` que sean una coincidencia de _secuencia **completa** y **exacta**_ según sus HSPs:

# %% [markdown] tags=["ejercicio"]
# > *💡 Pista*: revisa el atributo `.identities` de un objeto *HSP*...

# %% tags=["ejercicio"]
# Filtrado de objetos Alignment según sus HSPs:

MAX_EVALUE = 1E-19      # Máximo valor de E-value que aceptaremos

exact_alignmts = []     # Lista que contendrá las tuplas (hit, alignment) que superen el filtro

query_len = record_blast.query_length # Longitud de la secuencia a buscar completa



# %% tags=["ejercicio", "solucion"] jupyter={"source_hidden": true}
# 📎 Solución: Filtrado de objetos Alignment según sus HSPs:

MAX_EVALUE = 1E-19         # Máximo valor de E-value que aceptaremos

exact_alignmts = []        # Lista que contendrá las tuplas (hit, alignment) que superen el filtro

query_len = record_blast.query_length # Longitud de la secuencia a buscar completa

for idx, alignment in enumerate(record_blast.alignments):
    matching_description = record_blast.descriptions[idx]
    if matching_description.e < MAX_EVALUE:
        for hsp in alignment.hsps:
            if hsp.identities == query_len:   # Coincidencia de secuencia completa y exacta:
                hit = idx + 1
                exact_alignmts.append( (hit, alignment) )
                break

# %% tags=["ejercicio"]
# Comprobación del filtrado de objetos Alignment:

print("Exact alignments:", len(exact_alignmts), "\n")

for hit, alignment in exact_alignmts:
    print("Hit " + str(hit) + ":", alignment.hit_id, alignment.hit_def[14:99] + "... ")

# %% [markdown] tags=["sabermas"]
# # VII.- Referencias y material de ampliación 🔗
#
# * El _notebook_ [Biopython Extras.ipynb](Supplementary%20Course%20Materials/Biopython%20Extras.ipynb) (en la sub-carpeta `Supplementary Course Materials/`), dónde se tratan los siguientes temas:
#     - Obtención de información sobre proteínas desde [UniProt](https://www.uniprot.org/).
#     - Lectura de Anotaciones de Gene Ontology ([GOA](https://geneontology.github.io/docs/go-annotations/)) para proteínas y filtrado según estas anotaciones.
#     - Referencias y material de ampliación.
#
#
# * [Official Biopython Documentation](https://biopython.org/wiki/Documentation).
# * [A Visual Intro to NumPy and Data Representation](https://jalammar.github.io/visual-numpy/), por [Jay Alammar](https://jalammar.github.io/about/).
# * [Numpy Basics Cheat Sheet](Supplementary%20Course%20Materials/Numpy%20Basics%20Cheat%20Sheet%20-%20DataCamp.pdf "Chuleta de Numpy"), de [DataCamp](https://www.datacamp.com).
# * [matplotlib Official Documentation](https://matplotlib.org/contents.html).
# * [matplotlib Cheat Sheet](Supplementary%20Course%20Materials/matplotlib%20Cheat%20Sheet%20-%20DataCamp.pdf "Chuleta de matplotlib"), de [DataCamp](https://www.datacamp.com).
#
#
# * [Biopython notebook Tutorials](https://github.com/tiagoantao/biopython-notebook), adaptación por Vincent Davis y Tiago Antao.
# * [NumPy Official Quickstart tutorial](https://www.numpy.org/devdocs/user/quickstart.html).
# * [Python Matplotlib Tutorial – Learn Plotting in 3 hours](https://www.listendata.com/2019/06/matplotlib-tutorial-learn-plot-python.html), por Deepanshu Bhalla en [ListenData](https://www.listendata.com/).
# * [Python Plotting With Matplotlib](https://realpython.com/python-matplotlib-guide/#the-matplotlib-object-hierarchy) (Guide), en [Real Python Tutorials](https://realpython.com/).
# * [Chapter 7: Matplotlib](https://scipython.com/book/chapter-7-matplotlib/), del libro [Learning Scientific Programming with Python](https://scipython.com/book/) por [Chistian Hill](https://christianhill.co.uk/) (2016).
