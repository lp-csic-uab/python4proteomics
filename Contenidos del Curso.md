Python4Proteomics. Contenidos
=============================

Version 0.5

## Introducción a Python

- Python (lenguaje de programación y versiones)
- Instalación del entorno de desarrollo Python
- REPL/*shell*, *scripts*, y *Jupyter Notebooks*
- Variables
- Objetos y clases
- Tipos/clases básicas (*built-in*): números (**int**, **float**), secuencias (**str**, **tuple**, **list**), y otros contenedores de objetos (**set**, **dict**)
- Control de flujo de ejecución del código: condicionales (`if... elif... else...`), bucles (`for...`, `while...`, `continue`, `break`), y captura de errores/excepciones (`try... except... else...`)
- Funciones básicas de Python (*built-in*) y funciones definidas por el usuario (`def...`)
- Módulos y paquetes (`import`)

## BioPython

- Tratamiento de ficheros FASTA
- Búsquedas con Blastp
- Obtención de información sobre proteínas desde UniProt

## Pandas y numpy

- Matrices  numéricas (*ndarray*) y cálculo vectorial
- **Series** y **DataFrame**
- Cargar/importar y guardar/exportar sets de datos (CSV, Excel)
- Operaciones básicas con *dataframes*: *slicing*, filtros booleanos, añadir y eliminar filas y columnas, `concat( )`, `merge( )`, iterar, `map( )`, `apply( )`, `groupby( )`

## Gráficas (matplotlib, plotnine)

- Gráficas básicas
- Dibujando espectros de masas
- Gráficas interactivas

## Proteómica con pyteomics

- Manipulación de ficheros de espectros en formato MGF
- Manipulación de ficheros de espectros en formato mzML

## Estadística (scipy, scikit-learn)