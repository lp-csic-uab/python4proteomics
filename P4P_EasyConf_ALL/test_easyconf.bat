@echo off

SETLOCAL EnableDelayedExpansion

REM set _cwd=%CD%^\
set _cwd=%~dp0
call :UPDATE_DATATIME
set _logfile="%_cwd%p4p_easyconf%_now%.log"
set _laststep_file="%_cwd%p4p_last_step.log"

set "_valid_labels= :STEP1 :STEP2 :STEP3 :STEP4 :STEP5 :STEP6 "

:CHECK_PARAMETERS
REM Go to the label passed as first argument, if any and valid:

set _label=%1
if "%_label%" equ "" (
    if EXIST %_laststep_file% (
        set /p _label=<%_laststep_file%
        echo La �ltima vez que se ejecuto este script se detuvo en !_label!
        set /p _use_filelabel=Desea continuar la ejecuci�n desde !_label! ^(s/[n]^)^? || set _use_filelabel=n
        if /i !_use_filelabel! equ N set _label=%_label%
    )
)
call set _valid_wo_label=%%_valid_labels: %_label% =%%
if %_valid_wo_label% neq %_valid_labels% goto %_label%

:CHECK_CONDA
if not DEFINED CONDA_PREFIX (
    if EXIST %_cwd%Scripts\activate.bat (
        call %_cwd%Scripts\activate.bat
    ) else (
        echo.
        echo Lo sentimos. Este script debe ejecutarse s�lo en un entorno conda.
        echo Ejecute Inicio ^> Todos los Programas ^> Anaconda3 ^> Anaconda Prompt
        echo y desde su l�nea de comandos vuelva a lanzar este script [ "%~f0" ]
        goto :ERROR
    )
) else (
    set _old_title=Anaconda Promt
)

:WELCOME
title %~nx0 - Preparando el entorno %CONDA_PREFIX% 
echo.
echo ** Bienvenido a %~nx0 **
echo.
echo Este script configurar� a continuaci�n el entorno conda ubicado en
echo  %CONDA_PREFIX% para el curso Python For Proteomics (P4P).
echo.
echo Para ello, es necesaria una conexi�n a Internet y unos 3 GiB de espacio
echo libre en la unidad %~d0
echo.
echo.
echo Pulse una tecla para Continuar o Control+C para Detener el proceso...
pause > nul

:START_LOG
(
echo.
echo -------------------------------
echo %0  ^>  %_logfile%
echo %DATE% - %TIME%
echo.
echo Parameters: %*
echo Current Working Directory: %_cwd%
echo Current Directory: %CD%
echo Conda base environment: %CONDA_PREFIX%
echo -------------------------------
echo.
)> %_logfile%

REM Trick to put all the batch file output in one file (see https://superuser.com/a/1123378 plus https://stackoverflow.com/a/4237137)
call %0 :STEP1 2>&1 | tee -a %_logfile%
exit /B %ERRORLEVEL%

:STEP1
echo :STEP1> %_laststep_file%
:SUCCESFULL_FINISH
set _exit_code=0
echo.
echo * Proceso de actualizaci�n e instalaci�n finalizado correctamente.
echo.
goto :END

:ERROR
set _exit_code=1
echo.
echo Atenci�n!! Ha ocurrido alg�n ERROR!
echo.

:END
echo Pulse una tecla para terminar...
pause > nul
if DEFINED _old_title title %_old_title%
exit /b %_exit_code%


:UPDATE_DATATIME
set _date=%DATE:/=-%
set _date=%_date: =_%
set _time=%TIME::=%
set _time=%_time:.=_%
set _now=%_date%_%_time%
goto :eof