#!/bin/bash

# From absolute symbolic links to relative ones:
# Ex.: SymLink "/home/user/folder1/folder2.2/file.ext" referencing "/home/user/folder1/folder2.1/file.ext"
#      Changes referenced file from "/home/user/folder1/folder2.1/file.ext" 
#      to "../folder2.1/file.ext"

# Current script directory (based on https://stackoverflow.com/a/4774063):
cd $(dirname "$0")

# Working directory:
cd "P4P_Course - Template from Repository"

# Standard form (has problems with links to folders):
find . -type l | while read x; do ln -srf "$(readlink -f "$x")" "$x"; done

# To fix problems with folders (-T), but doesn't work with all files (??) and has compatibility problems:
#find . -type l | while read x; do ln -srTf "$(readlink -f "$x")" "$x"; done

# For debugging:
#find . -type l | while read x; do echo "$(readlink -f "$x")"; echo "$x"; done
