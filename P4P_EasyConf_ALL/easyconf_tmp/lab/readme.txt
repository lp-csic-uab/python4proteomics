JupyterLab Static Files build from (and for) JupyterLab 1.2.4, with the following extensions:

jupyterlab-toc-1.0.1.tgz
jupyterlab-celltags-0.2.0.tgz
jupyter-widgets-jupyterlab-manager-1.1.0.tgz
jupyterlab-python-file-0.3.0.tgz
jupyterlab-spreadsheet-0.2.0.tgz
krassowski-jupyterlab_go_to_definition-0.7.1.tgz
lckr-jupyterlab_variableinspector-0.3.0.tgz
jupyterlab-python-bytecode-0.7.0.tgz
jupyterlab-jupytext-1.1.0.tgz

Static files in sub-folders static/ and extensions/ can be used to *replace* the ones inside ~/.local/share/jupyter/lab/ folder (folder %CONDA_PREFIX%\share\jupyter\lab\ on windows).  
But those in sub-folder schemas/ should be *added* to the ones inside ~/.local/share/jupyter/lab/schemas/ (folder %CONDA_PREFIX%\share\jupyter\lab\schemas\ on windows).
