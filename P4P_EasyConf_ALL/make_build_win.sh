#!/bin/bash

# CONFIGURATION GLOBAL VARIABLES:

# Current working directory (based on https://stackoverflow.com/a/4774063):
cd $(dirname "$0")
CWD="$(pwd -P)"
if [ "$(echo "${CWD}" | tail -c 2 | head -c 1)" != '/' ]; then
    CWD="${CWD}/"
else
    CWD="${CWD}"
fi

# Source Data Folders:
BASE_SRC_FOLDER="${CWD}"
SRC_COURSE_TMPLT="${BASE_SRC_FOLDER}P4P_Course - Template from Repository/"
SRC_EASYCONF="${BASE_SRC_FOLDER}easyconf_tmp/"

# Destination Builds Folder:
BASE_DST_FOLDER="${CWD}Builds/"
DST_FOLDER="${BASE_DST_FOLDER}P4P_EasyConf_WIN/"

LOG_FOLDER="${BASE_DST_FOLDER}"



# FUNCTIONS:

now() {
    # Date and time in a file-friendly pseudo-ISO format:
    date +%Y%m%d_%H%M%S
}


move_old_builds() {
    # 
    if [ -e "${DST_FOLDER}" ]; then
        mv "${DST_FOLDER}" "${BASE_DST_FOLDER}OLD_WIN_$(now)"
    fi
    mkdir "${DST_FOLDER}"
}


copy_course_materials() {
    # Copy referenced files (no symlinks) from source template folder containing symlinks (based on https://stackoverflow.com/a/10738708) to a new folder (based on and https://superuser.com/a/61619):
    cp -RL "${SRC_COURSE_TMPLT}" "${DST_FOLDER}P4P_Course"
}


copy_easyconf_tmp() {
    # Copy easyconf_tmp files to a new folder (based on and https://superuser.com/a/61619):
    cp -R "${SRC_EASYCONF}" "${DST_FOLDER}easyconf_tmp"
}


copy_general_files() {
    copy_course_materials
    copy_easyconf_tmp
}


copy_files4win() {
    files="P4P_EasyConf.bat
qtconsole_menu.json
conda_requirements.txt
ipython.ico
jupyterlab_menu.json
desktop.ini
tee.exe"
    for file in ${files}; do
        cp -L "${file}" "${DST_FOLDER}"
    done
}


main() {
    local last_error=0
    
    # Main Workflow:
    move_old_builds
    copy_general_files
    copy_files4win
    
    # End:
    if [ ${last_error} -eq 0 ]; then
        echo 
        echo "** Build Artifacts succesfully copied **"
        echo 
    fi
    return ${last_error}
}



# MAIN:

main
exit $?
