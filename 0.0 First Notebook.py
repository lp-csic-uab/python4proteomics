# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     comment_magics: true
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.2'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Notebook de Ejemplo
#
# Este es un ejemplo de un **notebook** de Jupyter.
#
# Los _notebooks_ están compuestos por diferentes _bloques de contenido_ llamados **celdas** (_cells_), situados cada uno bajo el anterior.

# %% [markdown]
# De entre todas las celdas que forman un notebook, verás que la que está actualmente seleccionada presenta una línea vertical de color azul en su margen izquierdo.

# %% [markdown]
# Puedes seleccionar una celda haciendo **clic** con el ratón en ella (o en su margen izquierdo), o bien utilizando los _cursores del teclado_ para ir moviéndote _arriba y abajo_ entre diferente celdas.

# %% [markdown]
# En un notebook de Jupyter existen básicamente **3 tipos de celdas**, tal como veremos a continuación:
#
# ![Tipos de celdas](images/cell_types.png "Tipos de celdas")

# %% [markdown]
# ## Celda de texto Markdown (texto _enriquecido_)
#
# De hecho, este bloque de texto (al igual que el anterior) es una **celda** de tipo **Markdown**: aunque se vea como ***texto enriquecido*** (con **negritas**, _cursivas_, [enlaces](https://en.wikipedia.org/wiki/Hyperlink), ...) realmente contiene texto plano en formato [Markdown](https://daringfireball.net/projects/markdown/):
#
#  * Para poder ver su auténtico contenido en formato Markdown, sólo hay que hacer **doble clic** sobre el contenido de la celda o sobre su margen izquierdo. Y para pasar ahora al **modo edición** de la celda (y poder _editar_ su contenido), sólo hay que hacer clic sobre el contenido dentro del cuadro gris tenue (si lo hacemos, veremos como el cuadro pasa a estar enmarcado en azul, y el fondo pasa a ser blanco, para facilitar la edición).  
#  Una manera rápida de pasar directamente al **modo edición** de una celda es pulsar la tecla **return ↵** estando _seleccionada_ la celda.
#  * Para que el texto en Markdown sea _convertido_ de nuevo por el notebook en texto formateado en _HTML_, es necesario **"ejecutar"** la celda, bien usando la combinación de teclas **mayúsculas + retorno (⇧ + ↵)**, bien haciendo _clic_ en el **botón ▶** de la _barra de herramientas_ del notebook.

# %% [markdown]
# ## Celdas de Código (con código y el resultado de su ejecución)

# %% {"tags": ["suma_simple"]}
# Ésta es una celda de Código ("Code"): permite escribir código en el 
# lenguaje de programación de la kernel asociada al notebook.
# En nuestro caso, tal como vemos en zona derecha de la barra de herramientas,
# estamos usando una kernel de Python 3, y por tanto hemos de escribir código
# en dicho lenguaje de programación. Por ejemplo:

40 + 2

# Para que se ejecute el código de la celda y poder ver el resultado justo 
# debajo de ella, hay que pulsar mayúsculas + retorno (⇧ + ↵), o el botón ▶ de la 
# barra de herramientas.

# %% {"tags": ["string"]}
"Esta celda también es una celda de Código, y lo que ves entre comillas es una cadena de carácteres"

# %% [markdown] {"toc-hr-collapsed": false}
# ## Celda Raw (texto _plano_)

# %% [raw] {"tags": ["solo_texto"]}
# Esta celda es una celda de tipo Raw: texto sin formato. 
#
# Se muestra tal cual aunque sea ejecutada (⇧ + ↵, o pulsando el botón ▶); no cambia por ello ni genera una sub-celda de resultado debajo.

# %% [markdown] {"toc-hr-collapsed": false}
# ## Celdas ocultas
#
# Una _celda_ de un notebook puede ***ocultarse*** y ***des-ocultar*** haciendo _clic_ en la _barra vertical azul_ del margen izquierdo de la celda.
#
# Cuando están ocultas, sólo se muestran **tres puntos** (`•••`), como en los ejemplos debajo de esta celda.
#
# Prueba a ***des-ocultar*** las celdas de debajo haciendo _clic_ en los _tres puntos_ (`•••`), o en la _barra vertical azul_ de cada celda oculta:

# %% [markdown] {"tags": ["oculta"], "jupyter": {"source_hidden": true}}
# Esta es una celda Oculta

# %% {"jupyter": {"outputs_hidden": true, "source_hidden": true}, "tags": ["oculta"]}
# Esta es una celda de código con resultado, y ambos están ocultos

import this

# Para des-ocultar el resultado, y poder verlo, hay que hacer clic en los tres puntos de debajo (•••)

# %% [markdown]
# # Consolas de Python
#
# Para abrir una _nueva consola de Python 3_, lo más sencillo es haciendo **clic** sobre el **botón** correspondiente en la _pestaña del **Launcher**_ (o, alternativamente, desde el menú *File -> New -> Console*):
#
# ![Consola](images/console_1.png "Abrir una nueva consola de Python 3 - Launcher")
#
# La _nueva consola_ estará asociada a una _nueva kernel de Python 3_ (un _nuevo intérprete de Python 3_).

# %% [markdown]
# Pero, también puede suceder que queramos abrir una _consola para el notebook_ en que nos encontramos trabajando (de manera que ambos utilicen la _misma kernel_ o intérprete).  
# En ese caso, haremos **clic** con el **botón secundario del ratón** sobre cualquier parte del notebook para que se habra el _**menú contextual**_, y de éste, seleccionaremos la opción _**New Console for Notebook**_:
#
# ![Consola](images/console_2.png "Abrir una consola de Python 3 asociada a un notebook")
#
# La _nueva consola_ estará asociada a la _misma kernel que el notebook_ (ésta consola es como una _"vista"_ diferente del notebook, pero compartiendo el mismo _intérprete_ de código, y por tanto el mismo _estado_).

# %% [markdown]
# # Referencias y material de ampliación 🔗
#
# * [Documentación oficial de JupyterLab](https://jupyterlab.readthedocs.io/en/stable/user/interface.html "The JupyterLab Interface — JupyterLab documentation")
# * [Notebooks](https://jupyterlab.readthedocs.io/en/stable/user/notebook.html "Notebooks - JupyterLab Documentation"), de la documentación oficial de JupyterLab.
# * [Using the JupyterLab Environment](https://developers.arcgis.com/python/guide/using-the-jupyter-lab-environment/#Using-the-JupyterLab-Environment "Using the JupyterLab Environment"), por [ArcGIS for Developers](https://developers.arcgis.com/ "ArcGIS for Developers")
# * [Vídeos de cómo utilizar JupyterLab](https://www.youtube.com/playlist?list=PLUrHeD2K9CmlEvyGGgZXDf_u31MvLB_Lg) en YouTube. Aunque la _versión_ de JupyterLab que utilizan es un poco _antigua_, siguen siendo válidos para comprobar las posibilidades de éste entorno de desarrollo y análisis de datos.
