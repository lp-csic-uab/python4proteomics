# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     comment_magics: true
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %% [markdown]
# ![Python4Proteomics](images/logo_p4p.png) **(Python For Proteomics)**
#
# # INTRODUCCIÓN A PYTHON (Parte 2)
#
# En este apartado veremos los conceptos básicos del lenguaje de programación **Python**:
#
#  * Parte 1 (_notebook_ [1.1 Introduccion a Python.ipynb](1.1%20Introduccion%20a%20Python.ipynb "Abrir el notebook de la parte 1")):
#     - Variables.
#     - Objetos y clases de objetos.
#     - Tipos básicos (*built-in*): números (**int**, **float**), secuencias (**str**, **tuple**, **list**), y otros contenedores de objetos (**set**, **dict**).
#  * **Parte 2** (este _notebook_):
#     - Control de flujo de ejecución del código: condicionales (`if... elif... else...`), bucles (`for...`, `while...`, `continue`, `break`), y captura de errores/excepciones (`try... except... else...`).
#     - Algunas funciones básicas de Python (*built-in*), y funciones definidas por el usuario (`def...`).
#     - Módulos y paquetes (`import`).
#     - Referencias y material de ampliación (como [Memento, la _chuleta_ de Python 3](Supplementary%20Course%20Materials/Python%203%20Cheat%20Sheet%20-%20Memento.pdf "Chuleta de Python 3")).
#     
# ---
#

# %% [markdown]
# **Índice de la Parte 2:**
#
#  * [III.- Control de flujo](#III.--Control-de-flujo)
#    * [Condicionales ( *if* )](#Condicionales-(-if-...-elif-...-else-...-))
#    * [Bucles](#Bucles)
#      * [Bucle iterativo ( *for* )](#Bucle-iterativo-(-for-...-))
#      * [Bucle condicional ( *while* )](#Bucle-condicional-(-while-...-))
#      * [continue y break](#continue-y-break)
#    * [Captura de Excepciones ( *try* )](#Captura-de-Excepciones-(-try-...-except-...-else-...-))
#  * [IV.- Funciones](#IV.--Funciones)
#    * [Funciones _Built-In_](#Funciones-Built-In)
#    * [Funciones definidas por el usuario ( *def* )](#Funciones-definidas-por-el-usuario)
#  * [V.- Módulos y Paquetes](#V.--M%C3%B3dulos-y-Paquetes)
#    * [import](#import)
#  * [VI.- Ejercicio final del tema (opcional)](#VI.--Ejercicio-final-del-tema-(opcional))
#  * [VII.- Referencias y material de ampliación 🔗](#VII.--Referencias-y-material-de-ampliaci%C3%B3n-🔗)
# ---
#

# %% [markdown] toc-hr-collapsed=false
# # III.- Control de flujo
#
# La ejecución del código no tiene que ser únicamente lineal (una sentencia ejecutada tras otra), ya que existen instrucciones que permiten controlar el orden de ejecución de las sentencias: los *condicionales*, los *bucles* y la *captura de errores/excepciones*.

# %% [markdown]
# ## Condicionales ( if ... elif ... else ... )
#
# Permiten determinar _qué_ _bloque de código_ se debe _ejecutar_, dependiendo si se cumplen o no determinadas _**condiciones**_ (expresiones que _evaluan_ a un valor _booleano_, como las que ya hemos visto utilizando los _operadores de comparación_ y _pertenencia_, o bien directamente _objetos_ que evalúan a `True` o `False`).
#
# En Python se usan las palabras clave `if` (_si_), `elif` (_si no, pero si_), `else` (_si no_), junto a las _condiciones_ a evaluar, dos puntos (`:`) tras las condiciones, y los _bloques de código_ a ejecutar en _cada caso_, [_"indentados" / sangrados_](https://es.wikipedia.org/wiki/Indentaci%C3%B3n) a la derecha mediante 4 _espacios_ (recomendado) o tabuladores; tal como se muestra a continuación:
#
# ```python
#     if first_condition:
#         indented_block_of_code_to_execute
#         when_first_condition_is_True
#     elif second_condition:
#         indented_block_of_code_to_execute
#         when_second_condition_is_True
#         and_previous_ones_are_False
#     else:
#         indented_block_of_code_to_execute
#         when_no_previous_condition_is_satisfied
# ```

# %%
# El caso más simple, con sólo una condición (sin cláusulas `elif`, y sin cláusula `else`):

condition = True      # Condición que siempre se cumple (siempre evalúa a `True`)

if condition:         # Primera (y única) condición:
    print('Condición es verdadera:')                   # Bloque de código indentado, a ejecutar
    print('`condition` evalua a True.')                # sólo si se cumple la condición

# %%
# Ejemplo con 2 condiciones (1 cláusula `if` y 1 cláusula `elif`) y cláusula final `else`:

# Pregunta al usuario y guarda la respuesta, como entero, en la variable `num`:
num = int( input('Dime un número entero:') )

if num > 0:                                       # Primera condición:
    print('Primera condición es verdadera:')
    print("El número", num, "es positivo.")
elif num < 0:                                     # Segunda condición:
    print('Primera condición falsa, segunda condición verdadera:')
    print("El número", num, "es negativo.")
else:                                             # En caso de que Ninguna condición se cumpla:
    print('Ninguna de las condiciones era verdadera:')
    print('El número es 0.')

# %% tags=["sabermas"] jupyter={"source_hidden": true}
# 📌 Ejemplo con múltiples cláusulas `elif`:

print('Menú de opciones:')
print('A     - Primera opción.')
print('B     - Segunda opción.')
print('C ó D - Tercera opción.')
# Pregunta al usuario y guarda la respuesta en la variable `opcion`:
opcion = input('Por favor, elija una opción (A - D): ').upper()

if opcion == 'A':
    mensaje = "la primera opción"
elif opcion == 'B':
    mensaje = "la segunda opción"
elif opcion == 'C' or opcion == 'D':
    mensaje = "la última opción"
else:
    mensaje = "una opción no válida ('" + opcion + "')"

print("Has elegido " + mensaje + ".")    

# %%
known_names = {'javascript', 'perl', 'ruby', 'r', 'lua'}   # Nombres conocidos (estado inicial)

# %%
# Ejemplo con condicionales "anidados" (condicionales dentro de condicionales):

name = input('Cómo te llamas?')       # Pregunta al usuario y guarda la respuesta en la variable `name`

name_no_sp = name.strip()             # Eliminamos espacios en blanco a principio y final

if name_no_sp:                        # Una cadena de caracteres no vacía evaluará a `True`
    print("Hola " + name_no_sp + "!")
    name_lower = name_no_sp.lower()   # Pasar a minúsculas para simplificar las comparaciones
   
    if name_lower == 'python':        # Nuevo condicional (condicional "anidado"):
        print('Nos llamamos igual! :-P')
    elif name_lower in known_names:
        print("Me suena el nombre " + name_no_sp + "... Nos conocemos?")
    else:
        print('Me alegro de conocerte.')
        known_names.add(name_lower)   # Añade el nombre al conjunto de nombres conocidos

else:                                 # `name_no_sp` es pues una cadena vacía (''):
    print('No se ha introducido ningún nombre.')
    
    if name:                          # Nuevo condicional (condicional "anidado"):
        print('... Sólo caracteres en blanco.')

# %% [markdown] tags=["ejercicio"]
# <br/>
#
# > 📝 **Práctica:**
# >
# > **Preguntar** al usuario (función `input()`) por la _secuencia de un péptido_.  
# > Comprobar _si_ tiene un _mínimo_ de _**6 aminoácidos**_; y de ser así, indicar _si_ pudiera tratarse de un péptido _**tríptico** o no_:

# %% tags=["ejercicio"]
min_pep_len = 6    # Longitud mínima aceptada de un péptido.

peptide = 



# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución:

min_pep_len = 6 # Longitud mínima aceptada de un péptido.

peptide = input('Cuál es la secuencia del péptido a testear?')

peptide = peptide.strip() # Elimina espacios extra y pasa a mayúsculas para facilitar comparaciones

if len(peptide) >= min_pep_len: # Si el péptido tiene suficientes AAs:
    if peptide[-1].upper() in ('K', 'R'): # Comprueba si el último AA es típico de un péptido tríptico:
        print("El péptido \"" + peptide + "\" parece ser tríptico.")
    else:
        print("El péptido \"" + peptide + "\" no es un péptido tríptico.")
else:
    print("La secuencia peptídica ha de contener " + str(min_pep_len) + " o más aminoácidos...")

# %% [markdown] toc-hr-collapsed=false
# ## Bucles
#
# Los bucles permiten ejecutar _bloques de código_ de manera _repetida_ (un número determinado de veces o mientras se cumpla una condición).

# %% [markdown]
# ### Bucle iterativo ( for ... )
#
# Permite recorrer los _elementos_ (_items_) de un objeto [_iterable_](https://docs.python.org/3/library/stdtypes.html#typeiter) (un objeto que pueda ser _recorrido_), como por ejemplo un _contenedor_ (strings, listas, tuplas, sets, diccionarios).  
# Así, en principio, el bloque de código delimitado por el bucle `for` se repetirá tantas veces como _items_ contenga el _iterable_.
#
# Se utiliza la instrucción `for` (_para_) seguida de un _nombre de variable_, la palabra reservada `in` (_en_), el objeto _iterable_, dos puntos (`:`), y el _bloque de código_ (_indentado_ a la derecha) que se ejecutará por cada elemento (_item_) del objeto _iterable_ que irá siendo referenciado por el _nombre de variable_ indicado:
#
# ```python
#     for item in iterable:
#         indented_block_of_code
#         to_do_something_with_each
#         item_variable_from_iterable
# ```

# %%
# Ejemplo simple de Iteración:

peptide = 'MsWPAy'

print('Antes del bucle for.')

# En cada iteración `aa` apuntará al siguiente item de `peptide` (hasta que se agoten):
for aa in peptide:    
    # Bloque de código que se repetirá:
    print('    Dentro del bucle for...')
    print("    AA:", aa)

print('Después del bucle for.')

# %%
# Ejemplo simple de Repetición (interesa simplemente repetir un bloque de código, no iterar):

# `range(1, 5)` devuelve una secuencia de los números 1 a 4 [1,5):
for x in range(1, 5): 
    # Bloque de código que se repetirá:
    print("Repetición:", x)

print("El bloque se ha repetido", x, "veces.")

# %%
# Ejemplo de Iteración usando `enumerate()` para conocer además el número de repetición (los objetos enumerate
# son iterables que van devolviendo tuplas del tipo (número, item) a partir de otro iterable de items) :

peptide = 'MEAIPKRWK'

for pos, aa in enumerate(peptide): # Itera la posición e items de `peptide`:
    print(pos, aa)    

# %%
# Ejemplo de bucle para recorrer (Iterar) los pares key ➞ value de un diccionario:

aa2mass = {'S': 87.03203, 'R': 156.10111, 'K': 128.09496, 'T': 101.04768, 'Y': 163.06333}
one2three = {'S': 'Ser', 'R': 'Arg', 'K': 'Lys', 'Y': 'Tyr'}

# Recordar que el método .items() de un diccionario devuelve una "view" con los pares 
# key ➞ value en forma de tuplas del tipo (key, value) :
for aa, mass in aa2mass.items():
    # Ahora, para obtener el código de 3 letras para el aminoácido de la iteración actual (`aa`), usamos el 
    # método `.get()` del diccionario `one2three`, de manera que si éste no está presente, se utilizará 
    # (`default`) el código de 1 letra (el propio valor de `aa`):
    aa_three = one2three.get(aa, aa)
    print(aa_three, ":", mass)

# %% [markdown] tags=["ejercicio"]
# <br />
#
# > 📝 **Práctica:**
# >
# > Crea un _nuevo_ diccionario `three2one` a partir del diccionario anterior `one2three` _"invirtiendo"_ los pares _key ➞ value_ de éste (las _keys_ han de pasar a ser _values_, y los _values_ han de pasar a ser _keys_: _value ➞ key_):

# %% tags=["ejercicio"]
three2one = dict() # Nuevo diccionario



# %% tags=["ejercicio", "solucion"] jupyter={"source_hidden": true}
# 📎 Solución:
# 👁 Ojo!: Sólo es válida si los values son únicos y por tanto No se repiten para diferentes keys.

three2one = dict()   # Nuevo diccionario

for aa_one, aa_three in one2three.items():   # Itera los pares (key, value):
    three2one[aa_three] = aa_one             # Usa el value (`aa_three`) como key, y la key (`aa_one`) como value
    
three2one

# %% [markdown]
# ### Bucle condicional ( while ... )
#
# Estos bucles permiten ejecutar un _bloque de código_ _mientras_ se cumpla una _condición_.
#
# Se utiliza la instrucción `while` (_mientras_) seguida de la _condición_ que ha de cumplirse para que el bucle se vaya repitiendo, dos puntos (`:`), y el _bloque de código_ (_indentado_ a la derecha) que se ejecutará _mientras_ se _cumpla_ la _condición_:
#
# ```python
#     while condition:
#         indented_block_of_code
#         to_do_something
# ```

# %%
# Ejemplo `while`:

countdown = 3

print('Antes del bucle while:')
print(countdown)

while countdown > 0:        # Mientras la variable de cuenta atrás no llegue a 0...:
    # Bloque de código que se repetirá:
    print('    Dentro del bucle while...')
    print("    ", countdown)
    countdown = countdown - 1

print('Después del bucle while:')
print(countdown)

# %% tags=["error"]
# Ejemplo de "bucle infinito". 

# 🚨 ATENCIÓN!! : Para interrumpirlo pulsar 2 veces la tecla i del teclado, 
# o el botón ⯀ ("Interrupt the kernel") de la barra de herramientas superior!!!

countup = 0

while True:   # La condición siempre evalúa a True (siempre se cumple)
    countup = countup + 1

# %%
print("Antes de la interrupción el bucle se ha repetido", countup, "veces.")

# %% [markdown] tags=["ejercicio"]
# <br/>
#
# > 📝 **Práctica:**
# >
# > Usando la función `input()` pregunta por una _secuencia peptídica_ (`peptide`) una y otra vez hasta que la secuencia introducida _cumpla_ las siguientes _condiciones_:
# > * Contenga _6 o más_ aminoácidos _diferentes_.
# > * Los _aminoácidos_ de su secuencia formen parte del conjunto `valid_aas`:

# %% tags=["ejercicio"]
valid_aas = {'A', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'P', 'R', 'S', 'T', 'V', 'W', 'Y'}

# %% tags=["ejercicio"]
peptide = ''           # Definimos la variable a comprobar, apuntando a una cadena vacía



# %% tags=["ejercicio", "solucion"] jupyter={"source_hidden": true}
# 📎 Solución:

peptide = ''          # Definimos la variable a comprobar, apuntando a una cadena vacía
tries = 0             # Variable para contar el número de intentos (las veces que se repite el bucle)

while len(set(peptide)) < 6 or set(peptide).difference(valid_aas): # Mientras se cumpla alguna de las "anti-condiciones":
    tries += 1        # Añade 1 al número de intentos. Equivalente a:  tries = tries + 1
    peptide = input("\nEscribe la secuencia:")
    peptide = peptide.strip().upper()          # Elimina espacios y pasa a mayúsculas para facilitar las comparaciones

print("\n\nConseguido un péptido válido en", tries, "intentos!")
print(peptide)

# %% [markdown]
# ### continue y break
#
# Las _instrucciones_ `continue` y `break` se pueden utilizar _dentro de un bucle_, bien para pasar a la _siguiente repetición_ del bucle, con `continue`; bien para _salir inmediatamente_ del bucle, con`break`; en ambos casos _sin_ que se ejecute lo que quedase del _bloque de código_ del bucle.

# %%
# Ejemplo con `continue` (pasar a la siguiente iteración del bucle):

numbers = [1, 3, 6.5, 3.0, 7, 8.2, 1E1]   # Números a procesar
exceptions = {3, 10}                      # Excepciones: números que no queremos que se procesen

num2result = dict()

for num in numbers:
    if num in exceptions:   # Si el número actual forma parte de las excepciones:
        print("El número", num, "no se procesará.")
        continue            # Pasa a la siguiente iteración, sin ejecutar el resto de código del bucle.
    # Procesado de `num`:
    result = num ** 2
    print(num, "\t➞", result)
    num2result[num] = result

print('\nNúmeros procesados y sus resultados:')
print(num2result)

# %%
# Ejemplo con `break` (salir del bucle):

counts = 0

print('Contando...')

while True:                 # Parece un bucle infinito... pero no lo es...:
    if counts == 10_000_000:
        break               # Sale del bucle, sin ejecutar el resto de código del bucle.
    counts = counts + 1

print(counts)

# %% [markdown]
# ## Captura de Excepciones ( try ... except ... else ... )
#
# Tal como hemos visto, cuándo en Python se produce un **error** en el código, se **detiene** la _ejecución_ de dicho código, mostrándose por pantalla _qué error_, y en _qué punto_ del programa se ha producido:

# %% tags=["error"]
print('a) Ejecución iniciada...')

print('b) Antes del error...')

# Código que generará un Error:
x = 1 / 0       # Error! ➞ La ejecución del código se detiene aquí. 
                #        ➞ Se emite una excepción (tipo ZeroDivisionError)

print('c) Después del error...') # Este código ya NO se ejecutará al haberse producido el error...

print('d) Fin!')                 # Y este código TAMPOCO se ejecutará...

# %% [markdown]
# <br/>
#
# Estos _errores_ también se pueden _tratar_ dentro del propio programa, para _evitar_ que se detenga y poder responder a ellos de maneras más adecuadas.
#
# Para ello, en cada _error_ Python genera una _excepción_, es decir, un _objeto_ de la clase **Exception**, o de alguna de sus _**sub-clases**_ (como **KeyError**, **ValueError**, **TypeError**, **AttributeError**, ...), que puede ser _capturado_ usando una construcción con las instrucciones `try`, `except`, `else` como la siguiente:
#
# ```python
#     try:
#         code_that_can_fail
#     except ExceptionClass as variable_name:
#         do_only_if_error
#     else:
#         do_only_if_no_errors
# ```
#
# Estas construcciones también _alteran_ el _flujo de ejecución_ lineal de un programa: 
#   * Hay un _bloque de código_ (tras la instrucción `except`) que _sólo_ se ejecutará si se genera una _excepción_ en el _código bajo "vigilancia"_ (el código tras la instrucción `try`) y además dicha excepción es "capturada" por la cláusula `except` (es decir, si la _clase_ de la excepción coincide con la indicada a continuación de `except`, o es una _sub-clase_ de ésta).
#   * Y otro _bloque de código_ (tras la instrucción `else`) que _sólo_ se ejecutará si no se produce _ningún error_ en el codigo "vigilado".

# %%
# Ejemplo sencillo en que se capturan Todas las posibles excepciones 
# que se puedan producir en el código "vigilado":

print('a) Ejecución iniciada...')

y = 0   # Cambiando el valor de `y` podemos evitar el error... o producir otros errores...

try:    # Código "vigilado" (to try):
    print('b) Antes ...')
    # Código que puede fallar, generando una excepción:
    value = 1 / y
    print('c) Después ...')
except Exception as error:    # Si se genera una excepción, y su tipo es Exception o una de sus sub-clases:
    print('Atención!! Se ha producido el siguiente error:')
    print(error, " ➞ ", type(error))
else:                         # Si no se ha generado Ninguna excepción:
    print('No se ha producido ningún error.')

print('d) Fin!')

# %% tags=["error"]
value    # Si se produjo un error, la variable `value` no se ha modificado/definido ...

# %% [markdown] tags=["ejercicio"]
# <br />
#
# > 📝 **Práctica:**
# >
# > En el código anterior, cambia el valor de `y` por uno que **no** ocasione ningún _error_, y comprueba _qué pasa_ ...
#
# > Cambia después el valor de `y` por `"a"`, y comprueba _qué pasa_, y _qué referencía_ la variable `value`...

# %% [markdown] toc-hr-collapsed=false
# # IV.- Funciones
#
# En Python las _**funciones**_ son _objetos_ (del tipo **function** o similares), que representan _bloques de código_; los cuales sólo se ejecutarán cuando la función sea _"llamada"_ (las funciones son objetos [_"callables"_](https://docs.python.org/3/library/functions.html#callable "Documentación de Python")).  
# Como resultado de su ejecución, las funciones _devuelven / retornan siempre_ un objeto (aunque éste sea `None`).  
# Para _**llamar**_ a una función se utiliza el _nombre_ de la función seguido de _paréntesis_ `()`, entre los cuales se pueden o no especificar otros _objetos a "pasar"_ como _argumentos_ de la función:
#
# ```python
# function_name(argument_0, argument_1, ...)  ➞ returned_object
# function_name(parameter_0=argument_0, parameter_1=argument_1, ...)  ➞ returned_object
# ```
#
# Como se puede ver (primera notación), los _argumentos_ se pueden pasar _"tal cual"_, de manera que la función los interpretará según la _posición_ en que se le pasen (_positional arguments_).  
# O, alternativamente (segunda notación), se pueden pasar indicando el *nombre* (_keyword_) del *parámetro* seguido de un signo igual (`=`) y el _objeto_ que se quiere usar como argumento (_keyword arguments_) para dicho parámetro.

# %%
print    # La primera función de Python que hemos visto...

# %%
# Variables y objetos pasados como argumentos posicionales 
# (incluyendo la propia función, ya que también es un objeto):
argument1 = 'first positional argument'
argument2 = 2

returned_object = print(argument1, argument2, 3.0, print) 

# Hemos asignado la variable `returned_object` al objeto retornado por `print()`:
print('Returned object:', returned_object)

# %%
# Pasando también argumentos usando los nombres de parámetros (opcionales en este caso)
# `sep` (separador) y `end` (final de línea):
print(argument1, argument2, 3.0, print,  sep="+++", end=">>>>")

# %% jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 Puesto que las funciones son objetos, pertenecen a un tipo/clase, tienen identificador, 
# además de atributos y métodos:

print('Tipo:',  type(print) ) # Clase/tipo al que pertenece como objeto [ función `type()` ]
print('ID  :',  id(print) ) # Identificador de objeto [ función `id()` ]
dir(print) # Métodos y atributos del objeto [ función `dir()` ]

# %% jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 Y podemos hacer con ellas lo que podemos hacer con cualquier otro objeto:

p = print # Asignación de la función a una variable...

# Llamarlas usando cualquiera de las referencias a que las hayamos asignado:
p("print() ahora tiene 2 nombres: `print` y `p`:")
p('`p` sigue siendo print()\n')

funcs = [p, type, abs, max, min, round, len, input] # Incluirlas como elementos de contenedores...
print('Funciones vistas por ahora:')
for func in funcs:
    print("*", func.__name__, ":", func.__doc__[:90].replace('\n', ' ') + "...") # Acceder a sus atributos...

funcs[0]('\n`funcs[0]` sigue siendo print()') # Llamarlas usando cualquiera de las referencias a que las hayamos asociado...


# %% [markdown]
# ## Funciones *Built-In*
#
# Son _funciones internas_, _propias_ del interprete de Python, disponibles directamente para ser _llamadas_ desde cualquier parte del código.
#
# Ya hemos visto _algunas_ de ellas, aunque haya sido de manera superficial:  
#   `print(objects[, sep, end])`  
#   `type(object)`  
#   `abs(number)`  
#   `max(objects_or_container)`  
#   `min(objects_or_container)`  
#   `round(number[, decimals])`  
#   `len(container)`  
#   `input(string)`
#
# Pero hay _más **disponibles**_ ...  
# 📌 Y puedes consultarlas tanto en la [documentación de Python para _Built-in Functions_](https://docs.python.org/3/library/functions.html "Documentación on-line"), como también en nuestra [_chuleta_ de Clases y Funciones _Built-in_](Supplementary%20Course%20Materials/Clases%20y%20Funciones%20built-in%20de%20Python3%20-%20P4P%202019.pdf "Chuleta en PDF").

# %% [markdown] toc-hr-collapsed=false
# ## Funciones definidas por el usuario
#
# Además de las _funciones internas_ (y las disponibles en otros _módulos_ o _paquetes_ de Python), nosotros también podemos _definir_ nuestras propias funciones, usando para ello la instrucción `def` de la siguiente manera:
#
# ```python
#     def function_name(parameter_0, parameter_1, ..., parameter_X=default_argument_X, ...):
#         """documentation string ('docstring')"""
#         indented_block_of_code
#         to_do_something_with_the_arguments
#         return some_value_object
# ```

# %%
# Ejemplo de función muy simple, sin parámetros ni cláusula `return`:

def f():      # No acepta ningún argumento (ya que no se define ningún parámetro):
    # Bloque de código de la función (indentado):
    print('Soy `f()` y sólo imprimo ésto :-(')
    # No incluye la cláusula return  ➞ por defecto retornará `None`.


# %%
returned_object = f()    # Llamada a la función simple (sin pasar argumentos)
print('Returned object:', returned_object)   # Ver qué ha retornado:


# %%
# Ejemplo de función sencilla con 2 parámetros (`x` e `y`):
# El parámetro `x` referenciará al primer objeto que pasemos como argumento, e `y` al segundo

def add(x, y): 
    """Adds `y` to `x` and return the result"""
    z = x + y    # Suma o concatenación (dependiendo del tipo de los argumentos `x` e `y` pasados)
    return z     # Retorna el objeto resultante de la operación anterior (`z`).


# %%
add(3, 2.5)   # Argumentos pasados según su posición (positional arguments)

# %%
add(y='a', x='b') # Argumentos pasados usando los nombres de parámetros (keyword arguments)

# %% tags=["error"]
add(2)        # Llamada a función sin pasarle todos los argumentos requeridos ➞ Error (TypeError)!


# %%
# Ejemplo de función con un parámetro posicional (`header`), 
# y un parámetro opcional (`sep`) con valor por defecto:
# Por defecto `sep` apuntará a '|', salvo que pasemos otro objeto para éste parámetro

def get_accession(header, sep='|'): 
    """Returns the accession number from a FASTA `header` string"""
    if type(header) is str:                         # Type Checking (`header` debe ser un objeto de tipo `str`)
        if header[0] != '>' or sep not in header:   # Avisar si no se parece a un encabezado FASTA:
            print("Warning!: Argument `header` is not a valid FASTA header.")
        tokens = header.split(sep)                  # Divide la cadena de caracteres `header` por el separador `sep`
        return tokens[1]                            # Retorna el segundo elemento (Accession number).
    else:
        print("Error!!: First argument `header` should be a string!") # Informa del problema
        return                                      # Cláusula `return` sin objeto a retornar
                                                    # ➞ por defecto retornará `None`.


# %%
uniprot_header = '>sp|P00761|TRYP_PIG Trypsin OS=Sus scrofa PE=1 SV=1'

get_accession(uniprot_header)        # No necesitamos pasar un argumento para el parámetro `sep`.

# %%
get_accession( set(uniprot_header) ) # Probamos a llamarla pasando algo diferente a un string...

# %%
get_accession('lalala|P00001')       # Esto no parece un encabezado de FASTA...

# %% [markdown] jupyter={"source_hidden": true} tags=["sabermas"]
# ### 📌 Alcance/ámbito (_scope_) de las variables
#
# En Python, las variables pueden ser, según su _alcance / ámbito_ (_scope_):
#  * **Globales**: son _asignadas_ a un objeto (son "declaradas") en bloques de código _fuera_ de funciones, clases, ... (o sea, en lo que podríamos denominar el "ámbito de trabajo/ejecución principal").  
#    Las variables _globales_ son _accesibles_ tanto desde _fuera_ como desde _dentro_ de las funciones o clases del mismo fichero de código. Pero _sólo_ pueden ser _re-asignadas_ en bloques de código _fuera_ de éstas (si se intenta re-asignar una variable global dentro de una función, sólo se consigue crear una variable local con el mismo nombre que la global, y accesible sólo en el ámbito de dicha función).
#  * **Locales**: aquellas que se _asignan_ a un objeto en la definición o el cuerpo de una _función_, en _clases_, ...  
#    Las variables _locales_ sólo son _accesibles_ desde _dentro_ del ámbito de la función en que se han asignado/declarado (no se pueden acceder ni re-asignar desde otras funciones, ni desde el ámbito de ejecución principal).

# %% jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 Variables "globales" (declaradas en el "ámbito principal/global":
x = 10
z = 'z'


# %% jupyter={"source_hidden": true} tags=["sabermas"]
def try_to_change_x_to(y): # 📌 El parámetro `y` es una variable "local" de esta función
    x = y # Al ser re-asignada dentro de la función, `x` pasa a ser también el nombre de una variable "local" de esta función
    print(x, z) # Mientras que `z` sigue siendo una variable "global", accesible desde el ámbito de la función


# %% jupyter={"source_hidden": true} tags=["sabermas"]
print(x, z) # 📌 Acceso a las variables "globales" desde el "ámbito principal"

# %% jupyter={"source_hidden": true} tags=["sabermas"]
try_to_change_x_to(-5) # 📌 Parece, desde el ámbito de la función, que se haya cambiado la variable "globale" `x`

# %% jupyter={"source_hidden": true} tags=["sabermas"]
print(x, z) # 📌 Pero realmente sólo la cambió en el ámbito "local" de la función, no en el ámbito "global".

# %% [markdown] tags=["ejercicio"]
# <br/>
#
# > 📝 **Práctica:**
# >
# > Define una nueva **función** llamada `molecular_weight` que calcule la **masa** (redondeada a 6 decimales) de una _secuencia peptídica_.  
# > Ésta secuencia ha de ser pasada como argumento del _parámetro obligatorio_ (`seq`) de la función.  
# > Además, la función `molecular_weight` ha de poder aceptar como _argumento_, para el _parámetro opcional_ `alphabet2mass`, un _diccionario_ que indique la masa de cada aminoácido (_AA ➞ masa_), tal como el siguiente diccionario `aa2mass`:

# %% tags=["ejercicio"]
# La masa monoisotópica de cada aminoácido en el siguiente diccionario es la masa del residuo aminoacídico 
# en una secuencia, y por tanto ya tiene en cuenta la pérdida de 1 molécula de H₂O 
# (18.010565 u.m.a.) para cada residuo debido a la formación del enlace peptídico:

aa2mass = {'A': 71.037114, 'C': 103.00919, 'D': 115.02694, 'E': 129.04259, 'F': 147.06841,
           'G': 57.021464, 'H': 137.05891, 'I': 113.08406, 'K': 128.09496, 'L': 113.08406, 
           'M': 131.04048, 'N': 114.04293, 'P': 97.052764, 'Q': 128.05858, 'R': 156.10111, 
           'S': 87.032029, 'T': 101.04768, 'V': 99.068414, 'W': 186.07931, 'Y': 163.06333}


# %% tags=["ejercicio"]
def molecular_weight(seq, alphabet2mass=aa2mass):
    """
    Calcula y devuelve la masa molecular de una secuencia peptídica (`seq`), según
    las masas de los diferentes aminoácidos proporcionada por un diccionario (`alphabet2mass`), 
    que por defecto es igual al diccionario global `aa2mass`.
    """



# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución:

def molecular_weight(seq, alphabet2mass=aa2mass):
    """
    Calcula y devuelve la masa molecular de una secuencia peptídica (`seq`), según
    las masas de los diferentes aminoácidos proporcionada por un diccionario (`alphabet2mass`), 
    que por defecto es igual al diccionario global `aa2mass`.
    """
    seq = seq.strip().upper()     # Limpia la secuencia y la pasa a mayúsculas
    if not seq:                   # Evita procesar una secuencia vacía:
        return 0
    mass = 0                      # Masa inicial
    
    # Añade la masa de cada aminoácido de la secuencia peptídica:
    for aa in seq:
        mass += alphabet2mass[aa] # Equivale a mass = mass + alphabet2mass[aa]
    
    # Añadir la masa de 1 molécula de H₂O por los aminoácidos de los extremos 
    # (+1 H por el N-term y +1 OH por el C-term):
    mass += 18.010565             # Masa monoisotópica del H₂O
    #
    return round(mass, 6)


# %% tags=["ejercicio"]
# Probamos la función `molecular_weigth`:
molecular_weight("GARyCATWEEK")

# %% [markdown]
# ### Funciones que acepten un número de argumentos indeterminado
#
# Volviendo de nuevo a la función `add(x, y)` que hemos definido antes; si en lugar de 2 números quisiéramos sumar 3, 4, o más números:

# %% tags=["error"]
add(3, 2, 5) # Más argumentos pasados que parámetros definidos ➞ Error!


# %% [markdown]
# Cuando queremos que nuestra función acepte un _número **indefinido**_ de _argumentos **posicionales**_, hemos de indicarlo, al definir la función, con un _asterísco_ (`*`) delante del _nombre de la variable/parámetro_ que queremos que referencíe **todos** los _posibles argumentos posicionales_ que se le puedan llegar a pasar a la función, en forma de una *tupla*:
#
# ```python
#     def function_name(*args_tuple_name):
#         """documentation string ('docstring')"""
#         indented_block_of_code
#         to_do_something_with_arguments_in_args_tuple_name
#         return some_value_object
# ```
#
# Es como si _**empaquetáramos**_ los _atributos posicionales_ pasados a la función en una *tupla* referenciada por el _parámetro/variable_ a continuación del asterísco (`*`).

# %%
# Ejemplo reformulando la función add(x, y) ➞ add_multiple(*values) :

# Todos los parámetros que pasemos a la función estarán almacenados en `values` (una tupla):
def add_multiple(*values):
    """Sum/concatenate all supplied values (all values should be of the same type)"""
    
    print("Passed values (", len(values), "): ", values)
    
    # `result` comienza con el primer objeto de los argumentos contenidos en `values`:
    result = values[0]
    
    # Iteramos sobre `values`, a partir del segundo argumento que contiene, 
    # para obtener y sumar/concatenar todos los argumentos pasados a la función:
    for value in values[1:]:
        result = result + value
    return result


# %%
add_multiple(1, 0, 4, 2, 3)

# %%
add_multiple('Hola', ' ', 'mundo', '!')

# %% [markdown] toc-hr-collapsed=false
# # V.- Módulos y Paquetes
#
# En Python, un **módulo** (_module_) es un conjunto de _código_ (de nombres de variables, declaraciones de clases y funciones, ...) que proporciona una funcionalidad específica.  
# Generalmente, cada módulo Python es un fichero de texto con extensión **.py** que contiene código Python funcionalmente relacionado:
#
# [![module](images/python_intro/modules_1.png)](p4p.py "Ver código del módulo p4p (archivo 'p4p.py')")  
# (haz clic sobre la imagen superior para abrir el módulo [*p4p*](p4p.py "Ver código del módulo p4p (archivo 'p4p.py')") y poder echar un vistazo a su código)
#
# Por otro lado un **paquete** (_package_) es un _conjunto de módulos_ relacionados entre ellos.  
# Generalmente, un paquete está formado por una _carpeta_ o directorio y los _módulos_ (archivos de código con extensión _.py_) que contiene; aunque un paquete en Python también puede contener otros *sub-paquetes* (otras sub-carpetas con más módulos, ...):
#
# ![packages](images/python_intro/packages_1.png)

# %% [markdown] jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 Los módulos y los paquetes permiten dividir y _organizar_ grandes cantidades de código en bloques más pequeños, lo que facilita:
#  * La _simplicidad y manejabilidad_ del código: hacer el código modular es como descomponer un problema complejo en partes más sencillas en las que poder centrarnos. Es mejor tener el código dividido en bloques pequeños de funcionalidad bien definida, que no tenerlo todo mezclado en un gran y único fichero.
#  * La _mantenibilidad_ del código: a la hora de corregir errores y añadir nueva funcionalidad se puede modificar sólo el módulo o el paquete que sea necesario, sin tener que editar todo el código. Además, diferentes programadores pueden encargarse del mantenimiento de diferentes módulos/paquetes (división del trabajo).
#  * La _reutilización_ del código: podemos utilizar el código presente en los módulos y paquetes sin necesidad de tener que reescribirlos cada vez o ir copiando y pegando código de un programa a otro.  
#    Asimismo, podemos utilizar módulos y paquetes creados por _otros_ desarrolladores para realizar tareas muy complejas y/o especificas, o muy comunes y repetitivas (pero que requieren un buen mantenimiento y actualización continua), o cuya implementación queda fuera de nuestra área de conocimiento. De manera que podemos aprovechar/reutilizar el conocimiento de la _comunidad_ de desarrolladores Python, encapsulado en forma de módulos y paquetes que podemos [buscar](https://pypi.org/ "Buscar en el 'Python Package Index'") e [instalarnos](https://pip.pypa.io/ "Gestor de paquetes 'pip'") en nuestro ordenador, para ayudarnos en nuestros propios programas.

# %% [markdown] toc-hr-collapsed=false
# ## import
#
# La manera más sencilla para **cargar / importar** tanto _módulos_ como _paquetes_ dentro de nuestro espacio de trabajo es utilizando la instrucción `import` seguida del _nombre del módulo o paquete_ que queremos importar, incluyendo _opcionalmente_ la palabra clave `as` seguida de un _nombre alternativo_ o _"alias"_ para referenciar dicho módulo o paquete en nuestro código y poder acceder a su contenido:
#
# ```python
#     import module_or_package_name [as alternative_name]
# ```
#
# Si utilizamos esta notación, se crea en nuestro espacio de trabajo un objeto de tipo **module** con el _nombre del módulo o paquete_ (o su _nombre alternativo_, si hemos usado la cláusula `as`):

# %%
# Cargamos/importamos el módulo `p4p`, haciendo accesibles
# todos sus objetos bajo el mismo nombre de módulo `p4p`:

import p4p

# Comprobamos que el módulo importado efectivamente pertenece a la clase module
print( type(p4p) ) 
p4p


# %% jupyter={"source_hidden": true} tags=["sabermas"]
dir(p4p) # 📌 Examinamos qué objetos contiene el módulo `p4p`

# %% [markdown] toc-hr-collapsed=false
# Y, es a través de este objeto _module_ que podremos **acceder** a todos los objetos definidos en dicho módulo o paquete, utilizando el nombre del _objeto module_ seguido de un _punto_ (`.`) y el nombre del objeto dentro de él al que queremos acceder (como si se tratara de _atributos_ o _métodos_ del objeto _module_):
#
# ```python
#     module_or_package_name.object_name    [alternative_name.object_name]
# ```

# %% tags=["sabermas"] jupyter={"source_hidden": true}
p4p.__package__ # 📌 El módulo example_módule no pertenece a ningún paquete:

# %%
# Accedemos al objeto `aa2mass` del módulo `p4p`
p4p.aa2mass 

# %%
# Accedemos a la ayuda para la función `protein_coverage()` del módulo `p4p`:
# p4p.protein_coverage?

# %%
# Uso de la función `protein_coverage()` del módulo `p4p`:
protein_seq = 'MAAAAAKCCCCKDDDDDKEEEEEEKCCCCK'
peptides = {'AAAAAK', 'DDDDDK', 'CCCCK'}

# Accedemos a la función `protein_coverage()` del módulo `p4p`:
coverage = p4p.protein_coverage(protein_seq, peptides)

print("Coverage:", round(coverage, 2), "%")

# %%
# Ahora importamos el paquete `example_package`, haciendo accesibles todos sus objetos 
# bajo el "alias" `exp_pckg` (utilizando para ello la cláusula `as`):
import example_package as exp_pckg

print( type(exp_pckg) ) # El paquete también es de tipo module, como si se tratara de un módulo más...
exp_pckg

# %% tags=["sabermas"] jupyter={"source_hidden": true}
exp_pckg.__package__   # 📌 Como se trata de un paquete, nos indica que pertenece a un paquete: a él mismo

# %% jupyter={"source_hidden": true} tags=["sabermas"]
dir(exp_pckg)         # 📌 Ahora examinamos qué objetos contiene el paquete `exp_pckg` (`example_package`)

# %%
# ...Pero, como en este caso se trata de un paquete, contiene otros módulos:
print( type(exp_pckg.module1) )
print(exp_pckg.module1)

print( type(exp_pckg.module2) )
print(exp_pckg.module2)

print( type(exp_pckg.sub_package) )
print(exp_pckg.sub_package)         # ← Módulo parece, sub-paquete es...

# %% jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 Podemos comprobar como todos los módulos de example_package pertenecen a dicho paquete:
print(exp_pckg.__package__)
print(exp_pckg.module1.__package__)
print(exp_pckg.module2.__package__)
print(exp_pckg.sub_package.__package__) # ← Módulo parece, sub-paquete es...

# %%
# Tal como en el caso anterior, accedemos a objetos de los módulos `module1` y `module2`  
# del paquete `exp_pckg` (`example_package`):
print( exp_pckg.module1.x )
print( exp_pckg.module2.x )

# %% jupyter={"source_hidden": true} tags=["sabermas"]
# 📌 Como hemos visto, un paquete puede contener sub-paquetes, que a su vez contienen otros módulos:
dir(exp_pckg.sub_package)

# %% jupyter={"source_hidden": true} tags=["sabermas"]
print(type(exp_pckg.sub_package.module1))
print(exp_pckg.sub_package.module1)
exp_pckg.sub_package.module1.__package__ # 📌 Éste module1 pertenece al paquete sub_package del paquete example_package

# %%
# Llamamos a la función `f()` del módulo `module1` del sub-paquete `sub_package` 
# del paquete `exp_pckg` (`example_package`):
exp_pckg.sub_package.module1.f(2)

# %% [markdown] tags=["ejercicio"]
# <br/>
#
# > 📝 **Práctica:**
# >
# > **Cargar** el módulo `this` y comprueba _qué pasa_: 

# %% tags=["ejercicio"]



# %% tags=["ejercicio", "solucion"] jupyter={"source_hidden": true}
# 📎 Solución:

import this

# %% [markdown] tags=["ejercicio"]
# > **Cargar** el módulo `random` (de la [librería estándar de Python](https://docs.python.org/3/library/index.html "The Python Standard Library Documentation")). Este módulo contiene funciones y clases que permiten generar números _aleatorios_ de diferentes maneras, seleccionar elementos aleatorios de secuencias, ...:

# %% tags=["ejercicio"]



# %% tags=["ejercicio", "solucion"] jupyter={"source_hidden": true}
# 📎 Solución:

import random

# %% [markdown] tags=["ejercicio"]
# > **Llamar** a la _función_ `randint(a, b)` del _módulo_ `random` para generar un _número aleatorio entero_ entre los valores `0` (argumento `a`) y `100` (argumento `b`), ambos incluidos.
#
# 💡 Además de utilizar `help()` y `?` para obtener ayuda sobre un objeto (una función o método, una clase, una instancia, ...), en el caso de los _callables_ (objetos que pueden ser _"llamados"_, como una función o método, una clase, ...) se puede utilizar también la combinación de teclas `Mayúsculas ⇧` + `Tab ↹` tras escribir el nombre del _callable_ para obtener rápidamente dicha ayuda:

# %% tags=["ejercicio"]



# %% jupyter={"source_hidden": true} tags=["ejercicio", "solucion"]
# 📎 Solución:

random.randint(0, 100)

# %% [markdown]
# <br/>
#
# Para importar _**sólo** un/os objeto/s_ de un _módulo_ (o _un/os módulo/s_ dentro de un _paquete_) en nuestro espacio de trabajo, en lugar de utilizar la instrucción `import` cómo hasta ahora, se utiliza alguna de las siguientes notaciones alternativas:
#
# ```python
#     from module import object_name1 [as alternative_name1], object_name2 [as alternative_name2], ...
# ```
# ```python
#     from package import module1 [as alternative_module1_name], module2 [as alternative_module2_name], ...
# ```
# ```python
#     from package.module import object_name1 [as alternative_name1], object_name2 [as alternative_name2], ...
# ```

# %%
del p4p # Borramos el nombre del módulo `p4p` de nuestro espacio de trabajo.

# %%
# Cargamos el módulo `p4p`, pero sólo hacemos accesibles la función `protein_coverage()` 
# y el diccionario `aa2mass`, directamente en nuestro espacio de trabajo (sin que sea necesario
# anteponerles el nombre del paquete del que provienen para accederlos):

from p4p import protein_coverage, aa2mass

protein_coverage     # Ahora no hay que poner `p4p.protein_coverage` para acceder a protein_coverage

# %%
print("Protein coverage:", round(protein_coverage(protein_seq, peptides), 2), "%")

# %% tags=["error"]
# Sólo hemos hecho accesibles la función `protein_coverage()` y la variable `aa2mass`, 
# de manera que no podemos acceder al módulo directamente (salvo que lo importemos a parte):

p4p    # ➞ Error!

# %%
# Importamos el módulo `module1` del paquete `example_package` directamente en nuestro espacio de nombres:

from example_package import module1

module1    # Ahora no hay que poner `example_package.module1`

# %%
# Importamos el módulo `module1` del sub-paquete `sub_package` del paquete `example_package` 
# directamente en nuestro espacio de nombres, pero usando un "alias" (cláusula `as`) para 
# evitar la colisión de nombres con el anterior `module1` del paquete principal `example_package`:

from example_package.sub_package import module1 as sbpkg_module1       # Uso de la cláusula `as` para hacerlo accesible bajo un "alias"
sbpkg_module1        # Ahora no hay que poner `example_package.sub_package.module1`

# %% [markdown] tags=["ejercicio"]
# <br/>
#
# > 📝 **Práctica:**
# >
# > **Cargar** el módulo `module1` del sub-paquete `sub_package` del paquete `example_package`, pero **sólo** haciendo accesible en nuestro espacio de trabajo la _función_ `f()` de dicho módulo, bajo el _"alias"_ `func`:

# %% tags=["ejercicio"]



# %% tags=["ejercicio", "solucion"] jupyter={"source_hidden": true}
# 📎 Solución:

from example_package.sub_package.module1 import f as func

# %% [markdown] tags=["ejercicio"]
# > **Llamar** a la función recién importada para calcular _2²_  (👉 _recuerda_: la combinación de teclas `Mayúsculas ⇧` + `Tab ↹` permite consultar rápidamente la documentación de la función y averiguar cómo utilizarla):

# %% tags=["ejercicio"]



# %% tags=["ejercicio", "solucion"] jupyter={"source_hidden": true}
# 📎 Solución:

func(2)

# %% [markdown] tags=["ejercicio"]
# # VI.- Ejercicio final del tema (opcional)
#
# > 📝 **Ejercicio final:**
# >
# > Crear una _función_ para un juego, llamada `adivina_el_numero`, que genere un _número aleatorio entero_ (función `randint()` del módulo `random`) entre 2 valores (_parámetros opcionales_ `num_min` y `num_max`) y vaya _preguntando al usuario_ por el número, dándole _pistas_ sobre si el número introducido por el usuario es _mayor o menor_, hasta que éste lo _averigue_ o supere el número _máximo de intentos_ (_parámetro opcional_ `max_intentos`).
# >
# > Además, el usuario debe poder _rendirse_ o salir del juego escribiendo una de las siguientes palabras: fin, terminar, salir, acabar.
# >
# > Por último, La función debe devolver `True` si el usuario _acierta_ el número o `False` en caso contrario, así como el _número de intentos_ que ha empleado (sin contar los errores en la entrada de datos, como dejar la respuesta en blanco o no introducir un número entero). 

# %% tags=["ejercicio"]
import 

def adivina_el_numero(    ):
    
    


# %% tags=["ejercicio", "solucion"] jupyter={"source_hidden": true}
# 📎 Solución:

import random # Importamos el módulo `random` de la librería estándar para poder usar la función `randint()`.

def adivina_el_numero(num_min=0, num_max=100, max_intentos=None):
    """Genera un número aleatorio entero entre `num_min` y `num_max`, y va preguntando al usuario 
    por el número, dándole pistas sobre si el número introducido por el usuario es mayor o menor, 
    hasta que éste lo averigüe o supere el número máximo de intentos (`max_intentos`).
    Devuelve `True` si el usuario acierta el número o `False` en caso contrario, así como el 
    número de intentos que ha empleado."""
    numero = random.randint(num_min, num_max) # Número aleatorio entre `num_min` y `num_max`.
    pregunta = "En que número del " + str(num_min) + " al " + str(num_max) + " estoy pensando?"
    num_usuario = None # Último número que introdujo el usuario.
    intentos = 0 # Número de intentos que lleva el usuario.
    
    while num_usuario != numero and (not max_intentos or intentos < max_intentos):
        # Informar del número de intentos pendientes o consumidos (según si hay límite o no):
        if max_intentos and intentos:
            print("\nTe quedan", max_intentos - intentos, "intentos para adivinarlo...\n")
        elif num_usuario:
            print("\nLlevas", intentos, "intentos...\n")
        # Preguntar el número al usuario:
        respuesta = input(pregunta)
        respuesta = respuesta.strip().upper()
        # Comprobar si el usuario se rinde:
        if respuesta in ('FIN', 'ACABAR', 'TERMINAR', 'SALIR', 'ME RINDO', 'SOCORRO'):
            confirmacion = None
            while confirmacion is None:
                confirmacion = input("\tSeguro de que quieres rendirte y acabar (S/N)?")
                confirmacion = confirmacion.strip().upper()
                if confirmacion in ('S', 'Sí'):
                    confirmacion = True
                elif confirmacion in ('N', 'NO'):
                    confirmacion = False
                else:
                    confirmacion = None
            if confirmacion:
                break
            else:
                continue
        # Intentar convertir en entero la respuesta del usuario:
        try:
            num_usuario = int(respuesta)
        except ValueError as error:
            print('\nLo siento, no he entendido tu respuesta. Vuelve a intentarlo...\n')
            continue
        # Si la respuesta es válida (o sea, es un entero):
        intentos += 1 # Cuenta como un intento de averiguar el número
        # Dar pistas:
        if numero > num_usuario:
            print("\nLo siento, el número en que estoy pensando es MAYOR que", num_usuario)
        elif numero < num_usuario:
            print("\nLo siento, el número en que estoy pensando es MENOR que", num_usuario)
    
    return (num_usuario == numero), intentos


# %% tags=["ejercicio"]
# Probemos la función del juego con un número entre 0 y 100, sin máximo número de intentos:
adivina_el_numero(num_min=0, num_max=100, max_intentos=None)

# %% [markdown] tags=["sabermas"]
# ## VII.- Referencias y material de ampliación 🔗
#
# * [Documentación oficial de Python 3](https://docs.python.org/3/index.html).
# * [Python 3 Tutorial](https://www.python-course.eu/python3_course.php), por [Bernd Klein](https://www.bklein.de/).
# * [Real Python Tutorials](https://realpython.com/).
# * [Memento - Python 3 CheatSheet](Supplementary%20Course%20Materials/Python%203%20Cheat%20Sheet%20-%20Memento.pdf), por [Laurent Pointal](https://www.researchgate.net/profile/Laurent_Pointal).
# * [Python en StackOverflow](https://stackoverflow.com/questions/tagged/python?sort=frequent), la comunidad de preguntas y respuestas sobre programación.
#
#
# * Tutorial interactivo de Python en [LearnPython.org](https://www.learnpython.org/), por [Ron Reiter](https://github.com/ronreiter).
# * [Full-Stack Python](https://www.fullstackpython.com/), por [Matt Makai](https://github.com/mattmakai).
# * [Tutoriales Python para todos](https://unipython.com/), de Unipython (en castellano).
# * [Foundations of Python Programming](https://runestone.academy/runestone/static/fopp/index.html), en [Runestone Academy](https://runestone.academy/).
# * [Object Oriented Programming in Python](https://stackabuse.com/object-oriented-programming-in-python/), por [Usman Malik](https://twitter.com/usman_malikk).
# * [¿Por qué mis números no se suman bien?](http://puntoflotante.org/ "Lo que todo programador debería saber sobre aritmética de punto flotante"), traducción del original en inglés por [Michael Borgwardt](https://brazzy.de/) en http://floating-point-gui.de/.
# * [Rosalind](http://rosalind.info/), aprende bioinformática y programación mediante la resolución de problemas.
